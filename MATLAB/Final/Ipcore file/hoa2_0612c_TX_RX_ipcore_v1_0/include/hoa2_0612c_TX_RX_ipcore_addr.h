/*
 * File Name:         hdl_prj_JAE2\ipcore\hoa2_0612c_TX_RX_ipcore_v1_0\include\hoa2_0612c_TX_RX_ipcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-08 06:31:53
*/

#ifndef HOA2_0612C_TX_RX_IPCORE_H_
#define HOA2_0612C_TX_RX_IPCORE_H_

#define  IPCore_Reset_hoa2_0612c_TX_RX_ipcore      0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_hoa2_0612c_TX_RX_ipcore     0x4  //enabled (by default) when bit 0 is 0x1
#define  RXDataOutI_Data_hoa2_0612c_TX_RX_ipcore   0x100  //data register for port RXDataOutI
#define  RXDataOutQ_Data_hoa2_0612c_TX_RX_ipcore   0x104  //data register for port RXDataOutQ

#endif /* HOA2_0612C_TX_RX_IPCORE_H_ */
