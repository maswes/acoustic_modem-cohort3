// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\TX_RX.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// 
// -- -------------------------------------------------------------
// -- Rate and Clocking Details
// -- -------------------------------------------------------------
// Model base rate: 2.5e-08
// Target subsystem base rate: 2.5e-08
// 
// 
// Clock Enable  Sample Time
// -- -------------------------------------------------------------
// ce_out        2.5e-08
// -- -------------------------------------------------------------
// 
// 
// Output Signal                 Clock Enable  Sample Time
// -- -------------------------------------------------------------
// RXDataOutI                    ce_out        2.5e-08
// RXDataOutQ                    ce_out        2.5e-08
// TXOut                         ce_out        2.5e-08
// -- -------------------------------------------------------------
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: TX_RX
// Source Path: hoa2_0612c/TX_RX
// Hierarchy Level: 0
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module TX_RX
          (
           clk,
           reset,
           clk_enable,
           RXIn,
           ce_out,
           RXDataOutI,
           RXDataOutQ,
           TXOut
          );


  input   clk;
  input   reset;
  input   clk_enable;
  input   signed [11:0] RXIn;  // sfix12_En8
  output  ce_out;
  output  signed [11:0] RXDataOutI;  // sfix12
  output  signed [11:0] RXDataOutQ;  // sfix12
  output  signed [11:0] TXOut;  // sfix12_En8


  wire enb;
  wire enb_1_1_1;
  wire enb_1_16_1;
  wire enb_1_4_0;
  wire signed [11:0] Tx_Rx_out1;  // sfix12
  wire signed [11:0] Tx_Rx_out2;  // sfix12
  wire signed [11:0] Tx_Rx_out3;  // sfix12_En8


  TX_RX_tc   u_TX_RX_tc   (.clk(clk),
                           .reset(reset),
                           .clk_enable(clk_enable),
                           .enb(enb),
                           .enb_1_1_1(enb_1_1_1),
                           .enb_1_4_0(enb_1_4_0),
                           .enb_1_16_1(enb_1_16_1)
                           );

  Tx_Rx_block   u_Tx_Rx   (.clk(clk),
                           .reset(reset),
                           .enb(enb),
                           .enb_1_1_1(enb_1_1_1),
                           .enb_1_16_1(enb_1_16_1),
                           .enb_1_4_0(enb_1_4_0),
                           .RXIn(RXIn),  // sfix12_En8
                           .RxOutI(Tx_Rx_out1),  // sfix12
                           .RxOutQ(Tx_Rx_out2),  // sfix12
                           .TXOut(Tx_Rx_out3)  // sfix12_En8
                           );

  assign RXDataOutI = Tx_Rx_out1;

  assign RXDataOutQ = Tx_Rx_out2;

  assign TXOut = Tx_Rx_out3;

  assign ce_out = enb_1_1_1;

endmodule  // TX_RX

