// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\FineHold.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: FineHold
// Source Path: hoa2_0612c/TX_RX/Tx_Rx/TxRx/Receiver/RX/DlyRotCtl/FineHold
// Hierarchy Level: 6
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module FineHold
          (
           clk,
           reset,
           enb,
           GolDet,
           FinIn,
           FinOut
          );


  input   clk;
  input   reset;
  input   enb;
  input   GolDet;
  input   [31:0] FinIn;  // uint32
  output  [31:0] FinOut;  // uint32


  wire [7:0] C2_out1;  // uint8
  wire [7:0] C3_out1;  // uint8
  reg [7:0] Delay3_out1;  // uint8
  wire [7:0] CoarseFine2_out1;  // uint8
  wire [7:0] Switch1_out1;  // uint8
  wire switch_compare_1;
  wire switch_compare_1_1;
  reg [31:0] Delay_out1;  // uint32
  wire [31:0] Switch_out1;  // uint32
  wire [31:0] Switch2_out1;  // uint32


  assign C2_out1 = 8'b00000000;



  assign C3_out1 = 8'b00000001;



  assign CoarseFine2_out1 = Delay3_out1 + C3_out1;



  assign Switch1_out1 = (GolDet == 1'b0 ? C2_out1 :
              CoarseFine2_out1);



  always @(posedge clk or posedge reset)
    begin : Delay3_process
      if (reset == 1'b1) begin
        Delay3_out1 <= 8'b00000000;
      end
      else begin
        if (enb) begin
          Delay3_out1 <= Switch1_out1;
        end
      end
    end


  assign switch_compare_1 = Delay3_out1 > 8'b00000010;



  assign switch_compare_1_1 = Delay3_out1 > 8'b00000001;



  assign Switch_out1 = (switch_compare_1_1 == 1'b0 ? Delay_out1 :
              FinIn);



  assign Switch2_out1 = (switch_compare_1 == 1'b0 ? Switch_out1 :
              Delay_out1);



  always @(posedge clk or posedge reset)
    begin : Delay_process
      if (reset == 1'b1) begin
        Delay_out1 <= 32'b00000000000000000000000000000000;
      end
      else begin
        if (enb) begin
          Delay_out1 <= Switch2_out1;
        end
      end
    end


  assign FinOut = Delay_out1;


endmodule  // FineHold

