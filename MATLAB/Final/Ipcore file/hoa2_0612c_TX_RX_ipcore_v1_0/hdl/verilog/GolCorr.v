// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\GolCorr.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: GolCorr
// Source Path: hoa2_0612c/TX_RX/Tx_Rx/TxRx/Receiver/RX/Subsystem/GolCorQuadFront/GolCorFx0/GolCorr
// Hierarchy Level: 8
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module GolCorr
          (
           clk,
           reset,
           enb,
           Real_rsvd,
           Im,
           CorrRealOut,
           CorrImOut
          );


  input   clk;
  input   reset;
  input   enb;
  input   signed [15:0] Real_rsvd;  // sfix16_En12
  input   signed [15:0] Im;  // sfix16_En12
  output  signed [15:0] CorrRealOut;  // sfix16_En4
  output  signed [15:0] CorrImOut;  // sfix16_En4


  wire signed [15:0] ReFilt_out1;  // sfix16_En4
  reg signed [15:0] Delay1_out1;  // sfix16_En4
  wire signed [15:0] TmpGroundAtAddInport2_out1;  // sfix16_En4
  wire signed [15:0] Add_out1;  // sfix16_En4
  wire signed [15:0] TmpGroundAtAdd1Inport1_out1;  // sfix16_En4
  wire signed [15:0] ReFilt1_out1;  // sfix16_En4
  reg signed [15:0] Delay4_out1;  // sfix16_En4
  wire signed [15:0] Add1_out1;  // sfix16_En4

  // Coefficients from Golay32aI and Golay32aQ variables in 
  // the Cparametersv3.m file


  ReFilt   u_ReFilt   (.clk(clk),
                       .reset(reset),
                       .enb(enb),
                       .ReFilt_in(Real_rsvd),  // sfix16_En12
                       .ReFilt_out(ReFilt_out1)  // sfix16_En4
                       );

  always @(posedge clk or posedge reset)
    begin : Delay1_process
      if (reset == 1'b1) begin
        Delay1_out1 <= 16'sb0000000000000000;
      end
      else begin
        if (enb) begin
          Delay1_out1 <= ReFilt_out1;
        end
      end
    end


  assign TmpGroundAtAddInport2_out1 = 16'sb0000000000000000;



  assign Add_out1 = Delay1_out1 + TmpGroundAtAddInport2_out1;



  assign CorrRealOut = Add_out1;

  assign TmpGroundAtAdd1Inport1_out1 = 16'sb0000000000000000;



  ReFilt1   u_ReFilt1   (.clk(clk),
                         .reset(reset),
                         .enb(enb),
                         .ReFilt1_in(Im),  // sfix16_En12
                         .ReFilt1_out(ReFilt1_out1)  // sfix16_En4
                         );

  always @(posedge clk or posedge reset)
    begin : Delay4_process
      if (reset == 1'b1) begin
        Delay4_out1 <= 16'sb0000000000000000;
      end
      else begin
        if (enb) begin
          Delay4_out1 <= ReFilt1_out1;
        end
      end
    end


  assign Add1_out1 = TmpGroundAtAdd1Inport1_out1 - Delay4_out1;



  assign CorrImOut = Add1_out1;

endmodule  // GolCorr

