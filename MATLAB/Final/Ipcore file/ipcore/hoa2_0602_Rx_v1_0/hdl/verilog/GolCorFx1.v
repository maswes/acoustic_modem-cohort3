// -------------------------------------------------------------
// 
// File Name: C:\WES207\hoa\Final\hdlsrc\hoa2_0602\GolCorFx1.v
// Created: 2015-06-02 20:54:40
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: GolCorFx1
// Source Path: Subsystem1/RX/Subsystem/GolCorQuadFront/GolCorFx1
// Hierarchy Level: 4
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module GolCorFx1
          (
           clk,
           reset,
           enb_1_4_0,
           In1,
           In2,
           CorOut_re_0,
           CorOut_re_1,
           CorOut_re_2,
           CorOut_re_3,
           CorOut_re_4,
           CorOut_re_5,
           CorOut_re_6,
           CorOut_re_7,
           CorOut_re_8,
           CorOut_re_9,
           CorOut_re_10,
           CorOut_re_11,
           CorOut_re_12,
           CorOut_re_13,
           CorOut_re_14,
           CorOut_re_15,
           CorOut_re_16,
           CorOut_re_17,
           CorOut_re_18,
           CorOut_re_19,
           CorOut_re_20,
           CorOut_re_21,
           CorOut_re_22,
           CorOut_re_23,
           CorOut_re_24,
           CorOut_re_25,
           CorOut_re_26,
           CorOut_re_27,
           CorOut_re_28,
           CorOut_re_29,
           CorOut_re_30,
           CorOut_re_31,
           CorOut_im_0,
           CorOut_im_1,
           CorOut_im_2,
           CorOut_im_3,
           CorOut_im_4,
           CorOut_im_5,
           CorOut_im_6,
           CorOut_im_7,
           CorOut_im_8,
           CorOut_im_9,
           CorOut_im_10,
           CorOut_im_11,
           CorOut_im_12,
           CorOut_im_13,
           CorOut_im_14,
           CorOut_im_15,
           CorOut_im_16,
           CorOut_im_17,
           CorOut_im_18,
           CorOut_im_19,
           CorOut_im_20,
           CorOut_im_21,
           CorOut_im_22,
           CorOut_im_23,
           CorOut_im_24,
           CorOut_im_25,
           CorOut_im_26,
           CorOut_im_27,
           CorOut_im_28,
           CorOut_im_29,
           CorOut_im_30,
           CorOut_im_31,
           AbsCorOut_0,
           AbsCorOut_1,
           AbsCorOut_2,
           AbsCorOut_3,
           AbsCorOut_4,
           AbsCorOut_5,
           AbsCorOut_6,
           AbsCorOut_7,
           AbsCorOut_8,
           AbsCorOut_9,
           AbsCorOut_10,
           AbsCorOut_11,
           AbsCorOut_12,
           AbsCorOut_13,
           AbsCorOut_14,
           AbsCorOut_15,
           AbsCorOut_16,
           AbsCorOut_17,
           AbsCorOut_18,
           AbsCorOut_19,
           AbsCorOut_20,
           AbsCorOut_21,
           AbsCorOut_22,
           AbsCorOut_23,
           AbsCorOut_24,
           AbsCorOut_25,
           AbsCorOut_26,
           AbsCorOut_27,
           AbsCorOut_28,
           AbsCorOut_29,
           AbsCorOut_30,
           AbsCorOut_31
          );


  input   clk;
  input   reset;
  input   enb_1_4_0;
  input   signed [15:0] In1;  // sfix16_En12
  input   signed [15:0] In2;  // sfix16_En12
  output  signed [15:0] CorOut_re_0;  // sfix16_En4
  output  signed [15:0] CorOut_re_1;  // sfix16_En4
  output  signed [15:0] CorOut_re_2;  // sfix16_En4
  output  signed [15:0] CorOut_re_3;  // sfix16_En4
  output  signed [15:0] CorOut_re_4;  // sfix16_En4
  output  signed [15:0] CorOut_re_5;  // sfix16_En4
  output  signed [15:0] CorOut_re_6;  // sfix16_En4
  output  signed [15:0] CorOut_re_7;  // sfix16_En4
  output  signed [15:0] CorOut_re_8;  // sfix16_En4
  output  signed [15:0] CorOut_re_9;  // sfix16_En4
  output  signed [15:0] CorOut_re_10;  // sfix16_En4
  output  signed [15:0] CorOut_re_11;  // sfix16_En4
  output  signed [15:0] CorOut_re_12;  // sfix16_En4
  output  signed [15:0] CorOut_re_13;  // sfix16_En4
  output  signed [15:0] CorOut_re_14;  // sfix16_En4
  output  signed [15:0] CorOut_re_15;  // sfix16_En4
  output  signed [15:0] CorOut_re_16;  // sfix16_En4
  output  signed [15:0] CorOut_re_17;  // sfix16_En4
  output  signed [15:0] CorOut_re_18;  // sfix16_En4
  output  signed [15:0] CorOut_re_19;  // sfix16_En4
  output  signed [15:0] CorOut_re_20;  // sfix16_En4
  output  signed [15:0] CorOut_re_21;  // sfix16_En4
  output  signed [15:0] CorOut_re_22;  // sfix16_En4
  output  signed [15:0] CorOut_re_23;  // sfix16_En4
  output  signed [15:0] CorOut_re_24;  // sfix16_En4
  output  signed [15:0] CorOut_re_25;  // sfix16_En4
  output  signed [15:0] CorOut_re_26;  // sfix16_En4
  output  signed [15:0] CorOut_re_27;  // sfix16_En4
  output  signed [15:0] CorOut_re_28;  // sfix16_En4
  output  signed [15:0] CorOut_re_29;  // sfix16_En4
  output  signed [15:0] CorOut_re_30;  // sfix16_En4
  output  signed [15:0] CorOut_re_31;  // sfix16_En4
  output  signed [15:0] CorOut_im_0;  // sfix16_En4
  output  signed [15:0] CorOut_im_1;  // sfix16_En4
  output  signed [15:0] CorOut_im_2;  // sfix16_En4
  output  signed [15:0] CorOut_im_3;  // sfix16_En4
  output  signed [15:0] CorOut_im_4;  // sfix16_En4
  output  signed [15:0] CorOut_im_5;  // sfix16_En4
  output  signed [15:0] CorOut_im_6;  // sfix16_En4
  output  signed [15:0] CorOut_im_7;  // sfix16_En4
  output  signed [15:0] CorOut_im_8;  // sfix16_En4
  output  signed [15:0] CorOut_im_9;  // sfix16_En4
  output  signed [15:0] CorOut_im_10;  // sfix16_En4
  output  signed [15:0] CorOut_im_11;  // sfix16_En4
  output  signed [15:0] CorOut_im_12;  // sfix16_En4
  output  signed [15:0] CorOut_im_13;  // sfix16_En4
  output  signed [15:0] CorOut_im_14;  // sfix16_En4
  output  signed [15:0] CorOut_im_15;  // sfix16_En4
  output  signed [15:0] CorOut_im_16;  // sfix16_En4
  output  signed [15:0] CorOut_im_17;  // sfix16_En4
  output  signed [15:0] CorOut_im_18;  // sfix16_En4
  output  signed [15:0] CorOut_im_19;  // sfix16_En4
  output  signed [15:0] CorOut_im_20;  // sfix16_En4
  output  signed [15:0] CorOut_im_21;  // sfix16_En4
  output  signed [15:0] CorOut_im_22;  // sfix16_En4
  output  signed [15:0] CorOut_im_23;  // sfix16_En4
  output  signed [15:0] CorOut_im_24;  // sfix16_En4
  output  signed [15:0] CorOut_im_25;  // sfix16_En4
  output  signed [15:0] CorOut_im_26;  // sfix16_En4
  output  signed [15:0] CorOut_im_27;  // sfix16_En4
  output  signed [15:0] CorOut_im_28;  // sfix16_En4
  output  signed [15:0] CorOut_im_29;  // sfix16_En4
  output  signed [15:0] CorOut_im_30;  // sfix16_En4
  output  signed [15:0] CorOut_im_31;  // sfix16_En4
  output  [15:0] AbsCorOut_0;  // ufix16_En4
  output  [15:0] AbsCorOut_1;  // ufix16_En4
  output  [15:0] AbsCorOut_2;  // ufix16_En4
  output  [15:0] AbsCorOut_3;  // ufix16_En4
  output  [15:0] AbsCorOut_4;  // ufix16_En4
  output  [15:0] AbsCorOut_5;  // ufix16_En4
  output  [15:0] AbsCorOut_6;  // ufix16_En4
  output  [15:0] AbsCorOut_7;  // ufix16_En4
  output  [15:0] AbsCorOut_8;  // ufix16_En4
  output  [15:0] AbsCorOut_9;  // ufix16_En4
  output  [15:0] AbsCorOut_10;  // ufix16_En4
  output  [15:0] AbsCorOut_11;  // ufix16_En4
  output  [15:0] AbsCorOut_12;  // ufix16_En4
  output  [15:0] AbsCorOut_13;  // ufix16_En4
  output  [15:0] AbsCorOut_14;  // ufix16_En4
  output  [15:0] AbsCorOut_15;  // ufix16_En4
  output  [15:0] AbsCorOut_16;  // ufix16_En4
  output  [15:0] AbsCorOut_17;  // ufix16_En4
  output  [15:0] AbsCorOut_18;  // ufix16_En4
  output  [15:0] AbsCorOut_19;  // ufix16_En4
  output  [15:0] AbsCorOut_20;  // ufix16_En4
  output  [15:0] AbsCorOut_21;  // ufix16_En4
  output  [15:0] AbsCorOut_22;  // ufix16_En4
  output  [15:0] AbsCorOut_23;  // ufix16_En4
  output  [15:0] AbsCorOut_24;  // ufix16_En4
  output  [15:0] AbsCorOut_25;  // ufix16_En4
  output  [15:0] AbsCorOut_26;  // ufix16_En4
  output  [15:0] AbsCorOut_27;  // ufix16_En4
  output  [15:0] AbsCorOut_28;  // ufix16_En4
  output  [15:0] AbsCorOut_29;  // ufix16_En4
  output  [15:0] AbsCorOut_30;  // ufix16_En4
  output  [15:0] AbsCorOut_31;  // ufix16_En4


  wire signed [15:0] GolCorr_out1;  // sfix16_En4
  wire signed [15:0] GolCorr_out2;  // sfix16_En4
  reg signed [15:0] D2_out1_re;  // sfix16_En4
  reg signed [15:0] D2_out1_im;  // sfix16_En4
  reg signed [15:0] TapDly_reg_re [0:31];  // sfix16_En4 [32]
  reg signed [15:0] TapDly_reg_im [0:31];  // sfix16_En4 [32]
  wire signed [15:0] TapDly_reg_next_re [0:31];  // sfix16_En4 [32]
  wire signed [15:0] TapDly_reg_next_im [0:31];  // sfix16_En4 [32]
  wire signed [15:0] TapDly_out1_re [0:31];  // sfix16_En4 [32]
  wire signed [15:0] TapDly_out1_im [0:31];  // sfix16_En4 [32]
  wire [15:0] Abs_out1;  // ufix16_En4
  reg [15:0] TapDly1_reg [0:31];  // ufix16 [32]
  wire [15:0] TapDly1_reg_next [0:31];  // ufix16_En4 [32]
  wire [15:0] TapDly1_out1 [0:31];  // ufix16_En4 [32]

  // THIS BLOCK IS PROCESSED AT THE CHIP RATE
  // 
  // 


  GolCorr_block   u_GolCorr   (.clk(clk),
                               .reset(reset),
                               .enb_1_4_0(enb_1_4_0),
                               .Real_rsvd(In1),  // sfix16_En12
                               .Im(In2),  // sfix16_En12
                               .CorrRealOut(GolCorr_out1),  // sfix16_En4
                               .CorrImOut(GolCorr_out2)  // sfix16_En4
                               );

  always @(posedge clk or posedge reset)
    begin : D2_process
      if (reset == 1'b1) begin
        D2_out1_re <= 16'sb0000000000000000;
        D2_out1_im <= 16'sb0000000000000000;
      end
      else begin
        if (enb_1_4_0) begin
          D2_out1_re <= GolCorr_out1;
          D2_out1_im <= GolCorr_out2;
        end
      end
    end


  always @(posedge clk or posedge reset)
    begin : TapDly_process
      if (reset == 1'b1) begin
        TapDly_reg_re[0] <= 16'sb0000000000000000;
        TapDly_reg_im[0] <= 16'sb0000000000000000;
        TapDly_reg_re[1] <= 16'sb0000000000000000;
        TapDly_reg_im[1] <= 16'sb0000000000000000;
        TapDly_reg_re[2] <= 16'sb0000000000000000;
        TapDly_reg_im[2] <= 16'sb0000000000000000;
        TapDly_reg_re[3] <= 16'sb0000000000000000;
        TapDly_reg_im[3] <= 16'sb0000000000000000;
        TapDly_reg_re[4] <= 16'sb0000000000000000;
        TapDly_reg_im[4] <= 16'sb0000000000000000;
        TapDly_reg_re[5] <= 16'sb0000000000000000;
        TapDly_reg_im[5] <= 16'sb0000000000000000;
        TapDly_reg_re[6] <= 16'sb0000000000000000;
        TapDly_reg_im[6] <= 16'sb0000000000000000;
        TapDly_reg_re[7] <= 16'sb0000000000000000;
        TapDly_reg_im[7] <= 16'sb0000000000000000;
        TapDly_reg_re[8] <= 16'sb0000000000000000;
        TapDly_reg_im[8] <= 16'sb0000000000000000;
        TapDly_reg_re[9] <= 16'sb0000000000000000;
        TapDly_reg_im[9] <= 16'sb0000000000000000;
        TapDly_reg_re[10] <= 16'sb0000000000000000;
        TapDly_reg_im[10] <= 16'sb0000000000000000;
        TapDly_reg_re[11] <= 16'sb0000000000000000;
        TapDly_reg_im[11] <= 16'sb0000000000000000;
        TapDly_reg_re[12] <= 16'sb0000000000000000;
        TapDly_reg_im[12] <= 16'sb0000000000000000;
        TapDly_reg_re[13] <= 16'sb0000000000000000;
        TapDly_reg_im[13] <= 16'sb0000000000000000;
        TapDly_reg_re[14] <= 16'sb0000000000000000;
        TapDly_reg_im[14] <= 16'sb0000000000000000;
        TapDly_reg_re[15] <= 16'sb0000000000000000;
        TapDly_reg_im[15] <= 16'sb0000000000000000;
        TapDly_reg_re[16] <= 16'sb0000000000000000;
        TapDly_reg_im[16] <= 16'sb0000000000000000;
        TapDly_reg_re[17] <= 16'sb0000000000000000;
        TapDly_reg_im[17] <= 16'sb0000000000000000;
        TapDly_reg_re[18] <= 16'sb0000000000000000;
        TapDly_reg_im[18] <= 16'sb0000000000000000;
        TapDly_reg_re[19] <= 16'sb0000000000000000;
        TapDly_reg_im[19] <= 16'sb0000000000000000;
        TapDly_reg_re[20] <= 16'sb0000000000000000;
        TapDly_reg_im[20] <= 16'sb0000000000000000;
        TapDly_reg_re[21] <= 16'sb0000000000000000;
        TapDly_reg_im[21] <= 16'sb0000000000000000;
        TapDly_reg_re[22] <= 16'sb0000000000000000;
        TapDly_reg_im[22] <= 16'sb0000000000000000;
        TapDly_reg_re[23] <= 16'sb0000000000000000;
        TapDly_reg_im[23] <= 16'sb0000000000000000;
        TapDly_reg_re[24] <= 16'sb0000000000000000;
        TapDly_reg_im[24] <= 16'sb0000000000000000;
        TapDly_reg_re[25] <= 16'sb0000000000000000;
        TapDly_reg_im[25] <= 16'sb0000000000000000;
        TapDly_reg_re[26] <= 16'sb0000000000000000;
        TapDly_reg_im[26] <= 16'sb0000000000000000;
        TapDly_reg_re[27] <= 16'sb0000000000000000;
        TapDly_reg_im[27] <= 16'sb0000000000000000;
        TapDly_reg_re[28] <= 16'sb0000000000000000;
        TapDly_reg_im[28] <= 16'sb0000000000000000;
        TapDly_reg_re[29] <= 16'sb0000000000000000;
        TapDly_reg_im[29] <= 16'sb0000000000000000;
        TapDly_reg_re[30] <= 16'sb0000000000000000;
        TapDly_reg_im[30] <= 16'sb0000000000000000;
        TapDly_reg_re[31] <= 16'sb0000000000000000;
        TapDly_reg_im[31] <= 16'sb0000000000000000;
      end
      else begin
        if (enb_1_4_0) begin
          TapDly_reg_re[0] <= TapDly_reg_next_re[0];
          TapDly_reg_im[0] <= TapDly_reg_next_im[0];
          TapDly_reg_re[1] <= TapDly_reg_next_re[1];
          TapDly_reg_im[1] <= TapDly_reg_next_im[1];
          TapDly_reg_re[2] <= TapDly_reg_next_re[2];
          TapDly_reg_im[2] <= TapDly_reg_next_im[2];
          TapDly_reg_re[3] <= TapDly_reg_next_re[3];
          TapDly_reg_im[3] <= TapDly_reg_next_im[3];
          TapDly_reg_re[4] <= TapDly_reg_next_re[4];
          TapDly_reg_im[4] <= TapDly_reg_next_im[4];
          TapDly_reg_re[5] <= TapDly_reg_next_re[5];
          TapDly_reg_im[5] <= TapDly_reg_next_im[5];
          TapDly_reg_re[6] <= TapDly_reg_next_re[6];
          TapDly_reg_im[6] <= TapDly_reg_next_im[6];
          TapDly_reg_re[7] <= TapDly_reg_next_re[7];
          TapDly_reg_im[7] <= TapDly_reg_next_im[7];
          TapDly_reg_re[8] <= TapDly_reg_next_re[8];
          TapDly_reg_im[8] <= TapDly_reg_next_im[8];
          TapDly_reg_re[9] <= TapDly_reg_next_re[9];
          TapDly_reg_im[9] <= TapDly_reg_next_im[9];
          TapDly_reg_re[10] <= TapDly_reg_next_re[10];
          TapDly_reg_im[10] <= TapDly_reg_next_im[10];
          TapDly_reg_re[11] <= TapDly_reg_next_re[11];
          TapDly_reg_im[11] <= TapDly_reg_next_im[11];
          TapDly_reg_re[12] <= TapDly_reg_next_re[12];
          TapDly_reg_im[12] <= TapDly_reg_next_im[12];
          TapDly_reg_re[13] <= TapDly_reg_next_re[13];
          TapDly_reg_im[13] <= TapDly_reg_next_im[13];
          TapDly_reg_re[14] <= TapDly_reg_next_re[14];
          TapDly_reg_im[14] <= TapDly_reg_next_im[14];
          TapDly_reg_re[15] <= TapDly_reg_next_re[15];
          TapDly_reg_im[15] <= TapDly_reg_next_im[15];
          TapDly_reg_re[16] <= TapDly_reg_next_re[16];
          TapDly_reg_im[16] <= TapDly_reg_next_im[16];
          TapDly_reg_re[17] <= TapDly_reg_next_re[17];
          TapDly_reg_im[17] <= TapDly_reg_next_im[17];
          TapDly_reg_re[18] <= TapDly_reg_next_re[18];
          TapDly_reg_im[18] <= TapDly_reg_next_im[18];
          TapDly_reg_re[19] <= TapDly_reg_next_re[19];
          TapDly_reg_im[19] <= TapDly_reg_next_im[19];
          TapDly_reg_re[20] <= TapDly_reg_next_re[20];
          TapDly_reg_im[20] <= TapDly_reg_next_im[20];
          TapDly_reg_re[21] <= TapDly_reg_next_re[21];
          TapDly_reg_im[21] <= TapDly_reg_next_im[21];
          TapDly_reg_re[22] <= TapDly_reg_next_re[22];
          TapDly_reg_im[22] <= TapDly_reg_next_im[22];
          TapDly_reg_re[23] <= TapDly_reg_next_re[23];
          TapDly_reg_im[23] <= TapDly_reg_next_im[23];
          TapDly_reg_re[24] <= TapDly_reg_next_re[24];
          TapDly_reg_im[24] <= TapDly_reg_next_im[24];
          TapDly_reg_re[25] <= TapDly_reg_next_re[25];
          TapDly_reg_im[25] <= TapDly_reg_next_im[25];
          TapDly_reg_re[26] <= TapDly_reg_next_re[26];
          TapDly_reg_im[26] <= TapDly_reg_next_im[26];
          TapDly_reg_re[27] <= TapDly_reg_next_re[27];
          TapDly_reg_im[27] <= TapDly_reg_next_im[27];
          TapDly_reg_re[28] <= TapDly_reg_next_re[28];
          TapDly_reg_im[28] <= TapDly_reg_next_im[28];
          TapDly_reg_re[29] <= TapDly_reg_next_re[29];
          TapDly_reg_im[29] <= TapDly_reg_next_im[29];
          TapDly_reg_re[30] <= TapDly_reg_next_re[30];
          TapDly_reg_im[30] <= TapDly_reg_next_im[30];
          TapDly_reg_re[31] <= TapDly_reg_next_re[31];
          TapDly_reg_im[31] <= TapDly_reg_next_im[31];
        end
      end
    end
  assign TapDly_out1_re[0] = TapDly_reg_re[0];
  assign TapDly_out1_im[0] = TapDly_reg_im[0];
  assign TapDly_out1_re[1] = TapDly_reg_re[1];
  assign TapDly_out1_im[1] = TapDly_reg_im[1];
  assign TapDly_out1_re[2] = TapDly_reg_re[2];
  assign TapDly_out1_im[2] = TapDly_reg_im[2];
  assign TapDly_out1_re[3] = TapDly_reg_re[3];
  assign TapDly_out1_im[3] = TapDly_reg_im[3];
  assign TapDly_out1_re[4] = TapDly_reg_re[4];
  assign TapDly_out1_im[4] = TapDly_reg_im[4];
  assign TapDly_out1_re[5] = TapDly_reg_re[5];
  assign TapDly_out1_im[5] = TapDly_reg_im[5];
  assign TapDly_out1_re[6] = TapDly_reg_re[6];
  assign TapDly_out1_im[6] = TapDly_reg_im[6];
  assign TapDly_out1_re[7] = TapDly_reg_re[7];
  assign TapDly_out1_im[7] = TapDly_reg_im[7];
  assign TapDly_out1_re[8] = TapDly_reg_re[8];
  assign TapDly_out1_im[8] = TapDly_reg_im[8];
  assign TapDly_out1_re[9] = TapDly_reg_re[9];
  assign TapDly_out1_im[9] = TapDly_reg_im[9];
  assign TapDly_out1_re[10] = TapDly_reg_re[10];
  assign TapDly_out1_im[10] = TapDly_reg_im[10];
  assign TapDly_out1_re[11] = TapDly_reg_re[11];
  assign TapDly_out1_im[11] = TapDly_reg_im[11];
  assign TapDly_out1_re[12] = TapDly_reg_re[12];
  assign TapDly_out1_im[12] = TapDly_reg_im[12];
  assign TapDly_out1_re[13] = TapDly_reg_re[13];
  assign TapDly_out1_im[13] = TapDly_reg_im[13];
  assign TapDly_out1_re[14] = TapDly_reg_re[14];
  assign TapDly_out1_im[14] = TapDly_reg_im[14];
  assign TapDly_out1_re[15] = TapDly_reg_re[15];
  assign TapDly_out1_im[15] = TapDly_reg_im[15];
  assign TapDly_out1_re[16] = TapDly_reg_re[16];
  assign TapDly_out1_im[16] = TapDly_reg_im[16];
  assign TapDly_out1_re[17] = TapDly_reg_re[17];
  assign TapDly_out1_im[17] = TapDly_reg_im[17];
  assign TapDly_out1_re[18] = TapDly_reg_re[18];
  assign TapDly_out1_im[18] = TapDly_reg_im[18];
  assign TapDly_out1_re[19] = TapDly_reg_re[19];
  assign TapDly_out1_im[19] = TapDly_reg_im[19];
  assign TapDly_out1_re[20] = TapDly_reg_re[20];
  assign TapDly_out1_im[20] = TapDly_reg_im[20];
  assign TapDly_out1_re[21] = TapDly_reg_re[21];
  assign TapDly_out1_im[21] = TapDly_reg_im[21];
  assign TapDly_out1_re[22] = TapDly_reg_re[22];
  assign TapDly_out1_im[22] = TapDly_reg_im[22];
  assign TapDly_out1_re[23] = TapDly_reg_re[23];
  assign TapDly_out1_im[23] = TapDly_reg_im[23];
  assign TapDly_out1_re[24] = TapDly_reg_re[24];
  assign TapDly_out1_im[24] = TapDly_reg_im[24];
  assign TapDly_out1_re[25] = TapDly_reg_re[25];
  assign TapDly_out1_im[25] = TapDly_reg_im[25];
  assign TapDly_out1_re[26] = TapDly_reg_re[26];
  assign TapDly_out1_im[26] = TapDly_reg_im[26];
  assign TapDly_out1_re[27] = TapDly_reg_re[27];
  assign TapDly_out1_im[27] = TapDly_reg_im[27];
  assign TapDly_out1_re[28] = TapDly_reg_re[28];
  assign TapDly_out1_im[28] = TapDly_reg_im[28];
  assign TapDly_out1_re[29] = TapDly_reg_re[29];
  assign TapDly_out1_im[29] = TapDly_reg_im[29];
  assign TapDly_out1_re[30] = TapDly_reg_re[30];
  assign TapDly_out1_im[30] = TapDly_reg_im[30];
  assign TapDly_out1_re[31] = TapDly_reg_re[31];
  assign TapDly_out1_im[31] = TapDly_reg_im[31];
  assign TapDly_reg_next_re[0] = TapDly_reg_re[1];
  assign TapDly_reg_next_im[0] = TapDly_reg_im[1];
  assign TapDly_reg_next_re[1] = TapDly_reg_re[2];
  assign TapDly_reg_next_im[1] = TapDly_reg_im[2];
  assign TapDly_reg_next_re[2] = TapDly_reg_re[3];
  assign TapDly_reg_next_im[2] = TapDly_reg_im[3];
  assign TapDly_reg_next_re[3] = TapDly_reg_re[4];
  assign TapDly_reg_next_im[3] = TapDly_reg_im[4];
  assign TapDly_reg_next_re[4] = TapDly_reg_re[5];
  assign TapDly_reg_next_im[4] = TapDly_reg_im[5];
  assign TapDly_reg_next_re[5] = TapDly_reg_re[6];
  assign TapDly_reg_next_im[5] = TapDly_reg_im[6];
  assign TapDly_reg_next_re[6] = TapDly_reg_re[7];
  assign TapDly_reg_next_im[6] = TapDly_reg_im[7];
  assign TapDly_reg_next_re[7] = TapDly_reg_re[8];
  assign TapDly_reg_next_im[7] = TapDly_reg_im[8];
  assign TapDly_reg_next_re[8] = TapDly_reg_re[9];
  assign TapDly_reg_next_im[8] = TapDly_reg_im[9];
  assign TapDly_reg_next_re[9] = TapDly_reg_re[10];
  assign TapDly_reg_next_im[9] = TapDly_reg_im[10];
  assign TapDly_reg_next_re[10] = TapDly_reg_re[11];
  assign TapDly_reg_next_im[10] = TapDly_reg_im[11];
  assign TapDly_reg_next_re[11] = TapDly_reg_re[12];
  assign TapDly_reg_next_im[11] = TapDly_reg_im[12];
  assign TapDly_reg_next_re[12] = TapDly_reg_re[13];
  assign TapDly_reg_next_im[12] = TapDly_reg_im[13];
  assign TapDly_reg_next_re[13] = TapDly_reg_re[14];
  assign TapDly_reg_next_im[13] = TapDly_reg_im[14];
  assign TapDly_reg_next_re[14] = TapDly_reg_re[15];
  assign TapDly_reg_next_im[14] = TapDly_reg_im[15];
  assign TapDly_reg_next_re[15] = TapDly_reg_re[16];
  assign TapDly_reg_next_im[15] = TapDly_reg_im[16];
  assign TapDly_reg_next_re[16] = TapDly_reg_re[17];
  assign TapDly_reg_next_im[16] = TapDly_reg_im[17];
  assign TapDly_reg_next_re[17] = TapDly_reg_re[18];
  assign TapDly_reg_next_im[17] = TapDly_reg_im[18];
  assign TapDly_reg_next_re[18] = TapDly_reg_re[19];
  assign TapDly_reg_next_im[18] = TapDly_reg_im[19];
  assign TapDly_reg_next_re[19] = TapDly_reg_re[20];
  assign TapDly_reg_next_im[19] = TapDly_reg_im[20];
  assign TapDly_reg_next_re[20] = TapDly_reg_re[21];
  assign TapDly_reg_next_im[20] = TapDly_reg_im[21];
  assign TapDly_reg_next_re[21] = TapDly_reg_re[22];
  assign TapDly_reg_next_im[21] = TapDly_reg_im[22];
  assign TapDly_reg_next_re[22] = TapDly_reg_re[23];
  assign TapDly_reg_next_im[22] = TapDly_reg_im[23];
  assign TapDly_reg_next_re[23] = TapDly_reg_re[24];
  assign TapDly_reg_next_im[23] = TapDly_reg_im[24];
  assign TapDly_reg_next_re[24] = TapDly_reg_re[25];
  assign TapDly_reg_next_im[24] = TapDly_reg_im[25];
  assign TapDly_reg_next_re[25] = TapDly_reg_re[26];
  assign TapDly_reg_next_im[25] = TapDly_reg_im[26];
  assign TapDly_reg_next_re[26] = TapDly_reg_re[27];
  assign TapDly_reg_next_im[26] = TapDly_reg_im[27];
  assign TapDly_reg_next_re[27] = TapDly_reg_re[28];
  assign TapDly_reg_next_im[27] = TapDly_reg_im[28];
  assign TapDly_reg_next_re[28] = TapDly_reg_re[29];
  assign TapDly_reg_next_im[28] = TapDly_reg_im[29];
  assign TapDly_reg_next_re[29] = TapDly_reg_re[30];
  assign TapDly_reg_next_im[29] = TapDly_reg_im[30];
  assign TapDly_reg_next_re[30] = TapDly_reg_re[31];
  assign TapDly_reg_next_im[30] = TapDly_reg_im[31];
  assign TapDly_reg_next_re[31] = D2_out1_re;
  assign TapDly_reg_next_im[31] = D2_out1_im;



  assign CorOut_re_0 = TapDly_out1_re[0];

  assign CorOut_re_1 = TapDly_out1_re[1];

  assign CorOut_re_2 = TapDly_out1_re[2];

  assign CorOut_re_3 = TapDly_out1_re[3];

  assign CorOut_re_4 = TapDly_out1_re[4];

  assign CorOut_re_5 = TapDly_out1_re[5];

  assign CorOut_re_6 = TapDly_out1_re[6];

  assign CorOut_re_7 = TapDly_out1_re[7];

  assign CorOut_re_8 = TapDly_out1_re[8];

  assign CorOut_re_9 = TapDly_out1_re[9];

  assign CorOut_re_10 = TapDly_out1_re[10];

  assign CorOut_re_11 = TapDly_out1_re[11];

  assign CorOut_re_12 = TapDly_out1_re[12];

  assign CorOut_re_13 = TapDly_out1_re[13];

  assign CorOut_re_14 = TapDly_out1_re[14];

  assign CorOut_re_15 = TapDly_out1_re[15];

  assign CorOut_re_16 = TapDly_out1_re[16];

  assign CorOut_re_17 = TapDly_out1_re[17];

  assign CorOut_re_18 = TapDly_out1_re[18];

  assign CorOut_re_19 = TapDly_out1_re[19];

  assign CorOut_re_20 = TapDly_out1_re[20];

  assign CorOut_re_21 = TapDly_out1_re[21];

  assign CorOut_re_22 = TapDly_out1_re[22];

  assign CorOut_re_23 = TapDly_out1_re[23];

  assign CorOut_re_24 = TapDly_out1_re[24];

  assign CorOut_re_25 = TapDly_out1_re[25];

  assign CorOut_re_26 = TapDly_out1_re[26];

  assign CorOut_re_27 = TapDly_out1_re[27];

  assign CorOut_re_28 = TapDly_out1_re[28];

  assign CorOut_re_29 = TapDly_out1_re[29];

  assign CorOut_re_30 = TapDly_out1_re[30];

  assign CorOut_re_31 = TapDly_out1_re[31];

  assign CorOut_im_0 = TapDly_out1_im[0];

  assign CorOut_im_1 = TapDly_out1_im[1];

  assign CorOut_im_2 = TapDly_out1_im[2];

  assign CorOut_im_3 = TapDly_out1_im[3];

  assign CorOut_im_4 = TapDly_out1_im[4];

  assign CorOut_im_5 = TapDly_out1_im[5];

  assign CorOut_im_6 = TapDly_out1_im[6];

  assign CorOut_im_7 = TapDly_out1_im[7];

  assign CorOut_im_8 = TapDly_out1_im[8];

  assign CorOut_im_9 = TapDly_out1_im[9];

  assign CorOut_im_10 = TapDly_out1_im[10];

  assign CorOut_im_11 = TapDly_out1_im[11];

  assign CorOut_im_12 = TapDly_out1_im[12];

  assign CorOut_im_13 = TapDly_out1_im[13];

  assign CorOut_im_14 = TapDly_out1_im[14];

  assign CorOut_im_15 = TapDly_out1_im[15];

  assign CorOut_im_16 = TapDly_out1_im[16];

  assign CorOut_im_17 = TapDly_out1_im[17];

  assign CorOut_im_18 = TapDly_out1_im[18];

  assign CorOut_im_19 = TapDly_out1_im[19];

  assign CorOut_im_20 = TapDly_out1_im[20];

  assign CorOut_im_21 = TapDly_out1_im[21];

  assign CorOut_im_22 = TapDly_out1_im[22];

  assign CorOut_im_23 = TapDly_out1_im[23];

  assign CorOut_im_24 = TapDly_out1_im[24];

  assign CorOut_im_25 = TapDly_out1_im[25];

  assign CorOut_im_26 = TapDly_out1_im[26];

  assign CorOut_im_27 = TapDly_out1_im[27];

  assign CorOut_im_28 = TapDly_out1_im[28];

  assign CorOut_im_29 = TapDly_out1_im[29];

  assign CorOut_im_30 = TapDly_out1_im[30];

  assign CorOut_im_31 = TapDly_out1_im[31];

  Abs_block   u_Abs   (.DIn_re(D2_out1_re),  // sfix16_En4
                       .DIn_im(D2_out1_im),  // sfix16_En4
                       .DOut(Abs_out1)  // ufix16_En4
                       );

  always @(posedge clk or posedge reset)
    begin : TapDly1_process
      if (reset == 1'b1) begin
        TapDly1_reg[0] <= 16'b0000000000000000;
        TapDly1_reg[1] <= 16'b0000000000000000;
        TapDly1_reg[2] <= 16'b0000000000000000;
        TapDly1_reg[3] <= 16'b0000000000000000;
        TapDly1_reg[4] <= 16'b0000000000000000;
        TapDly1_reg[5] <= 16'b0000000000000000;
        TapDly1_reg[6] <= 16'b0000000000000000;
        TapDly1_reg[7] <= 16'b0000000000000000;
        TapDly1_reg[8] <= 16'b0000000000000000;
        TapDly1_reg[9] <= 16'b0000000000000000;
        TapDly1_reg[10] <= 16'b0000000000000000;
        TapDly1_reg[11] <= 16'b0000000000000000;
        TapDly1_reg[12] <= 16'b0000000000000000;
        TapDly1_reg[13] <= 16'b0000000000000000;
        TapDly1_reg[14] <= 16'b0000000000000000;
        TapDly1_reg[15] <= 16'b0000000000000000;
        TapDly1_reg[16] <= 16'b0000000000000000;
        TapDly1_reg[17] <= 16'b0000000000000000;
        TapDly1_reg[18] <= 16'b0000000000000000;
        TapDly1_reg[19] <= 16'b0000000000000000;
        TapDly1_reg[20] <= 16'b0000000000000000;
        TapDly1_reg[21] <= 16'b0000000000000000;
        TapDly1_reg[22] <= 16'b0000000000000000;
        TapDly1_reg[23] <= 16'b0000000000000000;
        TapDly1_reg[24] <= 16'b0000000000000000;
        TapDly1_reg[25] <= 16'b0000000000000000;
        TapDly1_reg[26] <= 16'b0000000000000000;
        TapDly1_reg[27] <= 16'b0000000000000000;
        TapDly1_reg[28] <= 16'b0000000000000000;
        TapDly1_reg[29] <= 16'b0000000000000000;
        TapDly1_reg[30] <= 16'b0000000000000000;
        TapDly1_reg[31] <= 16'b0000000000000000;
      end
      else begin
        if (enb_1_4_0) begin
          TapDly1_reg[0] <= TapDly1_reg_next[0];
          TapDly1_reg[1] <= TapDly1_reg_next[1];
          TapDly1_reg[2] <= TapDly1_reg_next[2];
          TapDly1_reg[3] <= TapDly1_reg_next[3];
          TapDly1_reg[4] <= TapDly1_reg_next[4];
          TapDly1_reg[5] <= TapDly1_reg_next[5];
          TapDly1_reg[6] <= TapDly1_reg_next[6];
          TapDly1_reg[7] <= TapDly1_reg_next[7];
          TapDly1_reg[8] <= TapDly1_reg_next[8];
          TapDly1_reg[9] <= TapDly1_reg_next[9];
          TapDly1_reg[10] <= TapDly1_reg_next[10];
          TapDly1_reg[11] <= TapDly1_reg_next[11];
          TapDly1_reg[12] <= TapDly1_reg_next[12];
          TapDly1_reg[13] <= TapDly1_reg_next[13];
          TapDly1_reg[14] <= TapDly1_reg_next[14];
          TapDly1_reg[15] <= TapDly1_reg_next[15];
          TapDly1_reg[16] <= TapDly1_reg_next[16];
          TapDly1_reg[17] <= TapDly1_reg_next[17];
          TapDly1_reg[18] <= TapDly1_reg_next[18];
          TapDly1_reg[19] <= TapDly1_reg_next[19];
          TapDly1_reg[20] <= TapDly1_reg_next[20];
          TapDly1_reg[21] <= TapDly1_reg_next[21];
          TapDly1_reg[22] <= TapDly1_reg_next[22];
          TapDly1_reg[23] <= TapDly1_reg_next[23];
          TapDly1_reg[24] <= TapDly1_reg_next[24];
          TapDly1_reg[25] <= TapDly1_reg_next[25];
          TapDly1_reg[26] <= TapDly1_reg_next[26];
          TapDly1_reg[27] <= TapDly1_reg_next[27];
          TapDly1_reg[28] <= TapDly1_reg_next[28];
          TapDly1_reg[29] <= TapDly1_reg_next[29];
          TapDly1_reg[30] <= TapDly1_reg_next[30];
          TapDly1_reg[31] <= TapDly1_reg_next[31];
        end
      end
    end
  assign TapDly1_out1[0] = TapDly1_reg[0];
  assign TapDly1_out1[1] = TapDly1_reg[1];
  assign TapDly1_out1[2] = TapDly1_reg[2];
  assign TapDly1_out1[3] = TapDly1_reg[3];
  assign TapDly1_out1[4] = TapDly1_reg[4];
  assign TapDly1_out1[5] = TapDly1_reg[5];
  assign TapDly1_out1[6] = TapDly1_reg[6];
  assign TapDly1_out1[7] = TapDly1_reg[7];
  assign TapDly1_out1[8] = TapDly1_reg[8];
  assign TapDly1_out1[9] = TapDly1_reg[9];
  assign TapDly1_out1[10] = TapDly1_reg[10];
  assign TapDly1_out1[11] = TapDly1_reg[11];
  assign TapDly1_out1[12] = TapDly1_reg[12];
  assign TapDly1_out1[13] = TapDly1_reg[13];
  assign TapDly1_out1[14] = TapDly1_reg[14];
  assign TapDly1_out1[15] = TapDly1_reg[15];
  assign TapDly1_out1[16] = TapDly1_reg[16];
  assign TapDly1_out1[17] = TapDly1_reg[17];
  assign TapDly1_out1[18] = TapDly1_reg[18];
  assign TapDly1_out1[19] = TapDly1_reg[19];
  assign TapDly1_out1[20] = TapDly1_reg[20];
  assign TapDly1_out1[21] = TapDly1_reg[21];
  assign TapDly1_out1[22] = TapDly1_reg[22];
  assign TapDly1_out1[23] = TapDly1_reg[23];
  assign TapDly1_out1[24] = TapDly1_reg[24];
  assign TapDly1_out1[25] = TapDly1_reg[25];
  assign TapDly1_out1[26] = TapDly1_reg[26];
  assign TapDly1_out1[27] = TapDly1_reg[27];
  assign TapDly1_out1[28] = TapDly1_reg[28];
  assign TapDly1_out1[29] = TapDly1_reg[29];
  assign TapDly1_out1[30] = TapDly1_reg[30];
  assign TapDly1_out1[31] = TapDly1_reg[31];
  assign TapDly1_reg_next[0] = TapDly1_reg[1];
  assign TapDly1_reg_next[1] = TapDly1_reg[2];
  assign TapDly1_reg_next[2] = TapDly1_reg[3];
  assign TapDly1_reg_next[3] = TapDly1_reg[4];
  assign TapDly1_reg_next[4] = TapDly1_reg[5];
  assign TapDly1_reg_next[5] = TapDly1_reg[6];
  assign TapDly1_reg_next[6] = TapDly1_reg[7];
  assign TapDly1_reg_next[7] = TapDly1_reg[8];
  assign TapDly1_reg_next[8] = TapDly1_reg[9];
  assign TapDly1_reg_next[9] = TapDly1_reg[10];
  assign TapDly1_reg_next[10] = TapDly1_reg[11];
  assign TapDly1_reg_next[11] = TapDly1_reg[12];
  assign TapDly1_reg_next[12] = TapDly1_reg[13];
  assign TapDly1_reg_next[13] = TapDly1_reg[14];
  assign TapDly1_reg_next[14] = TapDly1_reg[15];
  assign TapDly1_reg_next[15] = TapDly1_reg[16];
  assign TapDly1_reg_next[16] = TapDly1_reg[17];
  assign TapDly1_reg_next[17] = TapDly1_reg[18];
  assign TapDly1_reg_next[18] = TapDly1_reg[19];
  assign TapDly1_reg_next[19] = TapDly1_reg[20];
  assign TapDly1_reg_next[20] = TapDly1_reg[21];
  assign TapDly1_reg_next[21] = TapDly1_reg[22];
  assign TapDly1_reg_next[22] = TapDly1_reg[23];
  assign TapDly1_reg_next[23] = TapDly1_reg[24];
  assign TapDly1_reg_next[24] = TapDly1_reg[25];
  assign TapDly1_reg_next[25] = TapDly1_reg[26];
  assign TapDly1_reg_next[26] = TapDly1_reg[27];
  assign TapDly1_reg_next[27] = TapDly1_reg[28];
  assign TapDly1_reg_next[28] = TapDly1_reg[29];
  assign TapDly1_reg_next[29] = TapDly1_reg[30];
  assign TapDly1_reg_next[30] = TapDly1_reg[31];
  assign TapDly1_reg_next[31] = Abs_out1;



  assign AbsCorOut_0 = TapDly1_out1[0];

  assign AbsCorOut_1 = TapDly1_out1[1];

  assign AbsCorOut_2 = TapDly1_out1[2];

  assign AbsCorOut_3 = TapDly1_out1[3];

  assign AbsCorOut_4 = TapDly1_out1[4];

  assign AbsCorOut_5 = TapDly1_out1[5];

  assign AbsCorOut_6 = TapDly1_out1[6];

  assign AbsCorOut_7 = TapDly1_out1[7];

  assign AbsCorOut_8 = TapDly1_out1[8];

  assign AbsCorOut_9 = TapDly1_out1[9];

  assign AbsCorOut_10 = TapDly1_out1[10];

  assign AbsCorOut_11 = TapDly1_out1[11];

  assign AbsCorOut_12 = TapDly1_out1[12];

  assign AbsCorOut_13 = TapDly1_out1[13];

  assign AbsCorOut_14 = TapDly1_out1[14];

  assign AbsCorOut_15 = TapDly1_out1[15];

  assign AbsCorOut_16 = TapDly1_out1[16];

  assign AbsCorOut_17 = TapDly1_out1[17];

  assign AbsCorOut_18 = TapDly1_out1[18];

  assign AbsCorOut_19 = TapDly1_out1[19];

  assign AbsCorOut_20 = TapDly1_out1[20];

  assign AbsCorOut_21 = TapDly1_out1[21];

  assign AbsCorOut_22 = TapDly1_out1[22];

  assign AbsCorOut_23 = TapDly1_out1[23];

  assign AbsCorOut_24 = TapDly1_out1[24];

  assign AbsCorOut_25 = TapDly1_out1[25];

  assign AbsCorOut_26 = TapDly1_out1[26];

  assign AbsCorOut_27 = TapDly1_out1[27];

  assign AbsCorOut_28 = TapDly1_out1[28];

  assign AbsCorOut_29 = TapDly1_out1[29];

  assign AbsCorOut_30 = TapDly1_out1[30];

  assign AbsCorOut_31 = TapDly1_out1[31];

endmodule  // GolCorFx1

