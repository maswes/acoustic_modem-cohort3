/*
 * File Name:         C:\WES207\hoa\Final\ipcore\hoa2_0602_Rx_v1_0\include\hoa2_0602_Rx_addr.h
 * Description:       C Header File
 * Created:           2015-06-02 20:54:48
*/

#ifndef HOA2_0602_RX_H_
#define HOA2_0602_RX_H_

#define  IPCore_Reset_hoa2_0602_Rx    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_hoa2_0602_Rx   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* HOA2_0602_RX_H_ */
