/*
 * File Name:         C:\WES207\hoa\ipcore\hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_v1_0\include\hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-02 15:09:29
*/

#ifndef HOA2_0602_SUBSYSTEM1_SUBSYSTEM_TRANSMI_IPCORE_H_
#define HOA2_0602_SUBSYSTEM1_SUBSYSTEM_TRANSMI_IPCORE_H_

#define  IPCore_Reset_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* HOA2_0602_SUBSYSTEM1_SUBSYSTEM_TRANSMI_IPCORE_H_ */
