// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\GolAcc.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: GolAcc
// Source Path: hoa2_0612c/TX_RX/Tx_Rx/TxRx/Receiver/RX/DlyRotCtl/GolIHold/GolAcc
// Hierarchy Level: 7
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module GolAcc
          (
           clk,
           reset,
           enb,
           GolDet,
           Reset_1,
           Out1
          );


  input   clk;
  input   reset;
  input   enb;
  input   GolDet;
  input   Reset_1;
  output  [7:0] Out1;  // uint8


  reg  Delay1_out1;
  wire [7:0] C2_out1;  // uint8
  wire [7:0] C3_out1;  // uint8
  reg [7:0] Delay3_out1;  // uint8
  wire [7:0] CoarseFine2_out1;  // uint8
  wire [7:0] Switch1_out1;  // uint8
  wire switch_compare_1;
  wire [7:0] AccCtrl1_out1;  // uint8
  wire [7:0] Switch_out1;  // uint8


  always @(posedge clk or posedge reset)
    begin : Delay1_process
      if (reset == 1'b1) begin
        Delay1_out1 <= 1'b0;
      end
      else begin
        if (enb) begin
          Delay1_out1 <= GolDet;
        end
      end
    end


  assign C2_out1 = 8'b00000000;



  assign C3_out1 = 8'b00000001;



  assign CoarseFine2_out1 = Delay3_out1 + C3_out1;



  assign Switch1_out1 = (Delay1_out1 == 1'b0 ? C2_out1 :
              CoarseFine2_out1);



  assign switch_compare_1 = Delay3_out1 >= 8'b00000111;



  assign AccCtrl1_out1 = (switch_compare_1 == 1'b0 ? Switch1_out1 :
              Delay3_out1);



  assign Switch_out1 = (Reset_1 == 1'b0 ? AccCtrl1_out1 :
              C2_out1);



  always @(posedge clk or posedge reset)
    begin : Delay3_process
      if (reset == 1'b1) begin
        Delay3_out1 <= 8'b00000000;
      end
      else begin
        if (enb) begin
          Delay3_out1 <= Switch_out1;
        end
      end
    end


  assign Out1 = Delay3_out1;

endmodule  // GolAcc

