// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\Raised_Cosine_Transmit_Filter1.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: Raised_Cosine_Transmit_Filter1
// Source Path: hoa2_0612c/TX_RX/Tx_Rx/TxRx/Transmitter/TxProcessing/Raised Cosine Transmit Filter1
// Hierarchy Level: 5
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module Raised_Cosine_Transmit_Filter1
          (
           clk,
           reset,
           enb_1_1_1,
           In1,
           Out1
          );


  input   clk;
  input   reset;
  input   enb_1_1_1;
  input   signed [11:0] In1;  // sfix12_En8
  output  signed [11:0] Out1;  // sfix12_En8


  wire signed [11:0] FIR_Interpolation_out1;  // sfix12_En8


  FIR_Interpolation   u_FIR_Interpolation   (.clk(clk),
                                             .enb_1_1_1(enb_1_1_1),
                                             .reset(reset),
                                             .FIR_Interpolation_in(In1),  // sfix12_En8
                                             .FIR_Interpolation_out(FIR_Interpolation_out1)  // sfix12_En8
                                             );

  assign Out1 = FIR_Interpolation_out1;

endmodule  // Raised_Cosine_Transmit_Filter1

