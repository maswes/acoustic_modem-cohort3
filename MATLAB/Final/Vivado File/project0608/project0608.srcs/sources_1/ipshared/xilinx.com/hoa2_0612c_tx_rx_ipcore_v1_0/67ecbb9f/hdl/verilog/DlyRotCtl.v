// -------------------------------------------------------------
// 
// File Name: hdl_prj_JAE2\hdlsrc\hoa2_0612c\DlyRotCtl.v
// Created: 2015-06-08 06:31:43
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: DlyRotCtl
// Source Path: hoa2_0612c/TX_RX/Tx_Rx/TxRx/Receiver/RX/DlyRotCtl
// Hierarchy Level: 5
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module DlyRotCtl
          (
           clk,
           reset,
           enb,
           GolII,
           GolIQ,
           CoarseDly,
           FineDly,
           GolDet,
           DlyTune,
           Reset_1,
           SymbEn,
           RxDly,
           sinSH,
           cosSH,
           Coarse,
           Fine
          );


  input   clk;
  input   reset;
  input   enb;
  input   signed [15:0] GolII;  // sfix16_En8
  input   signed [15:0] GolIQ;  // sfix16_En8
  input   [31:0] CoarseDly;  // uint32
  input   [31:0] FineDly;  // uint32
  input   GolDet;
  input   signed [15:0] DlyTune;  // int16
  input   Reset_1;
  input   SymbEn;
  output  [7:0] RxDly;  // uint8
  output  signed [15:0] sinSH;  // sfix16_En8
  output  signed [15:0] cosSH;  // sfix16_En8
  output  [31:0] Coarse;  // uint32
  output  [31:0] Fine;  // uint32


  wire enb_gated;
  reg  GolDetReg_out1;
  reg signed [15:0] DlyTuneQ_out1;  // int16
  wire [7:0] Cx4_F_out1;  // uint8
  wire [7:0] OffsetHold_out1;  // uint8
  wire [7:0] OffsetHold_out1_bypass;  // uint8
  reg [7:0] OffsetHold_out1_last_value;  // uint8
  reg signed [15:0] GolIReg_out1;  // sfix16_En8
  wire signed [7:0] C1_out1;  // int8
  wire signed [16:0] cos_sub_cast;  // sfix17_En8
  wire signed [16:0] cos_sub_cast_1;  // sfix17_En8
  wire signed [16:0] cos_sub_temp;  // sfix17_En8
  wire signed [15:0] cos_out1;  // sfix16_En8
  wire signed [15:0] GolIHold_out1;  // sfix16_En8
  wire signed [15:0] GolIHold_out1_bypass;  // sfix16_En8
  reg signed [15:0] GolIHold_out1_last_value;  // sfix16_En8
  reg signed [15:0] GolQReg_out1;  // sfix16_En8
  wire signed [16:0] sin_sub_cast;  // sfix17_En8
  wire signed [16:0] sin_sub_cast_1;  // sfix17_En8
  wire signed [16:0] sin_sub_temp;  // sfix17_En8
  wire signed [15:0] sin_out1;  // sfix16_En8
  wire signed [15:0] GolQHold_out1;  // sfix16_En8
  wire signed [15:0] GolQHold_out1_bypass;  // sfix16_En8
  reg signed [15:0] GolQHold_out1_last_value;  // sfix16_En8
  wire [31:0] CoarseHold_out1;  // uint32
  wire [31:0] CoarseHold_out1_bypass;  // uint32
  reg [31:0] CoarseHold_out1_last_value;  // uint32
  wire [31:0] FineHold_out1;  // uint32
  wire [31:0] FineHold_out1_bypass;  // uint32
  reg [31:0] FineHold_out1_last_value;  // uint32


  assign enb_gated = SymbEn && enb;

  always @(posedge clk or posedge reset)
    begin : GolDetReg_process
      if (reset == 1'b1) begin
        GolDetReg_out1 <= 1'b0;
      end
      else begin
        if (enb_gated) begin
          GolDetReg_out1 <= GolDet;
        end
      end
    end


  always @(posedge clk or posedge reset)
    begin : DlyTuneQ_process
      if (reset == 1'b1) begin
        DlyTuneQ_out1 <= 16'sb0000000000000000;
      end
      else begin
        if (enb_gated) begin
          DlyTuneQ_out1 <= DlyTune;
        end
      end
    end


  Cx4_F   u_Cx4_F   (.Coarse(CoarseDly),  // uint32
                     .Fine(FineDly),  // uint32
                     .Tune(DlyTuneQ_out1),  // int16
                     .Dly(Cx4_F_out1)  // uint8
                     );

  OffsetHold   u_OffsetHold   (.clk(clk),
                               .reset(reset),
                               .enb(enb_gated),
                               .GolDet(GolDetReg_out1),
                               .RxDlyIn(Cx4_F_out1),  // uint8
                               .Reset_1(Reset_1),
                               .RxDlyOut(OffsetHold_out1)  // uint8
                               );

  always @(posedge clk or posedge reset)
    begin : RxDly_bypass_process
      if (reset == 1'b1) begin
        OffsetHold_out1_last_value <= 8'b00000000;
      end
      else begin
        if (enb_gated) begin
          OffsetHold_out1_last_value <= OffsetHold_out1_bypass;
        end
      end
    end


  assign OffsetHold_out1_bypass = (SymbEn == 1'b0 ? OffsetHold_out1_last_value :
              OffsetHold_out1);



  assign RxDly = OffsetHold_out1_bypass;

  always @(posedge clk or posedge reset)
    begin : GolIReg_process
      if (reset == 1'b1) begin
        GolIReg_out1 <= 16'sb0000000000000000;
      end
      else begin
        if (enb_gated) begin
          GolIReg_out1 <= GolII;
        end
      end
    end


  assign C1_out1 = 8'sb00000000;



  assign cos_sub_cast = {GolIReg_out1[15], GolIReg_out1};
  assign cos_sub_cast_1 = {C1_out1[7], {C1_out1, 8'b00000000}};
  assign cos_sub_temp = cos_sub_cast - cos_sub_cast_1;
  assign cos_out1 = cos_sub_temp[15:0];



  GolIHold   u_GolIHold   (.clk(clk),
                           .reset(reset),
                           .enb(enb_gated),
                           .GolDet(GolDetReg_out1),
                           .DIn(cos_out1),  // sfix16_En8
                           .Reset_1(Reset_1),
                           .AvgOut(GolIHold_out1)  // sfix16_En8
                           );

  always @(posedge clk or posedge reset)
    begin : sinSH_bypass_process
      if (reset == 1'b1) begin
        GolIHold_out1_last_value <= 16'sb0000000000000000;
      end
      else begin
        if (enb_gated) begin
          GolIHold_out1_last_value <= GolIHold_out1_bypass;
        end
      end
    end


  assign GolIHold_out1_bypass = (SymbEn == 1'b0 ? GolIHold_out1_last_value :
              GolIHold_out1);



  assign sinSH = GolIHold_out1_bypass;

  always @(posedge clk or posedge reset)
    begin : GolQReg_process
      if (reset == 1'b1) begin
        GolQReg_out1 <= 16'sb0000000000000000;
      end
      else begin
        if (enb_gated) begin
          GolQReg_out1 <= GolIQ;
        end
      end
    end


  assign sin_sub_cast = {GolQReg_out1[15], GolQReg_out1};
  assign sin_sub_cast_1 = {C1_out1[7], {C1_out1, 8'b00000000}};
  assign sin_sub_temp = sin_sub_cast - sin_sub_cast_1;
  assign sin_out1 = sin_sub_temp[15:0];



  GolQHold   u_GolQHold   (.clk(clk),
                           .reset(reset),
                           .enb(enb_gated),
                           .GolDet(GolDetReg_out1),
                           .DIn(sin_out1),  // sfix16_En8
                           .Reset_1(Reset_1),
                           .AvgOut(GolQHold_out1)  // sfix16_En8
                           );

  always @(posedge clk or posedge reset)
    begin : cosSH_bypass_process
      if (reset == 1'b1) begin
        GolQHold_out1_last_value <= 16'sb0000000000000000;
      end
      else begin
        if (enb_gated) begin
          GolQHold_out1_last_value <= GolQHold_out1_bypass;
        end
      end
    end


  assign GolQHold_out1_bypass = (SymbEn == 1'b0 ? GolQHold_out1_last_value :
              GolQHold_out1);



  assign cosSH = GolQHold_out1_bypass;

  CoarseHold   u_CoarseHold   (.clk(clk),
                               .reset(reset),
                               .enb(enb_gated),
                               .GolDet(GolDetReg_out1),
                               .CrsIn(CoarseDly),  // uint32
                               .CrsOut(CoarseHold_out1)  // uint32
                               );

  always @(posedge clk or posedge reset)
    begin : Coarse_bypass_process
      if (reset == 1'b1) begin
        CoarseHold_out1_last_value <= 32'b00000000000000000000000000000000;
      end
      else begin
        if (enb_gated) begin
          CoarseHold_out1_last_value <= CoarseHold_out1_bypass;
        end
      end
    end


  assign CoarseHold_out1_bypass = (SymbEn == 1'b0 ? CoarseHold_out1_last_value :
              CoarseHold_out1);



  assign Coarse = CoarseHold_out1_bypass;

  FineHold   u_FineHold   (.clk(clk),
                           .reset(reset),
                           .enb(enb_gated),
                           .GolDet(GolDetReg_out1),
                           .FinIn(FineDly),  // uint32
                           .FinOut(FineHold_out1)  // uint32
                           );

  always @(posedge clk or posedge reset)
    begin : Fine_bypass_process
      if (reset == 1'b1) begin
        FineHold_out1_last_value <= 32'b00000000000000000000000000000000;
      end
      else begin
        if (enb_gated) begin
          FineHold_out1_last_value <= FineHold_out1_bypass;
        end
      end
    end


  assign FineHold_out1_bypass = (SymbEn == 1'b0 ? FineHold_out1_last_value :
              FineHold_out1);



  assign Fine = FineHold_out1_bypass;

endmodule  // DlyRotCtl

