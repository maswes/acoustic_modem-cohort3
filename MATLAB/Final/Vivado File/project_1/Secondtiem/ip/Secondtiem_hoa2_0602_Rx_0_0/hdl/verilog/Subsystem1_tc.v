// -------------------------------------------------------------
// 
// File Name: C:\WES207\hoa\Final\hdlsrc\hoa2_0602\Subsystem1_tc.v
// Created: 2015-06-02 20:54:40
// 
// Generated by MATLAB 8.5 and HDL Coder 3.6
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: Subsystem1_tc
// Source Path: Subsystem1_tc
// Hierarchy Level: 1
// 
// Master clock enable input: clk_enable
// 
// enb         : identical to clk_enable
// enb_1_1_1   : identical to clk_enable
// enb_1_4_0   : 4x slower than clk_enable with last phase
// enb_1_4_1   : 4x slower than clk_enable with phase 1
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module Subsystem1_tc
          (
           clk,
           reset,
           clk_enable,
           enb,
           enb_1_1_1,
           enb_1_4_0,
           enb_1_4_1
          );


  input   clk;
  input   reset;
  input   clk_enable;
  output  enb;
  output  enb_1_1_1;
  output  enb_1_4_0;
  output  enb_1_4_1;


  reg [1:0] count4;  // ufix2
  wire phase_all;
  reg  phase_0;
  wire phase_0_tmp;
  reg  phase_1;
  wire phase_1_tmp;


  always @ (posedge clk or posedge reset)
    begin: Counter4
      if (reset == 1'b1) begin
        count4 <= 2'b01;
      end
      else begin
        if (clk_enable == 1'b1) begin
          if (count4 == 2'b11) begin
            count4 <= 2'b00;
          end
          else begin
            count4 <= count4 + 1;
          end
        end
      end
    end // Counter4

  assign  phase_all = clk_enable? 1 : 0;

  always @ (posedge clk or posedge reset)
    begin: temp_process1
      if (reset == 1'b1) begin
        phase_0 <= 1'b0;
      end
      else begin
        if (clk_enable == 1'b1) begin
          phase_0 <= phase_0_tmp;
        end
      end
    end // temp_process1

  assign  phase_0_tmp = (count4 == 2'b11 && clk_enable == 1'b1)? 1 : 0;

  always @ (posedge clk or posedge reset)
    begin: temp_process2
      if (reset == 1'b1) begin
        phase_1 <= 1'b1;
      end
      else begin
        if (clk_enable == 1'b1) begin
          phase_1 <= phase_1_tmp;
        end
      end
    end // temp_process2

  assign  phase_1_tmp = (count4 == 2'b00 && clk_enable == 1'b1)? 1 : 0;

  assign enb =  phase_all & clk_enable;

  assign enb_1_1_1 =  phase_all & clk_enable;

  assign enb_1_4_0 =  phase_0 & clk_enable;

  assign enb_1_4_1 =  phase_1 & clk_enable;


endmodule  // Subsystem1_tc

