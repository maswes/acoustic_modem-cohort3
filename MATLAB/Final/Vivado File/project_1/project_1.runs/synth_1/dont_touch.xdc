# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# XDC: C:/Users/Owner/Desktop/hoa/Project/Working Project/uw_modem_constraints.xdc

# Block Designs: C:/Users/Owner/Desktop/Final/project_1/Secondtiem/Secondtiem.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem || ORIG_REF_NAME==Secondtiem}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_0_0/Secondtiem_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_0_0 || ORIG_REF_NAME==Secondtiem_hoa2_0602_Subsystem1_Subsystem_Transmi_ipcore_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_processing_system7_0_0/Secondtiem_processing_system7_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_processing_system7_0_0 || ORIG_REF_NAME==Secondtiem_processing_system7_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_axi_gpio_0_0/Secondtiem_axi_gpio_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_axi_gpio_0_0 || ORIG_REF_NAME==Secondtiem_axi_gpio_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_processing_system7_0_axi_periph_0/Secondtiem_processing_system7_0_axi_periph_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_processing_system7_0_axi_periph_0 || ORIG_REF_NAME==Secondtiem_processing_system7_0_axi_periph_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_rst_processing_system7_0_100M_0/Secondtiem_rst_processing_system7_0_100M_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_rst_processing_system7_0_100M_0 || ORIG_REF_NAME==Secondtiem_rst_processing_system7_0_100M_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xbar_0/Secondtiem_xbar_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_xbar_0 || ORIG_REF_NAME==Secondtiem_xbar_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xlslice_0_0/Secondtiem_xlslice_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_xlslice_0_0 || ORIG_REF_NAME==Secondtiem_xlslice_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_hoa2_0602_Rx_0_0/Secondtiem_hoa2_0602_Rx_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_hoa2_0602_Rx_0_0 || ORIG_REF_NAME==Secondtiem_hoa2_0602_Rx_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xlconcat_0_0/Secondtiem_xlconcat_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_xlconcat_0_0 || ORIG_REF_NAME==Secondtiem_xlconcat_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xlconstant_0_0/Secondtiem_xlconstant_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_xlconstant_0_0 || ORIG_REF_NAME==Secondtiem_xlconstant_0_0}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xlconstant_0_1/Secondtiem_xlconstant_0_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_xlconstant_0_1 || ORIG_REF_NAME==Secondtiem_xlconstant_0_1}]

# IP: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_auto_pc_0/Secondtiem_auto_pc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_auto_pc_0 || ORIG_REF_NAME==Secondtiem_auto_pc_0}]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_processing_system7_0_0/Secondtiem_processing_system7_0_0.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Secondtiem_processing_system7_0_0 || ORIG_REF_NAME==Secondtiem_processing_system7_0_0}] {/inst }]/inst ]]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_axi_gpio_0_0/Secondtiem_axi_gpio_0_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Secondtiem_axi_gpio_0_0 || ORIG_REF_NAME==Secondtiem_axi_gpio_0_0}] {/U0 }]/U0 ]]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_axi_gpio_0_0/Secondtiem_axi_gpio_0_0_ooc.xdc

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_axi_gpio_0_0/Secondtiem_axi_gpio_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Secondtiem_axi_gpio_0_0 || ORIG_REF_NAME==Secondtiem_axi_gpio_0_0}] {/U0 }]/U0 ]]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_rst_processing_system7_0_100M_0/Secondtiem_rst_processing_system7_0_100M_0_board.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_rst_processing_system7_0_100M_0 || ORIG_REF_NAME==Secondtiem_rst_processing_system7_0_100M_0}]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_rst_processing_system7_0_100M_0/Secondtiem_rst_processing_system7_0_100M_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Secondtiem_rst_processing_system7_0_100M_0 || ORIG_REF_NAME==Secondtiem_rst_processing_system7_0_100M_0}]

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_rst_processing_system7_0_100M_0/Secondtiem_rst_processing_system7_0_100M_0_ooc.xdc

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_xbar_0/Secondtiem_xbar_0_ooc.xdc

# XDC: c:/Users/Owner/Desktop/Final/project_1/Secondtiem/ip/Secondtiem_auto_pc_0/Secondtiem_auto_pc_0_ooc.xdc

# XDC: C:/Users/Owner/Desktop/Final/project_1/Secondtiem/Secondtiem_ooc.xdc
