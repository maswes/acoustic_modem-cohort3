-------------------------------------------------------------------
-- System Generator version 6.1 VHDL source file.
-- Copyright (c) 2003, Xilinx, Inc.  All rights reserved.
-- Reproduction or reuse, in any form, without the explicit written
-- consent of Xilinx, Inc., is strictly prohibited.
-------------------------------------------------------------------
LIBRARY std, ieee;
USE std.standard.ALL;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------
-- Copy Entity Description from core.vhd file
-- in the section for entity declaration.
-- Rename entity name from core to core_wrapper
-------------------------------------------------------------------

------------- Begin Entity Declaration

ENTITY core_wrapper IS

   -- **** Copy top level port descriptions as specified for
   -- **** the component declaration in core.vho file
   -- **** If the top-level description only has a "CLK" port
   -- **** and does not have a "CE" port add a CE port to the top 
   -- **** entity description
   -- **** CE: IN std_logic;
   
END core_wrapper;

------------- End Entity Declaration

ARCHITECTURE xilinx OF core_wrapper IS

attribute black_box : boolean;
attribute syn_black_box : boolean;
attribute fpga_dont_touch: string;
attribute box_type :  string;

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG

   -- **** Copy the component declaration from the core.vho
   -- **** file to this location
   
-- COMP_TAG_END ------ End COMPONENT Declaration ------------

attribute syn_black_box of core : component is true;
attribute fpga_dont_touch of core : component is "true";
attribute box_type of core :  component  is "black_box";

begin
------------- Begin here for INSTANTIATION Template ----- INST_TAG

   -- **** Copy the INSTANTIATION Template from the core.vho
   -- **** to this location.
   -- Rename your_instance_name to core_instance
   
------ End INSTANTIATION Template ------------
END xilinx;
