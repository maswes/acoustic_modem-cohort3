library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity word_parity_block is
  generic (
    width: integer := 8
  );
  port (
    din: in std_logic_vector(width - 1 downto 0);
    parity: out std_logic
  );
end word_parity_block;

architecture behavior of word_parity_block is
begin
 WORD_PARITY_Process:  process (din)
   variable partial_parity : std_logic := '0';
  begin
    partial_parity := '0';

    XOR_BIT_LOOP: for N in din'range loop
      partial_parity := partial_parity xor din(N);
    end loop;  -- N

    parity <= partial_parity after 1 ns ;
  end process WORD_PARITY_Process;
end behavior;
