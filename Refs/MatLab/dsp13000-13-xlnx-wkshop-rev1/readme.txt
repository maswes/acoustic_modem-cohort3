Date: March 29, 2011

File Name: dsp13000-13-xlnx-rev1.zip

Description: Course materials for the How to Design a Xilinx Digital Signal Processing System in 1 Day v13.1 workshop

Software: Xilinx ISE� Design Suite: DSP or System Edition 13.1, MATLAB� with Simulink� software R2010a or R2010b

Hardware: Virtex�-6 FPGA ML605 demo board

Platform: All

Installation/Use: Follow along with the presentation slides


Who to Contact if you have problems?

Registrar
Cyndy Limon
cyndy.limon@xilinx.com
(408-626-4251)

Web Support
http://www.xilinx.com/support

North American Support
(Mon,Tues,Wed,Fri 6:30am-5pm  
 Thr 6:30am-4:00pm Pacific Standard Time)
Technical Support Center: (408) 879-5199 
Fax: (408) 879-4442 
 
United Kingdom Support
(Mon-Fri 8:00am-7:30pm GMT)
Technical Support Center: +44 870 7350 610
Email : eurosupport@xilinx.com 
 
France Support
(Mon-Fri 8:00am-5:30pm GMT)
Technical Support Center: +33 1 34 63 0100 
Email : eurosupport@xilinx.com 
 
Germany Support
(Mon-Fri 8:00am-5:30pm GMT)
Technical Support Center: +49 180 3 60 60 60 
Email : eurosupport@xilinx.com 
 
Sweden Support
(Mon-Fri 8:00am-5:30pm GMT)
Technical Support Center: +46 8 33 14 00 
Email : eurosupport@xilinx.com 
 
Japan Support
(Mon-Fri 9:00am-5:30pm)
Technical Support Center: (81)3-6744-7800
Fax: (81)3-5436-0535
Email: jhotline@xilinx.com
