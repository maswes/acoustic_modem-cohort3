function Y = wcdma_interference
%% WCDMA interference signal based on 3GPP TS 25.104 Annex C

% DPDCH
code = 4;
level_dB = 0;

level = 10.^(level_dB/20);   % Translate level value from dB scale                
NumSym = 38400/16;           % Number of DPCH Symbols in each channel
                             % for 1 frame worth of data (38400 chips)
SF = hadamard(16);           % Spreading Factor
Sym = complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data

Xi_dpdch = zeros([1, 38400]);
Xq_dpdch = zeros([1, 38400]);

for m = 1: NumSym
  Xi_dpdch(1, (1:16)+16*(m-1)) = real(Sym(1, m))*SF(code+1, :)*level;
  Xq_dpdch(1, (1:16)+16*(m-1)) = imag(Sym(1, m))*SF(code+1, :)*level;
end    

% DPCCH
code = 0;
level_dB = -5.46;

 
level = 10.^(level_dB/20);   % Translate level value from dB scale                
NumSym = 38400/256;          % Number of DPCH Symbols in each channel
                             % for 1 frame worth of data (38400 chips)          
SF = hadamard(256);          % Spreading Factor
Sym = complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data

Xi_dpcch = zeros([1, 38400]);
Xq_dpcch = zeros([1, 38400]);

for m = 1: NumSym
  Xi_dpcch(1, (1:256)+256*(m-1)) = real(Sym(1, m))*SF(code+1, :)*level;
  Xq_dpcch(1, (1:256)+256*(m-1)) = imag(Sym(1, m))*SF(code+1, :)*level;
end    

Xi = Xi_dpdch + Xi_dpcch;
Xq = Xq_dpdch + Xq_dpcch;
 
% Sum up all the DPCHs
Y = Xi + sqrt(-1)*Xq;
Y = Y/std(Y);

 








