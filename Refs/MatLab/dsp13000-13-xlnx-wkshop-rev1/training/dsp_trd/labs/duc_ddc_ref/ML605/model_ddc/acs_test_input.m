function [yout, y_ref, h_duc] = acs_test_input(Fc)

Quantize = 1;                 % double (0) or quantized (1) coefficients and data 
Nfft = 1024;                  % FFT size for spectral plot
ShowPlots = 0;                % Show filter impluse response plots

% Generate input based on Test Model 1
u0 = wcdma_input_tm1; 
u1 = wcdma_input_tm1;
u2 = wcdma_input_tm1;
u3 = wcdma_input_tm1;
u4 = wcdma_input_tm1;

% Quantize input to 16 bits and keep the range between +/-0.5
u0_q = xquantize(u0, 16)/2^15;
u1_q = xquantize(u1, 16)/2^15;
u2_q = xquantize(u2, 16)/2^15;
u3_q = xquantize(u3, 16)/2^15;
u4_q = xquantize(u4, 16)/2^15;

% Carrier Frequency
Fc2 = Fc;
Fc1 = Fc2 - 5; 
Fc3 = Fc2 + 5; 
Fc0 = Fc2 - 10; 
Fc4 = Fc2 + 10; 

% Sampling Frequency
Fs = 61.44;

% Normalize and quantize the carrier frequency to 24 bits
w0 = 2*pi*round(Fc0/Fs*2^24)/2^24;
w1 = 2*pi*round(Fc1/Fs*2^24)/2^24;
w2 = 2*pi*round(Fc2/Fs*2^24)/2^24;
w3 = 2*pi*round(Fc3/Fs*2^24)/2^24;
w4 = 2*pi*round(Fc4/Fs*2^24)/2^24;

% Filter Coefficients and Interpolation filtering
[h1_duc, h2_duc, h3_duc, h4_duc, h_duc] = umts_duc_filter_conf1(Quantize, Nfft, ShowPlots);
y10 = upfirdn(u0_q, h1_duc, 2, 1);   
y20 = upfirdn(y10, h2_duc, 2, 1);
y30 = upfirdn(y20, h3_duc, 2, 1);
y0 = upfirdn(y30, h4_duc, 2, 1);
y0 = y0/std(y0) * 10.^(-40/20);

y11 = upfirdn(u1_q, h1_duc, 2, 1);   
y21 = upfirdn(y11, h2_duc, 2, 1);
y31 = upfirdn(y21, h3_duc, 2, 1);
y1 = upfirdn(y31, h4_duc, 2, 1);
y1 = y1/std(y1) * 10.^(-52/20);

y12 = upfirdn(u2_q, h1_duc, 2, 1);   
y22 = upfirdn(y12, h2_duc, 2, 1);
y32 = upfirdn(y22, h3_duc, 2, 1);
y2 = upfirdn(y32, h4_duc, 2, 1);
y2 = y2/std(y2) * 10.^(-115/20);

y13 = upfirdn(u3_q, h1_duc, 2, 1);   
y23 = upfirdn(y13, h2_duc, 2, 1);
y33 = upfirdn(y23, h3_duc, 2, 1);
y3 = upfirdn(y33, h4_duc, 2, 1);
y3 = y3/std(y3) * 10.^(-52/20);

y14 = upfirdn(u4_q, h1_duc, 2, 1);   
y24 = upfirdn(y14, h2_duc, 2, 1);
y34 = upfirdn(y24, h3_duc, 2, 1);
y4 = upfirdn(y34, h4_duc, 2, 1);
y4 = y4/std(y4) * 10.^(-40/20);

% Complex Mixing
yout = y0 .* exp(j*w0*[1:length(y0)]') + y1 .* exp(j*w1*[1:length(y1)]') + y2 .* exp(j*w2*[1:length(y2)]')...
     + y3 .* exp(j*w3*[1:length(y3)]') + y4 .* exp(j*w4*[1:length(y4)]');

% Create reference signal
y_ref = real(y2 .* exp(j*w2*[1:length(y2)]')) .* exp(-j*w2*[1:length(y2)]');

% Plot power spectral density of the DDC
figure;
[Pxx, f] = psd(yout, Nfft, 1, Nfft, Nfft/2);
Pxx = Pxx./max(Pxx);
plot((f-1/2)*Fs, 10*log10(fftshift(Pxx)),'b');
hold on; grid on; zoom on;
axis([-Fs/2, Fs/2, -100, 0]);
xlabel('Frequency (MHz)');
ylabel('Watts/Hz (dB)');
title('Power Spectral Density for WCDMA DDC Input');

% pick the real part as the input to DDC
yout = real(yout);