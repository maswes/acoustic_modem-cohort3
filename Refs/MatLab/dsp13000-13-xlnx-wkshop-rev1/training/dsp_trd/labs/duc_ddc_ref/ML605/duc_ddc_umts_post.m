%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This funcion verifies the DUC I/F output from System Generator implementation     %
% comparing it to a statistically equivalent MATLAB simulation model (not bit-true) %  
% Power spectral density is plotted and performance metrics generated.              %
%                                                                                   %
% The I/F output data of the SysGen DUC is fed into the input of the DDC            % 
% Similarly, the baseband output of the DDC from SysGen implementation is           %
% compared to a MATLAB simulation of the DDC.                                       % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SG versus MATLAB DUC comparisons

if (exist('vif'))
    % add the path that includes simulation scripts and sub-functions
    addpath ./model_duc
    
    % Retrieve I/F output of DUC as input to MATLAB DDC model
    
    SG_duc_IQ_if = dif_I(find(vif == 1))' + sqrt(-1)*dif_Q(find(vif == 1))';
    
    % Total number of samples (remove the last sample)
    NUM_SAMPS = length(SG_duc_IQ_if) - 1;
    
    % cross correlate implementation baseband output with desired reference from
    % simulation model and find the matching starting point
    c = xcorr(real(SG_duc_IQ_if(1:NUM_SAMPS)), real(MATLAB_duc_IQ_if(1:NUM_SAMPS)));
    del = find(c == max(c));
    tl = del - NUM_SAMPS;
    
    % Plot the real I/F output samples from MATLAB simulation and SysGen implementation and compare the difference
    
    scrsz = get(0,'ScreenSize');
    fig_x = 0.1*scrsz(3);
    fig_y = scrsz(4)/2 + 70;
    fig_width = scrsz(3)/3;
    fig_height = scrsz(4)/3;
    duc_IQ_if_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    
    subplot(2,1,1);
    plot(real(SG_duc_IQ_if(tl+1:end)),'r*');
    hold on; grid on;
    plot(real(MATLAB_duc_IQ_if(1:NUM_SAMPS)), 'bo');
    plot(real(MATLAB_duc_IQ_if(1:NUM_SAMPS-tl)) - real(SG_duc_IQ_if(tl+1:NUM_SAMPS)), 'c');
    title('System Generator versus MATLAB REAL I/F output of DUC');
    legend('System Generator', 'MATLAB Reference', 'Error');
    
    subplot(2,1,2);
    plot(imag(SG_duc_IQ_if(tl+1:end)),'r*');
    hold on; grid on;
    plot(imag(MATLAB_duc_IQ_if(1:NUM_SAMPS)), 'bo');
    plot(imag(MATLAB_duc_IQ_if(1:NUM_SAMPS-tl)) - imag(SG_duc_IQ_if(tl+1:NUM_SAMPS)),'c');
    title('System Generator versus MATLAB IMAG I/F output of DUC');
    legend('System Generator', 'MATLAB Reference', 'Error');
    
%     % Display the maximum difference between SysGen and MATLAB simulation model for both I and Q channels of I/F outputs of the DUC
%     fprintf('*********************************************************************************************************\n');
%     fprintf('Maximum absolute-value difference between MATLAB model and SysGen at DUC I/F output (I channel): %1.7f \n', max(abs(real(MATLAB_duc_IQ_if(1:NUM_SAMPS-tl)) - real(SG_duc_IQ_if(tl+1:NUM_SAMPS)))));
%     fprintf('Maximum absolute-value difference between MATLAB model and SysGen at DUC I/F output (Q channel): %1.7f \n', max(abs(imag(MATLAB_duc_IQ_if(1:NUM_SAMPS-tl)) - imag(SG_duc_IQ_if(tl+1:NUM_SAMPS)))));
%     fprintf('*********************************************************************************************************\n');
    
    % Performance metrics calculation
    % The figures are meaningful only when enough samples are generated
    vec_length = floor(length(SG_duc_IQ_if)/16 - 100); % removing the tail
    [PAPR, ACLR1, ACLR2, EVM, PCDE] = calculate_metrics(h_duc, u0_q(1:vec_length), SG_duc_IQ_if(tl+1:NUM_SAMPS), Fc, Fs);
    
    % % Print performance metrics in a figure
    fig_x = 0.1*scrsz(3);
    fig_y = scrsz(4)/7 - 90; % place this figure right beneath the PSD figure
    fig_width = scrsz(3)/3;
    fig_height = 100;
    duc_perf_metrics_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    textline = ['DUC Output, Peak to Average Power Ratio : ' num2str(PAPR) ' dB'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 25) fig_width 25]);
    textline = ['DUC Output, ACLR1 (offset by  5 MHz): ' num2str(ACLR1) ' dB'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 25 - 25) fig_width 25]);
    textline = ['DUC Output, ACLR2 (offset by  10 MHz): ' num2str(ACLR2) ' dB'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 25 - 25 - 25) fig_width 25]);
    textline = ['DUC Output, Error Vector Magnitude (EVM): ' num2str(EVM) ' %'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 25 - 25 - 25 - 25) fig_width 25]);
    
    % Plot power spectral density of the DUC
    % The figures are meaningful only when enough samples are generated
    
    fig_x = 0.1*scrsz(3);
    fig_y = scrsz(4)/7 + 20;
    fig_width = scrsz(3)/3;
    fig_height = scrsz(4)/3;
    duc_psd_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    [Pxx, f] = psd(MATLAB_duc_IQ_if, Nfft, 1, Nfft, Nfft/2);  % PSD from simulation model
    Pxx = Pxx./max(Pxx);
    plot((f-1/2)*Fs, 10*log10(fftshift(Pxx)),'b');
    hold on; grid on; zoom on;
    [Pxx, f] = psd(SG_duc_IQ_if(tl+1:NUM_SAMPS), Nfft, 1, Nfft, Nfft/2);  % PSD from implementation model
    Pxx = Pxx./max(Pxx);
    plot((f-1/2)*Fs, 10*log10(fftshift(Pxx)),'r');
    hold on; grid on; zoom on;
    plot_psd_umts_duc_mask(Fs, Nfft, Fc)  % spectral emission mask
    axis([-Fs/2, Fs/2, -120, 0]);
    xlabel('Frequency (MHz)');
    ylabel('Watts/Hz (dB)');
    title('Power Spectral Density for Single Carrier UMTS at DUC output');
    legend('MATLAB simulation', 'System Generator implementation', 'mask', 'Location', 'Best');
    
    %-----------------------------------------------------------------------
    % SG versus MATLAB DDC comparisons
    
    % Compare SG versus MATLAB output of DDC
    % Total number of samples (remove the last sample)
    SG_ddc_I_bb_out = dout_I(vout == 1);
    SG_ddc_Q_bb_out = dout_Q(vout == 1);
    NUM_SAMPS = min(length(SG_ddc_I_bb_out), length(SG_ddc_Q_bb_out));
    
    % cross correlate SG implementation output with desired reference from
    % MATLAB simulation model and find the matching starting point
    c = xcorr(SG_ddc_I_bb_out(1:NUM_SAMPS), real(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS)));
    del = find(c == max(c));
    tl = del - NUM_SAMPS;
    
    fig_x = 0.6*scrsz(3);
    fig_y = scrsz(4)/2 + 70;
    fig_width = scrsz(3)/3;
    fig_height = scrsz(4)/3;
    ddc_I_bb_out_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    
    subplot(2,1,1);
    plot(SG_ddc_I_bb_out(tl+1:end), 'r*');
    hold on; grid on;
    plot(real(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS)), 'bo');
    hold on;
    plot(SG_ddc_I_bb_out(tl+1:NUM_SAMPS) - real(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS-tl))', 'c');
    title('System Generator versus MATLAB REAL output of DDC');
    legend('System Generator', 'MATLAB Reference', 'Error');
    
    subplot(2,1,2);
    plot(SG_ddc_Q_bb_out(tl+1:end), 'r*');
    hold on; grid on;
    plot(imag(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS)), 'bo');
    hold on;
    plot(SG_ddc_Q_bb_out(tl+1:NUM_SAMPS) - imag(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS-tl))', 'c');
    title('System Generator versus MATLAB IMAG output of DDC');
    legend('System Generator', 'MATLAB Reference', 'Error');
    
    %-----------------------------------------------------------------------
    SG_IQ_out_DDC = SG_ddc_I_bb_out' +  sqrt(-1)*SG_ddc_Q_bb_out';
    
    % Calculate DDC performance metrics
    
    err = SG_IQ_out_DDC(tl+1:NUM_SAMPS) - MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS-tl);
    EVM = 100*std(err)/std(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS));
    
    % SNR Calculation
    SNR = -20*log10(EVM/100);
    
    % % Print performance metrics in a figure
    fig_x = 0.6*scrsz(3);
    fig_y = scrsz(4)/7 - 40; % place this figure right beneath the PSD figure
    fig_width = scrsz(3)/3;
    fig_height = 60;
    ddc_perf_metrics_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    textline = ['DDC Output, Error Vector Magnitude (EVM): ' num2str(EVM) '%'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 30) fig_width 30]);
    textline = ['DDC Output, Signal-to-Noise Ratio  (SNR): ' num2str(SNR) ' dB'];
    uicontrol('Style', 'edit', 'String', textline,'FontSize',12, 'Position', [0 (fig_height - 30 - 30) fig_width 30]);
    
    % Plot power spectral density of the DDC
    % The figures are meaningful only when enough samples are generated
    fig_x = 0.6*scrsz(3);
    fig_y = scrsz(4)/7 + 20;
    fig_width = scrsz(3)/3;
    fig_height = scrsz(4)/3;
    ddc_psd_fig_h = figure('Position',[fig_x fig_y fig_width fig_height]);
    [Pxx, f] = psd(MATLAB_ddc_IQ_bb_out(1:NUM_SAMPS-tl), Nfft, 1, Nfft, Nfft/2);  % PSD from simulation model
    Pxx = Pxx./max(Pxx);
    Fs_DDC_bb = Fs/8;
    plot((f-1/2)*Fs_DDC_bb, 10*log10(fftshift(Pxx)),'b');
    hold on; grid on; zoom on;
    [Pxx, f] = psd(SG_IQ_out_DDC(1:NUM_SAMPS), Nfft, 1, Nfft, Nfft/2); % PSD from implementation model
    Pxx = Pxx./max(Pxx);
    plot((f-1/2)*Fs_DDC_bb, 10*log10(fftshift(Pxx)),'r');
    hold on; grid on; zoom on;
    axis([-Fs_DDC_bb/2, Fs_DDC_bb/2, -140, 0]);
    xlabel('Frequency (MHz)');
    ylabel('Watts/Hz (dB)');
    title('Power Spectral Density for Single Carrier UMTS at DDC Output');
    legend('MATLAB Reference', 'System Generator');
    
    % remove the path that includes simulation scripts and sub-functions
    
    rmpath ./model_duc
end