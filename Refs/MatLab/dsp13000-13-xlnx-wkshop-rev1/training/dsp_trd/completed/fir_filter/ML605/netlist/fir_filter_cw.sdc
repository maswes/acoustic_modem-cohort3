
define_attribute {clk} syn_maxfan {1000000}
define_attribute {n:default_clock_driver.xlclockdriver_32.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_32.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_4.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_4.ce_vec*} max_fanout {"REDUCE"}

define_scope_collection ce_32_7a55e6af_group \
  {find -seq * -in [ expand -hier -from {n:ce_32_sg_x1} ]}
define_scope_collection ce_4_7a55e6af_group \
  {find -seq * -in [ expand -hier -from {n:ce_4_sg_x1} ]}

define_multicycle_path -from {$ce_32_7a55e6af_group} \
  -to {$ce_32_7a55e6af_group} 32
define_multicycle_path -from {$ce_4_7a55e6af_group} \
  -to {$ce_4_7a55e6af_group} 4

# Group-to-group constraints
define_multicycle_path -from {$ce_32_7a55e6af_group} \
  -to {$ce_4_7a55e6af_group} 4
define_multicycle_path -from {$ce_4_7a55e6af_group} \
  -to {$ce_32_7a55e6af_group} 4
