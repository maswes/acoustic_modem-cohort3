#
# Created by System Generator     Wed Mar 16 16:53:25 2011
#
# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator.
#

source SgIseProject.tcl

namespace eval ::xilinx::dsptool::iseproject::param {

    set Project {fir_filter_cw}
    set Family {Virtex6}
    set Device {xc6vlx240t}
    set Package {ff1156}
    set Speed {-1}
    set HDLLanguage {vhdl}
    set SynthesisTool {XST}
    set Simulator {Modelsim-SE}
    set ReadCores {False}
    set MapEffortLevel {High}
    set ParEffortLevel {High}
    set Frequency {100}
    set ProjectFiles {
        {{fir_filter_cw.vhd} -view All}
        {{fir_filter.vhd} -view All}
        {{fir_filter_cw.ucf}}
        {{fir_compiler_v6_2.mif}}
        {{fr_cmplr_v6_2_67563495bfaf2b0e.mif}}
        {{C:\training\dsp_trd\labs\fir_filter\ML605\fir_filter.mdl}}
    }
    set TopLevelModule {fir_filter_cw}
    set SynthesisConstraintsFile {fir_filter_cw.xcf}
    set ImplementationStopView {Structural}
    set ProjectGenerator {SysgenDSP}
}
::xilinx::dsptool::iseproject::create
