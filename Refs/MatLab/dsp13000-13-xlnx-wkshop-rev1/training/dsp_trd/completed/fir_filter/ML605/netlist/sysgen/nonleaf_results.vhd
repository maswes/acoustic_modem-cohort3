library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "fir_filter/data_conversion"

entity data_conversion_entity_b64a195e6b is
  port (
    ce_32: in std_logic; 
    ce_4: in std_logic; 
    clk_32: in std_logic; 
    clk_4: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end data_conversion_entity_b64a195e6b;

architecture structural of data_conversion_entity_b64a195e6b is
  signal addsub_s_net: std_logic_vector(8 downto 0);
  signal ce_32_sg_x0: std_logic;
  signal ce_4_sg_x0: std_logic;
  signal clk_32_sg_x0: std_logic;
  signal clk_4_sg_x0: std_logic;
  signal constant2_op_net: std_logic_vector(8 downto 0);
  signal convert1_dout_net: std_logic_vector(8 downto 0);
  signal convert_dout_net_x0: std_logic_vector(7 downto 0);
  signal data_in_net_x0: std_logic_vector(7 downto 0);
  signal data_serializer_dout_net: std_logic;
  signal threshold_y_net: std_logic_vector(1 downto 0);

begin
  ce_32_sg_x0 <= ce_32;
  ce_4_sg_x0 <= ce_4;
  clk_32_sg_x0 <= clk_32;
  clk_4_sg_x0 <= clk_4;
  data_in_net_x0 <= in1;
  out1 <= convert_dout_net_x0;

  addsub: entity work.xladdsub
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 7,
      a_width => 9,
      b_arith => xlSigned,
      b_bin_pt => 7,
      b_width => 9,
      c_has_c_out => 0,
      c_latency => 0,
      c_output_width => 10,
      core_name0 => "addsb_11_0_162a3a6add2f1640",
      extra_registers => 0,
      full_s_arith => 2,
      full_s_width => 10,
      latency => 0,
      overflow => 1,
      quantization => 1,
      s_arith => xlSigned,
      s_bin_pt => 7,
      s_width => 9
    )
    port map (
      a => convert1_dout_net,
      b => constant2_op_net,
      ce => ce_4_sg_x0,
      clk => clk_4_sg_x0,
      clr => '0',
      en => "1",
      s => addsub_s_net
    );

  constant2: entity work.constant_585d1fbee5
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant2_op_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 0,
      din_width => 2,
      dout_arith => 2,
      dout_bin_pt => 6,
      dout_width => 8,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_4_sg_x0,
      clk => clk_4_sg_x0,
      clr => '0',
      din => threshold_y_net,
      en => "1",
      dout => convert_dout_net_x0
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 2,
      dout_bin_pt => 7,
      dout_width => 9,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_4_sg_x0,
      clk => clk_4_sg_x0,
      clr => '0',
      din(0) => data_serializer_dout_net,
      en => "1",
      dout => convert1_dout_net
    );

  data_serializer: entity work.xlp2s
    generic map (
      din_arith => xlUnsigned,
      din_bin_pt => 0,
      din_width => 8,
      dout_arith => xlUnsigned,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      lsb_first => 1,
      num_outputs => 8
    )
    port map (
      dest_ce => ce_4_sg_x0,
      dest_clk => clk_4_sg_x0,
      dest_clr => '0',
      din => data_in_net_x0,
      en => "1",
      rst => "0",
      src_ce => ce_32_sg_x0,
      src_clk => clk_32_sg_x0,
      src_clr => '0',
      dout(0) => data_serializer_dout_net
    );

  threshold: entity work.sgn_e65aefeb04
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      x => addsub_s_net,
      y => threshold_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "fir_filter"

entity fir_filter is
  port (
    ce_1: in std_logic; 
    ce_32: in std_logic; 
    ce_4: in std_logic; 
    ce_logic_1: in std_logic; 
    clk_1: in std_logic; 
    clk_32: in std_logic; 
    clk_4: in std_logic; 
    data_in: in std_logic_vector(7 downto 0); 
    filt_data: out std_logic_vector(24 downto 0)
  );
end fir_filter;

architecture structural of fir_filter is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "fir_filter,sysgen_core,{clock_period=10.00000000,clocking=Clock_Enables,compilation=HDL_Netlist,sample_periods=1.00000000000 4.00000000000 32.00000000000,testbench=0,total_blocks=55,xilinx_adder_subtracter_block=1,xilinx_constant_block_block=1,xilinx_fdatool_interface_block=1,xilinx_fir_compiler_6_2_block=1,xilinx_gateway_in_block=1,xilinx_gateway_out_block=2,xilinx_parallel_to_serial_converter_block=1,xilinx_system_generator_block=1,xilinx_threshold_block=1,xilinx_type_converter_block=2,xilinx_up_sampler_block=1,}";

  signal ce_1_sg_x0: std_logic;
  signal ce_32_sg_x1: std_logic;
  signal ce_4_sg_x1: std_logic;
  signal ce_logic_1_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal clk_32_sg_x1: std_logic;
  signal clk_4_sg_x1: std_logic;
  signal convert_dout_net_x0: std_logic_vector(7 downto 0);
  signal data_in_net: std_logic_vector(7 downto 0);
  signal filt_data_net: std_logic_vector(24 downto 0);
  signal up_sample_q_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x0 <= ce_1;
  ce_32_sg_x1 <= ce_32;
  ce_4_sg_x1 <= ce_4;
  ce_logic_1_sg_x0 <= ce_logic_1;
  clk_1_sg_x0 <= clk_1;
  clk_32_sg_x1 <= clk_32;
  clk_4_sg_x1 <= clk_4;
  data_in_net <= data_in;
  filt_data <= filt_data_net;

  data_conversion_b64a195e6b: entity work.data_conversion_entity_b64a195e6b
    port map (
      ce_32 => ce_32_sg_x1,
      ce_4 => ce_4_sg_x1,
      clk_32 => clk_32_sg_x1,
      clk_4 => clk_4_sg_x1,
      in1 => data_in_net,
      out1 => convert_dout_net_x0
    );

  fir_compiler_6_2: entity work.xlfir_compiler_fd95bf7f08d01e6df1a8275a82732275
    port map (
      ce => ce_1_sg_x0,
      ce_logic_1 => ce_logic_1_sg_x0,
      clk => clk_1_sg_x0,
      clk_logic_1 => clk_1_sg_x0,
      s_axis_data_tdata => up_sample_q_net,
      src_ce => ce_1_sg_x0,
      src_clk => clk_1_sg_x0,
      m_axis_data_tdata => filt_data_net
    );

  up_sample: entity work.xlusamp
    generic map (
      copy_samples => 0,
      d_arith => xlSigned,
      d_bin_pt => 6,
      d_width => 8,
      latency => 0,
      q_arith => xlSigned,
      q_bin_pt => 6,
      q_width => 8
    )
    port map (
      d => convert_dout_net_x0,
      dest_ce => ce_1_sg_x0,
      dest_clk => clk_1_sg_x0,
      dest_clr => '0',
      en => "1",
      src_ce => ce_4_sg_x1,
      src_clk => clk_4_sg_x1,
      src_clr => '0',
      q => up_sample_q_net
    );

end structural;
