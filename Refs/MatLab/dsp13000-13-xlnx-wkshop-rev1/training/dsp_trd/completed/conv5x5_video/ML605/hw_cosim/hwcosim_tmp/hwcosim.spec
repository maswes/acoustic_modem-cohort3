{
  'ClockGenerator' => {
    'MMCM' => {
      'BandwidthMode' => 'OPTIMIZED',
      'ClkDivisor' => 1,
      'ClkMultiplier' => 5.00000000000,
      'ClkOut0' => {
        'Divisor' => 5,
        'Used' => true
      },
      'ClkOut1' => {
        'Divisor' => 8,
        'Used' => true
      },
      'ClkOut2' => {
        'Divisor' => 20,
        'Used' => true
      },
      'ClkOut3' => {
        'Divisor' => 1,
        'Used' => false
      },
      'ClkOut4' => {
        'Divisor' => 1,
        'Used' => false
      },
      'ClkOut5' => {
        'Divisor' => 1,
        'Used' => false
      },
      'ClkOut6' => {
        'Divisor' => 1,
        'Used' => false
      }
    },
    'Type' => 'MMCM'
  },
  'CosimCore' => {
    'Constraints' => [
      'INST "ethernet_phy_rxd<?>"    TNM = "gmii_rx";',
      'INST "ethernet_phy_rx_dv"     TNM = "gmii_rx";',
      'INST "ethernet_phy_rx_er"     TNM = "gmii_rx";',
      'NET  "*emac_speed_is_10_100"  TIG;',
      'TIMEGRP "gmii_rx" OFFSET = IN 2 ns VALID 2.75 ns BEFORE "ethernet_phy_rx_clk" RISING;'
    ],
    'EntityName' => 'eth_cosim_core',
    'Interface' => {
      'Clock' => [
        {
          'Clock' => {
            'Period' => 8
          },
          'Direction' => 'in',
          'MMCMPort' => 'clkout1',
          'Name' => 'clk_125',
          'Width' => 1
        },
        {
          'Clock' => {
            'Period' => 5
          },
          'Direction' => 'in',
          'MMCMPort' => 'clkout0',
          'Name' => 'clk_200',
          'Width' => 1
        },
        {
          'Clock' => {
            'Period' => 20.00000000000
          },
          'Constraints' => [
            'NET "hwcosim_sys_clk_p" LOC = J9;',
            'NET "hwcosim_sys_clk_n" LOC = H9;'
          ],
          'Direction' => 'in',
          'IOType' => 'ibufg',
          'MMCMPort' => 'clkout2',
          'Name' => 'sys_clk',
          'Width' => 1
        }
      ],
      'Cosim' => [
        {
          'Direction' => 'out',
          'Name' => 'cosim_addr',
          'Width' => 24
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_bank_sel',
          'Width' => 8
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_clk_sel',
          'Width' => 1
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_data_in',
          'Width' => 32
        },
        {
          'Direction' => 'in',
          'Name' => 'cosim_data_out',
          'Width' => 32
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_sstep_clk',
          'Width' => 1
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_we',
          'Width' => 1
        },
        {
          'Direction' => 'out',
          'Name' => 'cosim_re',
          'Width' => 1
        }
      ],
      'Ethernet' => [
        {
          'Constraints' => [
            'NET  "ethernet_phy_rst_n"     LOC = AH13;',
            'NET  "ethernet_phy_rst_n"     TIG;'
          ],
          'Direction' => 'out',
          'ExternalName' => 'ethernet_phy_rst_n',
          'Invert' => true,
          'Name' => 'ethernet_phy_rst',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_intr_n"    LOC = AH14;',
            'NET  "ethernet_phy_intr_n"    PULLUP;',
            'NET  "ethernet_phy_intr_n"    TIG;'
          ],
          'Direction' => 'in',
          'ExternalName' => 'ethernet_phy_intr_n',
          'Invert' => true,
          'Name' => 'ethernet_phy_intr',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_gtx_clk"   LOC = AH12;',
            'NET  "ethernet_phy_gtx_clk"   SLEW = FAST;'
          ],
          'Direction' => 'out',
          'Name' => 'ethernet_phy_gtx_clk',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_tx_clk"    LOC = AD12;',
            'NET  "ethernet_phy_tx_clk"    CLOCK_DEDICATED_ROUTE = FALSE;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_tx_clk',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_txd(0)"    LOC = AM11;',
            'NET  "ethernet_phy_txd(0)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(1)"    LOC = AL11;',
            'NET  "ethernet_phy_txd(1)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(2)"    LOC = AG10;',
            'NET  "ethernet_phy_txd(2)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(3)"    LOC = AG11;',
            'NET  "ethernet_phy_txd(3)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(4)"    LOC = AL10;',
            'NET  "ethernet_phy_txd(4)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(5)"    LOC = AM10;',
            'NET  "ethernet_phy_txd(5)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(6)"    LOC = AE11;',
            'NET  "ethernet_phy_txd(6)"    SLEW = FAST;',
            'NET  "ethernet_phy_txd(7)"    LOC = AF11;',
            'NET  "ethernet_phy_txd(7)"    SLEW = FAST;'
          ],
          'Direction' => 'out',
          'Name' => 'ethernet_phy_txd',
          'Width' => 8
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_tx_en"     LOC = AJ10;',
            'NET  "ethernet_phy_tx_en"     SLEW = FAST;'
          ],
          'Direction' => 'out',
          'Name' => 'ethernet_phy_tx_en',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_tx_er"     LOC = AH10;',
            'NET  "ethernet_phy_tx_er"     SLEW = FAST;'
          ],
          'Direction' => 'out',
          'Name' => 'ethernet_phy_tx_er',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_rx_clk"    LOC = AP11;'
          ],
          'Direction' => 'in',
          'IOType' => 'ibufg',
          'Name' => 'ethernet_phy_rx_clk',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_rxd(0)"    LOC = AN13;',
            'NET  "ethernet_phy_rxd(1)"    LOC = AF14;',
            'NET  "ethernet_phy_rxd(2)"    LOC = AE14;',
            'NET  "ethernet_phy_rxd(3)"    LOC = AN12;',
            'NET  "ethernet_phy_rxd(4)"    LOC = AM12;',
            'NET  "ethernet_phy_rxd(5)"    LOC = AD11;',
            'NET  "ethernet_phy_rxd(6)"    LOC = AC12;',
            'NET  "ethernet_phy_rxd(7)"    LOC = AC13;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_rxd',
          'Width' => 8
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_rx_dv"     LOC = AM13;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_rx_dv',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_rx_er"     LOC = AG12;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_rx_er',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_col"       LOC = AK13;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_col',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_crs"       LOC = AL13;'
          ],
          'Direction' => 'in',
          'Name' => 'ethernet_phy_crs',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_mdc"       LOC = AP14;'
          ],
          'Direction' => 'out',
          'Name' => 'ethernet_phy_mdc',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "ethernet_phy_mdio"      LOC = AN14;'
          ],
          'Direction' => 'inout',
          'Name' => 'ethernet_phy_mdio',
          'Width' => 1
        }
      ],
      'SystemACE' => [
        {
          'Constraints' => [
            'NET  "sysace_clk"             TNM_NET = "T_sysace_clk";',
            'TIMESPEC "TS_sysace_clk"      = PERIOD "T_sysace_clk" 30 ns;',
            'NET  "sysace_clk"             LOC = AE16;'
          ],
          'Direction' => 'in',
          'Name' => 'sysace_clk',
          'Type' => 'bufgp',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "sysace_mpa(0)"          LOC = AC15;',
            'NET  "sysace_mpa(1)"          LOC = AP15;',
            'NET  "sysace_mpa(2)"          LOC = AG17;',
            'NET  "sysace_mpa(3)"          LOC = AH17;',
            'NET  "sysace_mpa(4)"          LOC = AG15;',
            'NET  "sysace_mpa(5)"          LOC = AK14;',
            'NET  "sysace_mpa(6)"          LOC = AJ15;'
          ],
          'Direction' => 'out',
          'Name' => 'sysace_mpa',
          'Width' => 7
        },
        {
          'Constraints' => [
            'NET  "sysace_mpd(0)"          LOC = AM15;',
            'NET  "sysace_mpd(1)"          LOC = AJ17;',
            'NET  "sysace_mpd(2)"          LOC = AJ16;',
            'NET  "sysace_mpd(3)"          LOC = AP16;',
            'NET  "sysace_mpd(4)"          LOC = AG16;',
            'NET  "sysace_mpd(5)"          LOC = AH15;',
            'NET  "sysace_mpd(6)"          LOC = AF16;',
            'NET  "sysace_mpd(7)"          LOC = AN15;'
          ],
          'Direction' => 'inout',
          'Name' => 'sysace_mpd',
          'Width' => 8
        },
        {
          'Constraints' => [
            'NET  "sysace_cen"             LOC = AJ14;'
          ],
          'Direction' => 'out',
          'Name' => 'sysace_cen',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "sysace_oen"             LOC = AL15;'
          ],
          'Direction' => 'out',
          'Name' => 'sysace_oen',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "sysace_wen"             LOC = AL14;'
          ],
          'Direction' => 'out',
          'Name' => 'sysace_wen',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "sysace_mpirq"           LOC = L9;',
            'NET  "sysace_mpirq"           TIG;'
          ],
          'Direction' => 'in',
          'Name' => 'sysace_mpirq',
          'Width' => 1
        },
        {
          'Constraints' => [
            'NET  "reset"                  LOC = H10;',
            'NET  "reset"                  IOSTANDARD = LVCMOS15;',
            'NET  "reset"                  PULLDOWN;',
            'NET  "reset"                  TIG;'
          ],
          'Direction' => 'in',
          'IsSystemReset' => true,
          'Name' => 'reset',
          'Width' => 1
        }
      ]
    },
    'Variant' => 'ml605'
  },
  'Design' => {
    'BasePath' => 'c:/training/dsp_trd/labs/conv5x5_video/ML605/hw_cosim',
    'Constraints' => [

    ],
    'ConstraintsFile' => 'c:/training/dsp_trd/labs/conv5x5_video/ML605/hw_cosim/sysgen_hwcosim.ucf',
    'EntityName' => 'conv5x5_video_ex_cw',
    'HDLFileExtension' => 'vhd',
    'HDLFileList' => [
      {
        'Library' => 'work',
        'Path' => 'c:/training/dsp_trd/labs/conv5x5_video/ML605/hw_cosim/conv5x5_video_ex.vhd',
        'Type' => 'vhdl'
      },
      {
        'Library' => 'work',
        'Path' => 'c:/training/dsp_trd/labs/conv5x5_video/ML605/hw_cosim/conv5x5_video_ex_cw.vhd',
        'Type' => 'vhdl'
      }
    ],
    'HDLLanguage' => 'vhdl',
    'InstanceName' => 'sysgen_dut',
    'PeripheralList' => [

    ],
    'PortList' => [
      {
        'ConnectTo' => 1,
        'DataType' => 'UFix_1_0',
        'Direction' => 'in',
        'DisplayName' => 'ce',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'ce',
        'SamplePeriod' => 1,
        'Type' => 'std_logic',
        'VerilogDirection' => 'input',
        'Width' => 1
      },
      {
        'Clock' => {
          'Period' => 20.00000000000
        },
        'DataType' => 'logic',
        'Direction' => 'in',
        'DisplayName' => 'clk',
        'IsClock' => true,
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'clk',
        'SamplePeriod' => 1,
        'Type' => 'std_logic',
        'VerilogDirection' => 'input',
        'Width' => 1
      },
      {
        'DataType' => 'UFix_8_0',
        'Direction' => 'out',
        'DisplayName' => 'data_out',
        'IsExternal' => false,
        'IsMemoryMapped' => true,
        'Name' => 'data_out',
        'Range' => {
          'Left' => '7',
          'Length' => 8,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 8
      },
      {
        'DataType' => 'Fix_20_17',
        'Direction' => 'in',
        'DisplayName' => 'gain',
        'IsExternal' => false,
        'IsMemoryMapped' => true,
        'Name' => 'gain',
        'Range' => {
          'Left' => '19',
          'Length' => 20,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 20
      },
      {
        'DataType' => 'UFix_10_0',
        'Direction' => 'in',
        'DisplayName' => 'offset',
        'IsExternal' => false,
        'IsMemoryMapped' => true,
        'Name' => 'offset',
        'Range' => {
          'Left' => '9',
          'Length' => 10,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 10
      },
      {
        'DataType' => 'UFix_5_0',
        'Direction' => 'out',
        'DisplayName' => 'addr',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_addr',
        'Range' => {
          'Left' => '4',
          'Length' => 5,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 1,
        'SharedRAMPort' => 'shared_memory_addra',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 5
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_ce',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_ce',
        'SamplePeriod' => 1,
        'SharedRAMPort' => 'shared_memory_ena',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_clk',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_clk',
        'SamplePeriod' => 1,
        'SharedRAMPort' => 'shared_memory_clka',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'Fix_6_0',
        'Direction' => 'out',
        'DisplayName' => 'data_in',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_data_in',
        'Range' => {
          'Left' => '5',
          'Length' => 6,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => -1,
        'SharedRAMPort' => 'shared_memory_dina',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 6
      },
      {
        'DataType' => 'Fix_6_0',
        'Direction' => 'in',
        'DisplayName' => 'data_out',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_data_out',
        'Range' => {
          'Left' => '5',
          'Length' => 6,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 1,
        'SharedRAMPort' => 'shared_memory_douta',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 6
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'we',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_we',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => -1,
        'SharedRAMPort' => 'shared_memory_wea',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'UFix_14_0',
        'Direction' => 'out',
        'DisplayName' => 'addr',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_addr',
        'Range' => {
          'Left' => '13',
          'Length' => 14,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_addra',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 14
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_x0_ce',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_ce',
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_ena',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_x0_clk',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_clk',
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_clka',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'UFix_8_0',
        'Direction' => 'out',
        'DisplayName' => 'data_in',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_data_in',
        'Range' => {
          'Left' => '7',
          'Length' => 8,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => -1,
        'SharedRAMPort' => 'shared_memory_x0_dina',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 8
      },
      {
        'DataType' => 'UFix_8_0',
        'Direction' => 'in',
        'DisplayName' => 'data_out',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_data_out',
        'Range' => {
          'Left' => '7',
          'Length' => 8,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_douta',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 8
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'in',
        'DisplayName' => 'grant',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_grant',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_hw_grant',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 1
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'req',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_req',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x0_hw_req',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'we',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x0_we',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => -1,
        'SharedRAMPort' => 'shared_memory_x0_wea',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'UFix_14_0',
        'Direction' => 'out',
        'DisplayName' => 'addr',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_addr',
        'Range' => {
          'Left' => '13',
          'Length' => 14,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_addra',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 14
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_x1_ce',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_ce',
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_ena',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'logic',
        'Direction' => 'out',
        'DisplayName' => 'shared_memory_x1_clk',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_clk',
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_clka',
        'Type' => 'std_logic',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'UFix_8_0',
        'Direction' => 'out',
        'DisplayName' => 'data_in',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_data_in',
        'Range' => {
          'Left' => '7',
          'Length' => 8,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_dina',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 8
      },
      {
        'DataType' => 'UFix_8_0',
        'Direction' => 'in',
        'DisplayName' => 'data_out',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_data_out',
        'Range' => {
          'Left' => '7',
          'Length' => 8,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_douta',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 8
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'in',
        'DisplayName' => 'grant',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_grant',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_hw_grant',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'input',
        'Width' => 1
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'req',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_req',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_hw_req',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 1
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'we',
        'IsExternal' => false,
        'IsMemoryMapped' => false,
        'Name' => 'shared_memory_x1_we',
        'Range' => {
          'Left' => '0',
          'Length' => 1,
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 5,
        'SharedRAMPort' => 'shared_memory_x1_wea',
        'Type' => 'std_logic_vector',
        'VerilogDirection' => 'output',
        'Width' => 1
      }
    ],
    'ProjectFile' => 'c:/training/dsp_trd/labs/conv5x5_video/ML605/hw_cosim/xst_conv5x5_video_ex.prj',
    'SharedFIFOList' => [

    ],
    'SharedRAMEntityList' => [
      {
        'AddressWidth' => 5,
        'DataWidth' => 6,
        'EntityName' => 'block_memory_generator_virtex6_5_2_3daed311e964671d'
      },
      {
        'AddressWidth' => 14,
        'DataWidth' => 8,
        'EntityName' => 'block_memory_generator_virtex6_5_2_91e4527b00c9951a'
      },
      {
        'AddressWidth' => 14,
        'DataWidth' => 8,
        'EntityName' => 'block_memory_generator_virtex6_5_2_032643e233f3bae0'
      }
    ],
    'SharedRAMList' => [
      {
        'AddressWidth' => 5,
        'BinaryPoint' => 0,
        'DataType' => 'Fix_6_0',
        'DataWidth' => 6,
        'Depth' => 32,
        'EntityName' => 'block_memory_generator_virtex6_5_2_3daed311e964671d',
        'InitialVector' => 'C:/TEMP/shmem_coe_4d810508/mem_670b14728ad9902aecba32e22fa4f6bd.coe',
        'InstanceName' => 'shared_memory',
        'IsLockable' => false,
        'Latency' => 1,
        'MemoryMap' => {
          'Bank' => 1
        },
        'Name' => 'coef_buffer',
        'Type' => 'ReadWrite',
        'WriteMode' => 'WriteFirst'
      },
      {
        'AddressWidth' => 14,
        'BinaryPoint' => 0,
        'DataType' => 'UFix_8_0',
        'DataWidth' => 8,
        'Depth' => 16384,
        'EntityName' => 'block_memory_generator_virtex6_5_2_91e4527b00c9951a',
        'InitialVector' => 'C:/TEMP/shmem_coe_4d810509/mem_cbc143712715404df410a1787dbf290c.coe',
        'InstanceName' => 'shared_memory_x0',
        'IsLockable' => true,
        'Latency' => 1,
        'MemoryMap' => {
          'Bank' => 2,
          'SoftwareGrantAddress' => 2,
          'SoftwareRequestAddress' => 1
        },
        'Name' => 'Foo',
        'Type' => 'ReadWrite',
        'WriteMode' => 'WriteFirst'
      },
      {
        'AddressWidth' => 14,
        'BinaryPoint' => 0,
        'DataType' => 'UFix_8_0',
        'DataWidth' => 8,
        'Depth' => 16384,
        'EntityName' => 'block_memory_generator_virtex6_5_2_032643e233f3bae0',
        'InitialVector' => 'C:/TEMP/shmem_coe_4d81050a/mem_dd4b21e9ef71e1291183a46b913ae6f2.coe',
        'InstanceName' => 'shared_memory_x1',
        'IsLockable' => true,
        'Latency' => 1,
        'MemoryMap' => {
          'Bank' => 3,
          'SoftwareGrantAddress' => 3,
          'SoftwareRequestAddress' => 2
        },
        'Name' => 'Bar',
        'Type' => 'ReadWrite',
        'WriteMode' => 'WriteFirst'
      }
    ],
    'SharedRegisterList' => [

    ],
    'SynplifySynthesisConstraints' => [
      'define_attribute {clk} syn_maxfan {1000000}',
      'define_attribute {n:default_clock_driver.xlclockdriver_5.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_5.ce_vec*} max_fanout {"REDUCE"}',
      'define_scope_collection ce_5_dd930c91_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_5_sg_x22} ]}',
      'define_multicycle_path -from {$ce_5_dd930c91_group} \\',
      '-to {$ce_5_dd930c91_group} 5'
    ],
    'SynthesisTool' => 'xst',
    'SynthesisToolExecutable' => 'xst',
    'UsesSynplify' => false,
    'UsesXST' => true,
    'XSTSynthesisConstraints' => [
      'NET "clk" TNM_NET = "clk_dd930c91";',
      'TIMESPEC "TS_clk_dd930c91" = PERIOD "clk_dd930c91" 20.0 ns HIGH 50 %;',
      'NET "ce_5_sg_x22*" TNM_NET = "ce_5_dd930c91_group";',
      'TIMESPEC "TS_ce_5_dd930c91_group_to_ce_5_dd930c91_group" = FROM "ce_5_dd930c91_group" TO "ce_5_dd930c91_group" 100.0 ns;'
    ]
  },
  'Flow' => 'sysgen',
  'Incremental' => '',
  'MemoryMap' => {
    'InputPortMapping' => [
      {
        'Address' => 0,
        'Port' => {
          'Name' => 'gain',
          'Range' => {
            'Left' => 19,
            'Length' => 20,
            'Order' => 'downto',
            'Right' => 0
          }
        }
      },
      {
        'Address' => 1,
        'Port' => {
          'Name' => 'offset',
          'Range' => {
            'Left' => 9,
            'Length' => 10,
            'Order' => 'downto',
            'Right' => 0
          }
        }
      }
    ],
    'OutputPortMapping' => [
      {
        'Address' => 0,
        'Port' => {
          'Name' => 'data_out',
          'Range' => {
            'Left' => 7,
            'Length' => 8,
            'Order' => 'downto',
            'Right' => 0
          }
        }
      }
    ]
  },
  'Platform' => {
    'BlockRAMSize' => 4096,
    'Board' => 'ml605',
    'BoundaryScanPosition' => 2,
    'Clock' => {
      'Differential' => true,
      'Period' => 5,
      'Pin' => {
        'Negative' => 'H9',
        'Positive' => 'J9'
      },
      'VariablePeriods' => [
        10,
        15,
        20,
        30
      ]
    },
    'CosimCore' => {
      'Constraints' => [
        'INST "ethernet_phy_rxd<?>"    TNM = "gmii_rx";',
        'INST "ethernet_phy_rx_dv"     TNM = "gmii_rx";',
        'INST "ethernet_phy_rx_er"     TNM = "gmii_rx";',
        'NET  "*emac_speed_is_10_100"  TIG;',
        'TIMEGRP "gmii_rx" OFFSET = IN 2 ns VALID 2.75 ns BEFORE "ethernet_phy_rx_clk" RISING;'
      ],
      'Interface' => {
        'Clock' => [
          {
            'Clock' => {
              'Period' => 8
            },
            'Direction' => 'in',
            'Name' => 'clk_125',
            'Width' => 1
          },
          {
            'Clock' => {
              'Period' => 5
            },
            'Direction' => 'in',
            'Name' => 'clk_200',
            'Width' => 1
          }
        ],
        'Ethernet' => [
          {
            'Constraints' => [
              'NET  "ethernet_phy_rst_n"     LOC = AH13;',
              'NET  "ethernet_phy_rst_n"     TIG;'
            ],
            'Direction' => 'out',
            'ExternalName' => 'ethernet_phy_rst_n',
            'Invert' => true,
            'Name' => 'ethernet_phy_rst',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_intr_n"    LOC = AH14;',
              'NET  "ethernet_phy_intr_n"    PULLUP;',
              'NET  "ethernet_phy_intr_n"    TIG;'
            ],
            'Direction' => 'in',
            'ExternalName' => 'ethernet_phy_intr_n',
            'Invert' => true,
            'Name' => 'ethernet_phy_intr',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_gtx_clk"   LOC = AH12;',
              'NET  "ethernet_phy_gtx_clk"   SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_gtx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_clk"    LOC = AD12;',
              'NET  "ethernet_phy_tx_clk"    CLOCK_DEDICATED_ROUTE = FALSE;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_tx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_txd(0)"    LOC = AM11;',
              'NET  "ethernet_phy_txd(0)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(1)"    LOC = AL11;',
              'NET  "ethernet_phy_txd(1)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(2)"    LOC = AG10;',
              'NET  "ethernet_phy_txd(2)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(3)"    LOC = AG11;',
              'NET  "ethernet_phy_txd(3)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(4)"    LOC = AL10;',
              'NET  "ethernet_phy_txd(4)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(5)"    LOC = AM10;',
              'NET  "ethernet_phy_txd(5)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(6)"    LOC = AE11;',
              'NET  "ethernet_phy_txd(6)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(7)"    LOC = AF11;',
              'NET  "ethernet_phy_txd(7)"    SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_txd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_en"     LOC = AJ10;',
              'NET  "ethernet_phy_tx_en"     SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_tx_en',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_er"     LOC = AH10;',
              'NET  "ethernet_phy_tx_er"     SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_tx_er',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_clk"    LOC = AP11;'
            ],
            'Direction' => 'in',
            'IOType' => 'ibufg',
            'Name' => 'ethernet_phy_rx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rxd(0)"    LOC = AN13;',
              'NET  "ethernet_phy_rxd(1)"    LOC = AF14;',
              'NET  "ethernet_phy_rxd(2)"    LOC = AE14;',
              'NET  "ethernet_phy_rxd(3)"    LOC = AN12;',
              'NET  "ethernet_phy_rxd(4)"    LOC = AM12;',
              'NET  "ethernet_phy_rxd(5)"    LOC = AD11;',
              'NET  "ethernet_phy_rxd(6)"    LOC = AC12;',
              'NET  "ethernet_phy_rxd(7)"    LOC = AC13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rxd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_dv"     LOC = AM13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rx_dv',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_er"     LOC = AG12;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rx_er',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_col"       LOC = AK13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_col',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_crs"       LOC = AL13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_crs',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_mdc"       LOC = AP14;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_mdc',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_mdio"      LOC = AN14;'
            ],
            'Direction' => 'inout',
            'Name' => 'ethernet_phy_mdio',
            'Width' => 1
          }
        ],
        'SystemACE' => [
          {
            'Constraints' => [
              'NET  "sysace_clk"             TNM_NET = "T_sysace_clk";',
              'TIMESPEC "TS_sysace_clk"      = PERIOD "T_sysace_clk" 30 ns;',
              'NET  "sysace_clk"             LOC = AE16;'
            ],
            'Direction' => 'in',
            'Name' => 'sysace_clk',
            'Type' => 'bufgp',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_mpa(0)"          LOC = AC15;',
              'NET  "sysace_mpa(1)"          LOC = AP15;',
              'NET  "sysace_mpa(2)"          LOC = AG17;',
              'NET  "sysace_mpa(3)"          LOC = AH17;',
              'NET  "sysace_mpa(4)"          LOC = AG15;',
              'NET  "sysace_mpa(5)"          LOC = AK14;',
              'NET  "sysace_mpa(6)"          LOC = AJ15;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_mpa',
            'Width' => 7
          },
          {
            'Constraints' => [
              'NET  "sysace_mpd(0)"          LOC = AM15;',
              'NET  "sysace_mpd(1)"          LOC = AJ17;',
              'NET  "sysace_mpd(2)"          LOC = AJ16;',
              'NET  "sysace_mpd(3)"          LOC = AP16;',
              'NET  "sysace_mpd(4)"          LOC = AG16;',
              'NET  "sysace_mpd(5)"          LOC = AH15;',
              'NET  "sysace_mpd(6)"          LOC = AF16;',
              'NET  "sysace_mpd(7)"          LOC = AN15;'
            ],
            'Direction' => 'inout',
            'Name' => 'sysace_mpd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "sysace_cen"             LOC = AJ14;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_cen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_oen"             LOC = AL15;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_oen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_wen"             LOC = AL14;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_wen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_mpirq"           LOC = L9;',
              'NET  "sysace_mpirq"           TIG;'
            ],
            'Direction' => 'in',
            'Name' => 'sysace_mpirq',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "reset"                  LOC = H10;',
              'NET  "reset"                  IOSTANDARD = LVCMOS15;',
              'NET  "reset"                  PULLDOWN;',
              'NET  "reset"                  TIG;'
            ],
            'Direction' => 'in',
            'IsSystemReset' => true,
            'Name' => 'reset',
            'Width' => 1
          }
        ]
      },
      'Variant' => 'ml605'
    },
    'Description' => 'ML605 (Point-to-point Ethernet)',
    'Interface' => 'ppethernet',
    'MaximumFrameSize' => 8184,
    'Part' => {
      'BaseFamily' => 'virtex6',
      'Device' => 'xc6vlx240t',
      'Family' => 'virtex6',
      'FamilyForSynplify' => 'virtex6',
      'Package' => 'ff1156',
      'Speed' => '-1'
    },
    'Type' => 'ppethernet',
    'Vendor' => 'Xilinx'
  },
  'SYSGEN' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
  'Target' => {
    'ExcludedModules' => [

    ],
    'Modules' => [
      'pp_ethernet'
    ]
  },
  'TopLevel' => {
    'EntityName' => 'hwcosim_top'
  },
  'Type' => 'hwcosim',
  'Version' => '9.2',
  'XILINX' => 'C:/Xilinx/13.1/ISE_DS/ISE'
}
