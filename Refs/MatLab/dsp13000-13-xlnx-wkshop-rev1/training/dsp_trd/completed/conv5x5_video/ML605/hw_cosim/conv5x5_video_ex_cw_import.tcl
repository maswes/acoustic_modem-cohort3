#
# Created by System Generator     Thu Mar 17 01:22:10 2011
#
# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator.
#

source SgIseProject.tcl

namespace eval ::xilinx::dsptool::iseproject::param {

    set Project {conv5x5_video_ex_cw}
    set Family {Virtex6}
    set Device {xc6vlx240t}
    set Package {ff1156}
    set Speed {-1}
    set HDLLanguage {vhdl}
    set SynthesisTool {XST}
    set Simulator {Modelsim-SE}
    set ReadCores {False}
    set MapEffortLevel {High}
    set ParEffortLevel {High}
    set Frequency {50}
    set ProjectFiles {
        {{conv5x5_video_ex_cw.vhd} -view All}
        {{conv5x5_video_ex.vhd} -view All}
        {{conv5x5_video_ex_cw.ucf}}
        {{asr_11_0_29013f50d38bac20.mif}}
        {{asr_11_0_bd1895f119d47f37.mif}}
        {{bmg_52_fdc56027487be399.mif}}
        {{dmg_61_82464f65ac43724c.mif}}
        {{dmg_61_d85abc95ece75324.mif}}
        {{dmg_61_f24ea902b883a8c8.mif}}
        {{c:\training\dsp_trd\labs\conv5x5_video\ML605\conv5x5_video_ex.mdl}}
    }
    set TopLevelModule {conv5x5_video_ex_cw}
    set SynthesisConstraintsFile {conv5x5_video_ex_cw.xcf}
    set ImplementationStopView {Structural}
    set ProjectGenerator {SysgenDSP}
}
::xilinx::dsptool::iseproject::create
