library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/ABS1"

entity abs1_entity_25f2358d5d is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    in1: in std_logic_vector(17 downto 0); 
    out1: out std_logic_vector(18 downto 0)
  );
end abs1_entity_25f2358d5d;

architecture structural of abs1_entity_25f2358d5d is
  signal addsub15_s_net_x0: std_logic_vector(17 downto 0);
  signal ce_5_sg_x0: std_logic;
  signal clk_5_sg_x0: std_logic;
  signal mux_y_net_x0: std_logic_vector(18 downto 0);
  signal negate_op_net: std_logic_vector(18 downto 0);
  signal register1_q_net: std_logic_vector(17 downto 0);
  signal register2_q_net: std_logic;
  signal slice_y_net: std_logic;

begin
  ce_5_sg_x0 <= ce_5;
  clk_5_sg_x0 <= clk_5;
  addsub15_s_net_x0 <= in1;
  out1 <= mux_y_net_x0;

  mux: entity work.mux_d05de07565
    port map (
      ce => ce_5_sg_x0,
      clk => clk_5_sg_x0,
      clr => '0',
      d0 => register1_q_net,
      d1 => negate_op_net,
      sel(0) => register2_q_net,
      y => mux_y_net_x0
    );

  negate: entity work.negate_a029bf1f3d
    port map (
      ce => ce_5_sg_x0,
      clk => clk_5_sg_x0,
      clr => '0',
      ip => addsub15_s_net_x0,
      op => negate_op_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 18,
      init_value => b"000000000000000000"
    )
    port map (
      ce => ce_5_sg_x0,
      clk => clk_5_sg_x0,
      d => addsub15_s_net_x0,
      en => "1",
      rst => "0",
      q => register1_q_net
    );

  register2: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x0,
      clk => clk_5_sg_x0,
      d(0) => slice_y_net,
      en => "1",
      rst => "0",
      q(0) => register2_q_net
    );

  slice: entity work.xlslice
    generic map (
      new_lsb => 17,
      new_msb => 17,
      x_width => 18,
      y_width => 1
    )
    port map (
      x => addsub15_s_net_x0,
      y(0) => slice_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/load_sequencer"

entity load_sequencer_entity_86fd75ca28 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    load: in std_logic; 
    load_1: out std_logic; 
    load_2: out std_logic; 
    load_3: out std_logic; 
    load_4: out std_logic; 
    load_5: out std_logic
  );
end load_sequencer_entity_86fd75ca28;

architecture structural of load_sequencer_entity_86fd75ca28 is
  signal ce_1_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal constant1_op_net: std_logic_vector(2 downto 0);
  signal constant2_op_net: std_logic_vector(2 downto 0);
  signal constant3_op_net: std_logic_vector(2 downto 0);
  signal constant4_op_net: std_logic_vector(2 downto 0);
  signal constant5_op_net: std_logic_vector(2 downto 0);
  signal constant7_op_net: std_logic_vector(2 downto 0);
  signal counter_op_net: std_logic_vector(2 downto 0);
  signal delay1_q_net_x0: std_logic;
  signal index_count_op_net: std_logic_vector(2 downto 0);
  signal logical1_y_net_x0: std_logic;
  signal logical2_y_net_x0: std_logic;
  signal logical3_y_net_x0: std_logic;
  signal logical4_y_net_x0: std_logic;
  signal logical5_y_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal relational1_op_net: std_logic;
  signal relational2_op_net: std_logic;
  signal relational3_op_net: std_logic;
  signal relational4_op_net: std_logic;
  signal relational5_op_net: std_logic;
  signal relational6_op_net: std_logic;
  signal relational_op_net: std_logic;

begin
  ce_1_sg_x0 <= ce_1;
  clk_1_sg_x0 <= clk_1;
  delay1_q_net_x0 <= load;
  load_1 <= logical_y_net_x0;
  load_2 <= logical1_y_net_x0;
  load_3 <= logical2_y_net_x0;
  load_4 <= logical3_y_net_x0;
  load_5 <= logical4_y_net_x0;

  constant1: entity work.constant_822933f89b
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant1_op_net
    );

  constant2: entity work.constant_a1c496ea88
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant2_op_net
    );

  constant3: entity work.constant_1f5cc32f1e
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant3_op_net
    );

  constant4: entity work.constant_0f59f02ba5
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant4_op_net
    );

  constant5: entity work.constant_469094441c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant5_op_net
    );

  constant7: entity work.constant_469094441c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant7_op_net
    );

  counter: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      clr => '0',
      en(0) => delay1_q_net_x0,
      rst(0) => relational6_op_net,
      op => counter_op_net
    );

  index_count: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      clr => '0',
      en(0) => relational6_op_net,
      rst(0) => logical5_y_net,
      op => index_count_op_net
    );

  logical: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational1_op_net,
      d1(0) => delay1_q_net_x0,
      y(0) => logical_y_net_x0
    );

  logical1: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational2_op_net,
      d1(0) => delay1_q_net_x0,
      y(0) => logical1_y_net_x0
    );

  logical2: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational3_op_net,
      d1(0) => delay1_q_net_x0,
      y(0) => logical2_y_net_x0
    );

  logical3: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational4_op_net,
      d1(0) => delay1_q_net_x0,
      y(0) => logical3_y_net_x0
    );

  logical4: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational5_op_net,
      d1(0) => delay1_q_net_x0,
      y(0) => logical4_y_net_x0
    );

  logical5: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational_op_net,
      d1(0) => relational6_op_net,
      y(0) => logical5_y_net
    );

  relational: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant7_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net
    );

  relational1: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant1_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational1_op_net
    );

  relational2: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant2_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational2_op_net
    );

  relational3: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant3_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational3_op_net
    );

  relational4: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant4_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational4_op_net
    );

  relational5: entity work.relational_8fc7f5539b
    port map (
      a => index_count_op_net,
      b => constant5_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational5_op_net
    );

  relational6: entity work.relational_8fc7f5539b
    port map (
      a => counter_op_net,
      b => constant7_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational6_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter/Coefficient RAM/T Flip Flop"

entity t_flip_flop_entity_8cc9d1ed76 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    t: in std_logic; 
    q: out std_logic
  );
end t_flip_flop_entity_8cc9d1ed76;

architecture structural of t_flip_flop_entity_8cc9d1ed76 is
  signal ce_1_sg_x1: std_logic;
  signal clk_1_sg_x1: std_logic;
  signal convert1_dout_net: std_logic;
  signal inverter_op_net: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;

begin
  ce_1_sg_x1 <= ce_1;
  clk_1_sg_x1 <= clk_1;
  relational_op_net_x0 <= t;
  q <= register_q_net_x0;

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din(0) => register_q_net_x0,
      en => "1",
      dout(0) => convert1_dout_net
    );

  inverter: entity work.inverter_e2b989a05e
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      ip(0) => convert1_dout_net,
      op(0) => inverter_op_net
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      d(0) => inverter_op_net,
      en(0) => relational_op_net_x0,
      rst => "0",
      q(0) => register_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter/Coefficient RAM"

entity coefficient_ram_entity_58b4332869 is
  port (
    addr: in std_logic_vector(2 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    coef_in: in std_logic_vector(5 downto 0); 
    load: in std_logic; 
    coef_out: out std_logic_vector(5 downto 0)
  );
end coefficient_ram_entity_58b4332869;

architecture structural of coefficient_ram_entity_58b4332869 is
  signal ce_1_sg_x2: std_logic;
  signal clk_1_sg_x2: std_logic;
  signal concat1_y_net: std_logic_vector(3 downto 0);
  signal concat_y_net: std_logic_vector(3 downto 0);
  signal constant_op_net: std_logic_vector(2 downto 0);
  signal convert_dout_net: std_logic_vector(5 downto 0);
  signal counter1_op_net: std_logic_vector(2 downto 0);
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal inverter_op_net: std_logic;
  signal logical_y_net_x1: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;
  signal shared_memory_data_out_net_x0: std_logic_vector(5 downto 0);

begin
  counter_op_net_x0 <= addr;
  ce_1_sg_x2 <= ce_1;
  clk_1_sg_x2 <= clk_1;
  shared_memory_data_out_net_x0 <= coef_in;
  logical_y_net_x1 <= load;
  coef_out <= dual_port_ram_doutb_net_x0;

  concat: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => inverter_op_net,
      in1 => counter1_op_net,
      y => concat_y_net
    );

  concat1: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => register_q_net_x0,
      in1 => counter_op_net_x0,
      y => concat1_y_net
    );

  constant_x0: entity work.constant_469094441c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 0,
      din_width => 6,
      dout_arith => 2,
      dout_bin_pt => 0,
      dout_width => 6,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x2,
      clk => clk_1_sg_x2,
      clr => '0',
      din => shared_memory_data_out_net_x0,
      en => "1",
      dout => convert_dout_net
    );

  counter1: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x2,
      clk => clk_1_sg_x2,
      clr => '0',
      en(0) => logical_y_net_x1,
      rst => "0",
      op => counter1_op_net
    );

  dual_port_ram: entity work.xldpram_dist
    generic map (
      addr_width => 4,
      c_address_width => 4,
      c_width => 6,
      core_name0 => "dmg_61_f24ea902b883a8c8",
      latency => 1
    )
    port map (
      a_ce => ce_1_sg_x2,
      a_clk => clk_1_sg_x2,
      addra => concat_y_net,
      addrb => concat1_y_net,
      b_ce => ce_1_sg_x2,
      b_clk => clk_1_sg_x2,
      dina => convert_dout_net,
      ena => "1",
      enb => "1",
      wea(0) => logical_y_net_x1,
      doutb => dual_port_ram_doutb_net_x0
    );

  inverter: entity work.inverter_e2b989a05e
    port map (
      ce => ce_1_sg_x2,
      clk => clk_1_sg_x2,
      clr => '0',
      ip(0) => register_q_net_x0,
      op(0) => inverter_op_net
    );

  relational: entity work.relational_8fc7f5539b
    port map (
      a => counter1_op_net,
      b => constant_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x0
    );

  t_flip_flop_8cc9d1ed76: entity work.t_flip_flop_entity_8cc9d1ed76
    port map (
      ce_1 => ce_1_sg_x2,
      clk_1 => clk_1_sg_x2,
      t => relational_op_net_x0,
      q => register_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter/MAC Engine"

entity mac_engine_entity_e69786eaf6 is
  port (
    a: in std_logic_vector(7 downto 0); 
    b: in std_logic_vector(5 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    rst: in std_logic; 
    q: out std_logic_vector(11 downto 0)
  );
end mac_engine_entity_e69786eaf6;

architecture structural of mac_engine_entity_e69786eaf6 is
  signal accumulator_q_net_x0: std_logic_vector(11 downto 0);
  signal ce_1_sg_x3: std_logic;
  signal clk_1_sg_x3: std_logic;
  signal mult_p_net: std_logic_vector(8 downto 0);
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;

begin
  r0_q_net_x0 <= a;
  r1_q_net_x0 <= b;
  ce_1_sg_x3 <= ce_1;
  clk_1_sg_x3 <= clk_1;
  r2_q_net_x0 <= rst;
  q <= accumulator_q_net_x0;

  accumulator: entity work.accum_ef93d7a882
    port map (
      b => mult_p_net,
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      clr => '0',
      rst(0) => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  mult: entity work.xlmult
    generic map (
      a_arith => xlUnsigned,
      a_bin_pt => 0,
      a_width => 8,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 6,
      c_a_type => 1,
      c_a_width => 8,
      c_b_type => 0,
      c_b_width => 6,
      c_baat => 8,
      c_output_width => 14,
      c_type => 0,
      core_name0 => "mult_11_2_92794ba97a271046",
      extra_registers => 0,
      multsign => 2,
      overflow => 1,
      p_arith => xlSigned,
      p_bin_pt => 0,
      p_width => 9,
      quantization => 1
    )
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      clr => '0',
      core_ce => ce_1_sg_x3,
      core_clk => clk_1_sg_x3,
      core_clr => '1',
      en => "1",
      rst => "0",
      p => mult_p_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter"

entity n_tap_mac_fir_filter_entity_0b74e4cb65 is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    coef: in std_logic_vector(5 downto 0); 
    din: in std_logic_vector(7 downto 0); 
    load: in std_logic; 
    dout: out std_logic_vector(11 downto 0)
  );
end n_tap_mac_fir_filter_entity_0b74e4cb65;

architecture structural of n_tap_mac_fir_filter_entity_0b74e4cb65 is
  signal accumulator_q_net_x0: std_logic_vector(11 downto 0);
  signal asr_q_net: std_logic_vector(7 downto 0);
  signal capture_register_q_net: std_logic_vector(11 downto 0);
  signal ce_1_sg_x4: std_logic;
  signal ce_5_sg_x1: std_logic;
  signal clk_1_sg_x4: std_logic;
  signal clk_5_sg_x1: std_logic;
  signal constant_0_op_net: std_logic;
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal down_sample1_q_net_x0: std_logic_vector(11 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal logical_y_net_x2: std_logic;
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;
  signal r3_q_net: std_logic_vector(7 downto 0);
  signal relational1_op_net: std_logic;
  signal shared_memory_data_out_net_x1: std_logic_vector(5 downto 0);
  signal single_port_ram_data_out_net_x0: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x4 <= ce_1;
  ce_5_sg_x1 <= ce_5;
  clk_1_sg_x4 <= clk_1;
  clk_5_sg_x1 <= clk_5;
  shared_memory_data_out_net_x1 <= coef;
  single_port_ram_data_out_net_x0 <= din;
  logical_y_net_x2 <= load;
  dout <= down_sample1_q_net_x0;

  asr: entity work.xladdrsr
    generic map (
      addr_arith => xlUnsigned,
      addr_bin_pt => 0,
      addr_width => 3,
      core_addr_width => 3,
      core_name0 => "asr_11_0_bd1895f119d47f37",
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 8,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 8
    )
    port map (
      addr => counter_op_net_x0,
      ce => ce_5_sg_x1,
      clk => clk_5_sg_x1,
      clr => '0',
      d => r3_q_net,
      en => "1",
      q => asr_q_net
    );

  capture_register: entity work.xlregister
    generic map (
      d_width => 12,
      init_value => b"000000000000"
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d => accumulator_q_net_x0,
      en(0) => r2_q_net_x0,
      rst => "0",
      q => capture_register_q_net
    );

  coefficient_ram_58b4332869: entity work.coefficient_ram_entity_58b4332869
    port map (
      addr => counter_op_net_x0,
      ce_1 => ce_1_sg_x4,
      clk_1 => clk_1_sg_x4,
      coef_in => shared_memory_data_out_net_x1,
      load => logical_y_net_x2,
      coef_out => dual_port_ram_doutb_net_x0
    );

  constant_0: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant_0_op_net
    );

  counter: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter_op_net_x0
    );

  down_sample1: entity work.xldsamp
    generic map (
      d_arith => xlSigned,
      d_bin_pt => 0,
      d_width => 12,
      ds_ratio => 5,
      latency => 1,
      phase => 4,
      q_arith => xlSigned,
      q_bin_pt => 0,
      q_width => 12
    )
    port map (
      d => capture_register_q_net,
      dest_ce => ce_5_sg_x1,
      dest_clk => clk_5_sg_x1,
      dest_clr => '0',
      en => "1",
      src_ce => ce_1_sg_x4,
      src_clk => clk_1_sg_x4,
      src_clr => '0',
      q => down_sample1_q_net_x0
    );

  mac_engine_e69786eaf6: entity work.mac_engine_entity_e69786eaf6
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce_1 => ce_1_sg_x4,
      clk_1 => clk_1_sg_x4,
      rst => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  r0: entity work.xldelay
    generic map (
      latency => 2,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d => asr_q_net,
      en => '1',
      q => r0_q_net_x0
    );

  r1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 6
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d => dual_port_ram_doutb_net_x0,
      en => '1',
      q => r1_q_net_x0
    );

  r2: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d(0) => relational1_op_net,
      en => '1',
      q(0) => r2_q_net_x0
    );

  r3: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_5_sg_x1,
      clk => clk_5_sg_x1,
      d => single_port_ram_data_out_net_x0,
      en => '1',
      q => r3_q_net
    );

  relational1: entity work.relational_0ac5056ef6
    port map (
      a => counter_op_net_x0,
      b(0) => constant_0_op_net,
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      clr => '0',
      op(0) => relational1_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter1/Coefficient RAM"

entity coefficient_ram_entity_ec8c20c14a is
  port (
    addr: in std_logic_vector(2 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    coef_in: in std_logic_vector(5 downto 0); 
    load: in std_logic; 
    coef_out: out std_logic_vector(5 downto 0)
  );
end coefficient_ram_entity_ec8c20c14a;

architecture structural of coefficient_ram_entity_ec8c20c14a is
  signal ce_1_sg_x6: std_logic;
  signal clk_1_sg_x6: std_logic;
  signal concat1_y_net: std_logic_vector(3 downto 0);
  signal concat_y_net: std_logic_vector(3 downto 0);
  signal constant_op_net: std_logic_vector(2 downto 0);
  signal convert_dout_net: std_logic_vector(5 downto 0);
  signal counter1_op_net: std_logic_vector(2 downto 0);
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal inverter_op_net: std_logic;
  signal logical1_y_net_x1: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;
  signal shared_memory_data_out_net_x2: std_logic_vector(5 downto 0);

begin
  counter_op_net_x0 <= addr;
  ce_1_sg_x6 <= ce_1;
  clk_1_sg_x6 <= clk_1;
  shared_memory_data_out_net_x2 <= coef_in;
  logical1_y_net_x1 <= load;
  coef_out <= dual_port_ram_doutb_net_x0;

  concat: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => inverter_op_net,
      in1 => counter1_op_net,
      y => concat_y_net
    );

  concat1: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => register_q_net_x0,
      in1 => counter_op_net_x0,
      y => concat1_y_net
    );

  constant_x0: entity work.constant_469094441c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 0,
      din_width => 6,
      dout_arith => 2,
      dout_bin_pt => 0,
      dout_width => 6,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x6,
      clk => clk_1_sg_x6,
      clr => '0',
      din => shared_memory_data_out_net_x2,
      en => "1",
      dout => convert_dout_net
    );

  counter1: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x6,
      clk => clk_1_sg_x6,
      clr => '0',
      en(0) => logical1_y_net_x1,
      rst => "0",
      op => counter1_op_net
    );

  dual_port_ram: entity work.xldpram_dist
    generic map (
      addr_width => 4,
      c_address_width => 4,
      c_width => 6,
      core_name0 => "dmg_61_d85abc95ece75324",
      latency => 1
    )
    port map (
      a_ce => ce_1_sg_x6,
      a_clk => clk_1_sg_x6,
      addra => concat_y_net,
      addrb => concat1_y_net,
      b_ce => ce_1_sg_x6,
      b_clk => clk_1_sg_x6,
      dina => convert_dout_net,
      ena => "1",
      enb => "1",
      wea(0) => logical1_y_net_x1,
      doutb => dual_port_ram_doutb_net_x0
    );

  inverter: entity work.inverter_e2b989a05e
    port map (
      ce => ce_1_sg_x6,
      clk => clk_1_sg_x6,
      clr => '0',
      ip(0) => register_q_net_x0,
      op(0) => inverter_op_net
    );

  relational: entity work.relational_8fc7f5539b
    port map (
      a => counter1_op_net,
      b => constant_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x0
    );

  t_flip_flop_53b68bdc00: entity work.t_flip_flop_entity_8cc9d1ed76
    port map (
      ce_1 => ce_1_sg_x6,
      clk_1 => clk_1_sg_x6,
      t => relational_op_net_x0,
      q => register_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter1/MAC Engine"

entity mac_engine_entity_4ca7c6a025 is
  port (
    a: in std_logic_vector(7 downto 0); 
    b: in std_logic_vector(5 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    rst: in std_logic; 
    q: out std_logic_vector(13 downto 0)
  );
end mac_engine_entity_4ca7c6a025;

architecture structural of mac_engine_entity_4ca7c6a025 is
  signal accumulator_q_net_x0: std_logic_vector(13 downto 0);
  signal ce_1_sg_x7: std_logic;
  signal clk_1_sg_x7: std_logic;
  signal mult_p_net: std_logic_vector(11 downto 0);
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;

begin
  r0_q_net_x0 <= a;
  r1_q_net_x0 <= b;
  ce_1_sg_x7 <= ce_1;
  clk_1_sg_x7 <= clk_1;
  r2_q_net_x0 <= rst;
  q <= accumulator_q_net_x0;

  accumulator: entity work.accum_299f86ac09
    port map (
      b => mult_p_net,
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      clr => '0',
      rst(0) => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  mult: entity work.xlmult
    generic map (
      a_arith => xlUnsigned,
      a_bin_pt => 0,
      a_width => 8,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 6,
      c_a_type => 1,
      c_a_width => 8,
      c_b_type => 0,
      c_b_width => 6,
      c_baat => 8,
      c_output_width => 14,
      c_type => 0,
      core_name0 => "mult_11_2_92794ba97a271046",
      extra_registers => 0,
      multsign => 2,
      overflow => 1,
      p_arith => xlSigned,
      p_bin_pt => 0,
      p_width => 12,
      quantization => 1
    )
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      clr => '0',
      core_ce => ce_1_sg_x7,
      core_clk => clk_1_sg_x7,
      core_clr => '1',
      en => "1",
      rst => "0",
      p => mult_p_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter1"

entity n_tap_mac_fir_filter1_entity_dcc4875675 is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    coef: in std_logic_vector(5 downto 0); 
    din: in std_logic_vector(7 downto 0); 
    load: in std_logic; 
    dout: out std_logic_vector(13 downto 0)
  );
end n_tap_mac_fir_filter1_entity_dcc4875675;

architecture structural of n_tap_mac_fir_filter1_entity_dcc4875675 is
  signal accumulator_q_net_x0: std_logic_vector(13 downto 0);
  signal asr_q_net: std_logic_vector(7 downto 0);
  signal capture_register_q_net: std_logic_vector(13 downto 0);
  signal ce_1_sg_x8: std_logic;
  signal ce_5_sg_x2: std_logic;
  signal clk_1_sg_x8: std_logic;
  signal clk_5_sg_x2: std_logic;
  signal constant_0_op_net: std_logic;
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal down_sample1_q_net_x0: std_logic_vector(13 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal logical1_y_net_x2: std_logic;
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;
  signal r3_q_net: std_logic_vector(7 downto 0);
  signal relational1_op_net: std_logic;
  signal shared_memory_data_out_net_x3: std_logic_vector(5 downto 0);
  signal single_port_ram_data_out_net_x0: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x8 <= ce_1;
  ce_5_sg_x2 <= ce_5;
  clk_1_sg_x8 <= clk_1;
  clk_5_sg_x2 <= clk_5;
  shared_memory_data_out_net_x3 <= coef;
  single_port_ram_data_out_net_x0 <= din;
  logical1_y_net_x2 <= load;
  dout <= down_sample1_q_net_x0;

  asr: entity work.xladdrsr
    generic map (
      addr_arith => xlUnsigned,
      addr_bin_pt => 0,
      addr_width => 3,
      core_addr_width => 3,
      core_name0 => "asr_11_0_bd1895f119d47f37",
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 8,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 8
    )
    port map (
      addr => counter_op_net_x0,
      ce => ce_5_sg_x2,
      clk => clk_5_sg_x2,
      clr => '0',
      d => r3_q_net,
      en => "1",
      q => asr_q_net
    );

  capture_register: entity work.xlregister
    generic map (
      d_width => 14,
      init_value => b"00000000000000"
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      d => accumulator_q_net_x0,
      en(0) => r2_q_net_x0,
      rst => "0",
      q => capture_register_q_net
    );

  coefficient_ram_ec8c20c14a: entity work.coefficient_ram_entity_ec8c20c14a
    port map (
      addr => counter_op_net_x0,
      ce_1 => ce_1_sg_x8,
      clk_1 => clk_1_sg_x8,
      coef_in => shared_memory_data_out_net_x3,
      load => logical1_y_net_x2,
      coef_out => dual_port_ram_doutb_net_x0
    );

  constant_0: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant_0_op_net
    );

  counter: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter_op_net_x0
    );

  down_sample1: entity work.xldsamp
    generic map (
      d_arith => xlSigned,
      d_bin_pt => 0,
      d_width => 14,
      ds_ratio => 5,
      latency => 1,
      phase => 4,
      q_arith => xlSigned,
      q_bin_pt => 0,
      q_width => 14
    )
    port map (
      d => capture_register_q_net,
      dest_ce => ce_5_sg_x2,
      dest_clk => clk_5_sg_x2,
      dest_clr => '0',
      en => "1",
      src_ce => ce_1_sg_x8,
      src_clk => clk_1_sg_x8,
      src_clr => '0',
      q => down_sample1_q_net_x0
    );

  mac_engine_4ca7c6a025: entity work.mac_engine_entity_4ca7c6a025
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce_1 => ce_1_sg_x8,
      clk_1 => clk_1_sg_x8,
      rst => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  r0: entity work.xldelay
    generic map (
      latency => 2,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      d => asr_q_net,
      en => '1',
      q => r0_q_net_x0
    );

  r1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 6
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      d => dual_port_ram_doutb_net_x0,
      en => '1',
      q => r1_q_net_x0
    );

  r2: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      d(0) => relational1_op_net,
      en => '1',
      q(0) => r2_q_net_x0
    );

  r3: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_5_sg_x2,
      clk => clk_5_sg_x2,
      d => single_port_ram_data_out_net_x0,
      en => '1',
      q => r3_q_net
    );

  relational1: entity work.relational_0ac5056ef6
    port map (
      a => counter_op_net_x0,
      b(0) => constant_0_op_net,
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      op(0) => relational1_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter2/Coefficient RAM"

entity coefficient_ram_entity_9faff5c91a is
  port (
    addr: in std_logic_vector(2 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    coef_in: in std_logic_vector(5 downto 0); 
    load: in std_logic; 
    coef_out: out std_logic_vector(5 downto 0)
  );
end coefficient_ram_entity_9faff5c91a;

architecture structural of coefficient_ram_entity_9faff5c91a is
  signal ce_1_sg_x10: std_logic;
  signal clk_1_sg_x10: std_logic;
  signal concat1_y_net: std_logic_vector(3 downto 0);
  signal concat_y_net: std_logic_vector(3 downto 0);
  signal constant_op_net: std_logic_vector(2 downto 0);
  signal convert_dout_net: std_logic_vector(5 downto 0);
  signal counter1_op_net: std_logic_vector(2 downto 0);
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal inverter_op_net: std_logic;
  signal logical2_y_net_x1: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;
  signal shared_memory_data_out_net_x4: std_logic_vector(5 downto 0);

begin
  counter_op_net_x0 <= addr;
  ce_1_sg_x10 <= ce_1;
  clk_1_sg_x10 <= clk_1;
  shared_memory_data_out_net_x4 <= coef_in;
  logical2_y_net_x1 <= load;
  coef_out <= dual_port_ram_doutb_net_x0;

  concat: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => inverter_op_net,
      in1 => counter1_op_net,
      y => concat_y_net
    );

  concat1: entity work.concat_949f038a6d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => register_q_net_x0,
      in1 => counter_op_net_x0,
      y => concat1_y_net
    );

  constant_x0: entity work.constant_469094441c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 0,
      din_width => 6,
      dout_arith => 2,
      dout_bin_pt => 0,
      dout_width => 6,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x10,
      clk => clk_1_sg_x10,
      clr => '0',
      din => shared_memory_data_out_net_x4,
      en => "1",
      dout => convert_dout_net
    );

  counter1: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x10,
      clk => clk_1_sg_x10,
      clr => '0',
      en(0) => logical2_y_net_x1,
      rst => "0",
      op => counter1_op_net
    );

  dual_port_ram: entity work.xldpram_dist
    generic map (
      addr_width => 4,
      c_address_width => 4,
      c_width => 6,
      core_name0 => "dmg_61_82464f65ac43724c",
      latency => 1
    )
    port map (
      a_ce => ce_1_sg_x10,
      a_clk => clk_1_sg_x10,
      addra => concat_y_net,
      addrb => concat1_y_net,
      b_ce => ce_1_sg_x10,
      b_clk => clk_1_sg_x10,
      dina => convert_dout_net,
      ena => "1",
      enb => "1",
      wea(0) => logical2_y_net_x1,
      doutb => dual_port_ram_doutb_net_x0
    );

  inverter: entity work.inverter_e2b989a05e
    port map (
      ce => ce_1_sg_x10,
      clk => clk_1_sg_x10,
      clr => '0',
      ip(0) => register_q_net_x0,
      op(0) => inverter_op_net
    );

  relational: entity work.relational_8fc7f5539b
    port map (
      a => counter1_op_net,
      b => constant_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x0
    );

  t_flip_flop_ea30b51a26: entity work.t_flip_flop_entity_8cc9d1ed76
    port map (
      ce_1 => ce_1_sg_x10,
      clk_1 => clk_1_sg_x10,
      t => relational_op_net_x0,
      q => register_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter2/MAC Engine"

entity mac_engine_entity_cfe24bd391 is
  port (
    a: in std_logic_vector(7 downto 0); 
    b: in std_logic_vector(5 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    rst: in std_logic; 
    q: out std_logic_vector(14 downto 0)
  );
end mac_engine_entity_cfe24bd391;

architecture structural of mac_engine_entity_cfe24bd391 is
  signal accumulator_q_net_x0: std_logic_vector(14 downto 0);
  signal ce_1_sg_x11: std_logic;
  signal clk_1_sg_x11: std_logic;
  signal mult_p_net: std_logic_vector(14 downto 0);
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;

begin
  r0_q_net_x0 <= a;
  r1_q_net_x0 <= b;
  ce_1_sg_x11 <= ce_1;
  clk_1_sg_x11 <= clk_1;
  r2_q_net_x0 <= rst;
  q <= accumulator_q_net_x0;

  accumulator: entity work.accum_14efba2cf2
    port map (
      b => mult_p_net,
      ce => ce_1_sg_x11,
      clk => clk_1_sg_x11,
      clr => '0',
      rst(0) => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  mult: entity work.xlmult
    generic map (
      a_arith => xlUnsigned,
      a_bin_pt => 0,
      a_width => 8,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 6,
      c_a_type => 1,
      c_a_width => 8,
      c_b_type => 0,
      c_b_width => 6,
      c_baat => 8,
      c_output_width => 14,
      c_type => 0,
      core_name0 => "mult_11_2_92794ba97a271046",
      extra_registers => 0,
      multsign => 2,
      overflow => 1,
      p_arith => xlSigned,
      p_bin_pt => 0,
      p_width => 15,
      quantization => 1
    )
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce => ce_1_sg_x11,
      clk => clk_1_sg_x11,
      clr => '0',
      core_ce => ce_1_sg_x11,
      core_clk => clk_1_sg_x11,
      core_clr => '1',
      en => "1",
      rst => "0",
      p => mult_p_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter/n-tap MAC FIR Filter2"

entity n_tap_mac_fir_filter2_entity_0affee0b1e is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    coef: in std_logic_vector(5 downto 0); 
    din: in std_logic_vector(7 downto 0); 
    load: in std_logic; 
    dout: out std_logic_vector(14 downto 0)
  );
end n_tap_mac_fir_filter2_entity_0affee0b1e;

architecture structural of n_tap_mac_fir_filter2_entity_0affee0b1e is
  signal accumulator_q_net_x0: std_logic_vector(14 downto 0);
  signal asr_q_net: std_logic_vector(7 downto 0);
  signal capture_register_q_net: std_logic_vector(14 downto 0);
  signal ce_1_sg_x12: std_logic;
  signal ce_5_sg_x3: std_logic;
  signal clk_1_sg_x12: std_logic;
  signal clk_5_sg_x3: std_logic;
  signal constant_0_op_net: std_logic;
  signal counter_op_net_x0: std_logic_vector(2 downto 0);
  signal down_sample1_q_net_x0: std_logic_vector(14 downto 0);
  signal dual_port_ram_doutb_net_x0: std_logic_vector(5 downto 0);
  signal logical2_y_net_x2: std_logic;
  signal r0_q_net_x0: std_logic_vector(7 downto 0);
  signal r1_q_net_x0: std_logic_vector(5 downto 0);
  signal r2_q_net_x0: std_logic;
  signal r3_q_net: std_logic_vector(7 downto 0);
  signal relational1_op_net: std_logic;
  signal shared_memory_data_out_net_x5: std_logic_vector(5 downto 0);
  signal single_port_ram_data_out_net_x0: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x12 <= ce_1;
  ce_5_sg_x3 <= ce_5;
  clk_1_sg_x12 <= clk_1;
  clk_5_sg_x3 <= clk_5;
  shared_memory_data_out_net_x5 <= coef;
  single_port_ram_data_out_net_x0 <= din;
  logical2_y_net_x2 <= load;
  dout <= down_sample1_q_net_x0;

  asr: entity work.xladdrsr
    generic map (
      addr_arith => xlUnsigned,
      addr_bin_pt => 0,
      addr_width => 3,
      core_addr_width => 3,
      core_name0 => "asr_11_0_bd1895f119d47f37",
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 8,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 8
    )
    port map (
      addr => counter_op_net_x0,
      ce => ce_5_sg_x3,
      clk => clk_5_sg_x3,
      clr => '0',
      d => r3_q_net,
      en => "1",
      q => asr_q_net
    );

  capture_register: entity work.xlregister
    generic map (
      d_width => 15,
      init_value => b"000000000000000"
    )
    port map (
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      d => accumulator_q_net_x0,
      en(0) => r2_q_net_x0,
      rst => "0",
      q => capture_register_q_net
    );

  coefficient_ram_9faff5c91a: entity work.coefficient_ram_entity_9faff5c91a
    port map (
      addr => counter_op_net_x0,
      ce_1 => ce_1_sg_x12,
      clk_1 => clk_1_sg_x12,
      coef_in => shared_memory_data_out_net_x5,
      load => logical2_y_net_x2,
      coef_out => dual_port_ram_doutb_net_x0
    );

  constant_0: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant_0_op_net
    );

  counter: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 4,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_82054cbc1c11cd4c",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter_op_net_x0
    );

  down_sample1: entity work.xldsamp
    generic map (
      d_arith => xlSigned,
      d_bin_pt => 0,
      d_width => 15,
      ds_ratio => 5,
      latency => 1,
      phase => 4,
      q_arith => xlSigned,
      q_bin_pt => 0,
      q_width => 15
    )
    port map (
      d => capture_register_q_net,
      dest_ce => ce_5_sg_x3,
      dest_clk => clk_5_sg_x3,
      dest_clr => '0',
      en => "1",
      src_ce => ce_1_sg_x12,
      src_clk => clk_1_sg_x12,
      src_clr => '0',
      q => down_sample1_q_net_x0
    );

  mac_engine_cfe24bd391: entity work.mac_engine_entity_cfe24bd391
    port map (
      a => r0_q_net_x0,
      b => r1_q_net_x0,
      ce_1 => ce_1_sg_x12,
      clk_1 => clk_1_sg_x12,
      rst => r2_q_net_x0,
      q => accumulator_q_net_x0
    );

  r0: entity work.xldelay
    generic map (
      latency => 2,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      d => asr_q_net,
      en => '1',
      q => r0_q_net_x0
    );

  r1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 6
    )
    port map (
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      d => dual_port_ram_doutb_net_x0,
      en => '1',
      q => r1_q_net_x0
    );

  r2: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      d(0) => relational1_op_net,
      en => '1',
      q(0) => r2_q_net_x0
    );

  r3: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_5_sg_x3,
      clk => clk_5_sg_x3,
      d => single_port_ram_data_out_net_x0,
      en => '1',
      q => r3_q_net
    );

  relational1: entity work.relational_0ac5056ef6
    port map (
      a => counter_op_net_x0,
      b(0) => constant_0_op_net,
      ce => ce_1_sg_x12,
      clk => clk_1_sg_x12,
      clr => '0',
      op(0) => relational1_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/5x5Filter"

entity x5x5filter_entity_bcadfb37d3 is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    coef: in std_logic_vector(5 downto 0); 
    gain: in std_logic_vector(19 downto 0); 
    line1: in std_logic_vector(7 downto 0); 
    line2: in std_logic_vector(7 downto 0); 
    line3: in std_logic_vector(7 downto 0); 
    line4: in std_logic_vector(7 downto 0); 
    line5: in std_logic_vector(7 downto 0); 
    load: in std_logic; 
    out1: out std_logic_vector(7 downto 0)
  );
end x5x5filter_entity_bcadfb37d3;

architecture structural of x5x5filter_entity_bcadfb37d3 is
  signal addsub15_s_net_x0: std_logic_vector(17 downto 0);
  signal addsub2_s_net: std_logic_vector(14 downto 0);
  signal addsub3_s_net: std_logic_vector(15 downto 0);
  signal addsub4_s_net: std_logic_vector(16 downto 0);
  signal ce_1_sg_x21: std_logic;
  signal ce_5_sg_x6: std_logic;
  signal clk_1_sg_x21: std_logic;
  signal clk_5_sg_x6: std_logic;
  signal convert1_dout_net_x0: std_logic_vector(7 downto 0);
  signal delay1_q_net_x1: std_logic;
  signal down_sample1_q_net_x0: std_logic_vector(11 downto 0);
  signal down_sample1_q_net_x1: std_logic_vector(13 downto 0);
  signal down_sample1_q_net_x2: std_logic_vector(14 downto 0);
  signal down_sample1_q_net_x3: std_logic_vector(13 downto 0);
  signal down_sample1_q_net_x4: std_logic_vector(11 downto 0);
  signal gain_net_x0: std_logic_vector(19 downto 0);
  signal logical1_y_net_x2: std_logic;
  signal logical2_y_net_x2: std_logic;
  signal logical3_y_net_x2: std_logic;
  signal logical4_y_net_x2: std_logic;
  signal logical_y_net_x2: std_logic;
  signal mult_p_net: std_logic_vector(38 downto 0);
  signal mux_y_net_x0: std_logic_vector(18 downto 0);
  signal register16_q_net_x1: std_logic_vector(7 downto 0);
  signal register1_q_net: std_logic_vector(11 downto 0);
  signal register2_q_net: std_logic_vector(11 downto 0);
  signal shared_memory_data_out_net_x10: std_logic_vector(5 downto 0);
  signal single_port_ram_data_out_net_x4: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x5: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x6: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x7: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x21 <= ce_1;
  ce_5_sg_x6 <= ce_5;
  clk_1_sg_x21 <= clk_1;
  clk_5_sg_x6 <= clk_5;
  shared_memory_data_out_net_x10 <= coef;
  gain_net_x0 <= gain;
  single_port_ram_data_out_net_x7 <= line1;
  single_port_ram_data_out_net_x6 <= line2;
  single_port_ram_data_out_net_x5 <= line3;
  single_port_ram_data_out_net_x4 <= line4;
  register16_q_net_x1 <= line5;
  delay1_q_net_x1 <= load;
  out1 <= convert1_dout_net_x0;

  abs1_25f2358d5d: entity work.abs1_entity_25f2358d5d
    port map (
      ce_5 => ce_5_sg_x6,
      clk_5 => clk_5_sg_x6,
      in1 => addsub15_s_net_x0,
      out1 => mux_y_net_x0
    );

  addsub15: entity work.xladdsub
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 0,
      a_width => 17,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 12,
      c_has_c_out => 0,
      c_latency => 1,
      c_output_width => 18,
      core_name0 => "addsb_11_0_4a6f838b58845151",
      extra_registers => 0,
      full_s_arith => 2,
      full_s_width => 18,
      latency => 1,
      overflow => 1,
      quantization => 1,
      s_arith => xlSigned,
      s_bin_pt => 0,
      s_width => 18
    )
    port map (
      a => addsub4_s_net,
      b => register2_q_net,
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      en => "1",
      s => addsub15_s_net_x0
    );

  addsub2: entity work.xladdsub
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 0,
      a_width => 12,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 14,
      c_has_c_out => 0,
      c_latency => 1,
      c_output_width => 15,
      core_name0 => "addsb_11_0_514677470377df72",
      extra_registers => 0,
      full_s_arith => 2,
      full_s_width => 15,
      latency => 1,
      overflow => 1,
      quantization => 1,
      s_arith => xlSigned,
      s_bin_pt => 0,
      s_width => 15
    )
    port map (
      a => down_sample1_q_net_x0,
      b => down_sample1_q_net_x1,
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      en => "1",
      s => addsub2_s_net
    );

  addsub3: entity work.xladdsub
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 0,
      a_width => 15,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 14,
      c_has_c_out => 0,
      c_latency => 1,
      c_output_width => 16,
      core_name0 => "addsb_11_0_30176bdc4e590e66",
      extra_registers => 0,
      full_s_arith => 2,
      full_s_width => 16,
      latency => 1,
      overflow => 1,
      quantization => 1,
      s_arith => xlSigned,
      s_bin_pt => 0,
      s_width => 16
    )
    port map (
      a => down_sample1_q_net_x2,
      b => down_sample1_q_net_x3,
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      en => "1",
      s => addsub3_s_net
    );

  addsub4: entity work.xladdsub
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 0,
      a_width => 15,
      b_arith => xlSigned,
      b_bin_pt => 0,
      b_width => 16,
      c_has_c_out => 0,
      c_latency => 1,
      c_output_width => 17,
      core_name0 => "addsb_11_0_00275fb8968adea7",
      extra_registers => 0,
      full_s_arith => 2,
      full_s_width => 17,
      latency => 1,
      overflow => 1,
      quantization => 1,
      s_arith => xlSigned,
      s_bin_pt => 0,
      s_width => 17
    )
    port map (
      a => addsub2_s_net,
      b => addsub3_s_net,
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      en => "1",
      s => addsub4_s_net
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 39,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 8,
      latency => 1,
      overflow => xlSaturate,
      quantization => xlTruncate
    )
    port map (
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      din => mult_p_net,
      en => "1",
      dout => convert1_dout_net_x0
    );

  load_sequencer_86fd75ca28: entity work.load_sequencer_entity_86fd75ca28
    port map (
      ce_1 => ce_1_sg_x21,
      clk_1 => clk_1_sg_x21,
      load => delay1_q_net_x1,
      load_1 => logical_y_net_x2,
      load_2 => logical1_y_net_x2,
      load_3 => logical2_y_net_x2,
      load_4 => logical3_y_net_x2,
      load_5 => logical4_y_net_x2
    );

  mult: entity work.xlmult
    generic map (
      a_arith => xlSigned,
      a_bin_pt => 0,
      a_width => 19,
      b_arith => xlSigned,
      b_bin_pt => 17,
      b_width => 20,
      c_a_type => 0,
      c_a_width => 19,
      c_b_type => 0,
      c_b_width => 20,
      c_baat => 19,
      c_output_width => 39,
      c_type => 0,
      core_name0 => "mult_11_2_7c8c171e9e9f440b",
      extra_registers => 0,
      multsign => 2,
      overflow => 1,
      p_arith => xlSigned,
      p_bin_pt => 17,
      p_width => 39,
      quantization => 1
    )
    port map (
      a => mux_y_net_x0,
      b => gain_net_x0,
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      clr => '0',
      core_ce => ce_5_sg_x6,
      core_clk => clk_5_sg_x6,
      core_clr => '1',
      en => "1",
      rst => "0",
      p => mult_p_net
    );

  n_tap_mac_fir_filter1_dcc4875675: entity work.n_tap_mac_fir_filter1_entity_dcc4875675
    port map (
      ce_1 => ce_1_sg_x21,
      ce_5 => ce_5_sg_x6,
      clk_1 => clk_1_sg_x21,
      clk_5 => clk_5_sg_x6,
      coef => shared_memory_data_out_net_x10,
      din => single_port_ram_data_out_net_x6,
      load => logical1_y_net_x2,
      dout => down_sample1_q_net_x1
    );

  n_tap_mac_fir_filter2_0affee0b1e: entity work.n_tap_mac_fir_filter2_entity_0affee0b1e
    port map (
      ce_1 => ce_1_sg_x21,
      ce_5 => ce_5_sg_x6,
      clk_1 => clk_1_sg_x21,
      clk_5 => clk_5_sg_x6,
      coef => shared_memory_data_out_net_x10,
      din => single_port_ram_data_out_net_x5,
      load => logical2_y_net_x2,
      dout => down_sample1_q_net_x2
    );

  n_tap_mac_fir_filter3_00a0692314: entity work.n_tap_mac_fir_filter1_entity_dcc4875675
    port map (
      ce_1 => ce_1_sg_x21,
      ce_5 => ce_5_sg_x6,
      clk_1 => clk_1_sg_x21,
      clk_5 => clk_5_sg_x6,
      coef => shared_memory_data_out_net_x10,
      din => single_port_ram_data_out_net_x4,
      load => logical3_y_net_x2,
      dout => down_sample1_q_net_x3
    );

  n_tap_mac_fir_filter4_2c722fe9c5: entity work.n_tap_mac_fir_filter_entity_0b74e4cb65
    port map (
      ce_1 => ce_1_sg_x21,
      ce_5 => ce_5_sg_x6,
      clk_1 => clk_1_sg_x21,
      clk_5 => clk_5_sg_x6,
      coef => shared_memory_data_out_net_x10,
      din => register16_q_net_x1,
      load => logical4_y_net_x2,
      dout => down_sample1_q_net_x4
    );

  n_tap_mac_fir_filter_0b74e4cb65: entity work.n_tap_mac_fir_filter_entity_0b74e4cb65
    port map (
      ce_1 => ce_1_sg_x21,
      ce_5 => ce_5_sg_x6,
      clk_1 => clk_1_sg_x21,
      clk_5 => clk_5_sg_x6,
      coef => shared_memory_data_out_net_x10,
      din => single_port_ram_data_out_net_x7,
      load => logical_y_net_x2,
      dout => down_sample1_q_net_x0
    );

  register1: entity work.xlregister
    generic map (
      d_width => 12,
      init_value => b"000000000000"
    )
    port map (
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      d => down_sample1_q_net_x4,
      en => "1",
      rst => "0",
      q => register1_q_net
    );

  register2: entity work.xlregister
    generic map (
      d_width => 12,
      init_value => b"000000000000"
    )
    port map (
      ce => ce_5_sg_x6,
      clk => clk_5_sg_x6,
      d => register1_q_net,
      en => "1",
      rst => "0",
      q => register2_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/Virtex2 5 Line Buffer1/Virtex2 Line Buffer"

entity virtex2_line_buffer_entity_cb93b83a95 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    i: in std_logic_vector(7 downto 0); 
    o: out std_logic_vector(7 downto 0)
  );
end virtex2_line_buffer_entity_cb93b83a95;

architecture structural of virtex2_line_buffer_entity_cb93b83a95 is
  signal ce_5_sg_x7: std_logic;
  signal clk_5_sg_x7: std_logic;
  signal constant3_op_net: std_logic;
  signal counter2_op_net: std_logic_vector(6 downto 0);
  signal register16_q_net_x2: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x5: std_logic_vector(7 downto 0);

begin
  ce_5_sg_x7 <= ce_5;
  clk_5_sg_x7 <= clk_5;
  register16_q_net_x2 <= i;
  o <= single_port_ram_data_out_net_x5;

  constant3: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant3_op_net
    );

  counter2: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 126,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_4a9b8d420482825f",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 7
    )
    port map (
      ce => ce_5_sg_x7,
      clk => clk_5_sg_x7,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter2_op_net
    );

  single_port_ram: entity work.xlspram
    generic map (
      c_address_width => 7,
      c_width => 8,
      core_name0 => "bmg_52_fdc56027487be399",
      latency => 1
    )
    port map (
      addr => counter2_op_net,
      ce => ce_5_sg_x7,
      clk => clk_5_sg_x7,
      data_in => register16_q_net_x2,
      en => "1",
      rst => "0",
      we(0) => constant3_op_net,
      data_out => single_port_ram_data_out_net_x5
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/Virtex2 5 Line Buffer1"

entity virtex2_5_line_buffer1_entity_5e1c5ab2c0 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    line1: out std_logic_vector(7 downto 0); 
    line2: out std_logic_vector(7 downto 0); 
    line3: out std_logic_vector(7 downto 0); 
    line4: out std_logic_vector(7 downto 0)
  );
end virtex2_5_line_buffer1_entity_5e1c5ab2c0;

architecture structural of virtex2_5_line_buffer1_entity_5e1c5ab2c0 is
  signal ce_5_sg_x11: std_logic;
  signal clk_5_sg_x11: std_logic;
  signal register16_q_net_x3: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x13: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x14: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x15: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x8: std_logic_vector(7 downto 0);

begin
  ce_5_sg_x11 <= ce_5;
  clk_5_sg_x11 <= clk_5;
  register16_q_net_x3 <= in1;
  line1 <= single_port_ram_data_out_net_x15;
  line2 <= single_port_ram_data_out_net_x14;
  line3 <= single_port_ram_data_out_net_x13;
  line4 <= single_port_ram_data_out_net_x8;

  virtex2_line_buffer1_08fcc828b7: entity work.virtex2_line_buffer_entity_cb93b83a95
    port map (
      ce_5 => ce_5_sg_x11,
      clk_5 => clk_5_sg_x11,
      i => single_port_ram_data_out_net_x8,
      o => single_port_ram_data_out_net_x13
    );

  virtex2_line_buffer2_387f350276: entity work.virtex2_line_buffer_entity_cb93b83a95
    port map (
      ce_5 => ce_5_sg_x11,
      clk_5 => clk_5_sg_x11,
      i => single_port_ram_data_out_net_x13,
      o => single_port_ram_data_out_net_x14
    );

  virtex2_line_buffer3_b5b914192a: entity work.virtex2_line_buffer_entity_cb93b83a95
    port map (
      ce_5 => ce_5_sg_x11,
      clk_5 => clk_5_sg_x11,
      i => single_port_ram_data_out_net_x14,
      o => single_port_ram_data_out_net_x15
    );

  virtex2_line_buffer_cb93b83a95: entity work.virtex2_line_buffer_entity_cb93b83a95
    port map (
      ce_5 => ce_5_sg_x11,
      clk_5 => clk_5_sg_x11,
      i => register16_q_net_x3,
      o => single_port_ram_data_out_net_x8
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/coefficient_memory/sr_flip_flop"

entity sr_flip_flop_entity_86eb525cfc is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    r: in std_logic; 
    s: in std_logic; 
    q: out std_logic
  );
end sr_flip_flop_entity_86eb525cfc;

architecture structural of sr_flip_flop_entity_86eb525cfc is
  signal ce_1_sg_x22: std_logic;
  signal clk_1_sg_x22: std_logic;
  signal constant2_op_net: std_logic;
  signal convert_dout_net_x0: std_logic;
  signal register1_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;

begin
  ce_1_sg_x22 <= ce_1;
  clk_1_sg_x22 <= clk_1;
  convert_dout_net_x0 <= r;
  relational_op_net_x0 <= s;
  q <= register1_q_net_x0;

  constant2: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant2_op_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x22,
      clk => clk_1_sg_x22,
      d(0) => constant2_op_net,
      en(0) => relational_op_net_x0,
      rst(0) => convert_dout_net_x0,
      q(0) => register1_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/coefficient_memory"

entity coefficient_memory_entity_fbd8310aed is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    shared_memory: in std_logic_vector(5 downto 0); 
    constant1_x0: out std_logic; 
    constant2_x0: out std_logic_vector(5 downto 0); 
    counter_x0: out std_logic_vector(4 downto 0); 
    load: out std_logic
  );
end coefficient_memory_entity_fbd8310aed;

architecture structural of coefficient_memory_entity_fbd8310aed is
  signal ce_1_sg_x23: std_logic;
  signal clk_1_sg_x23: std_logic;
  signal constant1_op_net_x0: std_logic;
  signal constant2_op_net_x0: std_logic_vector(5 downto 0);
  signal constant3_op_net: std_logic_vector(4 downto 0);
  signal convert_dout_net_x0: std_logic;
  signal counter_op_net_x0: std_logic_vector(4 downto 0);
  signal delay1_q_net_x2: std_logic;
  signal delay_q_net: std_logic_vector(5 downto 0);
  signal register1_q_net_x0: std_logic;
  signal register_q_net: std_logic;
  signal relational1_op_net: std_logic;
  signal relational_op_net_x0: std_logic;
  signal shared_memory_data_out_net_x11: std_logic_vector(5 downto 0);

begin
  ce_1_sg_x23 <= ce_1;
  clk_1_sg_x23 <= clk_1;
  shared_memory_data_out_net_x11 <= shared_memory;
  constant1_x0 <= constant1_op_net_x0;
  constant2_x0 <= constant2_op_net_x0;
  counter_x0 <= counter_op_net_x0;
  load <= delay1_q_net_x2;

  constant1: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net_x0
    );

  constant2: entity work.constant_7ea0f2fff7
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant2_op_net_x0
    );

  constant3: entity work.constant_bc74ae1a6c
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant3_op_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x23,
      clk => clk_1_sg_x23,
      clr => '0',
      din(0) => register_q_net,
      en => "1",
      dout(0) => convert_dout_net_x0
    );

  counter: entity work.xlcounter_limit
    generic map (
      cnt_15_0 => 24,
      cnt_31_16 => 0,
      cnt_47_32 => 0,
      cnt_63_48 => 0,
      core_name0 => "cntr_11_0_32605b2f9834e584",
      count_limited => 1,
      op_arith => xlUnsigned,
      op_width => 5
    )
    port map (
      ce => ce_1_sg_x23,
      clk => clk_1_sg_x23,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter_op_net_x0
    );

  delay: entity work.xldelay
    generic map (
      latency => 25,
      reg_retiming => 0,
      width => 6
    )
    port map (
      ce => ce_1_sg_x23,
      clk => clk_1_sg_x23,
      d => shared_memory_data_out_net_x11,
      en => '1',
      q => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x23,
      clk => clk_1_sg_x23,
      d(0) => register_q_net,
      en => '1',
      q(0) => delay1_q_net_x2
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x23,
      clk => clk_1_sg_x23,
      d(0) => register1_q_net_x0,
      en(0) => relational1_op_net,
      rst => "0",
      q(0) => register_q_net
    );

  relational: entity work.relational_d61514e317
    port map (
      a => delay_q_net,
      b => shared_memory_data_out_net_x11,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x0
    );

  relational1: entity work.relational_9ece3c8c4e
    port map (
      a => counter_op_net_x0,
      b => constant3_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational1_op_net
    );

  sr_flip_flop_86eb525cfc: entity work.sr_flip_flop_entity_86eb525cfc
    port map (
      ce_1 => ce_1_sg_x23,
      clk_1 => clk_1_sg_x23,
      r => convert_dout_net_x0,
      s => relational_op_net_x0,
      q => register1_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/valid_generator/one_shot"

entity one_shot_entity_04df8b5e78 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    din: in std_logic; 
    dout: out std_logic
  );
end one_shot_entity_04df8b5e78;

architecture structural of one_shot_entity_04df8b5e78 is
  signal ce_5_sg_x12: std_logic;
  signal clk_5_sg_x12: std_logic;
  signal inverter_op_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal register1_q_net: std_logic;
  signal register1_q_net_x1: std_logic;

begin
  ce_5_sg_x12 <= ce_5;
  clk_5_sg_x12 <= clk_5;
  register1_q_net_x1 <= din;
  dout <= logical_y_net_x0;

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_5_sg_x12,
      clk => clk_5_sg_x12,
      clr => '0',
      ip(0) => register1_q_net,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => register1_q_net_x1,
      d1(0) => inverter_op_net,
      y(0) => logical_y_net_x0
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x12,
      clk => clk_5_sg_x12,
      d(0) => register1_q_net_x1,
      en => "1",
      rst => "0",
      q(0) => register1_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter/valid_generator"

entity valid_generator_entity_5f73eec251 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    offset: in std_logic_vector(9 downto 0); 
    valid_in: in std_logic; 
    valid_out: out std_logic
  );
end valid_generator_entity_5f73eec251;

architecture structural of valid_generator_entity_5f73eec251 is
  signal asr_q_net_x0: std_logic;
  signal ce_5_sg_x13: std_logic;
  signal clk_5_sg_x13: std_logic;
  signal logical_y_net_x0: std_logic;
  signal offset_net_x0: std_logic_vector(9 downto 0);
  signal register1_q_net: std_logic_vector(9 downto 0);
  signal register1_q_net_x2: std_logic;
  signal register_q_net: std_logic;

begin
  ce_5_sg_x13 <= ce_5;
  clk_5_sg_x13 <= clk_5;
  offset_net_x0 <= offset;
  register1_q_net_x2 <= valid_in;
  valid_out <= asr_q_net_x0;

  asr: entity work.xladdrsr
    generic map (
      addr_arith => xlUnsigned,
      addr_bin_pt => 0,
      addr_width => 10,
      core_addr_width => 10,
      core_name0 => "asr_11_0_29013f50d38bac20",
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 1,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 1
    )
    port map (
      addr => register1_q_net,
      ce => ce_5_sg_x13,
      clk => clk_5_sg_x13,
      clr => '0',
      d(0) => register_q_net,
      en => "1",
      q(0) => asr_q_net_x0
    );

  one_shot_04df8b5e78: entity work.one_shot_entity_04df8b5e78
    port map (
      ce_5 => ce_5_sg_x13,
      clk_5 => clk_5_sg_x13,
      din => register1_q_net_x2,
      dout => logical_y_net_x0
    );

  register1: entity work.xlregister
    generic map (
      d_width => 10,
      init_value => b"0000000000"
    )
    port map (
      ce => ce_5_sg_x13,
      clk => clk_5_sg_x13,
      d => offset_net_x0,
      en(0) => logical_y_net_x0,
      rst => "0",
      q => register1_q_net
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x13,
      clk => clk_5_sg_x13,
      d(0) => register1_q_net_x2,
      en => "1",
      rst => "0",
      q(0) => register_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/5x5_filter"

entity x5x5_filter_entity_172a0d5e3e is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    din: in std_logic_vector(7 downto 0); 
    gain: in std_logic_vector(19 downto 0); 
    offset: in std_logic_vector(9 downto 0); 
    shared_memory: in std_logic_vector(5 downto 0); 
    valid_in: in std_logic; 
    coefficient_memory: out std_logic; 
    coefficient_memory_x0: out std_logic_vector(5 downto 0); 
    coefficient_memory_x1: out std_logic_vector(4 downto 0); 
    dout: out std_logic_vector(7 downto 0); 
    valid_out: out std_logic
  );
end x5x5_filter_entity_172a0d5e3e;

architecture structural of x5x5_filter_entity_172a0d5e3e is
  signal asr_q_net_x1: std_logic;
  signal ce_1_sg_x24: std_logic;
  signal ce_5_sg_x14: std_logic;
  signal clk_1_sg_x24: std_logic;
  signal clk_5_sg_x14: std_logic;
  signal constant1_op_net_x1: std_logic;
  signal constant2_op_net_x1: std_logic_vector(5 downto 0);
  signal convert1_dout_net_x0: std_logic_vector(7 downto 0);
  signal counter_op_net_x1: std_logic_vector(4 downto 0);
  signal delay1_q_net_x2: std_logic;
  signal gain_net_x1: std_logic_vector(19 downto 0);
  signal offset_net_x1: std_logic_vector(9 downto 0);
  signal register16_q_net_x3: std_logic_vector(7 downto 0);
  signal register1_q_net_x3: std_logic;
  signal register2_q_net_x0: std_logic_vector(7 downto 0);
  signal shared_memory_data_out_net_x0: std_logic_vector(7 downto 0);
  signal shared_memory_data_out_net_x12: std_logic_vector(5 downto 0);
  signal single_port_ram_data_out_net_x13: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x14: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x15: std_logic_vector(7 downto 0);
  signal single_port_ram_data_out_net_x8: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x24 <= ce_1;
  ce_5_sg_x14 <= ce_5;
  clk_1_sg_x24 <= clk_1;
  clk_5_sg_x14 <= clk_5;
  shared_memory_data_out_net_x0 <= din;
  gain_net_x1 <= gain;
  offset_net_x1 <= offset;
  shared_memory_data_out_net_x12 <= shared_memory;
  register1_q_net_x3 <= valid_in;
  coefficient_memory <= constant1_op_net_x1;
  coefficient_memory_x0 <= constant2_op_net_x1;
  coefficient_memory_x1 <= counter_op_net_x1;
  dout <= register2_q_net_x0;
  valid_out <= asr_q_net_x1;

  coefficient_memory_fbd8310aed: entity work.coefficient_memory_entity_fbd8310aed
    port map (
      ce_1 => ce_1_sg_x24,
      clk_1 => clk_1_sg_x24,
      shared_memory => shared_memory_data_out_net_x12,
      constant1_x0 => constant1_op_net_x1,
      constant2_x0 => constant2_op_net_x1,
      counter_x0 => counter_op_net_x1,
      load => delay1_q_net_x2
    );

  register16: entity work.xlregister
    generic map (
      d_width => 8,
      init_value => b"00000000"
    )
    port map (
      ce => ce_5_sg_x14,
      clk => clk_5_sg_x14,
      d => shared_memory_data_out_net_x0,
      en => "1",
      rst => "0",
      q => register16_q_net_x3
    );

  register2: entity work.xlregister
    generic map (
      d_width => 8,
      init_value => b"00000000"
    )
    port map (
      ce => ce_5_sg_x14,
      clk => clk_5_sg_x14,
      d => convert1_dout_net_x0,
      en => "1",
      rst => "0",
      q => register2_q_net_x0
    );

  valid_generator_5f73eec251: entity work.valid_generator_entity_5f73eec251
    port map (
      ce_5 => ce_5_sg_x14,
      clk_5 => clk_5_sg_x14,
      offset => offset_net_x1,
      valid_in => register1_q_net_x3,
      valid_out => asr_q_net_x1
    );

  virtex2_5_line_buffer1_5e1c5ab2c0: entity work.virtex2_5_line_buffer1_entity_5e1c5ab2c0
    port map (
      ce_5 => ce_5_sg_x14,
      clk_5 => clk_5_sg_x14,
      in1 => register16_q_net_x3,
      line1 => single_port_ram_data_out_net_x15,
      line2 => single_port_ram_data_out_net_x14,
      line3 => single_port_ram_data_out_net_x13,
      line4 => single_port_ram_data_out_net_x8
    );

  x5x5filter_bcadfb37d3: entity work.x5x5filter_entity_bcadfb37d3
    port map (
      ce_1 => ce_1_sg_x24,
      ce_5 => ce_5_sg_x14,
      clk_1 => clk_1_sg_x24,
      clk_5 => clk_5_sg_x14,
      coef => shared_memory_data_out_net_x12,
      gain => gain_net_x1,
      line1 => single_port_ram_data_out_net_x15,
      line2 => single_port_ram_data_out_net_x14,
      line3 => single_port_ram_data_out_net_x13,
      line4 => single_port_ram_data_out_net_x8,
      line5 => register16_q_net_x3,
      load => delay1_q_net_x2,
      out1 => convert1_dout_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/Input Buffer/sr_flip_flop"

entity sr_flip_flop_entity_73c0535780 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    r: in std_logic; 
    s: in std_logic; 
    q: out std_logic
  );
end sr_flip_flop_entity_73c0535780;

architecture structural of sr_flip_flop_entity_73c0535780 is
  signal ce_5_sg_x16: std_logic;
  signal clk_5_sg_x16: std_logic;
  signal constant2_op_net: std_logic;
  signal logical_y_net_x1: std_logic;
  signal register1_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;

begin
  ce_5_sg_x16 <= ce_5;
  clk_5_sg_x16 <= clk_5;
  relational_op_net_x0 <= r;
  logical_y_net_x1 <= s;
  q <= register1_q_net_x0;

  constant2: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant2_op_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x16,
      clk => clk_5_sg_x16,
      d(0) => constant2_op_net,
      en(0) => logical_y_net_x1,
      rst(0) => relational_op_net_x0,
      q(0) => register1_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/Input Buffer"

entity input_buffer_entity_0c531f7c0c is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    obuf_grant_pulse: in std_logic; 
    shared_memory: in std_logic; 
    constant1_x0: out std_logic; 
    constant2_x0: out std_logic_vector(7 downto 0); 
    counter_x0: out std_logic_vector(13 downto 0); 
    dout_valid: out std_logic; 
    register2_x0: out std_logic
  );
end input_buffer_entity_0c531f7c0c;

architecture structural of input_buffer_entity_0c531f7c0c is
  signal ce_5_sg_x18: std_logic;
  signal clk_5_sg_x18: std_logic;
  signal constant1_op_net_x0: std_logic;
  signal constant2_op_net_x0: std_logic_vector(7 downto 0);
  signal constant_op_net: std_logic_vector(13 downto 0);
  signal counter_op_net_x0: std_logic_vector(13 downto 0);
  signal inverter_op_net: std_logic;
  signal logical_y_net: std_logic;
  signal logical_y_net_x1: std_logic;
  signal logical_y_net_x2: std_logic;
  signal register1_q_net_x0: std_logic;
  signal register1_q_net_x4: std_logic;
  signal register1_q_net_x5: std_logic;
  signal register2_q_net_x0: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x1: std_logic;
  signal shared_memory_grant_net_x0: std_logic;

begin
  ce_5_sg_x18 <= ce_5;
  clk_5_sg_x18 <= clk_5;
  logical_y_net_x2 <= obuf_grant_pulse;
  shared_memory_grant_net_x0 <= shared_memory;
  constant1_x0 <= constant1_op_net_x0;
  constant2_x0 <= constant2_op_net_x0;
  counter_x0 <= counter_op_net_x0;
  dout_valid <= register1_q_net_x5;
  register2_x0 <= register2_q_net_x0;

  constant1: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net_x0
    );

  constant2: entity work.constant_91ef1678ca
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant2_op_net_x0
    );

  constant_x0: entity work.constant_05d829d2cd
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  counter: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_138d9c313af3f244",
      op_arith => xlUnsigned,
      op_width => 14
    )
    port map (
      ce => ce_5_sg_x18,
      clk => clk_5_sg_x18,
      clr => '0',
      en(0) => logical_y_net,
      rst(0) => relational_op_net_x1,
      op => counter_op_net_x0
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_5_sg_x18,
      clk => clk_5_sg_x18,
      clr => '0',
      ip(0) => relational_op_net_x1,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => register1_q_net_x0,
      d1(0) => register1_q_net_x4,
      y(0) => logical_y_net
    );

  one_shot_9fc7e22902: entity work.one_shot_entity_04df8b5e78
    port map (
      ce_5 => ce_5_sg_x18,
      clk_5 => clk_5_sg_x18,
      din => register_q_net_x0,
      dout => logical_y_net_x1
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x18,
      clk => clk_5_sg_x18,
      d(0) => logical_y_net,
      en => "1",
      rst => "0",
      q(0) => register1_q_net_x5
    );

  register2: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x18,
      clk => clk_5_sg_x18,
      d(0) => inverter_op_net,
      en => "1",
      rst => "0",
      q(0) => register2_q_net_x0
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x18,
      clk => clk_5_sg_x18,
      d(0) => shared_memory_grant_net_x0,
      en => "1",
      rst => "0",
      q(0) => register_q_net_x0
    );

  relational: entity work.relational_d500ab1630
    port map (
      a => constant_op_net,
      b => counter_op_net_x0,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x1
    );

  sr_flip_flop1_a168594cd3: entity work.sr_flip_flop_entity_73c0535780
    port map (
      ce_5 => ce_5_sg_x18,
      clk_5 => clk_5_sg_x18,
      r => relational_op_net_x1,
      s => logical_y_net_x2,
      q => register1_q_net_x4
    );

  sr_flip_flop_73c0535780: entity work.sr_flip_flop_entity_73c0535780
    port map (
      ce_5 => ce_5_sg_x18,
      clk_5 => clk_5_sg_x18,
      r => relational_op_net_x1,
      s => logical_y_net_x1,
      q => register1_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex/Output Buffer"

entity output_buffer_entity_018add31c3 is
  port (
    ce_5: in std_logic; 
    clk_5: in std_logic; 
    din_valid: in std_logic; 
    shared_memory: in std_logic_vector(7 downto 0); 
    shared_memory_x0: in std_logic; 
    counter_x0: out std_logic_vector(13 downto 0); 
    delay_x0: out std_logic_vector(7 downto 0); 
    logical_x0: out std_logic; 
    obuf_grant_pulse: out std_logic; 
    register2_x0: out std_logic
  );
end output_buffer_entity_018add31c3;

architecture structural of output_buffer_entity_018add31c3 is
  signal asr_q_net_x2: std_logic;
  signal ce_5_sg_x21: std_logic;
  signal clk_5_sg_x21: std_logic;
  signal constant_op_net: std_logic_vector(13 downto 0);
  signal counter_op_net_x0: std_logic_vector(13 downto 0);
  signal delay_q_net_x0: std_logic_vector(7 downto 0);
  signal inverter_op_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal logical_y_net_x5: std_logic;
  signal register1_q_net_x0: std_logic;
  signal register2_q_net_x0: std_logic;
  signal register_q_net_x0: std_logic;
  signal relational_op_net_x0: std_logic;
  signal shared_memory_data_out_net_x0: std_logic_vector(7 downto 0);
  signal shared_memory_grant_net_x0: std_logic;

begin
  ce_5_sg_x21 <= ce_5;
  clk_5_sg_x21 <= clk_5;
  asr_q_net_x2 <= din_valid;
  shared_memory_data_out_net_x0 <= shared_memory;
  shared_memory_grant_net_x0 <= shared_memory_x0;
  counter_x0 <= counter_op_net_x0;
  delay_x0 <= delay_q_net_x0;
  logical_x0 <= logical_y_net_x0;
  obuf_grant_pulse <= logical_y_net_x5;
  register2_x0 <= register2_q_net_x0;

  constant_x0: entity work.constant_05d829d2cd
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  counter: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_138d9c313af3f244",
      op_arith => xlUnsigned,
      op_width => 14
    )
    port map (
      ce => ce_5_sg_x21,
      clk => clk_5_sg_x21,
      clr => '0',
      en(0) => logical_y_net_x0,
      rst(0) => relational_op_net_x0,
      op => counter_op_net_x0
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 8
    )
    port map (
      ce => ce_5_sg_x21,
      clk => clk_5_sg_x21,
      d => shared_memory_data_out_net_x0,
      en => '1',
      q => delay_q_net_x0
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_5_sg_x21,
      clk => clk_5_sg_x21,
      clr => '0',
      ip(0) => relational_op_net_x0,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => register1_q_net_x0,
      d1(0) => asr_q_net_x2,
      y(0) => logical_y_net_x0
    );

  one_shot_97009cc92a: entity work.one_shot_entity_04df8b5e78
    port map (
      ce_5 => ce_5_sg_x21,
      clk_5 => clk_5_sg_x21,
      din => register_q_net_x0,
      dout => logical_y_net_x5
    );

  register2: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x21,
      clk => clk_5_sg_x21,
      d(0) => inverter_op_net,
      en => "1",
      rst => "0",
      q(0) => register2_q_net_x0
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_5_sg_x21,
      clk => clk_5_sg_x21,
      d(0) => shared_memory_grant_net_x0,
      en => "1",
      rst => "0",
      q(0) => register_q_net_x0
    );

  relational: entity work.relational_d500ab1630
    port map (
      a => constant_op_net,
      b => counter_op_net_x0,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net_x0
    );

  sr_flip_flop_e81c0a6fa3: entity work.sr_flip_flop_entity_73c0535780
    port map (
      ce_5 => ce_5_sg_x21,
      clk_5 => clk_5_sg_x21,
      r => relational_op_net_x0,
      s => logical_y_net_x5,
      q => register1_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "conv5x5_video_ex"

entity conv5x5_video_ex is
  port (
    ce_1: in std_logic; 
    ce_5: in std_logic; 
    clk_1: in std_logic; 
    clk_5: in std_logic; 
    data_out_x0: in std_logic_vector(5 downto 0); 
    data_out_x1: in std_logic_vector(7 downto 0); 
    data_out_x2: in std_logic_vector(7 downto 0); 
    gain: in std_logic_vector(19 downto 0); 
    grant: in std_logic; 
    grant_x0: in std_logic; 
    offset: in std_logic_vector(9 downto 0); 
    addr: out std_logic_vector(4 downto 0); 
    addr_x0: out std_logic_vector(13 downto 0); 
    addr_x1: out std_logic_vector(13 downto 0); 
    data_in: out std_logic_vector(5 downto 0); 
    data_in_x0: out std_logic_vector(7 downto 0); 
    data_in_x1: out std_logic_vector(7 downto 0); 
    data_out: out std_logic_vector(7 downto 0); 
    req: out std_logic; 
    req_x0: out std_logic; 
    we: out std_logic; 
    we_x0: out std_logic; 
    we_x1: out std_logic
  );
end conv5x5_video_ex;

architecture structural of conv5x5_video_ex is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "conv5x5_video_ex,sysgen_core,{clock_period=20.00000000,clocking=Clock_Enables,compilation=ML605_(Point-to-point_Ethernet),sample_periods=1.00000000000 5.00000000000,testbench=0,total_blocks=406,xilinx_accumulator_block=5,xilinx_adder_subtracter_block=4,xilinx_addressable_shift_register_block=6,xilinx_arithmetic_relational_operator_block=21,xilinx_bit_slice_extractor_block=1,xilinx_bus_concatenator_block=10,xilinx_bus_multiplexer_block=1,xilinx_constant_block_block=31,xilinx_copyright_notice_block=1,xilinx_counter_block=19,xilinx_delay_block=23,xilinx_down_sampler_block=5,xilinx_dual_port_random_access_memory_block=5,xilinx_gateway_in_block=2,xilinx_gateway_out_block=1,xilinx_inverter_block=15,xilinx_logical_block_block=11,xilinx_multiplier_block=6,xilinx_negate_block_block=1,xilinx_register_block=31,xilinx_shared_memory_random_access_memory_block=3,xilinx_single_port_random_access_memory_block=4,xilinx_system_generator_block=1,xilinx_type_converter_block=12,}";

  signal addr_net: std_logic_vector(4 downto 0);
  signal addr_x0_net: std_logic_vector(13 downto 0);
  signal addr_x1_net: std_logic_vector(13 downto 0);
  signal asr_q_net_x2: std_logic;
  signal ce_1_sg_x25: std_logic;
  signal ce_5_sg_x22: std_logic;
  signal clk_1_sg_x25: std_logic;
  signal clk_5_sg_x22: std_logic;
  signal data_in_net: std_logic_vector(5 downto 0);
  signal data_in_x0_net: std_logic_vector(7 downto 0);
  signal data_in_x1_net: std_logic_vector(7 downto 0);
  signal data_out_net: std_logic_vector(7 downto 0);
  signal data_out_net_x0: std_logic_vector(5 downto 0);
  signal data_out_x0_net: std_logic_vector(7 downto 0);
  signal data_out_x1_net: std_logic_vector(7 downto 0);
  signal gain_net: std_logic_vector(19 downto 0);
  signal grant_net: std_logic;
  signal grant_x0_net: std_logic;
  signal logical_y_net_x5: std_logic;
  signal offset_net: std_logic_vector(9 downto 0);
  signal register1_q_net_x5: std_logic;
  signal req_net: std_logic;
  signal req_x0_net: std_logic;
  signal we_net: std_logic;
  signal we_x0_net: std_logic;
  signal we_x1_net: std_logic;

begin
  ce_1_sg_x25 <= ce_1;
  ce_5_sg_x22 <= ce_5;
  clk_1_sg_x25 <= clk_1;
  clk_5_sg_x22 <= clk_5;
  data_out_net_x0 <= data_out_x0;
  data_out_x0_net <= data_out_x1;
  data_out_x1_net <= data_out_x2;
  gain_net <= gain;
  grant_net <= grant;
  grant_x0_net <= grant_x0;
  offset_net <= offset;
  addr <= addr_net;
  addr_x0 <= addr_x0_net;
  addr_x1 <= addr_x1_net;
  data_in <= data_in_net;
  data_in_x0 <= data_in_x0_net;
  data_in_x1 <= data_in_x1_net;
  data_out <= data_out_net;
  req <= req_net;
  req_x0 <= req_x0_net;
  we <= we_net;
  we_x0 <= we_x0_net;
  we_x1 <= we_x1_net;

  input_buffer_0c531f7c0c: entity work.input_buffer_entity_0c531f7c0c
    port map (
      ce_5 => ce_5_sg_x22,
      clk_5 => clk_5_sg_x22,
      obuf_grant_pulse => logical_y_net_x5,
      shared_memory => grant_net,
      constant1_x0 => we_x0_net,
      constant2_x0 => data_in_x0_net,
      counter_x0 => addr_x0_net,
      dout_valid => register1_q_net_x5,
      register2_x0 => req_net
    );

  output_buffer_018add31c3: entity work.output_buffer_entity_018add31c3
    port map (
      ce_5 => ce_5_sg_x22,
      clk_5 => clk_5_sg_x22,
      din_valid => asr_q_net_x2,
      shared_memory => data_out_x1_net,
      shared_memory_x0 => grant_x0_net,
      counter_x0 => addr_x1_net,
      delay_x0 => data_out_net,
      logical_x0 => we_x1_net,
      obuf_grant_pulse => logical_y_net_x5,
      register2_x0 => req_x0_net
    );

  x5x5_filter_172a0d5e3e: entity work.x5x5_filter_entity_172a0d5e3e
    port map (
      ce_1 => ce_1_sg_x25,
      ce_5 => ce_5_sg_x22,
      clk_1 => clk_1_sg_x25,
      clk_5 => clk_5_sg_x22,
      din => data_out_x0_net,
      gain => gain_net,
      offset => offset_net,
      shared_memory => data_out_net_x0,
      valid_in => register1_q_net_x5,
      coefficient_memory => we_net,
      coefficient_memory_x0 => data_in_net,
      coefficient_memory_x1 => addr_net,
      dout => data_in_x1_net,
      valid_out => asr_q_net_x2
    );

end structural;
