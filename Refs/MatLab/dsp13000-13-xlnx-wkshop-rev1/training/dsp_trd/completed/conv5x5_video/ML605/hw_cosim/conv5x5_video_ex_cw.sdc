
define_attribute {clk} syn_maxfan {1000000}
define_attribute {n:default_clock_driver.xlclockdriver_5.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_5.ce_vec*} max_fanout {"REDUCE"}

define_scope_collection ce_5_dd930c91_group \
  {find -seq * -in [ expand -hier -from {n:ce_5_sg_x22} ]}

define_multicycle_path -from {$ce_5_dd930c91_group} \
  -to {$ce_5_dd930c91_group} 5
