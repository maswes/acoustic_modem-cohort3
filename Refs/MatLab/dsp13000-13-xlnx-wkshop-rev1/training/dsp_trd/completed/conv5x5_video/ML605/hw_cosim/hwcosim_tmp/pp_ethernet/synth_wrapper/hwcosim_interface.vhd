------------------------------------------------------------------------------
-- System Generator version 12.3 VHDL source file.
--
-- Copyright(C) 2010 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2010 Xilinx, Inc.  All rights
-- reserved.
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity hwcosim_shared_memory_lock_manager is
  port (
    clk : in  std_logic;
    hw_req : in  std_logic;
    hw_grant : out std_logic;
    sw_req : out std_logic;
    sw_grant : in  std_logic
  );
end hwcosim_shared_memory_lock_manager;

architecture rtl of hwcosim_shared_memory_lock_manager is

  signal hw_req_int : std_logic;
  signal hr_reg1 : std_logic;
  signal hr_reg1_en : std_logic;
  signal hr_reg2 : std_logic;
  signal sw_req_int : std_logic;
  signal sw_grant_int : std_logic;

begin
  sw_grant_int <= sw_grant;
  sw_req <= sw_req_int;
  hr_reg1_en <= not hr_reg2;

  -- Increase # pulses for hw_req signal
  process (clk, hw_req)
  begin
    if hw_req = '0' then
      hr_reg1 <= '0';
    elsif rising_edge(clk) then
      if hr_reg1_en = '1' then
        hr_reg1 <= '1';
      end if;
    end if;
  end process;

  process (clk)
  begin
    if rising_edge(clk) then
      hr_reg2 <= hr_reg1;
    end if;
  end process;

  hw_req_int <= hw_req and hr_reg1;

  -- Generate sw_req signal
  process (clk, hw_req_int)
  begin
    if hw_req_int = '0' then
      sw_req_int <= '0';
    elsif rising_edge(clk) then
      if sw_grant_int = '0' then
        sw_req_int <= '1';
      end if;
    end if;
  end process;

  -- Generate hw_grant signal
  process (clk, hw_req_int)
  begin
    if hw_req_int = '0' then
      hw_grant <= '0';
    elsif rising_edge(clk) then
      if sw_req_int = '1' and sw_grant_int = '1' then
        hw_grant <= '1';
      end if;
    end if;
  end process;
end rtl;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity hwcosim_shared_register is
  generic (
    WIDTH : integer := 32;
    INIT_VALUE : bit_vector := "00000000000000000000000000000000"
  );
  port (
    clk : in  std_logic;
    ce : in  std_logic;
    clr : in  std_logic;
    i : in  std_logic_vector(WIDTH - 1 downto 0);
    o: out std_logic_vector(WIDTH - 1 downto 0)
  );
end hwcosim_shared_register;

architecture structural of hwcosim_shared_register is
begin
  fd_prim_array: for index in 0 to WIDTH - 1 generate
    bit_is_0: if INIT_VALUE(index) = '0' generate
      fdre_comp: FDRE
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          r => clr
        );
    end generate;

    bit_is_1: if INIT_VALUE(index) = '1' generate
      fdse_comp: FDSE
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          s => clr
        );
    end generate;
  end generate;
end structural;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hwcosim_memory_map is
  port (
    mm_o_gain : out std_logic_vector(19 downto 0);
    mm_o_offset : out std_logic_vector(9 downto 0);
    mm_i_data_out : in  std_logic_vector(7 downto 0);
    mm_mem_shared_memory_web : out std_logic;
    mm_mem_shared_memory_doutb : in  std_logic_vector(5 downto 0);
    mm_mem_shared_memory_x0_web : out std_logic;
    mm_mem_shared_memory_x0_doutb : in  std_logic_vector(7 downto 0);
    mm_mem_shared_memory_x0_sw_req : in  std_logic;
    mm_mem_shared_memory_x0_sw_grant : out std_logic;
    mm_mem_shared_memory_x1_web : out std_logic;
    mm_mem_shared_memory_x1_doutb : in  std_logic_vector(7 downto 0);
    mm_mem_shared_memory_x1_sw_req : in  std_logic;
    mm_mem_shared_memory_x1_sw_grant : out std_logic;
    hwcosim_mm_clk : in  std_logic;
    hwcosim_mm_we : in  std_logic;
    hwcosim_mm_re : in  std_logic;
    hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
    hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
    hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
    hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
  );
end hwcosim_memory_map;

architecture behavioral of hwcosim_memory_map is

  function resize_vector(x : std_logic; size : integer) return std_logic_vector is
    variable result : std_logic_vector(size-1 downto 0) := (others => '0');
  begin
    result(0) := x;
    return result;
  end resize_vector;

  function resize_vector(x : std_logic_vector; size : integer) return std_logic_vector is
    variable temp : std_logic_vector(x'length-1 downto 0) := x;
    variable result : std_logic_vector(size-1 downto 0) := (others => '0');
  begin
    if temp'length < result'length then
      result(temp'length-1 downto 0) := temp;
    else
      result := temp(result'length-1 downto 0);
    end if;
    return result;
  end resize_vector;

  signal hwcosim_mm_data_out_bank0 : std_logic_vector(31 downto 0);

  signal int_o_gain : std_logic_vector(19 downto 0);
  signal int_o_offset : std_logic_vector(9 downto 0);
  signal int_i_data_out : std_logic_vector(7 downto 0);

begin

  process (hwcosim_mm_clk)
  begin
    if rising_edge(hwcosim_mm_clk) then
      if (hwcosim_mm_we = '1') and (unsigned(hwcosim_mm_bank_sel) = 0) then
        case to_integer(unsigned(hwcosim_mm_addr)) is
          when 0 => int_o_gain(19 downto 0) <= hwcosim_mm_data_in(19 downto 0);
          when 1 => int_o_offset(9 downto 0) <= hwcosim_mm_data_in(9 downto 0);
          when 2 => mm_mem_shared_memory_x0_sw_grant <= hwcosim_mm_data_in(0);
          when 3 => mm_mem_shared_memory_x1_sw_grant <= hwcosim_mm_data_in(0);
          when others => null;
        end case;
      else
        case to_integer(unsigned(hwcosim_mm_addr)) is
          when 0 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_data_out(7 downto 0), hwcosim_mm_data_out_bank0'length);
          when 1 => hwcosim_mm_data_out_bank0 <= resize_vector(mm_mem_shared_memory_x0_sw_req, hwcosim_mm_data_out_bank0'length);
          when 2 => hwcosim_mm_data_out_bank0 <= resize_vector(mm_mem_shared_memory_x1_sw_req, hwcosim_mm_data_out_bank0'length);
          when others => null;
        end case;
      end if;
    end if;
  end process;

  int_i_data_out <= mm_i_data_out;
  mm_o_gain <= int_o_gain;
  mm_o_offset <= int_o_offset;

  process (hwcosim_mm_bank_sel,
           mm_mem_shared_memory_doutb,
           mm_mem_shared_memory_x0_doutb,
           mm_mem_shared_memory_x1_doutb,
           hwcosim_mm_data_out_bank0)
  begin
    case to_integer(unsigned(hwcosim_mm_bank_sel)) is
      when 0 => hwcosim_mm_data_out <= hwcosim_mm_data_out_bank0;
      when 1 => hwcosim_mm_data_out <= resize_vector(mm_mem_shared_memory_doutb, hwcosim_mm_data_out'length);
      when 2 => hwcosim_mm_data_out <= resize_vector(mm_mem_shared_memory_x0_doutb, hwcosim_mm_data_out'length);
      when 3 => hwcosim_mm_data_out <= resize_vector(mm_mem_shared_memory_x1_doutb, hwcosim_mm_data_out'length);
      when others => hwcosim_mm_data_out <= std_logic_vector(to_unsigned(0, hwcosim_mm_data_out'length));
    end case;
  end process;

  mm_mem_shared_memory_web <= '1' when (unsigned(hwcosim_mm_bank_sel) = 1 and hwcosim_mm_we = '1') else '0';

  mm_mem_shared_memory_x0_web <= '1' when (unsigned(hwcosim_mm_bank_sel) = 2 and hwcosim_mm_we = '1') else '0';

  mm_mem_shared_memory_x1_web <= '1' when (unsigned(hwcosim_mm_bank_sel) = 3 and hwcosim_mm_we = '1') else '0';

end behavioral;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hwcosim_interface is
  port (
    hwcosim_sys_clk : in  std_logic;
    hwcosim_dut_fr_clk : in  std_logic;
    hwcosim_dut_ss_clk : in  std_logic;
    hwcosim_mm_we : in  std_logic;
    hwcosim_mm_re : in  std_logic;
    hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
    hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
    hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
    hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
  );
end hwcosim_interface;

architecture rtl of hwcosim_interface is
  component hwcosim_shared_memory_lock_manager is
    port (
      clk : in  std_logic;
      hw_req : in  std_logic;
      hw_grant : out std_logic;
      sw_req : out std_logic;
      sw_grant : in  std_logic
    );
  end component;

  component hwcosim_shared_register is
    generic (
      WIDTH : integer := 32;
      INIT_VALUE : bit_vector := "00000000000000000000000000000000"
    );
    port (
      clk : in  std_logic;
      ce : in  std_logic;
      clr : in  std_logic;
      i : in  std_logic_vector(WIDTH - 1 downto 0);
      o: out std_logic_vector(WIDTH - 1 downto 0)
    );
  end component;

  component hwcosim_memory_map is
    port (
      mm_o_gain : out std_logic_vector(19 downto 0);
      mm_o_offset : out std_logic_vector(9 downto 0);
      mm_i_data_out : in  std_logic_vector(7 downto 0);
      mm_mem_shared_memory_web : out std_logic;
      mm_mem_shared_memory_doutb : in  std_logic_vector(5 downto 0);
      mm_mem_shared_memory_x0_web : out std_logic;
      mm_mem_shared_memory_x0_doutb : in  std_logic_vector(7 downto 0);
      mm_mem_shared_memory_x0_sw_req : in  std_logic;
      mm_mem_shared_memory_x0_sw_grant : out std_logic;
      mm_mem_shared_memory_x1_web : out std_logic;
      mm_mem_shared_memory_x1_doutb : in  std_logic_vector(7 downto 0);
      mm_mem_shared_memory_x1_sw_req : in  std_logic;
      mm_mem_shared_memory_x1_sw_grant : out std_logic;
      hwcosim_mm_clk : in  std_logic;
      hwcosim_mm_we : in  std_logic;
      hwcosim_mm_re : in  std_logic;
      hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
      hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
      hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
      hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
    );
  end component;

  component conv5x5_video_ex_cw is
    port (
      ce : in std_logic;
      clk : in std_logic;
      data_out : out std_logic_vector(7 downto 0);
      gain : in std_logic_vector(19 downto 0);
      offset : in std_logic_vector(9 downto 0);
      shared_memory_addr : out std_logic_vector(4 downto 0);
      shared_memory_ce : out std_logic;
      shared_memory_clk : out std_logic;
      shared_memory_data_in : out std_logic_vector(5 downto 0);
      shared_memory_data_out : in std_logic_vector(5 downto 0);
      shared_memory_we : out std_logic_vector(0 downto 0);
      shared_memory_x0_addr : out std_logic_vector(13 downto 0);
      shared_memory_x0_ce : out std_logic;
      shared_memory_x0_clk : out std_logic;
      shared_memory_x0_data_in : out std_logic_vector(7 downto 0);
      shared_memory_x0_data_out : in std_logic_vector(7 downto 0);
      shared_memory_x0_grant : in std_logic_vector(0 downto 0);
      shared_memory_x0_req : out std_logic_vector(0 downto 0);
      shared_memory_x0_we : out std_logic_vector(0 downto 0);
      shared_memory_x1_addr : out std_logic_vector(13 downto 0);
      shared_memory_x1_ce : out std_logic;
      shared_memory_x1_clk : out std_logic;
      shared_memory_x1_data_in : out std_logic_vector(7 downto 0);
      shared_memory_x1_data_out : in std_logic_vector(7 downto 0);
      shared_memory_x1_grant : in std_logic_vector(0 downto 0);
      shared_memory_x1_req : out std_logic_vector(0 downto 0);
      shared_memory_x1_we : out std_logic_vector(0 downto 0)
    );
  end component;

  component block_memory_generator_virtex6_5_2_3daed311e964671d is
    port (
      clka : in  std_logic;
      ena : in  std_logic;
      wea : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(4 downto 0);
      dina : in  std_logic_vector(5 downto 0);
      douta : out std_logic_vector(5 downto 0);
      clkb : in  std_logic;
      enb : in  std_logic;
      web : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(4 downto 0);
      dinb : in  std_logic_vector(5 downto 0);
      doutb : out std_logic_vector(5 downto 0)
    );
  end component;

  component block_memory_generator_virtex6_5_2_91e4527b00c9951a is
    port (
      clka : in  std_logic;
      ena : in  std_logic;
      wea : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(13 downto 0);
      dina : in  std_logic_vector(7 downto 0);
      douta : out std_logic_vector(7 downto 0);
      clkb : in  std_logic;
      enb : in  std_logic;
      web : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(13 downto 0);
      dinb : in  std_logic_vector(7 downto 0);
      doutb : out std_logic_vector(7 downto 0)
    );
  end component;

  component block_memory_generator_virtex6_5_2_032643e233f3bae0 is
    port (
      clka : in  std_logic;
      ena : in  std_logic;
      wea : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(13 downto 0);
      dina : in  std_logic_vector(7 downto 0);
      douta : out std_logic_vector(7 downto 0);
      clkb : in  std_logic;
      enb : in  std_logic;
      web : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(13 downto 0);
      dinb : in  std_logic_vector(7 downto 0);
      doutb : out std_logic_vector(7 downto 0)
    );
  end component;

  attribute box_type : string;
  attribute box_type of conv5x5_video_ex_cw : component is "user_black_box";
  attribute box_type of block_memory_generator_virtex6_5_2_3daed311e964671d : component is "user_black_box";
  attribute box_type of block_memory_generator_virtex6_5_2_91e4527b00c9951a : component is "user_black_box";
  attribute box_type of block_memory_generator_virtex6_5_2_032643e233f3bae0 : component is "user_black_box";
  attribute syn_black_box : boolean;
  attribute syn_black_box of conv5x5_video_ex_cw : component is true;
  attribute syn_black_box of block_memory_generator_virtex6_5_2_3daed311e964671d : component is true;
  attribute syn_black_box of block_memory_generator_virtex6_5_2_91e4527b00c9951a : component is true;
  attribute syn_black_box of block_memory_generator_virtex6_5_2_032643e233f3bae0 : component is true;

  signal mm_o_gain : std_logic_vector(19 downto 0);
  signal mm_o_offset : std_logic_vector(9 downto 0);
  signal mm_i_data_out : std_logic_vector(7 downto 0);

  signal dut_o_ce : std_logic;
  signal dut_o_clk : std_logic;
  signal dut_o_gain : std_logic_vector(19 downto 0);
  signal dut_o_offset : std_logic_vector(9 downto 0);
  signal dut_o_shared_memory_data_out : std_logic_vector(5 downto 0);
  signal dut_o_shared_memory_x0_data_out : std_logic_vector(7 downto 0);
  signal dut_o_shared_memory_x0_grant : std_logic_vector(0 downto 0);
  signal dut_o_shared_memory_x1_data_out : std_logic_vector(7 downto 0);
  signal dut_o_shared_memory_x1_grant : std_logic_vector(0 downto 0);
  signal dut_i_data_out : std_logic_vector(7 downto 0);
  signal dut_i_shared_memory_addr : std_logic_vector(4 downto 0);
  signal dut_i_shared_memory_ce : std_logic;
  signal dut_i_shared_memory_clk : std_logic;
  signal dut_i_shared_memory_data_in : std_logic_vector(5 downto 0);
  signal dut_i_shared_memory_we : std_logic_vector(0 downto 0);
  signal dut_i_shared_memory_x0_addr : std_logic_vector(13 downto 0);
  signal dut_i_shared_memory_x0_ce : std_logic;
  signal dut_i_shared_memory_x0_clk : std_logic;
  signal dut_i_shared_memory_x0_data_in : std_logic_vector(7 downto 0);
  signal dut_i_shared_memory_x0_req : std_logic_vector(0 downto 0);
  signal dut_i_shared_memory_x0_we : std_logic_vector(0 downto 0);
  signal dut_i_shared_memory_x1_addr : std_logic_vector(13 downto 0);
  signal dut_i_shared_memory_x1_ce : std_logic;
  signal dut_i_shared_memory_x1_clk : std_logic;
  signal dut_i_shared_memory_x1_data_in : std_logic_vector(7 downto 0);
  signal dut_i_shared_memory_x1_req : std_logic_vector(0 downto 0);
  signal dut_i_shared_memory_x1_we : std_logic_vector(0 downto 0);

  signal mm_mem_shared_memory_clka : std_logic;
  signal mm_mem_shared_memory_ena : std_logic;
  signal mm_mem_shared_memory_wea : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_addra : std_logic_vector(4 downto 0);
  signal mm_mem_shared_memory_dina : std_logic_vector(5 downto 0);
  signal mm_mem_shared_memory_douta : std_logic_vector(5 downto 0);
  signal mm_mem_shared_memory_clkb : std_logic;
  signal mm_mem_shared_memory_enb : std_logic;
  signal mm_mem_shared_memory_web : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_addrb : std_logic_vector(4 downto 0);
  signal mm_mem_shared_memory_dinb : std_logic_vector(5 downto 0);
  signal mm_mem_shared_memory_doutb : std_logic_vector(5 downto 0);
  signal mm_mem_shared_memory_x0_clka : std_logic;
  signal mm_mem_shared_memory_x0_ena : std_logic;
  signal mm_mem_shared_memory_x0_wea : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x0_addra : std_logic_vector(13 downto 0);
  signal mm_mem_shared_memory_x0_dina : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x0_douta : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x0_clkb : std_logic;
  signal mm_mem_shared_memory_x0_enb : std_logic;
  signal mm_mem_shared_memory_x0_web : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x0_addrb : std_logic_vector(13 downto 0);
  signal mm_mem_shared_memory_x0_dinb : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x0_doutb : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x0_hw_req : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x0_hw_grant : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x0_sw_req : std_logic;
  signal mm_mem_shared_memory_x0_sw_grant : std_logic;
  signal mm_mem_shared_memory_x1_clka : std_logic;
  signal mm_mem_shared_memory_x1_ena : std_logic;
  signal mm_mem_shared_memory_x1_wea : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x1_addra : std_logic_vector(13 downto 0);
  signal mm_mem_shared_memory_x1_dina : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x1_douta : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x1_clkb : std_logic;
  signal mm_mem_shared_memory_x1_enb : std_logic;
  signal mm_mem_shared_memory_x1_web : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x1_addrb : std_logic_vector(13 downto 0);
  signal mm_mem_shared_memory_x1_dinb : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x1_doutb : std_logic_vector(7 downto 0);
  signal mm_mem_shared_memory_x1_hw_req : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x1_hw_grant : std_logic_vector(0 downto 0);
  signal mm_mem_shared_memory_x1_sw_req : std_logic;
  signal mm_mem_shared_memory_x1_sw_grant : std_logic;

begin

  hwcosim_memory_map_inst : hwcosim_memory_map
    port map (
      mm_o_gain => mm_o_gain,
      mm_o_offset => mm_o_offset,
      mm_i_data_out => mm_i_data_out,
      mm_mem_shared_memory_web => mm_mem_shared_memory_web(0),
      mm_mem_shared_memory_doutb => mm_mem_shared_memory_doutb,
      mm_mem_shared_memory_x0_web => mm_mem_shared_memory_x0_web(0),
      mm_mem_shared_memory_x0_doutb => mm_mem_shared_memory_x0_doutb,
      mm_mem_shared_memory_x0_sw_req => mm_mem_shared_memory_x0_sw_req,
      mm_mem_shared_memory_x0_sw_grant => mm_mem_shared_memory_x0_sw_grant,
      mm_mem_shared_memory_x1_web => mm_mem_shared_memory_x1_web(0),
      mm_mem_shared_memory_x1_doutb => mm_mem_shared_memory_x1_doutb,
      mm_mem_shared_memory_x1_sw_req => mm_mem_shared_memory_x1_sw_req,
      mm_mem_shared_memory_x1_sw_grant => mm_mem_shared_memory_x1_sw_grant,
      hwcosim_mm_clk => hwcosim_dut_fr_clk,
      hwcosim_mm_we => hwcosim_mm_we,
      hwcosim_mm_re => hwcosim_mm_re,
      hwcosim_mm_bank_sel => hwcosim_mm_bank_sel,
      hwcosim_mm_addr => hwcosim_mm_addr,
      hwcosim_mm_data_in => hwcosim_mm_data_in,
      hwcosim_mm_data_out => hwcosim_mm_data_out
    );

  hwcosim_dut_inst : conv5x5_video_ex_cw
    port map (
      ce => dut_o_ce,
      clk => hwcosim_dut_ss_clk,
      data_out => dut_i_data_out,
      gain => dut_o_gain,
      offset => dut_o_offset,
      shared_memory_addr => mm_mem_shared_memory_addra,
      shared_memory_ce => mm_mem_shared_memory_ena,
      shared_memory_clk => mm_mem_shared_memory_clka,
      shared_memory_data_in => mm_mem_shared_memory_dina,
      shared_memory_data_out => mm_mem_shared_memory_douta,
      shared_memory_we => mm_mem_shared_memory_wea,
      shared_memory_x0_addr => mm_mem_shared_memory_x0_addra,
      shared_memory_x0_ce => mm_mem_shared_memory_x0_ena,
      shared_memory_x0_clk => mm_mem_shared_memory_x0_clka,
      shared_memory_x0_data_in => mm_mem_shared_memory_x0_dina,
      shared_memory_x0_data_out => mm_mem_shared_memory_x0_douta,
      shared_memory_x0_grant => mm_mem_shared_memory_x0_hw_grant,
      shared_memory_x0_req => mm_mem_shared_memory_x0_hw_req,
      shared_memory_x0_we => mm_mem_shared_memory_x0_wea,
      shared_memory_x1_addr => mm_mem_shared_memory_x1_addra,
      shared_memory_x1_ce => mm_mem_shared_memory_x1_ena,
      shared_memory_x1_clk => mm_mem_shared_memory_x1_clka,
      shared_memory_x1_data_in => mm_mem_shared_memory_x1_dina,
      shared_memory_x1_data_out => mm_mem_shared_memory_x1_douta,
      shared_memory_x1_grant => mm_mem_shared_memory_x1_hw_grant,
      shared_memory_x1_req => mm_mem_shared_memory_x1_hw_req,
      shared_memory_x1_we => mm_mem_shared_memory_x1_wea
    );

  shared_memory : block_memory_generator_virtex6_5_2_3daed311e964671d
    port map (
      clka => mm_mem_shared_memory_clka,
      ena => mm_mem_shared_memory_ena,
      wea => mm_mem_shared_memory_wea,
      addra => mm_mem_shared_memory_addra,
      dina => mm_mem_shared_memory_dina,
      douta => mm_mem_shared_memory_douta,
      clkb => mm_mem_shared_memory_clkb,
      enb => mm_mem_shared_memory_enb,
      web => mm_mem_shared_memory_web,
      addrb => mm_mem_shared_memory_addrb,
      dinb => mm_mem_shared_memory_dinb,
      doutb => mm_mem_shared_memory_doutb
    );

  shared_memory_x0 : block_memory_generator_virtex6_5_2_91e4527b00c9951a
    port map (
      clka => mm_mem_shared_memory_x0_clka,
      ena => mm_mem_shared_memory_x0_ena,
      wea => mm_mem_shared_memory_x0_wea,
      addra => mm_mem_shared_memory_x0_addra,
      dina => mm_mem_shared_memory_x0_dina,
      douta => mm_mem_shared_memory_x0_douta,
      clkb => mm_mem_shared_memory_x0_clkb,
      enb => mm_mem_shared_memory_x0_enb,
      web => mm_mem_shared_memory_x0_web,
      addrb => mm_mem_shared_memory_x0_addrb,
      dinb => mm_mem_shared_memory_x0_dinb,
      doutb => mm_mem_shared_memory_x0_doutb
    );

  hwcosim_lock_shared_memory_x0 : hwcosim_shared_memory_lock_manager
    port map (
      clk => hwcosim_dut_fr_clk,
      hw_req => mm_mem_shared_memory_x0_hw_req(0),
      hw_grant => mm_mem_shared_memory_x0_hw_grant(0),
      sw_req => mm_mem_shared_memory_x0_sw_req,
      sw_grant => mm_mem_shared_memory_x0_sw_grant
    );

  shared_memory_x1 : block_memory_generator_virtex6_5_2_032643e233f3bae0
    port map (
      clka => mm_mem_shared_memory_x1_clka,
      ena => mm_mem_shared_memory_x1_ena,
      wea => mm_mem_shared_memory_x1_wea,
      addra => mm_mem_shared_memory_x1_addra,
      dina => mm_mem_shared_memory_x1_dina,
      douta => mm_mem_shared_memory_x1_douta,
      clkb => mm_mem_shared_memory_x1_clkb,
      enb => mm_mem_shared_memory_x1_enb,
      web => mm_mem_shared_memory_x1_web,
      addrb => mm_mem_shared_memory_x1_addrb,
      dinb => mm_mem_shared_memory_x1_dinb,
      doutb => mm_mem_shared_memory_x1_doutb
    );

  hwcosim_lock_shared_memory_x1 : hwcosim_shared_memory_lock_manager
    port map (
      clk => hwcosim_dut_fr_clk,
      hw_req => mm_mem_shared_memory_x1_hw_req(0),
      hw_grant => mm_mem_shared_memory_x1_hw_grant(0),
      sw_req => mm_mem_shared_memory_x1_sw_req,
      sw_grant => mm_mem_shared_memory_x1_sw_grant
    );

  dut_o_ce <= '1';
  dut_o_gain <= mm_o_gain;
  dut_o_offset <= mm_o_offset;
  mm_i_data_out <= dut_i_data_out;

  mm_mem_shared_memory_clkb <= hwcosim_dut_fr_clk;
  mm_mem_shared_memory_enb <= '1';
  mm_mem_shared_memory_addrb <= hwcosim_mm_addr(4 downto 0);
  mm_mem_shared_memory_dinb <= hwcosim_mm_data_in(5 downto 0);
  mm_mem_shared_memory_x0_clkb <= hwcosim_dut_fr_clk;
  mm_mem_shared_memory_x0_enb <= '1';
  mm_mem_shared_memory_x0_addrb <= hwcosim_mm_addr(13 downto 0);
  mm_mem_shared_memory_x0_dinb <= hwcosim_mm_data_in(7 downto 0);
  mm_mem_shared_memory_x1_clkb <= hwcosim_dut_fr_clk;
  mm_mem_shared_memory_x1_enb <= '1';
  mm_mem_shared_memory_x1_addrb <= hwcosim_mm_addr(13 downto 0);
  mm_mem_shared_memory_x1_dinb <= hwcosim_mm_data_in(7 downto 0);

end rtl;
