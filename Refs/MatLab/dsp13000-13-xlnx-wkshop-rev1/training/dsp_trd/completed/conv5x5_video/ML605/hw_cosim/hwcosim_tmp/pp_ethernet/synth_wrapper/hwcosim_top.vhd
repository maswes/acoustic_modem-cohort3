------------------------------------------------------------------------------
-- System Generator version 12.3 VHDL source file.
--
-- Copyright(C) 2010 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2010 Xilinx, Inc.  All rights
-- reserved.
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity hwcosim_top is
  port (
    ethernet_phy_rst_n : out std_logic;
    ethernet_phy_intr_n : in std_logic;
    ethernet_phy_gtx_clk : out std_logic;
    ethernet_phy_tx_clk : in std_logic;
    ethernet_phy_txd : out std_logic_vector(7 downto 0);
    ethernet_phy_tx_en : out std_logic;
    ethernet_phy_tx_er : out std_logic;
    ethernet_phy_rx_clk : in std_logic;
    ethernet_phy_rxd : in std_logic_vector(7 downto 0);
    ethernet_phy_rx_dv : in std_logic;
    ethernet_phy_rx_er : in std_logic;
    ethernet_phy_col : in std_logic;
    ethernet_phy_crs : in std_logic;
    ethernet_phy_mdc : out std_logic;
    ethernet_phy_mdio : inout std_logic;
    sysace_clk : in std_logic;
    sysace_mpa : out std_logic_vector(6 downto 0);
    sysace_mpd : inout std_logic_vector(7 downto 0);
    sysace_cen : out std_logic;
    sysace_oen : out std_logic;
    sysace_wen : out std_logic;
    sysace_mpirq : in std_logic;
    reset : in std_logic;
    hwcosim_sys_clk_p : in std_logic;
    hwcosim_sys_clk_n : in std_logic
  );
end hwcosim_top;

architecture rtl of hwcosim_top is

  component hwcosim_interface is
    port (
      hwcosim_sys_clk : in  std_logic;
      hwcosim_dut_fr_clk : in  std_logic;
      hwcosim_dut_ss_clk : in  std_logic;
      hwcosim_mm_we : in  std_logic;
      hwcosim_mm_re : in  std_logic;
      hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
      hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
      hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
      hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
    );
  end component;

  component eth_cosim_core is
    port (
      clk_125 : in std_logic;
      clk_200 : in std_logic;
      sys_clk : in std_logic;
      cosim_addr : out std_logic_vector(23 downto 0);
      cosim_bank_sel : out std_logic_vector(7 downto 0);
      cosim_clk_sel : out std_logic;
      cosim_data_in : out std_logic_vector(31 downto 0);
      cosim_data_out : in std_logic_vector(31 downto 0);
      cosim_sstep_clk : out std_logic;
      cosim_we : out std_logic;
      cosim_re : out std_logic;
      ethernet_phy_rst : out std_logic;
      ethernet_phy_intr : in std_logic;
      ethernet_phy_gtx_clk : out std_logic;
      ethernet_phy_tx_clk : in std_logic;
      ethernet_phy_txd : out std_logic_vector(7 downto 0);
      ethernet_phy_tx_en : out std_logic;
      ethernet_phy_tx_er : out std_logic;
      ethernet_phy_rx_clk : in std_logic;
      ethernet_phy_rxd : in std_logic_vector(7 downto 0);
      ethernet_phy_rx_dv : in std_logic;
      ethernet_phy_rx_er : in std_logic;
      ethernet_phy_col : in std_logic;
      ethernet_phy_crs : in std_logic;
      ethernet_phy_mdc : out std_logic;
      ethernet_phy_mdio : inout std_logic;
      sysace_clk : in std_logic;
      sysace_mpa : out std_logic_vector(6 downto 0);
      sysace_mpd : inout std_logic_vector(7 downto 0);
      sysace_cen : out std_logic;
      sysace_oen : out std_logic;
      sysace_wen : out std_logic;
      sysace_mpirq : in std_logic;
      reset : in std_logic
    );
  end component;

  attribute core_generation_info: string;
  attribute core_generation_info of rtl : architecture is "hwcosim_top,hwcosim,{Board=ml605,Interface=ppethernet,HDLLanguage=vhdl,Flow=sysgen,}";
  attribute box_type : string;
  attribute box_type of eth_cosim_core : component is "user_black_box";
  attribute syn_black_box : boolean;
  attribute syn_black_box of eth_cosim_core : component is true;
  attribute syn_noprune : boolean;
  attribute syn_noprune of eth_cosim_core : component is true;

  attribute buffer_type : string;
  signal clk_125_int : std_logic;
  signal clk_200_int : std_logic;
  signal sys_clk_int : std_logic;
  signal ethernet_phy_rst_int : std_logic;
  signal ethernet_phy_rst_inv : std_logic;
  signal ethernet_phy_intr_int : std_logic;
  signal ethernet_phy_intr_inv : std_logic;
  signal ethernet_phy_gtx_clk_int : std_logic;
  signal ethernet_phy_tx_clk_int : std_logic;
  signal ethernet_phy_txd_int : std_logic_vector(7 downto 0);
  signal ethernet_phy_tx_en_int : std_logic;
  signal ethernet_phy_tx_er_int : std_logic;
  signal ethernet_phy_rx_clk_int : std_logic;
  signal ethernet_phy_rxd_int : std_logic_vector(7 downto 0);
  signal ethernet_phy_rx_dv_int : std_logic;
  signal ethernet_phy_rx_er_int : std_logic;
  signal ethernet_phy_col_int : std_logic;
  signal ethernet_phy_crs_int : std_logic;
  signal ethernet_phy_mdc_int : std_logic;
  signal ethernet_phy_mdio_int : std_logic;
  attribute buffer_type of ethernet_phy_mdio_int : signal is "none";
  signal sysace_clk_int : std_logic;
  signal sysace_mpa_int : std_logic_vector(6 downto 0);
  signal sysace_mpd_int : std_logic_vector(7 downto 0);
  attribute buffer_type of sysace_mpd_int : signal is "none";
  signal sysace_cen_int : std_logic;
  signal sysace_oen_int : std_logic;
  signal sysace_wen_int : std_logic;
  signal sysace_mpirq_int : std_logic;
  signal reset_int : std_logic;

  signal hwcosim_sys_clk_ibuf : std_logic;
  signal hwcosim_sys_rst : std_logic;

  signal hwcosim_clkgen_mmcm_clkfb : std_logic;
  signal hwcosim_clkgen_mmcm_clkout0 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout1 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout2 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout3 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout4 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout5 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout6 : std_logic;
  signal hwcosim_clkgen_mmcm_clkout0_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout1_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout2_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout3_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout4_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout5_buf : std_logic;
  signal hwcosim_clkgen_mmcm_clkout6_buf : std_logic;
  signal hwcosim_clkgen_mmcm_locked : std_logic;

  signal hwcosim_dut_src_clk : std_logic;
  signal hwcosim_dut_fr_clk : std_logic;
  signal hwcosim_dut_ss_clk : std_logic;

  signal hwcosim_cosim_sstep_clk_int : std_logic;
  signal hwcosim_cosim_clk_sel_int : std_logic;
  signal hwcosim_cosim_we_int : std_logic;
  signal hwcosim_cosim_re_int : std_logic;
  signal hwcosim_cosim_bank_sel_int : std_logic_vector(7 downto 0);
  signal hwcosim_cosim_addr_int : std_logic_vector(23 downto 0);
  signal hwcosim_cosim_data_in_int : std_logic_vector(31 downto 0);
  signal hwcosim_cosim_data_out_int : std_logic_vector(31 downto 0);

begin

  ibufgds_hwcosim_sys_clk : IBUFGDS
    port map (
      I => hwcosim_sys_clk_p,
      IB => hwcosim_sys_clk_n,
      O => hwcosim_sys_clk_ibuf
    );

  hwcosim_clkgen_mmcm : MMCM_BASE
    generic map (
      CLKIN1_PERIOD => 5.0,
      CLKFBOUT_MULT_F => 5.0,
      DIVCLK_DIVIDE => 1,
      CLKOUT0_DIVIDE_F => 5.0,
      CLKOUT1_DIVIDE => 8,
      CLKOUT2_DIVIDE => 20,
      BANDWIDTH => "OPTIMIZED"
    )
    port map (
      CLKFBIN => hwcosim_clkgen_mmcm_clkfb,
      CLKIN1 => hwcosim_sys_clk_ibuf,
      PWRDWN => '0',
      RST => '0',
      CLKFBOUT => hwcosim_clkgen_mmcm_clkfb,
      CLKFBOUTB => open,
      CLKOUT0 => hwcosim_clkgen_mmcm_clkout0,
      CLKOUT0B => open,
      CLKOUT1 => hwcosim_clkgen_mmcm_clkout1,
      CLKOUT1B => open,
      CLKOUT2 => hwcosim_clkgen_mmcm_clkout2,
      CLKOUT2B => open,
      CLKOUT3 => hwcosim_clkgen_mmcm_clkout3,
      CLKOUT3B => open,
      CLKOUT4 => hwcosim_clkgen_mmcm_clkout4,
      CLKOUT5 => hwcosim_clkgen_mmcm_clkout5,
      CLKOUT6 => hwcosim_clkgen_mmcm_clkout6,
      LOCKED => hwcosim_clkgen_mmcm_locked
    );

  bufgce_hwcosim_clkgen_mmcm_clkout0 : BUFGCE
    port map (
      I => hwcosim_clkgen_mmcm_clkout0,
      CE => hwcosim_clkgen_mmcm_locked,
      O => hwcosim_clkgen_mmcm_clkout0_buf
    );

  bufgce_hwcosim_clkgen_mmcm_clkout1 : BUFGCE
    port map (
      I => hwcosim_clkgen_mmcm_clkout1,
      CE => hwcosim_clkgen_mmcm_locked,
      O => hwcosim_clkgen_mmcm_clkout1_buf
    );

  bufgce_hwcosim_clkgen_mmcm_clkout2 : BUFGCE
    port map (
      I => hwcosim_clkgen_mmcm_clkout2,
      CE => hwcosim_clkgen_mmcm_locked,
      O => hwcosim_clkgen_mmcm_clkout2_buf
    );


  bufgmux_comp1 : BUFGMUX
    port map (
      I0 => hwcosim_dut_src_clk,
      I1 => hwcosim_cosim_sstep_clk_int,
      S => '0',
      O => hwcosim_dut_fr_clk
    );

  bufgmux_comp2 : BUFGMUX
    port map (
      I0 => hwcosim_cosim_sstep_clk_int,
      I1 => hwcosim_dut_src_clk,
      S => hwcosim_cosim_clk_sel_int,
      O => hwcosim_dut_ss_clk
    );

  clk_125_int <= hwcosim_clkgen_mmcm_clkout1_buf;
  clk_200_int <= hwcosim_clkgen_mmcm_clkout0_buf;
  hwcosim_dut_src_clk <= hwcosim_clkgen_mmcm_clkout2_buf;
  sys_clk_int <= hwcosim_dut_fr_clk;

  hwcif : hwcosim_interface
    port map (
      hwcosim_sys_clk => hwcosim_sys_clk_ibuf,
      hwcosim_dut_fr_clk => hwcosim_dut_fr_clk,
      hwcosim_dut_ss_clk => hwcosim_dut_ss_clk,
      hwcosim_mm_we => hwcosim_cosim_we_int,
      hwcosim_mm_re => hwcosim_cosim_re_int,
      hwcosim_mm_bank_sel => hwcosim_cosim_bank_sel_int,
      hwcosim_mm_addr => hwcosim_cosim_addr_int,
      hwcosim_mm_data_in => hwcosim_cosim_data_in_int,
      hwcosim_mm_data_out => hwcosim_cosim_data_out_int
    );

  cosim_core_inst : eth_cosim_core
    port map (
      clk_125 => clk_125_int,
      clk_200 => clk_200_int,
      sys_clk => sys_clk_int,
      cosim_addr => hwcosim_cosim_addr_int,
      cosim_bank_sel => hwcosim_cosim_bank_sel_int,
      cosim_clk_sel => hwcosim_cosim_clk_sel_int,
      cosim_data_in => hwcosim_cosim_data_in_int,
      cosim_data_out => hwcosim_cosim_data_out_int,
      cosim_sstep_clk => hwcosim_cosim_sstep_clk_int,
      cosim_we => hwcosim_cosim_we_int,
      cosim_re => hwcosim_cosim_re_int,
      ethernet_phy_rst => ethernet_phy_rst_int,
      ethernet_phy_intr => ethernet_phy_intr_int,
      ethernet_phy_gtx_clk => ethernet_phy_gtx_clk_int,
      ethernet_phy_tx_clk => ethernet_phy_tx_clk_int,
      ethernet_phy_txd => ethernet_phy_txd_int,
      ethernet_phy_tx_en => ethernet_phy_tx_en_int,
      ethernet_phy_tx_er => ethernet_phy_tx_er_int,
      ethernet_phy_rx_clk => ethernet_phy_rx_clk_int,
      ethernet_phy_rxd => ethernet_phy_rxd_int,
      ethernet_phy_rx_dv => ethernet_phy_rx_dv_int,
      ethernet_phy_rx_er => ethernet_phy_rx_er_int,
      ethernet_phy_col => ethernet_phy_col_int,
      ethernet_phy_crs => ethernet_phy_crs_int,
      ethernet_phy_mdc => ethernet_phy_mdc_int,
      ethernet_phy_mdio => ethernet_phy_mdio_int,
      sysace_clk => sysace_clk_int,
      sysace_mpa => sysace_mpa_int,
      sysace_mpd => sysace_mpd_int,
      sysace_cen => sysace_cen_int,
      sysace_oen => sysace_oen_int,
      sysace_wen => sysace_wen_int,
      sysace_mpirq => sysace_mpirq_int,
      reset => reset_int
    );

  ethernet_phy_rst_inv <= not ethernet_phy_rst_int;
  ethernet_phy_intr_int <= not ethernet_phy_intr_inv;

  obuf_ethernet_phy_rst_n : OBUF
    port map (
      I => ethernet_phy_rst_inv,
      O => ethernet_phy_rst_n
    );

  ibuf_ethernet_phy_intr_n : IBUF
    port map (
      I => ethernet_phy_intr_n,
      O => ethernet_phy_intr_inv
    );

  obuf_ethernet_phy_gtx_clk : OBUF
    port map (
      I => ethernet_phy_gtx_clk_int,
      O => ethernet_phy_gtx_clk
    );

  ibuf_ethernet_phy_tx_clk : IBUF
    port map (
      I => ethernet_phy_tx_clk,
      O => ethernet_phy_tx_clk_int
    );

  ethernet_phy_txd_bus : for i in 7 downto 0 generate
    obuf_ethernet_phy_txd : OBUF
      port map (
        I => ethernet_phy_txd_int(i),
        O => ethernet_phy_txd(i)
      );
  end generate;

  obuf_ethernet_phy_tx_en : OBUF
    port map (
      I => ethernet_phy_tx_en_int,
      O => ethernet_phy_tx_en
    );

  obuf_ethernet_phy_tx_er : OBUF
    port map (
      I => ethernet_phy_tx_er_int,
      O => ethernet_phy_tx_er
    );

  ibufg_ethernet_phy_rx_clk : IBUFG
    port map (
      I => ethernet_phy_rx_clk,
      O => ethernet_phy_rx_clk_int
    );

  ethernet_phy_rxd_bus : for i in 7 downto 0 generate
    ibuf_ethernet_phy_rxd : IBUF
      port map (
        I => ethernet_phy_rxd(i),
        O => ethernet_phy_rxd_int(i)
      );
  end generate;

  ibuf_ethernet_phy_rx_dv : IBUF
    port map (
      I => ethernet_phy_rx_dv,
      O => ethernet_phy_rx_dv_int
    );

  ibuf_ethernet_phy_rx_er : IBUF
    port map (
      I => ethernet_phy_rx_er,
      O => ethernet_phy_rx_er_int
    );

  ibuf_ethernet_phy_col : IBUF
    port map (
      I => ethernet_phy_col,
      O => ethernet_phy_col_int
    );

  ibuf_ethernet_phy_crs : IBUF
    port map (
      I => ethernet_phy_crs,
      O => ethernet_phy_crs_int
    );

  obuf_ethernet_phy_mdc : OBUF
    port map (
      I => ethernet_phy_mdc_int,
      O => ethernet_phy_mdc
    );

  ethernet_phy_mdio <= ethernet_phy_mdio_int;

  ibuf_sysace_clk : IBUF
    port map (
      I => sysace_clk,
      O => sysace_clk_int
    );

  sysace_mpa_bus : for i in 6 downto 0 generate
    obuf_sysace_mpa : OBUF
      port map (
        I => sysace_mpa_int(i),
        O => sysace_mpa(i)
      );
  end generate;

  sysace_mpd <= sysace_mpd_int;

  obuf_sysace_cen : OBUF
    port map (
      I => sysace_cen_int,
      O => sysace_cen
    );

  obuf_sysace_oen : OBUF
    port map (
      I => sysace_oen_int,
      O => sysace_oen
    );

  obuf_sysace_wen : OBUF
    port map (
      I => sysace_wen_int,
      O => sysace_wen
    );

  ibuf_sysace_mpirq : IBUF
    port map (
      I => sysace_mpirq,
      O => sysace_mpirq_int
    );

  ibuf_reset : IBUF
    port map (
      I => reset,
      O => reset_int
    );
  hwcosim_sys_rst <= reset_int;

end rtl;
