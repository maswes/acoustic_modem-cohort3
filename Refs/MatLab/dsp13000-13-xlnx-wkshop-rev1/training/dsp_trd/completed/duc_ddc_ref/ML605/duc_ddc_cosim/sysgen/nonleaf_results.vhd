library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC/Complex Mixer"

entity complex_mixer_entity_e3ec7de3fc is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    freq_we: in std_logic; 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end complex_mixer_entity_e3ec7de3fc;

architecture structural of complex_mixer_entity_e3ec7de3fc is
  signal ce_1_sg_x0: std_logic;
  signal ce_6_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal clk_6_sg_x0: std_logic;
  signal complex_multiplier_3_1_pi_net: std_logic_vector(16 downto 0);
  signal complex_multiplier_3_1_pr_net: std_logic_vector(16 downto 0);
  signal convert1_dout_net: std_logic_vector(15 downto 0);
  signal convert1_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert_dout_net: std_logic_vector(15 downto 0);
  signal convert_dout_net_x1: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x0: std_logic;
  signal dds_compiler_4_0_cosine_net: std_logic_vector(18 downto 0);
  signal dds_compiler_4_0_sine_net: std_logic_vector(18 downto 0);
  signal delay1_q_net_x0: std_logic_vector(15 downto 0);
  signal delay2_q_net: std_logic;
  signal delay3_q_net_x0: std_logic;
  signal delay_q_net_x0: std_logic_vector(15 downto 0);
  signal freq_net_x0: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x0: std_logic;

begin
  ce_1_sg_x0 <= ce_1;
  ce_6_sg_x0 <= ce_6;
  clk_1_sg_x0 <= clk_1;
  clk_6_sg_x0 <= clk_6;
  convert_dout_net_x1 <= din_i;
  convert1_dout_net_x1 <= din_q;
  freq_net_x0 <= freq;
  freq_we_delay_q_net_x0 <= freq_we;
  data_valid_delay_q_net_x0 <= vin;
  dout_i <= delay_q_net_x0;
  dout_q <= delay1_q_net_x0;
  vout <= delay3_q_net_x0;

  complex_multiplier_3_1: entity work.xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f
    port map (
      ai => dds_compiler_4_0_sine_net,
      ar => dds_compiler_4_0_cosine_net,
      bi => convert1_dout_net_x1,
      br => convert_dout_net_x1,
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      pi => complex_multiplier_3_1_pi_net,
      pr => complex_multiplier_3_1_pr_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 17,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      clr => '0',
      din => complex_multiplier_3_1_pr_net,
      en => "1",
      dout => convert_dout_net
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 17,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      clr => '0',
      din => complex_multiplier_3_1_pi_net,
      en => "1",
      dout => convert1_dout_net
    );

  dds_compiler_4_0: entity work.xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      data => freq_net_x0,
      we => freq_we_delay_q_net_x0,
      cosine => dds_compiler_4_0_cosine_net,
      sine => dds_compiler_4_0_sine_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 16
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      d => convert_dout_net,
      en => '1',
      q => delay_q_net_x0
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 16
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      d => convert1_dout_net,
      en => '1',
      q => delay1_q_net_x0
    );

  delay2: entity work.xldelay
    generic map (
      latency => 30,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      d(0) => data_valid_delay_q_net_x0,
      en => '1',
      q(0) => delay2_q_net
    );

  delay3: entity work.xldelay
    generic map (
      latency => 6,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      d(0) => delay2_q_net,
      en => '1',
      q(0) => delay3_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC/Decimation FIR Filters"

entity decimation_fir_filters_entity_08685b80e0 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end decimation_fir_filters_entity_08685b80e0;

architecture structural of decimation_fir_filters_entity_08685b80e0 is
  signal ce_1_sg_x1: std_logic;
  signal clk_1_sg_x1: std_logic;
  signal convert1_dout_net: std_logic_vector(16 downto 0);
  signal convert2_dout_net: std_logic_vector(16 downto 0);
  signal convert4_dout_net: std_logic_vector(17 downto 0);
  signal convert5_dout_net: std_logic_vector(17 downto 0);
  signal convert6_dout_net_x0: std_logic_vector(15 downto 0);
  signal convert7_dout_net_x0: std_logic_vector(15 downto 0);
  signal delay1_q_net: std_logic_vector(16 downto 0);
  signal delay1_q_net_x1: std_logic_vector(15 downto 0);
  signal delay3_q_net_x1: std_logic;
  signal delay_q_net: std_logic_vector(16 downto 0);
  signal delay_q_net_x1: std_logic_vector(15 downto 0);
  signal imf1_dout_1_net: std_logic_vector(18 downto 0);
  signal imf1_dout_2_net: std_logic_vector(18 downto 0);
  signal imf1_rdy_net: std_logic;
  signal imf2_dout_1_net: std_logic_vector(17 downto 0);
  signal imf2_dout_2_net: std_logic_vector(17 downto 0);
  signal imf2_rdy_net: std_logic;
  signal srrc_dout_1_net: std_logic_vector(37 downto 0);
  signal srrc_dout_2_net: std_logic_vector(37 downto 0);
  signal srrc_rdy_net_x0: std_logic;

begin
  ce_1_sg_x1 <= ce_1;
  clk_1_sg_x1 <= clk_1;
  delay_q_net_x1 <= din_i;
  delay1_q_net_x1 <= din_q;
  delay3_q_net_x1 <= vin;
  dout_i <= convert6_dout_net_x0;
  dout_q <= convert7_dout_net_x0;
  vout <= srrc_rdy_net_x0;

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf2_dout_1_net,
      en => "1",
      dout => convert1_dout_net
    );

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf2_dout_2_net,
      en => "1",
      dout => convert2_dout_net
    );

  convert4: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf1_dout_1_net,
      en => "1",
      dout => convert4_dout_net
    );

  convert5: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf1_dout_2_net,
      en => "1",
      dout => convert5_dout_net
    );

  convert6: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 32,
      din_width => 38,
      dout_arith => 2,
      dout_bin_pt => 12,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => srrc_dout_1_net,
      en => "1",
      dout => convert6_dout_net_x0
    );

  convert7: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 32,
      din_width => 38,
      dout_arith => 2,
      dout_bin_pt => 12,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => srrc_dout_2_net,
      en => "1",
      dout => convert7_dout_net_x0
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 17
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      d => convert1_dout_net,
      en => '1',
      q => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 17
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      d => convert2_dout_net,
      en => '1',
      q => delay1_q_net
    );

  imf1: entity work.xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => delay_q_net,
      din_2 => delay1_q_net,
      nd => imf2_rdy_net,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => imf1_dout_1_net,
      dout_2 => imf1_dout_2_net,
      rdy => imf1_rdy_net
    );

  imf2: entity work.xlfir_compiler_8bbf91ad55b0353987e969c586c6098d
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => delay_q_net_x1,
      din_2 => delay1_q_net_x1,
      nd => delay3_q_net_x1,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => imf2_dout_1_net,
      dout_2 => imf2_dout_2_net,
      rdy => imf2_rdy_net
    );

  srrc: entity work.xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => convert4_dout_net,
      din_2 => convert5_dout_net,
      nd => imf1_rdy_net,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => srrc_dout_1_net,
      dout_2 => srrc_dout_2_net,
      rdy => srrc_rdy_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC"

entity ddc_entity_66861caa4b is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    freq_we: in std_logic; 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end ddc_entity_66861caa4b;

architecture structural of ddc_entity_66861caa4b is
  signal ce_1_sg_x2: std_logic;
  signal ce_6_sg_x1: std_logic;
  signal clk_1_sg_x2: std_logic;
  signal clk_6_sg_x1: std_logic;
  signal convert1_dout_net_x2: std_logic_vector(15 downto 0);
  signal convert6_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert7_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert_dout_net_x2: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x1: std_logic;
  signal delay1_q_net_x1: std_logic_vector(15 downto 0);
  signal delay3_q_net_x1: std_logic;
  signal delay_q_net_x1: std_logic_vector(15 downto 0);
  signal freq_net_x1: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x1: std_logic;
  signal srrc_rdy_net_x1: std_logic;

begin
  ce_1_sg_x2 <= ce_1;
  ce_6_sg_x1 <= ce_6;
  clk_1_sg_x2 <= clk_1;
  clk_6_sg_x1 <= clk_6;
  convert_dout_net_x2 <= din_i;
  convert1_dout_net_x2 <= din_q;
  freq_net_x1 <= freq;
  freq_we_delay_q_net_x1 <= freq_we;
  data_valid_delay_q_net_x1 <= vin;
  dout_i <= convert6_dout_net_x1;
  dout_q <= convert7_dout_net_x1;
  vout <= srrc_rdy_net_x1;

  complex_mixer_e3ec7de3fc: entity work.complex_mixer_entity_e3ec7de3fc
    port map (
      ce_1 => ce_1_sg_x2,
      ce_6 => ce_6_sg_x1,
      clk_1 => clk_1_sg_x2,
      clk_6 => clk_6_sg_x1,
      din_i => convert_dout_net_x2,
      din_q => convert1_dout_net_x2,
      freq => freq_net_x1,
      freq_we => freq_we_delay_q_net_x1,
      vin => data_valid_delay_q_net_x1,
      dout_i => delay_q_net_x1,
      dout_q => delay1_q_net_x1,
      vout => delay3_q_net_x1
    );

  decimation_fir_filters_08685b80e0: entity work.decimation_fir_filters_entity_08685b80e0
    port map (
      ce_1 => ce_1_sg_x2,
      clk_1 => clk_1_sg_x2,
      din_i => delay_q_net_x1,
      din_q => delay1_q_net_x1,
      vin => delay3_q_net_x1,
      dout_i => convert6_dout_net_x1,
      dout_q => convert7_dout_net_x1,
      vout => srrc_rdy_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Complex Mixer/one_shot"

entity one_shot_entity_995adbad00 is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    in_x0: in std_logic; 
    one_shot: out std_logic
  );
end one_shot_entity_995adbad00;

architecture structural of one_shot_entity_995adbad00 is
  signal ce_1_sg_x3: std_logic;
  signal ce_6_sg_x2: std_logic;
  signal clk_1_sg_x3: std_logic;
  signal clk_6_sg_x2: std_logic;
  signal constant1_op_net: std_logic;
  signal delay1_q_net: std_logic;
  signal delay_q_net: std_logic;
  signal down_sample_q_net: std_logic;
  signal inverter_op_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal register_q_net: std_logic;
  signal up_sample_q_net_x0: std_logic;

begin
  ce_1_sg_x3 <= ce_1;
  ce_6_sg_x2 <= ce_6;
  clk_1_sg_x3 <= clk_1;
  clk_6_sg_x2 <= clk_6;
  up_sample_q_net_x0 <= in_x0;
  one_shot <= logical_y_net_x0;

  constant1: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 2,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      d(0) => up_sample_q_net_x0,
      en => '1',
      q(0) => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      d(0) => inverter_op_net,
      en => '1',
      q(0) => delay1_q_net
    );

  down_sample: entity work.xldsamp
    generic map (
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 1,
      ds_ratio => 6,
      latency => 14,
      phase => 5,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 1
    )
    port map (
      d(0) => register_q_net,
      dest_ce => ce_6_sg_x2,
      dest_clk => clk_6_sg_x2,
      dest_clr => '0',
      en => "1",
      src_ce => ce_1_sg_x3,
      src_clk => clk_1_sg_x3,
      src_clr => '0',
      q(0) => down_sample_q_net
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      clr => '0',
      ip(0) => down_sample_q_net,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_799f62af22
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      clr => '0',
      d0(0) => delay1_q_net,
      d1(0) => down_sample_q_net,
      y(0) => logical_y_net_x0
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      d(0) => constant1_op_net,
      en(0) => delay_q_net,
      rst => "0",
      q(0) => register_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Complex Mixer"

entity complex_mixer_entity_a71e69fac1 is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(17 downto 0); 
    din_q: in std_logic_vector(17 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    freq_we: out std_logic; 
    vout: out std_logic
  );
end complex_mixer_entity_a71e69fac1;

architecture structural of complex_mixer_entity_a71e69fac1 is
  signal ce_1_sg_x4: std_logic;
  signal ce_6_sg_x3: std_logic;
  signal clk_1_sg_x4: std_logic;
  signal clk_6_sg_x3: std_logic;
  signal complex_multiplier_3_1_pi_net: std_logic_vector(35 downto 0);
  signal complex_multiplier_3_1_pr_net: std_logic_vector(35 downto 0);
  signal convert1_dout_net_x3: std_logic_vector(15 downto 0);
  signal convert_dout_net_x3: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x2: std_logic;
  signal dds_compiler_4_0_cosine_net: std_logic_vector(18 downto 0);
  signal dds_compiler_4_0_sine_net: std_logic_vector(18 downto 0);
  signal delay4_q_net_x0: std_logic_vector(17 downto 0);
  signal delay5_q_net_x0: std_logic_vector(17 downto 0);
  signal freq_we_delay_q_net_x2: std_logic;
  signal logical_y_net_x0: std_logic;
  signal register1_q_net_x0: std_logic_vector(27 downto 0);
  signal register4_q_net_x0: std_logic;
  signal up_sample_q_net_x0: std_logic;

begin
  ce_1_sg_x4 <= ce_1;
  ce_6_sg_x3 <= ce_6;
  clk_1_sg_x4 <= clk_1;
  clk_6_sg_x3 <= clk_6;
  delay5_q_net_x0 <= din_i;
  delay4_q_net_x0 <= din_q;
  register1_q_net_x0 <= freq;
  register4_q_net_x0 <= vin;
  dout_i <= convert_dout_net_x3;
  dout_q <= convert1_dout_net_x3;
  freq_we <= freq_we_delay_q_net_x2;
  vout <= data_valid_delay_q_net_x2;

  complex_multiplier_3_1: entity work.xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b
    port map (
      ai => dds_compiler_4_0_sine_net,
      ar => dds_compiler_4_0_cosine_net,
      bi => delay4_q_net_x0,
      br => delay5_q_net_x0,
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      pi => complex_multiplier_3_1_pi_net,
      pr => complex_multiplier_3_1_pr_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 35,
      din_width => 36,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      clr => '0',
      din => complex_multiplier_3_1_pr_net,
      en => "1",
      dout => convert_dout_net_x3
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 35,
      din_width => 36,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      clr => '0',
      din => complex_multiplier_3_1_pi_net,
      en => "1",
      dout => convert1_dout_net_x3
    );

  data_valid_delay: entity work.xldelay
    generic map (
      latency => 24,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d(0) => up_sample_q_net_x0,
      en => '1',
      q(0) => data_valid_delay_q_net_x2
    );

  dds_compiler_4_0: entity work.xldds_compiler_94274096bf4e0b94b9506335cad96a90
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      data => register1_q_net_x0,
      we => logical_y_net_x0,
      cosine => dds_compiler_4_0_cosine_net,
      sine => dds_compiler_4_0_sine_net
    );

  freq_we_delay: entity work.xldelay
    generic map (
      latency => 4,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      d(0) => logical_y_net_x0,
      en => '1',
      q(0) => freq_we_delay_q_net_x2
    );

  one_shot_995adbad00: entity work.one_shot_entity_995adbad00
    port map (
      ce_1 => ce_1_sg_x4,
      ce_6 => ce_6_sg_x3,
      clk_1 => clk_1_sg_x4,
      clk_6 => clk_6_sg_x3,
      in_x0 => up_sample_q_net_x0,
      one_shot => logical_y_net_x0
    );

  up_sample: entity work.xlusamp
    generic map (
      copy_samples => 0,
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 1,
      latency => 0,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 1
    )
    port map (
      d(0) => register4_q_net_x0,
      dest_ce => ce_1_sg_x4,
      dest_clk => clk_1_sg_x4,
      dest_clr => '0',
      en => "1",
      src_ce => ce_6_sg_x3,
      src_clk => clk_6_sg_x3,
      src_clr => '0',
      q(0) => up_sample_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Input Registers1"

entity input_registers1_entity_b4c9c3869f is
  port (
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    in1: in std_logic_vector(15 downto 0); 
    in2: in std_logic_vector(15 downto 0); 
    in3: in std_logic; 
    in4: in std_logic_vector(27 downto 0); 
    out1: out std_logic_vector(15 downto 0); 
    out2: out std_logic_vector(15 downto 0); 
    out3: out std_logic; 
    out4: out std_logic_vector(27 downto 0)
  );
end input_registers1_entity_b4c9c3869f;

architecture structural of input_registers1_entity_b4c9c3869f is
  signal ce_6_sg_x4: std_logic;
  signal ce_96_sg_x0: std_logic;
  signal clk_6_sg_x4: std_logic;
  signal clk_96_sg_x0: std_logic;
  signal din_i_net_x0: std_logic_vector(15 downto 0);
  signal din_q_net_x0: std_logic_vector(15 downto 0);
  signal freq_net_x2: std_logic_vector(27 downto 0);
  signal register1_q_net_x1: std_logic_vector(27 downto 0);
  signal register4_q_net_x1: std_logic;
  signal register5_q_net_x0: std_logic_vector(15 downto 0);
  signal register6_q_net_x0: std_logic_vector(15 downto 0);
  signal vin_net_x0: std_logic;

begin
  ce_6_sg_x4 <= ce_6;
  ce_96_sg_x0 <= ce_96;
  clk_6_sg_x4 <= clk_6;
  clk_96_sg_x0 <= clk_96;
  din_i_net_x0 <= in1;
  din_q_net_x0 <= in2;
  vin_net_x0 <= in3;
  freq_net_x2 <= in4;
  out1 <= register6_q_net_x0;
  out2 <= register5_q_net_x0;
  out3 <= register4_q_net_x1;
  out4 <= register1_q_net_x1;

  register1: entity work.xlregister
    generic map (
      d_width => 28,
      init_value => b"0000000000000000000000000000"
    )
    port map (
      ce => ce_6_sg_x4,
      clk => clk_6_sg_x4,
      d => freq_net_x2,
      en => "1",
      rst => "0",
      q => register1_q_net_x1
    );

  register4: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_6_sg_x4,
      clk => clk_6_sg_x4,
      d(0) => vin_net_x0,
      en => "1",
      rst => "0",
      q(0) => register4_q_net_x1
    );

  register5: entity work.xlregister
    generic map (
      d_width => 16,
      init_value => b"0000000000000000"
    )
    port map (
      ce => ce_96_sg_x0,
      clk => clk_96_sg_x0,
      d => din_q_net_x0,
      en => "1",
      rst => "0",
      q => register5_q_net_x0
    );

  register6: entity work.xlregister
    generic map (
      d_width => 16,
      init_value => b"0000000000000000"
    )
    port map (
      ce => ce_96_sg_x0,
      clk => clk_96_sg_x0,
      d => din_i_net_x0,
      en => "1",
      rst => "0",
      q => register6_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Interpolation Filters"

entity interpolation_filters_entity_5d99efe737 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    dout_i: out std_logic_vector(17 downto 0); 
    dout_q: out std_logic_vector(17 downto 0)
  );
end interpolation_filters_entity_5d99efe737;

architecture structural of interpolation_filters_entity_5d99efe737 is
  signal ce_12_sg_x0: std_logic;
  signal ce_1_sg_x5: std_logic;
  signal ce_24_sg_x0: std_logic;
  signal ce_3_sg_x0: std_logic;
  signal ce_48_sg_x0: std_logic;
  signal ce_6_sg_x5: std_logic;
  signal ce_96_sg_x1: std_logic;
  signal ce_logic_12_sg_x0: std_logic;
  signal ce_logic_24_sg_x0: std_logic;
  signal ce_logic_48_sg_x0: std_logic;
  signal ce_logic_6_sg_x0: std_logic;
  signal clk_12_sg_x0: std_logic;
  signal clk_1_sg_x5: std_logic;
  signal clk_24_sg_x0: std_logic;
  signal clk_3_sg_x0: std_logic;
  signal clk_48_sg_x0: std_logic;
  signal clk_6_sg_x5: std_logic;
  signal clk_96_sg_x1: std_logic;
  signal convert2_dout_net: std_logic_vector(16 downto 0);
  signal convert3_dout_net: std_logic_vector(16 downto 0);
  signal convert4_dout_net: std_logic_vector(17 downto 0);
  signal delay4_q_net_x1: std_logic_vector(17 downto 0);
  signal delay5_q_net_x1: std_logic_vector(17 downto 0);
  signal delay_q_net: std_logic_vector(17 downto 0);
  signal imf1_dout_net: std_logic_vector(17 downto 0);
  signal imf2_dout_net: std_logic_vector(31 downto 0);
  signal imf3_dout_net: std_logic_vector(18 downto 0);
  signal register5_q_net_x1: std_logic_vector(15 downto 0);
  signal register6_q_net_x1: std_logic_vector(15 downto 0);
  signal srrc_dout_net: std_logic_vector(15 downto 0);
  signal time_division_demultiplexer_q0_net: std_logic_vector(17 downto 0);
  signal time_division_demultiplexer_q1_net: std_logic_vector(17 downto 0);
  signal time_division_multiplexer_q_net: std_logic_vector(15 downto 0);

begin
  ce_1_sg_x5 <= ce_1;
  ce_12_sg_x0 <= ce_12;
  ce_24_sg_x0 <= ce_24;
  ce_3_sg_x0 <= ce_3;
  ce_48_sg_x0 <= ce_48;
  ce_6_sg_x5 <= ce_6;
  ce_96_sg_x1 <= ce_96;
  ce_logic_12_sg_x0 <= ce_logic_12;
  ce_logic_24_sg_x0 <= ce_logic_24;
  ce_logic_48_sg_x0 <= ce_logic_48;
  ce_logic_6_sg_x0 <= ce_logic_6;
  clk_1_sg_x5 <= clk_1;
  clk_12_sg_x0 <= clk_12;
  clk_24_sg_x0 <= clk_24;
  clk_3_sg_x0 <= clk_3;
  clk_48_sg_x0 <= clk_48;
  clk_6_sg_x5 <= clk_6;
  clk_96_sg_x1 <= clk_96;
  register6_q_net_x1 <= din_i;
  register5_q_net_x1 <= din_q;
  dout_i <= delay5_q_net_x1;
  dout_q <= delay4_q_net_x1;

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 30,
      din_width => 32,
      dout_arith => 2,
      dout_bin_pt => 16,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      clr => '0',
      din => imf2_dout_net,
      en => "1",
      dout => convert2_dout_net
    );

  convert3: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 16,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 16,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_12_sg_x0,
      clk => clk_12_sg_x0,
      clr => '0',
      din => imf1_dout_net,
      en => "1",
      dout => convert3_dout_net
    );

  convert4: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 17,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_3_sg_x0,
      clk => clk_3_sg_x0,
      clr => '0',
      din => imf3_dout_net,
      en => "1",
      dout => convert4_dout_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 4,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => time_division_demultiplexer_q0_net,
      en => '1',
      q => delay_q_net
    );

  delay4: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => time_division_demultiplexer_q1_net,
      en => '1',
      q => delay4_q_net_x1
    );

  delay5: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => delay_q_net,
      en => '1',
      q => delay5_q_net_x1
    );

  imf1: entity work.xlfir_compiler_6b538e2e420c55fbcb179300d415c30e
    port map (
      ce => ce_1_sg_x5,
      ce_12 => ce_12_sg_x0,
      ce_24 => ce_24_sg_x0,
      ce_logic_24 => ce_logic_24_sg_x0,
      clk => clk_1_sg_x5,
      clk_12 => clk_12_sg_x0,
      clk_24 => clk_24_sg_x0,
      clk_logic_24 => clk_24_sg_x0,
      din => srrc_dout_net,
      src_ce => ce_24_sg_x0,
      src_clk => clk_24_sg_x0,
      dout => imf1_dout_net
    );

  imf2: entity work.xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758
    port map (
      ce => ce_1_sg_x5,
      ce_12 => ce_12_sg_x0,
      ce_6 => ce_6_sg_x5,
      ce_logic_12 => ce_logic_12_sg_x0,
      clk => clk_1_sg_x5,
      clk_12 => clk_12_sg_x0,
      clk_6 => clk_6_sg_x5,
      clk_logic_12 => clk_12_sg_x0,
      din => convert3_dout_net,
      src_ce => ce_12_sg_x0,
      src_clk => clk_12_sg_x0,
      dout => imf2_dout_net
    );

  imf3: entity work.xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397
    port map (
      ce => ce_1_sg_x5,
      ce_3 => ce_3_sg_x0,
      ce_6 => ce_6_sg_x5,
      ce_logic_6 => ce_logic_6_sg_x0,
      clk => clk_1_sg_x5,
      clk_3 => clk_3_sg_x0,
      clk_6 => clk_6_sg_x5,
      clk_logic_6 => clk_6_sg_x5,
      din => convert2_dout_net,
      src_ce => ce_6_sg_x5,
      src_clk => clk_6_sg_x5,
      dout => imf3_dout_net
    );

  srrc: entity work.xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26
    port map (
      ce => ce_1_sg_x5,
      ce_24 => ce_24_sg_x0,
      ce_48 => ce_48_sg_x0,
      ce_logic_48 => ce_logic_48_sg_x0,
      clk => clk_1_sg_x5,
      clk_24 => clk_24_sg_x0,
      clk_48 => clk_48_sg_x0,
      clk_logic_48 => clk_48_sg_x0,
      din => time_division_multiplexer_q_net,
      src_ce => ce_48_sg_x0,
      src_clk => clk_48_sg_x0,
      dout => srrc_dout_net
    );

  time_division_demultiplexer: entity work.xltdd_multich
    generic map (
      data_width => 18,
      frame_length => 2
    )
    port map (
      d => convert4_dout_net,
      dest_ce => ce_6_sg_x5,
      dest_clk => clk_6_sg_x5,
      dest_clr => '0',
      src_ce => ce_3_sg_x0,
      src_clk => clk_3_sg_x0,
      src_clr => '0',
      q0 => time_division_demultiplexer_q0_net,
      q1 => time_division_demultiplexer_q1_net
    );

  time_division_multiplexer: entity work.xltdm
    generic map (
      data_width => 16,
      hasValid => 0,
      num_inputs => 2
    )
    port map (
      d0 => register6_q_net_x1,
      d1 => register5_q_net_x1,
      dest_ce => ce_48_sg_x0,
      dest_clk => clk_48_sg_x0,
      src_ce => ce_96_sg_x1,
      src_clk => clk_96_sg_x1,
      q => time_division_multiplexer_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC"

entity duc_entity_8eb6622f87 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    freq_we: out std_logic; 
    vout: out std_logic
  );
end duc_entity_8eb6622f87;

architecture structural of duc_entity_8eb6622f87 is
  signal ce_12_sg_x1: std_logic;
  signal ce_1_sg_x6: std_logic;
  signal ce_24_sg_x1: std_logic;
  signal ce_3_sg_x1: std_logic;
  signal ce_48_sg_x1: std_logic;
  signal ce_6_sg_x6: std_logic;
  signal ce_96_sg_x2: std_logic;
  signal ce_logic_12_sg_x1: std_logic;
  signal ce_logic_24_sg_x1: std_logic;
  signal ce_logic_48_sg_x1: std_logic;
  signal ce_logic_6_sg_x1: std_logic;
  signal clk_12_sg_x1: std_logic;
  signal clk_1_sg_x6: std_logic;
  signal clk_24_sg_x1: std_logic;
  signal clk_3_sg_x1: std_logic;
  signal clk_48_sg_x1: std_logic;
  signal clk_6_sg_x6: std_logic;
  signal clk_96_sg_x2: std_logic;
  signal convert1_dout_net_x4: std_logic_vector(15 downto 0);
  signal convert_dout_net_x4: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x3: std_logic;
  signal delay4_q_net_x1: std_logic_vector(17 downto 0);
  signal delay5_q_net_x1: std_logic_vector(17 downto 0);
  signal din_i_net_x1: std_logic_vector(15 downto 0);
  signal din_q_net_x1: std_logic_vector(15 downto 0);
  signal freq_net_x3: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x3: std_logic;
  signal register1_q_net_x1: std_logic_vector(27 downto 0);
  signal register4_q_net_x1: std_logic;
  signal register5_q_net_x1: std_logic_vector(15 downto 0);
  signal register6_q_net_x1: std_logic_vector(15 downto 0);
  signal vin_net_x1: std_logic;

begin
  ce_1_sg_x6 <= ce_1;
  ce_12_sg_x1 <= ce_12;
  ce_24_sg_x1 <= ce_24;
  ce_3_sg_x1 <= ce_3;
  ce_48_sg_x1 <= ce_48;
  ce_6_sg_x6 <= ce_6;
  ce_96_sg_x2 <= ce_96;
  ce_logic_12_sg_x1 <= ce_logic_12;
  ce_logic_24_sg_x1 <= ce_logic_24;
  ce_logic_48_sg_x1 <= ce_logic_48;
  ce_logic_6_sg_x1 <= ce_logic_6;
  clk_1_sg_x6 <= clk_1;
  clk_12_sg_x1 <= clk_12;
  clk_24_sg_x1 <= clk_24;
  clk_3_sg_x1 <= clk_3;
  clk_48_sg_x1 <= clk_48;
  clk_6_sg_x6 <= clk_6;
  clk_96_sg_x2 <= clk_96;
  din_i_net_x1 <= din_i;
  din_q_net_x1 <= din_q;
  freq_net_x3 <= freq;
  vin_net_x1 <= vin;
  dout_i <= convert_dout_net_x4;
  dout_q <= convert1_dout_net_x4;
  freq_we <= freq_we_delay_q_net_x3;
  vout <= data_valid_delay_q_net_x3;

  complex_mixer_a71e69fac1: entity work.complex_mixer_entity_a71e69fac1
    port map (
      ce_1 => ce_1_sg_x6,
      ce_6 => ce_6_sg_x6,
      clk_1 => clk_1_sg_x6,
      clk_6 => clk_6_sg_x6,
      din_i => delay5_q_net_x1,
      din_q => delay4_q_net_x1,
      freq => register1_q_net_x1,
      vin => register4_q_net_x1,
      dout_i => convert_dout_net_x4,
      dout_q => convert1_dout_net_x4,
      freq_we => freq_we_delay_q_net_x3,
      vout => data_valid_delay_q_net_x3
    );

  input_registers1_b4c9c3869f: entity work.input_registers1_entity_b4c9c3869f
    port map (
      ce_6 => ce_6_sg_x6,
      ce_96 => ce_96_sg_x2,
      clk_6 => clk_6_sg_x6,
      clk_96 => clk_96_sg_x2,
      in1 => din_i_net_x1,
      in2 => din_q_net_x1,
      in3 => vin_net_x1,
      in4 => freq_net_x3,
      out1 => register6_q_net_x1,
      out2 => register5_q_net_x1,
      out3 => register4_q_net_x1,
      out4 => register1_q_net_x1
    );

  interpolation_filters_5d99efe737: entity work.interpolation_filters_entity_5d99efe737
    port map (
      ce_1 => ce_1_sg_x6,
      ce_12 => ce_12_sg_x1,
      ce_24 => ce_24_sg_x1,
      ce_3 => ce_3_sg_x1,
      ce_48 => ce_48_sg_x1,
      ce_6 => ce_6_sg_x6,
      ce_96 => ce_96_sg_x2,
      ce_logic_12 => ce_logic_12_sg_x1,
      ce_logic_24 => ce_logic_24_sg_x1,
      ce_logic_48 => ce_logic_48_sg_x1,
      ce_logic_6 => ce_logic_6_sg_x1,
      clk_1 => clk_1_sg_x6,
      clk_12 => clk_12_sg_x1,
      clk_24 => clk_24_sg_x1,
      clk_3 => clk_3_sg_x1,
      clk_48 => clk_48_sg_x1,
      clk_6 => clk_6_sg_x6,
      clk_96 => clk_96_sg_x2,
      din_i => register6_q_net_x1,
      din_q => register5_q_net_x1,
      dout_i => delay5_q_net_x1,
      dout_q => delay4_q_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6"

entity duc_ddc_umts_virtex6 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dif_i: out std_logic_vector(15 downto 0); 
    dif_q: out std_logic_vector(15 downto 0); 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vif: out std_logic; 
    vout: out std_logic
  );
end duc_ddc_umts_virtex6;

architecture structural of duc_ddc_umts_virtex6 is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "duc_ddc_umts_virtex6,sysgen_core,{clock_period=20.00000000,clocking=Clock_Enables,compilation=ML605_(Point-to-point_Ethernet),sample_periods=1.00000000000 3.00000000000 6.00000000000 12.00000000000 24.00000000000 48.00000000000 96.00000000000,testbench=0,total_blocks=199,xilinx_complex_multiplier_3_1_block=2,xilinx_constant_block_block=1,xilinx_copyright_notice_block=1,xilinx_dds_compiler_4_0_block=2,xilinx_delay_block=13,xilinx_down_sampler_block=1,xilinx_fir_compiler_5_0_block=7,xilinx_gateway_in_block=4,xilinx_gateway_out_block=6,xilinx_inverter_block=1,xilinx_logical_block_block=1,xilinx_register_block=5,xilinx_system_generator_block=1,xilinx_time_division_demultiplexer_block=1,xilinx_time_division_multiplexer_block=1,xilinx_type_converter_block=13,xilinx_up_sampler_block=1,}";

  signal ce_12_sg_x2: std_logic;
  signal ce_1_sg_x7: std_logic;
  signal ce_24_sg_x2: std_logic;
  signal ce_3_sg_x2: std_logic;
  signal ce_48_sg_x2: std_logic;
  signal ce_6_sg_x7: std_logic;
  signal ce_96_sg_x3: std_logic;
  signal ce_logic_12_sg_x2: std_logic;
  signal ce_logic_24_sg_x2: std_logic;
  signal ce_logic_48_sg_x2: std_logic;
  signal ce_logic_6_sg_x2: std_logic;
  signal clk_12_sg_x2: std_logic;
  signal clk_1_sg_x7: std_logic;
  signal clk_24_sg_x2: std_logic;
  signal clk_3_sg_x2: std_logic;
  signal clk_48_sg_x2: std_logic;
  signal clk_6_sg_x7: std_logic;
  signal clk_96_sg_x3: std_logic;
  signal dif_i_net: std_logic_vector(15 downto 0);
  signal dif_q_net: std_logic_vector(15 downto 0);
  signal din_i_net: std_logic_vector(15 downto 0);
  signal din_q_net: std_logic_vector(15 downto 0);
  signal dout_i_net: std_logic_vector(15 downto 0);
  signal dout_q_net: std_logic_vector(15 downto 0);
  signal freq_net: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x3: std_logic;
  signal vif_net: std_logic;
  signal vin_net: std_logic;
  signal vout_net: std_logic;

begin
  ce_1_sg_x7 <= ce_1;
  ce_12_sg_x2 <= ce_12;
  ce_24_sg_x2 <= ce_24;
  ce_3_sg_x2 <= ce_3;
  ce_48_sg_x2 <= ce_48;
  ce_6_sg_x7 <= ce_6;
  ce_96_sg_x3 <= ce_96;
  ce_logic_12_sg_x2 <= ce_logic_12;
  ce_logic_24_sg_x2 <= ce_logic_24;
  ce_logic_48_sg_x2 <= ce_logic_48;
  ce_logic_6_sg_x2 <= ce_logic_6;
  clk_1_sg_x7 <= clk_1;
  clk_12_sg_x2 <= clk_12;
  clk_24_sg_x2 <= clk_24;
  clk_3_sg_x2 <= clk_3;
  clk_48_sg_x2 <= clk_48;
  clk_6_sg_x7 <= clk_6;
  clk_96_sg_x3 <= clk_96;
  din_i_net <= din_i;
  din_q_net <= din_q;
  freq_net <= freq;
  vin_net <= vin;
  dif_i <= dif_i_net;
  dif_q <= dif_q_net;
  dout_i <= dout_i_net;
  dout_q <= dout_q_net;
  vif <= vif_net;
  vout <= vout_net;

  ddc_66861caa4b: entity work.ddc_entity_66861caa4b
    port map (
      ce_1 => ce_1_sg_x7,
      ce_6 => ce_6_sg_x7,
      clk_1 => clk_1_sg_x7,
      clk_6 => clk_6_sg_x7,
      din_i => dif_i_net,
      din_q => dif_q_net,
      freq => freq_net,
      freq_we => freq_we_delay_q_net_x3,
      vin => vif_net,
      dout_i => dout_i_net,
      dout_q => dout_q_net,
      vout => vout_net
    );

  duc_8eb6622f87: entity work.duc_entity_8eb6622f87
    port map (
      ce_1 => ce_1_sg_x7,
      ce_12 => ce_12_sg_x2,
      ce_24 => ce_24_sg_x2,
      ce_3 => ce_3_sg_x2,
      ce_48 => ce_48_sg_x2,
      ce_6 => ce_6_sg_x7,
      ce_96 => ce_96_sg_x3,
      ce_logic_12 => ce_logic_12_sg_x2,
      ce_logic_24 => ce_logic_24_sg_x2,
      ce_logic_48 => ce_logic_48_sg_x2,
      ce_logic_6 => ce_logic_6_sg_x2,
      clk_1 => clk_1_sg_x7,
      clk_12 => clk_12_sg_x2,
      clk_24 => clk_24_sg_x2,
      clk_3 => clk_3_sg_x2,
      clk_48 => clk_48_sg_x2,
      clk_6 => clk_6_sg_x7,
      clk_96 => clk_96_sg_x3,
      din_i => din_i_net,
      din_q => din_q_net,
      freq => freq_net,
      vin => vin_net,
      dout_i => dif_i_net,
      dout_q => dif_q_net,
      freq_we => freq_we_delay_q_net_x3,
      vout => vif_net
    );

end structural;
