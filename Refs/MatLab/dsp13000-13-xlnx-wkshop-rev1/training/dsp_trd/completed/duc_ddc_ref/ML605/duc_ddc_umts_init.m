% Load all default workspace variables, including simulation results
% Any subsequent explicit variable declarations will overwrite

load duc_ddc_umts_virtex6;

% DUC Initialization

% Run the fixed point simulation model to get filter coeff and input data

sim_performance = 0;       % set to 1 if simulation metric needs to be run (slower)

run umts_duc_ddc_fix_point_model;

% load baseband data input from simulation model
din_I = real(u0_q);
din_Q = imag(u0_q);

% chip rate - set chip rate to 1 as the base unit 
% (it's not advisable to set it to a large number such as 3.84e6 as the reciprocal will be
% quite small and thus causes some numerical issues 
Fcr = 1; 
%Fcr = 3.84e6; %don't use

% number of TDM channles
NTDM = 2; 

% input sampling rate for TDM signal
Fsin = NTDM*Fcr; 

% DUC up-sampling rate
DucUpRate = 16; 

% overclocking relative to IF (IF rate = 61.44 Msps)
OverClock = 6; 

% desired FPGA operating frequency (FPGA rate = 368.64 MHz)
Fmax = Fcr*DucUpRate*OverClock; 

% over-clocking rate
MaxOverClock = Fmax/Fcr;

% DDS startup latency
DDSStartup = 7; 

% SFDR for DDS settings
SFDR = 105;

% Write enable latency
WEDelay = 2;

% sine/cosine mixer latency
MixLatency = 7 + WEDelay + 1;  % DDS sine/cosine start-up delay = 7, phase delay = 1 (start with phase 1)
complex_mult_latency = 6;   % Latency through Complex Multiplier 3.0

% DDC down-sampling rate
DdcDownRate = 16; 

% input sampling rate - real only signal
Fsin_ddc = Fcr*DdcDownRate;

% SFDR
SFDR = 105;