
define_attribute {clk} syn_maxfan {1000000}
define_attribute {n:default_clock_driver.xlclockdriver_12.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_12.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_24.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_24.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_3.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_3.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_48.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_48.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_6.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_6.ce_vec*} max_fanout {"REDUCE"}
define_attribute {n:default_clock_driver.xlclockdriver_96.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_96.ce_vec*} max_fanout {"REDUCE"}

define_scope_collection ce_12_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_12_sg_x2} ]}
define_scope_collection ce_24_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_24_sg_x2} ]}
define_scope_collection ce_3_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_3_sg_x2} ]}
define_scope_collection ce_48_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_48_sg_x2} ]}
define_scope_collection ce_6_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_6_sg_x7} ]}
define_scope_collection ce_96_49d24bf1_group \
  {find -seq * -in [ expand -hier -from {n:ce_96_sg_x3} ]}

define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 12
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 24
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 48
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 6
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 96

# Group-to-group constraints
define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 12
define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 12
define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 6
define_multicycle_path -from {$ce_12_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 12
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 12
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 24
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 6
define_multicycle_path -from {$ce_24_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 24
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 3
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 3
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 3
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 3
define_multicycle_path -from {$ce_3_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 3
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 12
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 24
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 6
define_multicycle_path -from {$ce_48_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 48
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 6
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 6
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 6
define_multicycle_path -from {$ce_6_49d24bf1_group} \
  -to {$ce_96_49d24bf1_group} 6
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_12_49d24bf1_group} 12
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_24_49d24bf1_group} 24
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_3_49d24bf1_group} 3
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_48_49d24bf1_group} 48
define_multicycle_path -from {$ce_96_49d24bf1_group} \
  -to {$ce_6_49d24bf1_group} 6

# LOC constraints
define_attribute   {clk} xc_loc {Fixed}
