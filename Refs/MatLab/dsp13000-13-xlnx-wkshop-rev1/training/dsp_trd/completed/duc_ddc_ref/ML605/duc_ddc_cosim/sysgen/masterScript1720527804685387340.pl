
open(PIDFILE, '> pidfile.txt') || die 'Couldn\'t write process ID to file.';
print PIDFILE "$$\n";
close(PIDFILE);

eval {
  # Call script(s).
  my $instrs;
  my $results = [];
$ENV{'SYSGEN'} = 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen';
  use Sg;
  $instrs = {
    'HDLCodeGenStatus' => 0.0,
    'HDL_PATH' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605',
    'TEMP' => 'C:/TEMP',
    'TMP' => 'C:/TEMP',
    'Temp' => 'C:/TEMP',
    'Tmp' => 'C:/TEMP',
    'base_system_period_hardware' => 20.0,
    'base_system_period_simulink' => 0.010416666666666666,
    'block_icon_display' => 'Default',
    'block_type' => 'sysgen',
    'block_version' => '8.2.02',
    'ce_clr' => 0.0,
    'clock_domain' => 'default',
    'clock_loc' => 'Fixed',
    'clock_settings' => {
      'dut_period_allowed' => '[10,15,20,30]',
      'dut_period_default' => '20',
      'source_period' => '5',
    },
    'clock_wrapper' => 'Clock Enables',
    'compilation' => 'ML605 (Point-to-point Ethernet)',
    'compilation_lut' => {
      'keys' => [
        'HDL Netlist',
        'Bitstream',
        'ML501 (Point-to-point Ethernet)',
        'ML605 (Point-to-point Ethernet)',
        'NGC Netlist',
        'Timing and Power Analysis',
      ],
      'values' => [
        'target1',
        'target2',
        'target3',
        'target4',
        'target5',
        'target6',
      ],
    },
    'compilation_target' => 'ML605 (Point-to-point Ethernet)',
    'core_generation' => 1.0,
    'core_generation_sgadvanced' => '',
    'core_is_deployed' => 0.0,
    'coregen_core_generation_tmpdir' => 'C:/TEMP/sysgentmp-skoppol/cg_wk/c8e106dc6f1661582',
    'coregen_part_family' => 'virtex6',
    'cosim_library' => 'PPEthernetRuntimeCosim_r4',
    'createTestbench' => 0,
    'create_interface_document' => 'off',
    'dbl_ovrd' => -1.0,
    'dbl_ovrd_sgadvanced' => '',
    'dcm_input_clock_period' => 100.0,
    'deprecated_control' => 'off',
    'deprecated_control_sgadvanced' => '',
    'design' => 'duc_ddc_umts_virtex6',
    'design_full_path' => 'c:\\training\\dsp_trd\\labs\\duc_ddc_ref\\ML605\\duc_ddc_umts_virtex6.mdl',
    'device' => 'xc6vlx240t-1ff1156',
    'device_speed' => '-1',
    'directory' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim',
    'dsp_cache_root_path' => 'C:/TEMP/sysgentmp-skoppol',
    'eval_field' => '0',
    'fileDeliveryDefaults' => [
      [
        '(?i)\\.vhd$',
        { 'fileName' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/perl_results.vhd', },
      ],
      [
        '(?i)\\.v$',
        { 'fileName' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/perl_results.v', },
      ],
    ],
    'fxdptinstalled' => 1.0,
    'generateUsing71FrontEnd' => 1,
    'generating_island_subsystem_handle' => 2075.00048828125,
    'generating_subsystem_handle' => 2075.00048828125,
    'generation_directory' => './duc_ddc_cosim',
    'getimportblock_fcn' => 'xlGetHwcosimBlockName',
    'has_advanced_control' => '0',
    'has_system_reset' => 1,
    'hdlDir' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl',
    'hdlKind' => 'vhdl',
    'hdl_path' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605',
    'hwcosim_board' => 'ml605-ppethernet',
    'incr_netlist' => 'off',
    'incr_netlist_sgadvanced' => '',
    'infoedit' => ' System Generator',
    'isdeployed' => 0,
    'ise_version' => '13.1i',
    'master_sysgen_token_handle' => 2076.0003662109375,
    'matlab' => 'C:/Program Files/MATLAB/R2010b',
    'matlab_fixedpoint' => 1.0,
    'mdlHandle' => 2075.00048828125,
    'mdlPath' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_umts_virtex6.mdl',
    'modelDiagnostics' => [
      {
        'count' => 199.0,
        'isMask' => 0.0,
        'type' => 'duc_ddc_umts_virtex6 Total blocks',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'Constant',
      },
      {
        'count' => 2.0,
        'isMask' => 0.0,
        'type' => 'DiscretePulseGenerator',
      },
      {
        'count' => 3.0,
        'isMask' => 0.0,
        'type' => 'FrameConversion',
      },
      {
        'count' => 2.0,
        'isMask' => 0.0,
        'type' => 'FromWorkspace',
      },
      {
        'count' => 40.0,
        'isMask' => 0.0,
        'type' => 'Inport',
      },
      {
        'count' => 6.0,
        'isMask' => 0.0,
        'type' => 'Mux',
      },
      {
        'count' => 33.0,
        'isMask' => 0.0,
        'type' => 'Outport',
      },
      {
        'count' => 60.0,
        'isMask' => 0.0,
        'type' => 'S-Function',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'Step',
      },
      {
        'count' => 24.0,
        'isMask' => 0.0,
        'type' => 'SubSystem',
      },
      {
        'count' => 21.0,
        'isMask' => 0.0,
        'type' => 'Terminator',
      },
      {
        'count' => 6.0,
        'isMask' => 0.0,
        'type' => 'ToWorkspace',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'DSP Constant',
      },
      {
        'count' => 6.0,
        'isMask' => 1.0,
        'type' => 'Replaceable Contents',
      },
      {
        'count' => 2.0,
        'isMask' => 1.0,
        'type' => 'Signal From Workspace',
      },
      {
        'count' => 6.0,
        'isMask' => 1.0,
        'type' => 'Signal To Workspace',
      },
      {
        'count' => 2.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Complex Multiplier 3.1 Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Constant Block Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Copyright Notice Block',
      },
      {
        'count' => 2.0,
        'isMask' => 1.0,
        'type' => 'Xilinx DDS Compiler 4.0 Block',
      },
      {
        'count' => 13.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Delay Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Down Sampler Block',
      },
      {
        'count' => 7.0,
        'isMask' => 1.0,
        'type' => 'Xilinx FIR Compiler 5.0 Block',
      },
      {
        'count' => 4.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Gateway In Block',
      },
      {
        'count' => 6.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Gateway Out Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Inverter Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Logical Block Block',
      },
      {
        'count' => 5.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Register Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx System Generator Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Time Division Demultiplexer Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Time Division Multiplexer Block',
      },
      {
        'count' => 13.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Type Converter Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Up Sampler Block',
      },
    ],
    'model_globals_initialized' => 1.0,
    'model_path' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_umts_virtex6.mdl',
    'myxilinx' => 'C:/Xilinx/13.1/ISE_DS/ISE',
    'ngc_files' => [ 'xlpersistentdff.ngc', ],
    'num_sim_cycles' => '192000',
    'package' => 'ff1156',
    'part' => 'xc6vlx240t',
    'partFamily' => 'virtex6',
    'port_data_types_enabled' => 1.0,
    'postgeneration_fcn' => 'xlHwcosimPostGeneration',
    'precompile_fcn' => 'xlEthernetPreCompile',
    'preserve_hierarchy' => 0.0,
    'run_coregen' => 'off',
    'run_coregen_sgadvanced' => '',
    'sample_time_colors_enabled' => 0.0,
    'sampletimecolors' => 0.0,
    'settings_fcn' => 'xlHwcosimSettings',
    'sg_blockgui_xml' => '<!--  *  Copyright (c) 2005, Xilinx, Inc.  All Rights Reserved.            --><!--  *  Reproduction or reuse, in any form, without the explicit written  --><!--  *  consent of Xilinx, Inc., is strictly prohibited.                  --><sysgenblock has_userdata="true" tag="genX" block_type="sysgen" simulinkname=" System Generator" >
 <icon width="51" bg_color="beige" height="50" caption_format="System\\nGenerator" wmark_color="red" />
 <callbacks DeleteFcn="xlSysgenGUI(\'delete\', gcs, gcbh);" OpenFcn="xlSysgenGUI(\'startup\',gcs,gcbh)" ModelCloseFcn="xlSysgenGUI(\'Close\',gcs,gcbh)" PostSaveFcn="xlSysgenGUI(\'Save\')" />
 <libraries>
  <library name="xbsIndex" />
  <library name="xbsBasic" />
  <library name="xbsTools" />
 </libraries>
 <subsystem_model file="system_generator_subsystem.mdl" />
 <blockgui label="Xilinx System Generator" >
  <editbox evaluate="false" multi_line="true" name="infoedit" read_only="true" default=" System Generator" />
  <editbox evaluate="false" name="xilinxfamily" default="Virtex4" label="Xilinx family" />
  <editbox evaluate="false" name="part" default="xc4vsx35" label="Part" />
  <editbox evaluate="false" name="speed" default="-10" label="Speed" />
  <editbox evaluate="false" name="package" default="ff668" label="Package" />
  <listbox evaluate="true" name="synthesis_tool" default="XST" label="Synthesis tool" >
   <item value="Spectrum" />
   <item value="Synplify" />
   <item value="Synplify Pro" />
   <item value="XST" />
   <item value="Precision" />
  </listbox>
  <editbox evaluate="false" name="directory" default="./netlist" label="Target directory" />
  <checkbox evaluate="true" name="testbench" default="off" label="Testbench" />
  <editbox evaluate="true" name="simulink_period" default="1" label="Simulink period" />
  <editbox evaluate="true" name="sysclk_period" default="10" label="System clock period" />
  <checkbox evaluate="true" name="incr_netlist" default="off" label="Incremental netlisting" />
  <listbox evaluate="true" name="trim_vbits" default="Everywhere in SubSystem" label="Trim valid bits" >
   <item value="According to Block Masks" />
   <item value="Everywhere in SubSystem" />
   <item value="No Where in SubSystem" />
  </listbox>
  <listbox evaluate="true" name="dbl_ovrd" default="According to Block Masks" label="Override with doubles" >
   <item value="According to Block Masks" />
   <item value="Everywhere in SubSystem" />
   <item value="No Where in SubSystem" />
  </listbox>
  <listbox evaluate="true" name="core_generation" default="According to Block Masks" label="Generate cores" >
   <item value="According to Block Masks" />
   <item value="Everywhere Available" />
   <item value="Not Needed - Already Generated" />
  </listbox>
  <checkbox evaluate="true" name="run_coregen" default="off" label="Run CoreGen" />
  <checkbox evaluate="true" name="deprecated_control" default="off" label="Show deprecated controls" />
  <hiddenvar evaluate="true" name="eval_field" default="0" />
 </blockgui>
</sysgenblock>
',
    'sg_icon_stat' => '51,50,-1,-1,red,beige,0,07734',
    'sg_list_contents' => '',
    'sg_mask_display' => 'fprintf(\'\',\'COMMENT: begin icon graphics\');
patch([0 51 51 0 ],[0 0 50 50 ],[0.93 0.92 0.86]);
patch([12 4 16 4 12 25 29 33 47 36 25 17 29 17 25 36 47 33 29 25 12 ],[5 13 25 37 45 45 41 45 45 34 45 37 25 13 5 16 5 5 9 5 5 ],[0.6 0.2 0.25]);
plot([0 0 51 51 0 ],[0 50 50 0 0 ]);
fprintf(\'\',\'COMMENT: end icon graphics\');
fprintf(\'\',\'COMMENT: begin icon text\');
fprintf(\'\',\'COMMENT: end icon text\');
',
    'sg_version' => '',
    'sggui_pos' => '-1,-1,-1,-1',
    'simulation_island_subsystem_handle' => 2075.00048828125,
    'simulink_accelerator_running' => 0.0,
    'simulink_debugger_running' => 0.0,
    'simulink_period' => 0.010416666666666666,
    'speed' => '-1',
    'synthesisTool' => 'XST',
    'synthesis_language' => 'vhdl',
    'synthesis_tool' => 'XST',
    'synthesis_tool_sgadvanced' => '',
    'sysclk_period' => 20.0,
    'sysgen' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'sysgenRoot' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'sysgenTokenSettings' => {
      'base_system_period_hardware' => 20.0,
      'base_system_period_simulink' => 0.010416666666666666,
      'block_icon_display' => 'Default',
      'block_type' => 'sysgen',
      'block_version' => '8.2.02',
      'ce_clr' => 0.0,
      'clock_loc' => 'Fixed',
      'clock_settings' => {
        'dut_period_allowed' => '[10,15,20,30]',
        'dut_period_default' => '20',
        'source_period' => '5',
      },
      'clock_wrapper' => 'Clock Enables',
      'compilation' => 'ML605 (Point-to-point Ethernet)',
      'compilation_lut' => {
        'keys' => [
          'HDL Netlist',
          'Bitstream',
          'ML501 (Point-to-point Ethernet)',
          'ML605 (Point-to-point Ethernet)',
          'NGC Netlist',
          'Timing and Power Analysis',
        ],
        'values' => [
          'target1',
          'target2',
          'target3',
          'target4',
          'target5',
          'target6',
        ],
      },
      'core_generation' => 1.0,
      'core_generation_sgadvanced' => '',
      'coregen_part_family' => 'virtex6',
      'cosim_library' => 'PPEthernetRuntimeCosim_r4',
      'create_interface_document' => 'off',
      'dbl_ovrd' => -1.0,
      'dbl_ovrd_sgadvanced' => '',
      'dcm_input_clock_period' => 100.0,
      'deprecated_control' => 'off',
      'deprecated_control_sgadvanced' => '',
      'directory' => './duc_ddc_cosim',
      'eval_field' => '0',
      'getimportblock_fcn' => 'xlGetHwcosimBlockName',
      'has_advanced_control' => '0',
      'has_system_reset' => 1,
      'hwcosim_board' => 'ml605-ppethernet',
      'incr_netlist' => 'off',
      'incr_netlist_sgadvanced' => '',
      'infoedit' => ' System Generator',
      'master_sysgen_token_handle' => 2076.0003662109375,
      'package' => 'ff1156',
      'part' => 'xc6vlx240t',
      'postgeneration_fcn' => 'xlHwcosimPostGeneration',
      'precompile_fcn' => 'xlEthernetPreCompile',
      'preserve_hierarchy' => 0.0,
      'run_coregen' => 'off',
      'run_coregen_sgadvanced' => '',
      'settings_fcn' => 'xlHwcosimSettings',
      'sg_blockgui_xml' => '<!--  *  Copyright (c) 2005, Xilinx, Inc.  All Rights Reserved.            --><!--  *  Reproduction or reuse, in any form, without the explicit written  --><!--  *  consent of Xilinx, Inc., is strictly prohibited.                  --><sysgenblock has_userdata="true" tag="genX" block_type="sysgen" simulinkname=" System Generator" >
 <icon width="51" bg_color="beige" height="50" caption_format="System\\nGenerator" wmark_color="red" />
 <callbacks DeleteFcn="xlSysgenGUI(\'delete\', gcs, gcbh);" OpenFcn="xlSysgenGUI(\'startup\',gcs,gcbh)" ModelCloseFcn="xlSysgenGUI(\'Close\',gcs,gcbh)" PostSaveFcn="xlSysgenGUI(\'Save\')" />
 <libraries>
  <library name="xbsIndex" />
  <library name="xbsBasic" />
  <library name="xbsTools" />
 </libraries>
 <subsystem_model file="system_generator_subsystem.mdl" />
 <blockgui label="Xilinx System Generator" >
  <editbox evaluate="false" multi_line="true" name="infoedit" read_only="true" default=" System Generator" />
  <editbox evaluate="false" name="xilinxfamily" default="Virtex4" label="Xilinx family" />
  <editbox evaluate="false" name="part" default="xc4vsx35" label="Part" />
  <editbox evaluate="false" name="speed" default="-10" label="Speed" />
  <editbox evaluate="false" name="package" default="ff668" label="Package" />
  <listbox evaluate="true" name="synthesis_tool" default="XST" label="Synthesis tool" >
   <item value="Spectrum" />
   <item value="Synplify" />
   <item value="Synplify Pro" />
   <item value="XST" />
   <item value="Precision" />
  </listbox>
  <editbox evaluate="false" name="directory" default="./netlist" label="Target directory" />
  <checkbox evaluate="true" name="testbench" default="off" label="Testbench" />
  <editbox evaluate="true" name="simulink_period" default="1" label="Simulink period" />
  <editbox evaluate="true" name="sysclk_period" default="10" label="System clock period" />
  <checkbox evaluate="true" name="incr_netlist" default="off" label="Incremental netlisting" />
  <listbox evaluate="true" name="trim_vbits" default="Everywhere in SubSystem" label="Trim valid bits" >
   <item value="According to Block Masks" />
   <item value="Everywhere in SubSystem" />
   <item value="No Where in SubSystem" />
  </listbox>
  <listbox evaluate="true" name="dbl_ovrd" default="According to Block Masks" label="Override with doubles" >
   <item value="According to Block Masks" />
   <item value="Everywhere in SubSystem" />
   <item value="No Where in SubSystem" />
  </listbox>
  <listbox evaluate="true" name="core_generation" default="According to Block Masks" label="Generate cores" >
   <item value="According to Block Masks" />
   <item value="Everywhere Available" />
   <item value="Not Needed - Already Generated" />
  </listbox>
  <checkbox evaluate="true" name="run_coregen" default="off" label="Run CoreGen" />
  <checkbox evaluate="true" name="deprecated_control" default="off" label="Show deprecated controls" />
  <hiddenvar evaluate="true" name="eval_field" default="0" />
 </blockgui>
</sysgenblock>
',
      'sg_icon_stat' => '51,50,-1,-1,red,beige,0,07734',
      'sg_list_contents' => '',
      'sg_mask_display' => 'fprintf(\'\',\'COMMENT: begin icon graphics\');
patch([0 51 51 0 ],[0 0 50 50 ],[0.93 0.92 0.86]);
patch([12 4 16 4 12 25 29 33 47 36 25 17 29 17 25 36 47 33 29 25 12 ],[5 13 25 37 45 45 41 45 45 34 45 37 25 13 5 16 5 5 9 5 5 ],[0.6 0.2 0.25]);
plot([0 0 51 51 0 ],[0 50 50 0 0 ]);
fprintf(\'\',\'COMMENT: end icon graphics\');
fprintf(\'\',\'COMMENT: begin icon text\');
fprintf(\'\',\'COMMENT: end icon text\');
',
      'sggui_pos' => '-1,-1,-1,-1',
      'simulation_island_subsystem_handle' => 2075.00048828125,
      'simulink_period' => 0.010416666666666666,
      'speed' => '-1',
      'synthesis_language' => 'vhdl',
      'synthesis_tool' => 'XST',
      'synthesis_tool_sgadvanced' => '',
      'sysclk_period' => 20.0,
      'testbench' => 0,
      'testbench_sgadvanced' => '',
      'trim_vbits' => 1.0,
      'trim_vbits_sgadvanced' => '',
      'version' => '13.1',
      'xilinx_device' => 'xc6vlx240t-1ff1156',
      'xilinxfamily' => 'virtex6',
    },
    'sysgen_Root' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'systemClockPeriod' => 20.0,
    'tempdir' => 'C:/TEMP',
    'testbench' => 0,
    'testbench_sgadvanced' => '',
    'tmpDir' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen',
    'trim_vbits' => 1.0,
    'trim_vbits_sgadvanced' => '',
    'use_strict_names' => 1,
    'user_tips_enabled' => 0.0,
    'usertemp' => 'C:/TEMP/sysgentmp-skoppol',
    'using71Netlister' => 1,
    'verilog_files' => [
      'conv_pkg.v',
      'synth_reg.v',
      'synth_reg_w_init.v',
      'convert_type.v',
    ],
    'version' => '13.1',
    'vhdl_files' => [
      'conv_pkg.vhd',
      'synth_reg.vhd',
      'synth_reg_w_init.vhd',
    ],
    'vsimtime' => '4224275.000000 ns',
    'xilinx' => 'C:/Xilinx/13.1/ISE_DS/ISE',
    'xilinx_device' => 'xc6vlx240t-1ff1156',
    'xilinx_family' => 'virtex6',
    'xilinx_package' => 'ff1156',
    'xilinx_part' => 'xc6vlx240t',
    'xilinxdevice' => 'xc6vlx240t-1ff1156',
    'xilinxfamily' => 'virtex6',
    'xilinxpart' => 'xc6vlx240t',
  };
  push(@$results, &Sg::setAttributes($instrs));
  use SgDeliverFile;
  $instrs = {
    'collaborationName' => 'conv_pkg.vhd',
    'sourceFile' => 'hdl/conv_pkg.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'synth_reg.vhd',
    'sourceFile' => 'hdl/synth_reg.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'synth_reg_w_init.vhd',
    'sourceFile' => 'hdl/synth_reg_w_init.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'xlpersistentdff.ngc',
    'sourceFile' => 'hdl/xlpersistentdff.ngc',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'xlclockenablegenerator.vhd',
    'sourceFile' => 'hdl/xlclockenablegenerator.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  use SgGenerateCores;
  $instrs = [
    'SELECT Complex_Multiplier virtex6 Xilinx,_Inc. 3.1',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET aportwidth = 19',
    'CSET bportwidth = 16',
    'CSET clockenable = true',
    'CSET latency = -1',
    'CSET multtype = Use_Mults',
    'CSET optimizegoal = Resources',
    'CSET outputwidthhigh = 32',
    'CSET outputwidthlow = 16',
    'CSET roundmode = Truncate',
    'CSET sclrcepriority = SCLR_overrides_CE',
    'CSET syncclear = false',
    'CSET component_name = cmpy_v3_1_3bed636cf80cb482',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '1d782d542f9753b4a4f66ae42c3c83e4',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component cmpy_v3_1_3bed636cf80cb482
    port(
      ai:in std_logic_vector(18 downto 0);
      ar:in std_logic_vector(18 downto 0);
      bi:in std_logic_vector(15 downto 0);
      br:in std_logic_vector(15 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      pi:out std_logic_vector(16 downto 0);
      pr:out std_logic_vector(16 downto 0)
    );
end component;
begin
  cmpy_v3_1_3bed636cf80cb482_instance : cmpy_v3_1_3bed636cf80cb482
    port map(
      ai=>ai,
      ar=>ar,
      bi=>bi,
      br=>br,
      ce=>ce,
      clk=>clk,
      pi=>pi,
      pr=>pr
    );
end ',
      'crippled_entity' => 'is 
  port(
    ai:in std_logic_vector(18 downto 0);
    ar:in std_logic_vector(18 downto 0);
    bi:in std_logic_vector(15 downto 0);
    br:in std_logic_vector(15 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    pi:out std_logic_vector(16 downto 0);
    pr:out std_logic_vector(16 downto 0)
  );
end',
      'entity_name' => 'xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '6fa065080f6a54f3b0f1ef51ded17679',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = [
    'SELECT DDS_Compiler virtex6 Xilinx,_Inc. 4.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET amplitude_mode = Full_Range',
    'CSET channel_pin = false',
    'CSET channels = 1',
    'CSET clock_enable = true',
    'CSET dds_clock_rate = 61.44000000000',
    'CSET dsp48_use = Minimal',
    'CSET explicit_period = true',
    'CSET frequency_resolution = 0.25000000000',
    'CSET gui_behaviour = Sysgen',
    'CSET has_phase_out = false',
    'CSET latency = 7',
    'CSET latency_configuration = Auto',
    'CSET memory_type = Auto',
    'CSET negative_cosine = false',
    'CSET negative_sine = true',
    'CSET noise_shaping = Taylor_Series_Corrected',
    'CSET optimization_goal = Area',
    'CSET output_frequency1 = 0',
    'CSET output_frequency10 = 0',
    'CSET output_frequency11 = 0',
    'CSET output_frequency12 = 0',
    'CSET output_frequency13 = 0',
    'CSET output_frequency14 = 0',
    'CSET output_frequency15 = 0',
    'CSET output_frequency16 = 0',
    'CSET output_frequency2 = 0',
    'CSET output_frequency3 = 0',
    'CSET output_frequency4 = 0',
    'CSET output_frequency5 = 0',
    'CSET output_frequency6 = 0',
    'CSET output_frequency7 = 0',
    'CSET output_frequency8 = 0',
    'CSET output_frequency9 = 0',
    'CSET output_selection = Sine_and_Cosine',
    'CSET output_width = 19',
    'CSET parameter_entry = System_Parameters',
    'CSET partspresent = Phase_Generator_and_SIN_COS_LUT',
    'CSET period = 6',
    'CSET phase_increment = Programmable',
    'CSET phase_offset = None',
    'CSET phase_offset_angles1 = 0',
    'CSET phase_offset_angles10 = 0',
    'CSET phase_offset_angles11 = 0',
    'CSET phase_offset_angles12 = 0',
    'CSET phase_offset_angles13 = 0',
    'CSET phase_offset_angles14 = 0',
    'CSET phase_offset_angles15 = 0',
    'CSET phase_offset_angles16 = 0',
    'CSET phase_offset_angles2 = 0',
    'CSET phase_offset_angles3 = 0',
    'CSET phase_offset_angles4 = 0',
    'CSET phase_offset_angles5 = 0',
    'CSET phase_offset_angles6 = 0',
    'CSET phase_offset_angles7 = 0',
    'CSET phase_offset_angles8 = 0',
    'CSET phase_offset_angles9 = 0',
    'CSET phase_width = 28',
    'CSET pinc1 = 0',
    'CSET pinc10 = 0',
    'CSET pinc11 = 0',
    'CSET pinc12 = 0',
    'CSET pinc13 = 0',
    'CSET pinc14 = 0',
    'CSET pinc15 = 0',
    'CSET pinc16 = 0',
    'CSET pinc2 = 0',
    'CSET pinc3 = 0',
    'CSET pinc4 = 0',
    'CSET pinc5 = 0',
    'CSET pinc6 = 0',
    'CSET pinc7 = 0',
    'CSET pinc8 = 0',
    'CSET pinc9 = 0',
    'CSET poff1 = 0',
    'CSET poff10 = 0',
    'CSET poff11 = 0',
    'CSET poff12 = 0',
    'CSET poff13 = 0',
    'CSET poff14 = 0',
    'CSET poff15 = 0',
    'CSET poff16 = 0',
    'CSET poff2 = 0',
    'CSET poff3 = 0',
    'CSET poff4 = 0',
    'CSET poff5 = 0',
    'CSET poff6 = 0',
    'CSET poff7 = 0',
    'CSET poff8 = 0',
    'CSET poff9 = 0',
    'CSET por_mode = false',
    'CSET rdy = false',
    'CSET rfd = false',
    'CSET sclr_pin = false',
    'CSET spurious_free_dynamic_range = 105',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = dds_cmplr_v4_0_3754692d6c95399c',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '8f21eb77b57e1cfbd2ee30715a771287',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component dds_cmplr_v4_0_3754692d6c95399c
    port(
      ce:in std_logic;
      clk:in std_logic;
      cosine:out std_logic_vector(18 downto 0);
      data:in std_logic_vector(27 downto 0);
      sine:out std_logic_vector(18 downto 0);
      we:in std_logic
    );
end component;
begin
  dds_cmplr_v4_0_3754692d6c95399c_instance : dds_cmplr_v4_0_3754692d6c95399c
    port map(
      ce=>ce,
      clk=>clk,
      cosine=>cosine,
      data=>data,
      sine=>sine,
      we=>we
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    cosine:out std_logic_vector(18 downto 0);
    data:in std_logic_vector(27 downto 0);
    sine:out std_logic_vector(18 downto 0);
    we:in std_logic
  );
end',
      'entity_name' => 'xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '2e67b3723e253fc4809174c42222ee20',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '93836d6c593d196500d1b84b1f2e8168',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '968a6648414dc3c3c01b1ebdbce6a6b9',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '29fde398c218f8edf2c31a5b5724ecd7',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'bb0969a65f25d72f16994cdedf15cb35',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'ead1341893a20691ecde0d32d364d944',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'fa42115caef04ed0e8349847102aa14d',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Half_Band',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = 0.0009002685546875,0,-0.00844573974609375,0,0.04051971435546875,0,-0.14239501953125,0,0.60941314697265625,0.99999237060546875,0.60941314697265625,0,-0.14239501953125,0,0.04051971435546875,0,-0.00844573974609375,0,0.0009002685546875',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 15',
    'CSET data_sign = Signed',
    'CSET data_width = 17',
    'CSET decimation_rate = 2',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Decimation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 1',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 1',
    'CSET number_paths = 2',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 19',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 12',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_1877c081b7e2f892',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '5cc538d0097d29ce9a6ed0ece905722f',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_1877c081b7e2f892
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(16 downto 0);
      din_2:in std_logic_vector(16 downto 0);
      dout_1:out std_logic_vector(18 downto 0);
      dout_2:out std_logic_vector(18 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_1877c081b7e2f892_instance : fr_cmplr_v5_0_1877c081b7e2f892
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(16 downto 0);
    din_2:in std_logic_vector(16 downto 0);
    dout_1:out std_logic_vector(18 downto 0);
    dout_2:out std_logic_vector(18 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Half_Band',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = 0.01259613037109375,0,-0.1002044677734375,0,0.58760833740234375,0.99999237060546875,0.58760833740234375,0,-0.1002044677734375,0,0.01259613037109375',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 15',
    'CSET data_sign = Signed',
    'CSET data_width = 16',
    'CSET decimation_rate = 2',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Decimation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 1',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 1',
    'CSET number_paths = 2',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 18',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 6',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_f5119409b2c69958',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '084720955551f71b6a09345327566e3a',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_f5119409b2c69958
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(15 downto 0);
      din_2:in std_logic_vector(15 downto 0);
      dout_1:out std_logic_vector(17 downto 0);
      dout_2:out std_logic_vector(17 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_f5119409b2c69958_instance : fr_cmplr_v5_0_f5119409b2c69958
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(15 downto 0);
    din_2:in std_logic_vector(15 downto 0);
    dout_1:out std_logic_vector(17 downto 0);
    dout_2:out std_logic_vector(17 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_8bbf91ad55b0353987e969c586c6098d',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Inferred',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = -0.0001068115234375,-0.00011444091796875,-9.918212890625e-005,2.288818359375e-005,0.00018310546875,0.00020599365234375,7.62939453125e-006,-0.00029754638671875,-0.000396728515625,-9.1552734375e-005,0.0004730224609375,0.00080108642578125,0.00042724609375,-0.00057220458984375,-0.00144195556640625,-0.00124359130859375,0.000213623046875,0.00196075439453125,0.0024261474609375,0.00078582763671875,-0.00196075439453125,-0.00345611572265625,-0.00193023681640625,0.00185394287109375,0.00458526611328125,0.00298309326171875,-0.0027618408203125,-0.00792694091796875,-0.0063934326171875,0.00350189208984375,0.0153961181640625,0.01782989501953125,0.00334930419921875,-0.0224761962890625,-0.04064178466796875,-0.03052520751953125,0.01222991943359375,0.06497955322265625,0.08638763427734375,0.0434112548828125,-0.05821990966796875,-0.1623077392578125,-0.1831817626953125,-0.0531005859375,0.230255126953125,0.58699798583984375,0.8843841552734375,0.99999237060546875,0.8843841552734375,0.58699798583984375,0.230255126953125,-0.0531005859375,-0.1831817626953125,-0.1623077392578125,-0.05821990966796875,0.0434112548828125,0.08638763427734375,0.06497955322265625,0.01222991943359375,-0.03052520751953125,-0.04064178466796875,-0.0224761962890625,0.00334930419921875,0.01782989501953125,0.0153961181640625,0.00350189208984375,-0.0063934326171875,-0.00792694091796875,-0.0027618408203125,0.00298309326171875,0.00458526611328125,0.00185394287109375,-0.00193023681640625,-0.00345611572265625,-0.00196075439453125,0.00078582763671875,0.0024261474609375,0.00196075439453125,0.000213623046875,-0.00124359130859375,-0.00144195556640625,-0.00057220458984375,0.00042724609375,0.00080108642578125,0.0004730224609375,-9.1552734375e-005,-0.000396728515625,-0.00029754638671875,7.62939453125e-006,0.00020599365234375,0.00018310546875,2.288818359375e-005,-9.918212890625e-005,-0.00011444091796875,-0.0001068115234375',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 15',
    'CSET data_sign = Signed',
    'CSET data_width = 18',
    'CSET decimation_rate = 2',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Decimation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 1',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 1',
    'CSET number_paths = 2',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Full_Precision',
    'CSET output_width = 38',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 24',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_cd9071975244390f',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '5f6c400eab60ef89a639b523e6c0a8b6',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_cd9071975244390f
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(17 downto 0);
      din_2:in std_logic_vector(17 downto 0);
      dout_1:out std_logic_vector(37 downto 0);
      dout_2:out std_logic_vector(37 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_cd9071975244390f_instance : fr_cmplr_v5_0_cd9071975244390f
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(17 downto 0);
    din_2:in std_logic_vector(17 downto 0);
    dout_1:out std_logic_vector(37 downto 0);
    dout_2:out std_logic_vector(37 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '13366d021ddc9f5413827bc05cb9e24f',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => 'is
begin
  op <= "1";
end',
      'crippled_entity' => 'is
  port (
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end',
      'entity_name' => 'constant_6293007044',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => 'e91bf9db21254506c6a71f3036d6b931',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'd33c6dfb05498091f40429edaed649b9',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'fdf68895e277ba22e8e6428579763e15',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldsamp.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'b32a0080f8f47e0be7ec44c6ad81b20b',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => 'is
  signal ip_1_26: boolean;
  type array_type_op_mem_22_20 is array (0 to (1 - 1)) of boolean;
  signal op_mem_22_20: array_type_op_mem_22_20 := (
    0 => false);
  signal op_mem_22_20_front_din: boolean;
  signal op_mem_22_20_back: boolean;
  signal op_mem_22_20_push_front_pop_back_en: std_logic;
  signal internal_ip_12_1_bitnot: boolean;
begin
  ip_1_26 <= ((ip) = "1");
  op_mem_22_20_back <= op_mem_22_20(0);
  proc_op_mem_22_20: process (clk)
  is
    variable i: integer;
  begin
    if (clk\'event and (clk = \'1\')) then
      if ((ce = \'1\') and (op_mem_22_20_push_front_pop_back_en = \'1\')) then
        op_mem_22_20(0) <= op_mem_22_20_front_din;
      end if;
    end if;
  end process proc_op_mem_22_20;
  internal_ip_12_1_bitnot <= ((not boolean_to_vector(ip_1_26)) = "1");
  op_mem_22_20_push_front_pop_back_en <= \'0\';
  op <= boolean_to_vector(internal_ip_12_1_bitnot);
end',
      'crippled_entity' => 'is
  port (
    ip : in std_logic_vector((1 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end',
      'entity_name' => 'inverter_e5b38cca3b',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '7a72192302247cfdaed5f8aae13d083b',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => 'is
  signal d0_1_24: std_logic;
  signal d1_1_27: std_logic;
  type array_type_latency_pipe_5_26 is array (0 to (1 - 1)) of std_logic;
  signal latency_pipe_5_26: array_type_latency_pipe_5_26 := (
    0 => \'0\');
  signal latency_pipe_5_26_front_din: std_logic;
  signal latency_pipe_5_26_back: std_logic;
  signal latency_pipe_5_26_push_front_pop_back_en: std_logic;
  signal fully_2_1_bit: std_logic;
begin
  d0_1_24 <= d0(0);
  d1_1_27 <= d1(0);
  latency_pipe_5_26_back <= latency_pipe_5_26(0);
  proc_latency_pipe_5_26: process (clk)
  is
    variable i: integer;
  begin
    if (clk\'event and (clk = \'1\')) then
      if ((ce = \'1\') and (latency_pipe_5_26_push_front_pop_back_en = \'1\')) then
        latency_pipe_5_26(0) <= latency_pipe_5_26_front_din;
      end if;
    end if;
  end process proc_latency_pipe_5_26;
  fully_2_1_bit <= d0_1_24 and d1_1_27;
  latency_pipe_5_26_front_din <= fully_2_1_bit;
  latency_pipe_5_26_push_front_pop_back_en <= \'1\';
  y <= std_logic_to_vector(latency_pipe_5_26_back);
end',
      'crippled_entity' => 'is
  port (
    d0 : in std_logic_vector((1 - 1) downto 0);
    d1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end',
      'entity_name' => 'logical_799f62af22',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => 'eefad87532d55363367f51b734109f5a',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlregister.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = [
    'SELECT Complex_Multiplier virtex6 Xilinx,_Inc. 3.1',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET aportwidth = 19',
    'CSET bportwidth = 18',
    'CSET clockenable = true',
    'CSET latency = -1',
    'CSET multtype = Use_Mults',
    'CSET optimizegoal = Resources',
    'CSET outputwidthhigh = 35',
    'CSET outputwidthlow = 0',
    'CSET roundmode = Truncate',
    'CSET sclrcepriority = SCLR_overrides_CE',
    'CSET syncclear = false',
    'CSET component_name = cmpy_v3_1_974daf98b885ddbd',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '71c92fea2f86625ccacfc533b2feaa4a',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component cmpy_v3_1_974daf98b885ddbd
    port(
      ai:in std_logic_vector(18 downto 0);
      ar:in std_logic_vector(18 downto 0);
      bi:in std_logic_vector(17 downto 0);
      br:in std_logic_vector(17 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      pi:out std_logic_vector(35 downto 0);
      pr:out std_logic_vector(35 downto 0)
    );
end component;
begin
  cmpy_v3_1_974daf98b885ddbd_instance : cmpy_v3_1_974daf98b885ddbd
    port map(
      ai=>ai,
      ar=>ar,
      bi=>bi,
      br=>br,
      ce=>ce,
      clk=>clk,
      pi=>pi,
      pr=>pr
    );
end ',
      'crippled_entity' => 'is 
  port(
    ai:in std_logic_vector(18 downto 0);
    ar:in std_logic_vector(18 downto 0);
    bi:in std_logic_vector(17 downto 0);
    br:in std_logic_vector(17 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    pi:out std_logic_vector(35 downto 0);
    pr:out std_logic_vector(35 downto 0)
  );
end',
      'entity_name' => 'xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '7d9c67933b74b4fa84110787e4812368',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = [
    'SELECT DDS_Compiler virtex6 Xilinx,_Inc. 4.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET amplitude_mode = Full_Range',
    'CSET channel_pin = false',
    'CSET channels = 1',
    'CSET clock_enable = true',
    'CSET dds_clock_rate = 61.44000000000',
    'CSET dsp48_use = Minimal',
    'CSET explicit_period = false',
    'CSET frequency_resolution = 0.25000000000',
    'CSET gui_behaviour = Sysgen',
    'CSET has_phase_out = false',
    'CSET latency = 7',
    'CSET latency_configuration = Auto',
    'CSET memory_type = Auto',
    'CSET negative_cosine = false',
    'CSET negative_sine = false',
    'CSET noise_shaping = Taylor_Series_Corrected',
    'CSET optimization_goal = Area',
    'CSET output_frequency1 = 0',
    'CSET output_frequency10 = 0',
    'CSET output_frequency11 = 0',
    'CSET output_frequency12 = 0',
    'CSET output_frequency13 = 0',
    'CSET output_frequency14 = 0',
    'CSET output_frequency15 = 0',
    'CSET output_frequency16 = 0',
    'CSET output_frequency2 = 0',
    'CSET output_frequency3 = 0',
    'CSET output_frequency4 = 0',
    'CSET output_frequency5 = 0',
    'CSET output_frequency6 = 0',
    'CSET output_frequency7 = 0',
    'CSET output_frequency8 = 0',
    'CSET output_frequency9 = 0',
    'CSET output_selection = Sine_and_Cosine',
    'CSET output_width = 19',
    'CSET parameter_entry = System_Parameters',
    'CSET partspresent = Phase_Generator_and_SIN_COS_LUT',
    'CSET period = 1',
    'CSET phase_increment = Programmable',
    'CSET phase_offset = None',
    'CSET phase_offset_angles1 = 0',
    'CSET phase_offset_angles10 = 0',
    'CSET phase_offset_angles11 = 0',
    'CSET phase_offset_angles12 = 0',
    'CSET phase_offset_angles13 = 0',
    'CSET phase_offset_angles14 = 0',
    'CSET phase_offset_angles15 = 0',
    'CSET phase_offset_angles16 = 0',
    'CSET phase_offset_angles2 = 0',
    'CSET phase_offset_angles3 = 0',
    'CSET phase_offset_angles4 = 0',
    'CSET phase_offset_angles5 = 0',
    'CSET phase_offset_angles6 = 0',
    'CSET phase_offset_angles7 = 0',
    'CSET phase_offset_angles8 = 0',
    'CSET phase_offset_angles9 = 0',
    'CSET phase_width = 28',
    'CSET pinc1 = 0',
    'CSET pinc10 = 0',
    'CSET pinc11 = 0',
    'CSET pinc12 = 0',
    'CSET pinc13 = 0',
    'CSET pinc14 = 0',
    'CSET pinc15 = 0',
    'CSET pinc16 = 0',
    'CSET pinc2 = 0',
    'CSET pinc3 = 0',
    'CSET pinc4 = 0',
    'CSET pinc5 = 0',
    'CSET pinc6 = 0',
    'CSET pinc7 = 0',
    'CSET pinc8 = 0',
    'CSET pinc9 = 0',
    'CSET poff1 = 0',
    'CSET poff10 = 0',
    'CSET poff11 = 0',
    'CSET poff12 = 0',
    'CSET poff13 = 0',
    'CSET poff14 = 0',
    'CSET poff15 = 0',
    'CSET poff16 = 0',
    'CSET poff2 = 0',
    'CSET poff3 = 0',
    'CSET poff4 = 0',
    'CSET poff5 = 0',
    'CSET poff6 = 0',
    'CSET poff7 = 0',
    'CSET poff8 = 0',
    'CSET poff9 = 0',
    'CSET por_mode = false',
    'CSET rdy = false',
    'CSET rfd = false',
    'CSET sclr_pin = false',
    'CSET spurious_free_dynamic_range = 105',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = dds_cmplr_v4_0_d6a7998cfc77acb2',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '3ce104e89c08473205c51117b76cc513',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component dds_cmplr_v4_0_d6a7998cfc77acb2
    port(
      ce:in std_logic;
      clk:in std_logic;
      cosine:out std_logic_vector(18 downto 0);
      data:in std_logic_vector(27 downto 0);
      sine:out std_logic_vector(18 downto 0);
      we:in std_logic
    );
end component;
begin
  dds_cmplr_v4_0_d6a7998cfc77acb2_instance : dds_cmplr_v4_0_d6a7998cfc77acb2
    port map(
      ce=>ce,
      clk=>clk,
      cosine=>cosine,
      data=>data,
      sine=>sine,
      we=>we
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    cosine:out std_logic_vector(18 downto 0);
    data:in std_logic_vector(27 downto 0);
    sine:out std_logic_vector(18 downto 0);
    we:in std_logic
  );
end',
      'entity_name' => 'xldds_compiler_94274096bf4e0b94b9506335cad96a90',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '3b8286f4ef840b1b65701c072ce44f9c',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '848716af9c759f5f74bb41d0db640394',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlusamp.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'df6e63f6c95c034a6883f57c6cf3548e',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'd810aa3914be770fcb95cbdc0debcefb',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlregister.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '3410b5f86733881502ee6c652040bccc',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlregister.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'f3f22ed6c1dafe9d1adef65c0e922552',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '1be72e2aea07c5c60cd76d5ca8f5af7c',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'be8e4209add2918d67adbccbf7bc01ce',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlconvert.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '26ad8a72987430e515cc7dd0b8e95036',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => 'e146095a029afdc70d59fc5780fc3b77',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xldelay.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Inferred',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = -0.001708984375,0,0.00868988037109375,0,-0.0277862548828125,0,0.07128143310546875,0,-0.17310333251953125,0,0.62253570556640625,0.99999237060546875,0.62253570556640625,0,-0.17310333251953125,0,0.07128143310546875,0,-0.0277862548828125,0,0.00868988037109375,0,-0.001708984375',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 15',
    'CSET data_sign = Signed',
    'CSET data_width = 16',
    'CSET decimation_rate = 1',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Interpolation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 2',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 2',
    'CSET number_paths = 1',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 18',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 24',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_670158bb462abb3d',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '007cc2986a226ada68c1df0f25b923c6',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_670158bb462abb3d
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(15 downto 0);
      dout:out std_logic_vector(17 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal dout_ps_net: std_logic_vector(17 downto 0) := (others=>\'0\');
signal rdy_ps_net: std_logic := \'0\';
signal rdy_ps_net_captured: std_logic := \'0\';
signal rdy_ps_net_or_captured_net: std_logic := \'0\';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_24,
        clr => \'0\',
        clk => clk_24, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_12,
        clr => \'0\',
        clk => clk_12, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 18,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_12,
        clr => \'0\',
        clk => clk_12, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_12,
        clr => \'0\',
        clk => clk_12, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => \'1\',
        ce => rdy_ps_net,
        clr => \'0\',
        clk => clk_12, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_670158bb462abb3d_instance : fr_cmplr_v5_0_670158bb462abb3d
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_24,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    ce_12:in std_logic;
    ce_24:in std_logic;
    ce_logic_24:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_12:in std_logic;
    clk_24:in std_logic;
    clk_logic_24:in std_logic;
    din:in std_logic_vector(15 downto 0);
    dout:out std_logic_vector(17 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_6b538e2e420c55fbcb179300d415c30e',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Inferred',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = 0.0156707763671875,0,-0.10816192626953125,0,0.59255218505859375,0.99999237060546875,0.59255218505859375,0,-0.10816192626953125,0,0.0156707763671875',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 16',
    'CSET data_sign = Signed',
    'CSET data_width = 17',
    'CSET decimation_rate = 1',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Interpolation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 2',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 2',
    'CSET number_paths = 1',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 32',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 12',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_51d88ed999503974',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => 'fba1e621a4d5c7f288e404050c568ff9',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_51d88ed999503974
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(16 downto 0);
      dout:out std_logic_vector(31 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal dout_ps_net: std_logic_vector(31 downto 0) := (others=>\'0\');
signal rdy_ps_net: std_logic := \'0\';
signal rdy_ps_net_captured: std_logic := \'0\';
signal rdy_ps_net_or_captured_net: std_logic := \'0\';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_12,
        clr => \'0\',
        clk => clk_12, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_6,
        clr => \'0\',
        clk => clk_6, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 32,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_6,
        clr => \'0\',
        clk => clk_6, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_6,
        clr => \'0\',
        clk => clk_6, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => \'1\',
        ce => rdy_ps_net,
        clr => \'0\',
        clk => clk_6, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_51d88ed999503974_instance : fr_cmplr_v5_0_51d88ed999503974
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_12,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    ce_12:in std_logic;
    ce_6:in std_logic;
    ce_logic_12:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_12:in std_logic;
    clk_6:in std_logic;
    clk_logic_12:in std_logic;
    din:in std_logic_vector(16 downto 0);
    dout:out std_logic_vector(31 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Inferred',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = 0.01259613037109375,0,-0.1002044677734375,0,0.58760833740234375,0.99999237060546875,0.58760833740234375,0,-0.1002044677734375,0,0.01259613037109375',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 16',
    'CSET data_sign = Signed',
    'CSET data_width = 17',
    'CSET decimation_rate = 1',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Interpolation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 2',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 2',
    'CSET number_paths = 1',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 19',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 6',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_e37f692facadc69b',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => 'a0df3ce45e526c1f47fdc00ff31f0048',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_e37f692facadc69b
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(16 downto 0);
      dout:out std_logic_vector(18 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal dout_ps_net: std_logic_vector(18 downto 0) := (others=>\'0\');
signal rdy_ps_net: std_logic := \'0\';
signal rdy_ps_net_captured: std_logic := \'0\';
signal rdy_ps_net_or_captured_net: std_logic := \'0\';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_6,
        clr => \'0\',
        clk => clk_6, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_3,
        clr => \'0\',
        clk => clk_3, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 19,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_3,
        clr => \'0\',
        clk => clk_3, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_3,
        clr => \'0\',
        clk => clk_3, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => \'1\',
        ce => rdy_ps_net,
        clr => \'0\',
        clk => clk_3, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_e37f692facadc69b_instance : fr_cmplr_v5_0_e37f692facadc69b
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_6,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    ce_3:in std_logic;
    ce_6:in std_logic;
    ce_logic_6:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_3:in std_logic;
    clk_6:in std_logic;
    clk_logic_6:in std_logic;
    din:in std_logic_vector(16 downto 0);
    dout:out std_logic_vector(18 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT FIR_Compiler virtex6 Xilinx,_Inc. 5.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET allow_rounding_approximation = false',
    'CSET bestprecision = true',
    'CSET chan_in_adv = 0',
    'CSET clock_frequency = 300',
    'CSET coefficient_buffer_type = Automatic',
    'CSET coefficient_file = no_coe_file_loaded',
    'CSET coefficient_fractional_bits = 17',
    'CSET coefficient_reload = false',
    'CSET coefficient_sets = 1',
    'CSET coefficient_sign = Signed',
    'CSET coefficient_structure = Inferred',
    'CSET coefficient_width = 18',
    'CSET coefficientsource = Vector',
    'CSET coefficientvector = 0.00019073486328125,0.0003509521484375,-0.00042724609375,-0.0001220703125,0.00092315673828125,-0.0005950927734375,-0.00118255615234375,0.0017242431640625,0.000640869140625,-0.00264739990234375,0.0013427734375,0.002044677734375,-0.00518798828125,0.002197265625,0.01079559326171875,-0.01317596435546875,-0.01741790771484375,0.03624725341796875,0.023773193359375,-0.087615966796875,-0.02837371826171875,0.31177520751953125,0.53005218505859375,0.31177520751953125,-0.02837371826171875,-0.087615966796875,0.023773193359375,0.03624725341796875,-0.01741790771484375,-0.01317596435546875,0.01079559326171875,0.002197265625,-0.00518798828125,0.002044677734375,0.0013427734375,-0.00264739990234375,0.000640869140625,0.0017242431640625,-0.00118255615234375,-0.0005950927734375,0.00092315673828125,-0.0001220703125,-0.00042724609375,0.0003509521484375,0.00019073486328125',
    'CSET columnconfig = 1',
    'CSET data_buffer_type = Automatic',
    'CSET data_fractional_bits = 15',
    'CSET data_sign = Signed',
    'CSET data_width = 16',
    'CSET decimation_rate = 1',
    'CSET displayreloadorder = false',
    'CSET filter_architecture = Systolic_Multiply_Accumulate',
    'CSET filter_selection = 1',
    'CSET filter_type = Interpolation',
    'CSET gui_behaviour = Coregen',
    'CSET hardwareoversamplingrate = 1',
    'CSET has_ce = true',
    'CSET has_data_valid = false',
    'CSET has_nd = true',
    'CSET has_sclr = false',
    'CSET input_buffer_type = Automatic',
    'CSET inter_column_pipe_length = 4',
    'CSET interpolation_rate = 2',
    'CSET multi_column_support = Disabled',
    'CSET number_channels = 2',
    'CSET number_paths = 1',
    'CSET optimization_goal = Area',
    'CSET output_buffer_type = Automatic',
    'CSET output_rounding_mode = Symmetric_Rounding_to_Infinity',
    'CSET output_width = 16',
    'CSET passband_max = 0.50000000000',
    'CSET passband_min = 0',
    'CSET preference_for_other_storage = Automatic',
    'CSET quantization = Quantize_Only',
    'CSET rate_change_type = Integer',
    'CSET ratespecification = Sample_Period',
    'CSET registered_output = true',
    'CSET sample_frequency = 0.00100000000',
    'CSET sampleperiod = 48',
    'CSET sclr_deterministic = false',
    'CSET stopband_max = 1',
    'CSET stopband_min = 0.50000000000',
    'CSET usechan_in_adv = false',
    'CSET zero_pack_factor = 1',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = fr_cmplr_v5_0_059cf50081fc492c',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => '6815548e90fdafdd7f580b5400043ee0',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component fr_cmplr_v5_0_059cf50081fc492c
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(15 downto 0);
      dout:out std_logic_vector(15 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>\'0\');
signal dout_ps_net: std_logic_vector(15 downto 0) := (others=>\'0\');
signal rdy_ps_net: std_logic := \'0\';
signal rdy_ps_net_captured: std_logic := \'0\';
signal rdy_ps_net_or_captured_net: std_logic := \'0\';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_48,
        clr => \'0\',
        clk => clk_48, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_24,
        clr => \'0\',
        clk => clk_24, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 16,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_24,
        clr => \'0\',
        clk => clk_24, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_24,
        clr => \'0\',
        clk => clk_24, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => \'1\',
        ce => rdy_ps_net,
        clr => \'0\',
        clk => clk_24, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_059cf50081fc492c_instance : fr_cmplr_v5_0_059cf50081fc492c
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_48,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    ce_24:in std_logic;
    ce_48:in std_logic;
    ce_logic_48:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_24:in std_logic;
    clk_48:in std_logic;
    clk_logic_48:in std_logic;
    din:in std_logic_vector(15 downto 0);
    dout:out std_logic_vector(15 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end',
      'entity_name' => 'xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = {
    'entity_declaration_hash' => '0f521c7c62febddce3165e397b3dc95b',
    'sourceFile' => 'hdl/xltdd_multich.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '1557d23ece4083ba751beedcddaa5bd2',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xltdm.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  local *wrapup = $Sg::{'wrapup'};
  push(@$results, &Sg::wrapup())   if (defined(&wrapup));
  local *wrapup = $SgDeliverFile::{'wrapup'};
  push(@$results, &SgDeliverFile::wrapup())   if (defined(&wrapup));
  local *wrapup = $SgGenerateCores::{'wrapup'};
  push(@$results, &SgGenerateCores::wrapup())   if (defined(&wrapup));
  use Carp qw(croak);
  $ENV{'SYSGEN'} = 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen';
  open(RESULTS, '> c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058') || 
    croak 'couldn\'t open c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058';
  binmode(RESULTS);
  print RESULTS &Sg::toString($results) . "\n";
  close(RESULTS) || 
    croak 'trouble writing c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058';
};

if ($@) {
  open(RESULTS, '> c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058') || 
    croak 'couldn\'t open c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058';
  binmode(RESULTS);
  print RESULTS $@ . "\n";
  close(RESULTS) || 
    croak 'trouble writing c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen/script_results4455870339155724058';
  exit(1);
}

exit(0);
