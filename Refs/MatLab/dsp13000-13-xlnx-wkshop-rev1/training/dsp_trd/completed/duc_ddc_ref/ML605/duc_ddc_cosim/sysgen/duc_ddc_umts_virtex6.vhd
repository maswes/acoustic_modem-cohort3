--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file cmpy_v3_1_3bed636cf80cb482.vhd when simulating
-- the core, cmpy_v3_1_3bed636cf80cb482. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY cmpy_v3_1_3bed636cf80cb482 IS
	port (
	ar: in std_logic_vector(18 downto 0);
	ai: in std_logic_vector(18 downto 0);
	br: in std_logic_vector(15 downto 0);
	bi: in std_logic_vector(15 downto 0);
	clk: in std_logic;
	ce: in std_logic;
	pr: out std_logic_vector(16 downto 0);
	pi: out std_logic_vector(16 downto 0));
END cmpy_v3_1_3bed636cf80cb482;

ARCHITECTURE cmpy_v3_1_3bed636cf80cb482_a OF cmpy_v3_1_3bed636cf80cb482 IS
-- synthesis translate_off
component wrapped_cmpy_v3_1_3bed636cf80cb482
	port (
	ar: in std_logic_vector(18 downto 0);
	ai: in std_logic_vector(18 downto 0);
	br: in std_logic_vector(15 downto 0);
	bi: in std_logic_vector(15 downto 0);
	clk: in std_logic;
	ce: in std_logic;
	pr: out std_logic_vector(16 downto 0);
	pi: out std_logic_vector(16 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_cmpy_v3_1_3bed636cf80cb482 use entity XilinxCoreLib.cmpy_v3_1(behavioral)
		generic map(
			c_a_width => 19,
			c_ce_overrides_sclr => 0,
			has_negate => 0,
			c_has_sclr => 0,
			c_out_high => 32,
			c_verbosity => 0,
			c_mult_type => 1,
			c_latency => -1,
			c_xdevice => "xc6vcx75t",
			c_has_ce => 1,
			single_output => 0,
			round => 0,
			use_dsp_cascades => 1,
			c_optimize_goal => 0,
			c_xdevicefamily => "virtex6",
			c_out_low => 16,
			c_b_width => 16);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_cmpy_v3_1_3bed636cf80cb482
		port map (
			ar => ar,
			ai => ai,
			br => br,
			bi => bi,
			clk => clk,
			ce => ce,
			pr => pr,
			pi => pi);
-- synthesis translate_on

END cmpy_v3_1_3bed636cf80cb482_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file cmpy_v3_1_974daf98b885ddbd.vhd when simulating
-- the core, cmpy_v3_1_974daf98b885ddbd. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY cmpy_v3_1_974daf98b885ddbd IS
	port (
	ar: in std_logic_vector(18 downto 0);
	ai: in std_logic_vector(18 downto 0);
	br: in std_logic_vector(17 downto 0);
	bi: in std_logic_vector(17 downto 0);
	clk: in std_logic;
	ce: in std_logic;
	pr: out std_logic_vector(35 downto 0);
	pi: out std_logic_vector(35 downto 0));
END cmpy_v3_1_974daf98b885ddbd;

ARCHITECTURE cmpy_v3_1_974daf98b885ddbd_a OF cmpy_v3_1_974daf98b885ddbd IS
-- synthesis translate_off
component wrapped_cmpy_v3_1_974daf98b885ddbd
	port (
	ar: in std_logic_vector(18 downto 0);
	ai: in std_logic_vector(18 downto 0);
	br: in std_logic_vector(17 downto 0);
	bi: in std_logic_vector(17 downto 0);
	clk: in std_logic;
	ce: in std_logic;
	pr: out std_logic_vector(35 downto 0);
	pi: out std_logic_vector(35 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_cmpy_v3_1_974daf98b885ddbd use entity XilinxCoreLib.cmpy_v3_1(behavioral)
		generic map(
			c_a_width => 19,
			c_ce_overrides_sclr => 0,
			has_negate => 0,
			c_has_sclr => 0,
			c_out_high => 35,
			c_verbosity => 0,
			c_mult_type => 1,
			c_latency => -1,
			c_xdevice => "xc6vcx75t",
			c_has_ce => 1,
			single_output => 0,
			round => 0,
			use_dsp_cascades => 1,
			c_optimize_goal => 0,
			c_xdevicefamily => "virtex6",
			c_out_low => 0,
			c_b_width => 18);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_cmpy_v3_1_974daf98b885ddbd
		port map (
			ar => ar,
			ai => ai,
			br => br,
			bi => bi,
			clk => clk,
			ce => ce,
			pr => pr,
			pi => pi);
-- synthesis translate_on

END cmpy_v3_1_974daf98b885ddbd_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file dds_cmplr_v4_0_3754692d6c95399c.vhd when simulating
-- the core, dds_cmplr_v4_0_3754692d6c95399c. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY dds_cmplr_v4_0_3754692d6c95399c IS
	port (
	ce: in std_logic;
	clk: in std_logic;
	we: in std_logic;
	data: in std_logic_vector(27 downto 0);
	cosine: out std_logic_vector(18 downto 0);
	sine: out std_logic_vector(18 downto 0));
END dds_cmplr_v4_0_3754692d6c95399c;

ARCHITECTURE dds_cmplr_v4_0_3754692d6c95399c_a OF dds_cmplr_v4_0_3754692d6c95399c IS
-- synthesis translate_off
component wrapped_dds_cmplr_v4_0_3754692d6c95399c
	port (
	ce: in std_logic;
	clk: in std_logic;
	we: in std_logic;
	data: in std_logic_vector(27 downto 0);
	cosine: out std_logic_vector(18 downto 0);
	sine: out std_logic_vector(18 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_dds_cmplr_v4_0_3754692d6c95399c use entity XilinxCoreLib.dds_compiler_v4_0(behavioral)
		generic map(
			c_use_dsp48 => 0,
			c_phase_offset_value => "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			c_amplitude => 0,
			c_channels => 1,
			c_phase_increment_value => "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			c_has_rdy => 0,
			c_has_sincos => 1,
			c_has_sclr => 0,
			c_phase_offset => 0,
			c_phase_angle_width => 11,
			c_phase_increment => 1,
			c_has_rfd => 0,
			c_negative_sine => 1,
			c_has_phasegen => 1,
			c_has_channel_index => 0,
			c_latency => -1,
			c_por_mode => 0,
			c_has_ce => 1,
			c_outputs_required => 2,
			c_accumulator_width => 28,
			c_mem_type => 1,
			c_optimise_goal => 0,
			c_negative_cosine => 0,
			c_has_phase_out => 0,
			c_noise_shaping => 2,
			c_xdevicefamily => "virtex6",
			c_output_width => 19);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_dds_cmplr_v4_0_3754692d6c95399c
		port map (
			ce => ce,
			clk => clk,
			we => we,
			data => data,
			cosine => cosine,
			sine => sine);
-- synthesis translate_on

END dds_cmplr_v4_0_3754692d6c95399c_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file dds_cmplr_v4_0_d6a7998cfc77acb2.vhd when simulating
-- the core, dds_cmplr_v4_0_d6a7998cfc77acb2. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY dds_cmplr_v4_0_d6a7998cfc77acb2 IS
	port (
	ce: in std_logic;
	clk: in std_logic;
	we: in std_logic;
	data: in std_logic_vector(27 downto 0);
	cosine: out std_logic_vector(18 downto 0);
	sine: out std_logic_vector(18 downto 0));
END dds_cmplr_v4_0_d6a7998cfc77acb2;

ARCHITECTURE dds_cmplr_v4_0_d6a7998cfc77acb2_a OF dds_cmplr_v4_0_d6a7998cfc77acb2 IS
-- synthesis translate_off
component wrapped_dds_cmplr_v4_0_d6a7998cfc77acb2
	port (
	ce: in std_logic;
	clk: in std_logic;
	we: in std_logic;
	data: in std_logic_vector(27 downto 0);
	cosine: out std_logic_vector(18 downto 0);
	sine: out std_logic_vector(18 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_dds_cmplr_v4_0_d6a7998cfc77acb2 use entity XilinxCoreLib.dds_compiler_v4_0(behavioral)
		generic map(
			c_use_dsp48 => 0,
			c_phase_offset_value => "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			c_amplitude => 0,
			c_channels => 1,
			c_phase_increment_value => "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			c_has_rdy => 0,
			c_has_sincos => 1,
			c_has_sclr => 0,
			c_phase_offset => 0,
			c_phase_angle_width => 11,
			c_phase_increment => 1,
			c_has_rfd => 0,
			c_negative_sine => 0,
			c_has_phasegen => 1,
			c_has_channel_index => 0,
			c_latency => -1,
			c_por_mode => 0,
			c_has_ce => 1,
			c_outputs_required => 2,
			c_accumulator_width => 28,
			c_mem_type => 1,
			c_optimise_goal => 0,
			c_negative_cosine => 0,
			c_has_phase_out => 0,
			c_noise_shaping => 2,
			c_xdevicefamily => "virtex6",
			c_output_width => 19);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_dds_cmplr_v4_0_d6a7998cfc77acb2
		port map (
			ce => ce,
			clk => clk,
			we => we,
			data => data,
			cosine => cosine,
			sine => sine);
-- synthesis translate_on

END dds_cmplr_v4_0_d6a7998cfc77acb2_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_059cf50081fc492c.vhd when simulating
-- the core, fr_cmplr_v5_0_059cf50081fc492c. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_059cf50081fc492c IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(15 downto 0);
	dout: out std_logic_vector(15 downto 0));
END fr_cmplr_v5_0_059cf50081fc492c;

ARCHITECTURE fr_cmplr_v5_0_059cf50081fc492c_a OF fr_cmplr_v5_0_059cf50081fc492c IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_059cf50081fc492c
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(15 downto 0);
	dout: out std_logic_vector(15 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_059cf50081fc492c use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_059cf50081fc492c",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 16,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 96,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 61,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 2,
			data_width => 16,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 2,
			data_type => 0,
			accum_width => 33,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 1,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 2,
			num_taps => 45,
			c_mem_init_file => "fr_cmplr_v5_0_059cf50081fc492c.mif",
			zero_packing_factor => 1,
			num_paths => 1,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_059cf50081fc492c
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			chan_in => chan_in,
			chan_out => chan_out,
			din => din,
			dout => dout);
-- synthesis translate_on

END fr_cmplr_v5_0_059cf50081fc492c_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_1877c081b7e2f892.vhd when simulating
-- the core, fr_cmplr_v5_0_1877c081b7e2f892. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_1877c081b7e2f892 IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(16 downto 0);
	din_2: in std_logic_vector(16 downto 0);
	dout_1: out std_logic_vector(18 downto 0);
	dout_2: out std_logic_vector(18 downto 0));
END fr_cmplr_v5_0_1877c081b7e2f892;

ARCHITECTURE fr_cmplr_v5_0_1877c081b7e2f892_a OF fr_cmplr_v5_0_1877c081b7e2f892 IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_1877c081b7e2f892
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(16 downto 0);
	din_2: in std_logic_vector(16 downto 0);
	dout_1: out std_logic_vector(18 downto 0);
	dout_2: out std_logic_vector(18 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_1877c081b7e2f892 use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_1877c081b7e2f892",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 19,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 12,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 37,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 1,
			data_width => 17,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 6,
			data_type => 0,
			accum_width => 36,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 2,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 1,
			num_taps => 19,
			c_mem_init_file => "fr_cmplr_v5_0_1877c081b7e2f892.mif",
			zero_packing_factor => 1,
			num_paths => 2,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_1877c081b7e2f892
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			din_1 => din_1,
			din_2 => din_2,
			dout_1 => dout_1,
			dout_2 => dout_2);
-- synthesis translate_on

END fr_cmplr_v5_0_1877c081b7e2f892_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_51d88ed999503974.vhd when simulating
-- the core, fr_cmplr_v5_0_51d88ed999503974. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_51d88ed999503974 IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(16 downto 0);
	dout: out std_logic_vector(31 downto 0));
END fr_cmplr_v5_0_51d88ed999503974;

ARCHITECTURE fr_cmplr_v5_0_51d88ed999503974_a OF fr_cmplr_v5_0_51d88ed999503974 IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_51d88ed999503974
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(16 downto 0);
	dout: out std_logic_vector(31 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_51d88ed999503974 use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_51d88ed999503974",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 32,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 24,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 20,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 2,
			data_width => 17,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 7,
			data_type => 0,
			accum_width => 35,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 1,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 2,
			num_taps => 11,
			c_mem_init_file => "fr_cmplr_v5_0_51d88ed999503974.mif",
			zero_packing_factor => 1,
			num_paths => 1,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_51d88ed999503974
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			chan_in => chan_in,
			chan_out => chan_out,
			din => din,
			dout => dout);
-- synthesis translate_on

END fr_cmplr_v5_0_51d88ed999503974_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_670158bb462abb3d.vhd when simulating
-- the core, fr_cmplr_v5_0_670158bb462abb3d. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_670158bb462abb3d IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(15 downto 0);
	dout: out std_logic_vector(17 downto 0));
END fr_cmplr_v5_0_670158bb462abb3d;

ARCHITECTURE fr_cmplr_v5_0_670158bb462abb3d_a OF fr_cmplr_v5_0_670158bb462abb3d IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_670158bb462abb3d
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(15 downto 0);
	dout: out std_logic_vector(17 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_670158bb462abb3d use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_670158bb462abb3d",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 18,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 48,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 29,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 2,
			data_width => 16,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 7,
			data_type => 0,
			accum_width => 34,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 1,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 2,
			num_taps => 23,
			c_mem_init_file => "fr_cmplr_v5_0_670158bb462abb3d.mif",
			zero_packing_factor => 1,
			num_paths => 1,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_670158bb462abb3d
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			chan_in => chan_in,
			chan_out => chan_out,
			din => din,
			dout => dout);
-- synthesis translate_on

END fr_cmplr_v5_0_670158bb462abb3d_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_cd9071975244390f.vhd when simulating
-- the core, fr_cmplr_v5_0_cd9071975244390f. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_cd9071975244390f IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(17 downto 0);
	din_2: in std_logic_vector(17 downto 0);
	dout_1: out std_logic_vector(37 downto 0);
	dout_2: out std_logic_vector(37 downto 0));
END fr_cmplr_v5_0_cd9071975244390f;

ARCHITECTURE fr_cmplr_v5_0_cd9071975244390f_a OF fr_cmplr_v5_0_cd9071975244390f IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_cd9071975244390f
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(17 downto 0);
	din_2: in std_logic_vector(17 downto 0);
	dout_1: out std_logic_vector(37 downto 0);
	dout_2: out std_logic_vector(37 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_cd9071975244390f use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_cd9071975244390f",
			c_family => "virtex6",
			round_mode => 0,
			output_width => 38,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 24,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 32,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 1,
			data_width => 18,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 1,
			data_type => 0,
			accum_width => 38,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 2,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 1,
			num_taps => 95,
			c_mem_init_file => "fr_cmplr_v5_0_cd9071975244390f.mif",
			zero_packing_factor => 1,
			num_paths => 2,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_cd9071975244390f
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			din_1 => din_1,
			din_2 => din_2,
			dout_1 => dout_1,
			dout_2 => dout_2);
-- synthesis translate_on

END fr_cmplr_v5_0_cd9071975244390f_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_e37f692facadc69b.vhd when simulating
-- the core, fr_cmplr_v5_0_e37f692facadc69b. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_e37f692facadc69b IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(16 downto 0);
	dout: out std_logic_vector(18 downto 0));
END fr_cmplr_v5_0_e37f692facadc69b;

ARCHITECTURE fr_cmplr_v5_0_e37f692facadc69b_a OF fr_cmplr_v5_0_e37f692facadc69b IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_e37f692facadc69b
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	chan_in: out std_logic_vector(0 downto 0);
	chan_out: out std_logic_vector(0 downto 0);
	din: in std_logic_vector(16 downto 0);
	dout: out std_logic_vector(18 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_e37f692facadc69b use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_e37f692facadc69b",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 19,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 12,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 17,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 2,
			data_width => 17,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 7,
			data_type => 0,
			accum_width => 35,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 1,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 2,
			num_taps => 11,
			c_mem_init_file => "fr_cmplr_v5_0_e37f692facadc69b.mif",
			zero_packing_factor => 1,
			num_paths => 1,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_e37f692facadc69b
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			chan_in => chan_in,
			chan_out => chan_out,
			din => din,
			dout => dout);
-- synthesis translate_on

END fr_cmplr_v5_0_e37f692facadc69b_a;

--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------
-- You must compile the wrapper file fr_cmplr_v5_0_f5119409b2c69958.vhd when simulating
-- the core, fr_cmplr_v5_0_f5119409b2c69958. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY fr_cmplr_v5_0_f5119409b2c69958 IS
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(15 downto 0);
	din_2: in std_logic_vector(15 downto 0);
	dout_1: out std_logic_vector(17 downto 0);
	dout_2: out std_logic_vector(17 downto 0));
END fr_cmplr_v5_0_f5119409b2c69958;

ARCHITECTURE fr_cmplr_v5_0_f5119409b2c69958_a OF fr_cmplr_v5_0_f5119409b2c69958 IS
-- synthesis translate_off
component wrapped_fr_cmplr_v5_0_f5119409b2c69958
	port (
	clk: in std_logic;
	ce: in std_logic;
	nd: in std_logic;
	rfd: out std_logic;
	rdy: out std_logic;
	din_1: in std_logic_vector(15 downto 0);
	din_2: in std_logic_vector(15 downto 0);
	dout_1: out std_logic_vector(17 downto 0);
	dout_2: out std_logic_vector(17 downto 0));
end component;

-- Configuration specification 
	for all : wrapped_fr_cmplr_v5_0_f5119409b2c69958 use entity XilinxCoreLib.fir_compiler_v5_0(behavioral)
		generic map(
			coef_width => 18,
			c_has_sclr => 0,
			datapath_memtype => 0,
			c_component_name => "fr_cmplr_v5_0_f5119409b2c69958",
			c_family => "virtex6",
			round_mode => 3,
			output_width => 18,
			sclr_deterministic => 0,
			col_config => "1",
			coef_memtype => 0,
			clock_freq => 6,
			symmetry => 1,
			col_pipe_len => 4,
			c_latency => 25,
			chan_sel_width => 1,
			c_xdevicefamily => "virtex6",
			c_has_nd => 1,
			allow_approx => 0,
			num_channels => 1,
			data_width => 16,
			filter_sel_width => 1,
			sample_freq => 1,
			coef_reload => 0,
			neg_symmetry => 0,
			filter_type => 6,
			data_type => 0,
			accum_width => 35,
			rate_change_type => 0,
			ipbuff_memtype => 0,
			c_optimization => 1,
			output_reg => 1,
			data_memtype => 0,
			c_has_data_valid => 0,
			decim_rate => 2,
			coef_type => 0,
			filter_arch => 1,
			interp_rate => 1,
			num_taps => 11,
			c_mem_init_file => "fr_cmplr_v5_0_f5119409b2c69958.mif",
			zero_packing_factor => 1,
			num_paths => 2,
			num_filts => 1,
			col_mode => 0,
			c_has_ce => 1,
			chan_in_adv => 0,
			opbuff_memtype => 0,
			odd_symmetry => 1);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_fr_cmplr_v5_0_f5119409b2c69958
		port map (
			clk => clk,
			ce => ce,
			nd => nd,
			rfd => rfd,
			rdy => rdy,
			din_1 => din_1,
			din_2 => din_2,
			dout_1 => dout_1,
			dout_2 => dout_2);
-- synthesis translate_on

END fr_cmplr_v5_0_f5119409b2c69958_a;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
package conv_pkg is
    constant simulating : boolean := false
      -- synopsys translate_off
        or true
      -- synopsys translate_on
    ;
    constant xlUnsigned : integer := 1;
    constant xlSigned : integer := 2;
    constant xlWrap : integer := 1;
    constant xlSaturate : integer := 2;
    constant xlTruncate : integer := 1;
    constant xlRound : integer := 2;
    constant xlRoundBanker : integer := 3;
    constant xlAddMode : integer := 1;
    constant xlSubMode : integer := 2;
    attribute black_box : boolean;
    attribute syn_black_box : boolean;
    attribute fpga_dont_touch: string;
    attribute box_type :  string;
    attribute keep : string;
    attribute syn_keep : boolean;
    function std_logic_vector_to_unsigned(inp : std_logic_vector) return unsigned;
    function unsigned_to_std_logic_vector(inp : unsigned) return std_logic_vector;
    function std_logic_vector_to_signed(inp : std_logic_vector) return signed;
    function signed_to_std_logic_vector(inp : signed) return std_logic_vector;
    function unsigned_to_signed(inp : unsigned) return signed;
    function signed_to_unsigned(inp : signed) return unsigned;
    function pos(inp : std_logic_vector; arith : INTEGER) return boolean;
    function all_same(inp: std_logic_vector) return boolean;
    function all_zeros(inp: std_logic_vector) return boolean;
    function is_point_five(inp: std_logic_vector) return boolean;
    function all_ones(inp: std_logic_vector) return boolean;
    function convert_type (inp : std_logic_vector; old_width, old_bin_pt,
                           old_arith, new_width, new_bin_pt, new_arith,
                           quantization, overflow : INTEGER)
        return std_logic_vector;
    function cast (inp : std_logic_vector; old_bin_pt,
                   new_width, new_bin_pt, new_arith : INTEGER)
        return std_logic_vector;
    function vec_slice (inp : std_logic_vector; upper, lower : INTEGER)
        return std_logic_vector;
    function s2u_slice (inp : signed; upper, lower : INTEGER)
        return unsigned;
    function u2u_slice (inp : unsigned; upper, lower : INTEGER)
        return unsigned;
    function s2s_cast (inp : signed; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return signed;
    function u2s_cast (inp : unsigned; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return signed;
    function s2u_cast (inp : signed; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return unsigned;
    function u2u_cast (inp : unsigned; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return unsigned;
    function u2v_cast (inp : unsigned; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return std_logic_vector;
    function s2v_cast (inp : signed; old_bin_pt,
                   new_width, new_bin_pt : INTEGER)
        return std_logic_vector;
    function trunc (inp : std_logic_vector; old_width, old_bin_pt, old_arith,
                    new_width, new_bin_pt, new_arith : INTEGER)
        return std_logic_vector;
    function round_towards_inf (inp : std_logic_vector; old_width, old_bin_pt,
                                old_arith, new_width, new_bin_pt,
                                new_arith : INTEGER) return std_logic_vector;
    function round_towards_even (inp : std_logic_vector; old_width, old_bin_pt,
                                old_arith, new_width, new_bin_pt,
                                new_arith : INTEGER) return std_logic_vector;
    function max_signed(width : INTEGER) return std_logic_vector;
    function min_signed(width : INTEGER) return std_logic_vector;
    function saturation_arith(inp:  std_logic_vector;  old_width, old_bin_pt,
                              old_arith, new_width, new_bin_pt, new_arith
                              : INTEGER) return std_logic_vector;
    function wrap_arith(inp:  std_logic_vector;  old_width, old_bin_pt,
                        old_arith, new_width, new_bin_pt, new_arith : INTEGER)
                        return std_logic_vector;
    function fractional_bits(a_bin_pt, b_bin_pt: INTEGER) return INTEGER;
    function integer_bits(a_width, a_bin_pt, b_width, b_bin_pt: INTEGER)
        return INTEGER;
    function sign_ext(inp : std_logic_vector; new_width : INTEGER)
        return std_logic_vector;
    function zero_ext(inp : std_logic_vector; new_width : INTEGER)
        return std_logic_vector;
    function zero_ext(inp : std_logic; new_width : INTEGER)
        return std_logic_vector;
    function extend_MSB(inp : std_logic_vector; new_width, arith : INTEGER)
        return std_logic_vector;
    function align_input(inp : std_logic_vector; old_width, delta, new_arith,
                          new_width: INTEGER)
        return std_logic_vector;
    function pad_LSB(inp : std_logic_vector; new_width: integer)
        return std_logic_vector;
    function pad_LSB(inp : std_logic_vector; new_width, arith : integer)
        return std_logic_vector;
    function max(L, R: INTEGER) return INTEGER;
    function min(L, R: INTEGER) return INTEGER;
    function "="(left,right: STRING) return boolean;
    function boolean_to_signed (inp : boolean; width: integer)
        return signed;
    function boolean_to_unsigned (inp : boolean; width: integer)
        return unsigned;
    function boolean_to_vector (inp : boolean)
        return std_logic_vector;
    function std_logic_to_vector (inp : std_logic)
        return std_logic_vector;
    function integer_to_std_logic_vector (inp : integer;  width, arith : integer)
        return std_logic_vector;
    function std_logic_vector_to_integer (inp : std_logic_vector;  arith : integer)
        return integer;
    function std_logic_to_integer(constant inp : std_logic := '0')
        return integer;
    function bin_string_element_to_std_logic_vector (inp : string;  width, index : integer)
        return std_logic_vector;
    function bin_string_to_std_logic_vector (inp : string)
        return std_logic_vector;
    function hex_string_to_std_logic_vector (inp : string; width : integer)
        return std_logic_vector;
    function makeZeroBinStr (width : integer) return STRING;
    function and_reduce(inp: std_logic_vector) return std_logic;
    -- synopsys translate_off
    function is_binary_string_invalid (inp : string)
        return boolean;
    function is_binary_string_undefined (inp : string)
        return boolean;
    function is_XorU(inp : std_logic_vector)
        return boolean;
    function to_real(inp : std_logic_vector; bin_pt : integer; arith : integer)
        return real;
    function std_logic_to_real(inp : std_logic; bin_pt : integer; arith : integer)
        return real;
    function real_to_std_logic_vector (inp : real;  width, bin_pt, arith : integer)
        return std_logic_vector;
    function real_string_to_std_logic_vector (inp : string;  width, bin_pt, arith : integer)
        return std_logic_vector;
    constant display_precision : integer := 20;
    function real_to_string (inp : real) return string;
    function valid_bin_string(inp : string) return boolean;
    function std_logic_vector_to_bin_string(inp : std_logic_vector) return string;
    function std_logic_to_bin_string(inp : std_logic) return string;
    function std_logic_vector_to_bin_string_w_point(inp : std_logic_vector; bin_pt : integer)
        return string;
    function real_to_bin_string(inp : real;  width, bin_pt, arith : integer)
        return string;
    type stdlogic_to_char_t is array(std_logic) of character;
    constant to_char : stdlogic_to_char_t := (
        'U' => 'U',
        'X' => 'X',
        '0' => '0',
        '1' => '1',
        'Z' => 'Z',
        'W' => 'W',
        'L' => 'L',
        'H' => 'H',
        '-' => '-');
    -- synopsys translate_on
end conv_pkg;
package body conv_pkg is
    function std_logic_vector_to_unsigned(inp : std_logic_vector)
        return unsigned
    is
    begin
        return unsigned (inp);
    end;
    function unsigned_to_std_logic_vector(inp : unsigned)
        return std_logic_vector
    is
    begin
        return std_logic_vector(inp);
    end;
    function std_logic_vector_to_signed(inp : std_logic_vector)
        return signed
    is
    begin
        return  signed (inp);
    end;
    function signed_to_std_logic_vector(inp : signed)
        return std_logic_vector
    is
    begin
        return std_logic_vector(inp);
    end;
    function unsigned_to_signed (inp : unsigned)
        return signed
    is
    begin
        return signed(std_logic_vector(inp));
    end;
    function signed_to_unsigned (inp : signed)
        return unsigned
    is
    begin
        return unsigned(std_logic_vector(inp));
    end;
    function pos(inp : std_logic_vector; arith : INTEGER)
        return boolean
    is
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
    begin
        vec := inp;
        if arith = xlUnsigned then
            return true;
        else
            if vec(width-1) = '0' then
                return true;
            else
                return false;
            end if;
        end if;
        return true;
    end;
    function max_signed(width : INTEGER)
        return std_logic_vector
    is
        variable ones : std_logic_vector(width-2 downto 0);
        variable result : std_logic_vector(width-1 downto 0);
    begin
        ones := (others => '1');
        result(width-1) := '0';
        result(width-2 downto 0) := ones;
        return result;
    end;
    function min_signed(width : INTEGER)
        return std_logic_vector
    is
        variable zeros : std_logic_vector(width-2 downto 0);
        variable result : std_logic_vector(width-1 downto 0);
    begin
        zeros := (others => '0');
        result(width-1) := '1';
        result(width-2 downto 0) := zeros;
        return result;
    end;
    function and_reduce(inp: std_logic_vector) return std_logic
    is
        variable result: std_logic;
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
    begin
        vec := inp;
        result := vec(0);
        if width > 1 then
            for i in 1 to width-1 loop
                result := result and vec(i);
            end loop;
        end if;
        return result;
    end;
    function all_same(inp: std_logic_vector) return boolean
    is
        variable result: boolean;
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
    begin
        vec := inp;
        result := true;
        if width > 0 then
            for i in 1 to width-1 loop
                if vec(i) /= vec(0) then
                    result := false;
                end if;
            end loop;
        end if;
        return result;
    end;
    function all_zeros(inp: std_logic_vector)
        return boolean
    is
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
        variable zero : std_logic_vector(width-1 downto 0);
        variable result : boolean;
    begin
        zero := (others => '0');
        vec := inp;
        -- synopsys translate_off
        if (is_XorU(vec)) then
            return false;
        end if;
         -- synopsys translate_on
        if (std_logic_vector_to_unsigned(vec) = std_logic_vector_to_unsigned(zero)) then
            result := true;
        else
            result := false;
        end if;
        return result;
    end;
    function is_point_five(inp: std_logic_vector)
        return boolean
    is
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
        variable result : boolean;
    begin
        vec := inp;
        -- synopsys translate_off
        if (is_XorU(vec)) then
            return false;
        end if;
         -- synopsys translate_on
        if (width > 1) then
           if ((vec(width-1) = '1') and (all_zeros(vec(width-2 downto 0)) = true)) then
               result := true;
           else
               result := false;
           end if;
        else
           if (vec(width-1) = '1') then
               result := true;
           else
               result := false;
           end if;
        end if;
        return result;
    end;
    function all_ones(inp: std_logic_vector)
        return boolean
    is
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
        variable one : std_logic_vector(width-1 downto 0);
        variable result : boolean;
    begin
        one := (others => '1');
        vec := inp;
        -- synopsys translate_off
        if (is_XorU(vec)) then
            return false;
        end if;
         -- synopsys translate_on
        if (std_logic_vector_to_unsigned(vec) = std_logic_vector_to_unsigned(one)) then
            result := true;
        else
            result := false;
        end if;
        return result;
    end;
    function full_precision_num_width(quantization, overflow, old_width,
                                      old_bin_pt, old_arith,
                                      new_width, new_bin_pt, new_arith : INTEGER)
        return integer
    is
        variable result : integer;
    begin
        result := old_width + 2;
        return result;
    end;
    function quantized_num_width(quantization, overflow, old_width, old_bin_pt,
                                 old_arith, new_width, new_bin_pt, new_arith
                                 : INTEGER)
        return integer
    is
        variable right_of_dp, left_of_dp, result : integer;
    begin
        right_of_dp := max(new_bin_pt, old_bin_pt);
        left_of_dp := max((new_width - new_bin_pt), (old_width - old_bin_pt));
        result := (old_width + 2) + (new_bin_pt - old_bin_pt);
        return result;
    end;
    function convert_type (inp : std_logic_vector; old_width, old_bin_pt,
                           old_arith, new_width, new_bin_pt, new_arith,
                           quantization, overflow : INTEGER)
        return std_logic_vector
    is
        constant fp_width : integer :=
            full_precision_num_width(quantization, overflow, old_width,
                                     old_bin_pt, old_arith, new_width,
                                     new_bin_pt, new_arith);
        constant fp_bin_pt : integer := old_bin_pt;
        constant fp_arith : integer := old_arith;
        variable full_precision_result : std_logic_vector(fp_width-1 downto 0);
        constant q_width : integer :=
            quantized_num_width(quantization, overflow, old_width, old_bin_pt,
                                old_arith, new_width, new_bin_pt, new_arith);
        constant q_bin_pt : integer := new_bin_pt;
        constant q_arith : integer := old_arith;
        variable quantized_result : std_logic_vector(q_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        result := (others => '0');
        full_precision_result := cast(inp, old_bin_pt, fp_width, fp_bin_pt,
                                      fp_arith);
        if (quantization = xlRound) then
            quantized_result := round_towards_inf(full_precision_result,
                                                  fp_width, fp_bin_pt,
                                                  fp_arith, q_width, q_bin_pt,
                                                  q_arith);
        elsif (quantization = xlRoundBanker) then
            quantized_result := round_towards_even(full_precision_result,
                                                  fp_width, fp_bin_pt,
                                                  fp_arith, q_width, q_bin_pt,
                                                  q_arith);
        else
            quantized_result := trunc(full_precision_result, fp_width, fp_bin_pt,
                                      fp_arith, q_width, q_bin_pt, q_arith);
        end if;
        if (overflow = xlSaturate) then
            result := saturation_arith(quantized_result, q_width, q_bin_pt,
                                       q_arith, new_width, new_bin_pt, new_arith);
        else
             result := wrap_arith(quantized_result, q_width, q_bin_pt, q_arith,
                                  new_width, new_bin_pt, new_arith);
        end if;
        return result;
    end;
    function cast (inp : std_logic_vector; old_bin_pt, new_width,
                   new_bin_pt, new_arith : INTEGER)
        return std_logic_vector
    is
        constant old_width : integer := inp'length;
        constant left_of_dp : integer := (new_width - new_bin_pt)
                                         - (old_width - old_bin_pt);
        constant right_of_dp : integer := (new_bin_pt - old_bin_pt);
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
        variable j   : integer;
    begin
        vec := inp;
        for i in new_width-1 downto 0 loop
            j := i - right_of_dp;
            if ( j > old_width-1) then
                if (new_arith = xlUnsigned) then
                    result(i) := '0';
                else
                    result(i) := vec(old_width-1);
                end if;
            elsif ( j >= 0) then
                result(i) := vec(j);
            else
                result(i) := '0';
            end if;
        end loop;
        return result;
    end;
    function vec_slice (inp : std_logic_vector; upper, lower : INTEGER)
      return std_logic_vector
    is
    begin
        return inp(upper downto lower);
    end;
    function s2u_slice (inp : signed; upper, lower : INTEGER)
      return unsigned
    is
    begin
        return unsigned(vec_slice(std_logic_vector(inp), upper, lower));
    end;
    function u2u_slice (inp : unsigned; upper, lower : INTEGER)
      return unsigned
    is
    begin
        return unsigned(vec_slice(std_logic_vector(inp), upper, lower));
    end;
    function s2s_cast (inp : signed; old_bin_pt, new_width, new_bin_pt : INTEGER)
        return signed
    is
    begin
        return signed(cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlSigned));
    end;
    function s2u_cast (inp : signed; old_bin_pt, new_width,
                   new_bin_pt : INTEGER)
        return unsigned
    is
    begin
        return unsigned(cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlSigned));
    end;
    function u2s_cast (inp : unsigned; old_bin_pt, new_width,
                   new_bin_pt : INTEGER)
        return signed
    is
    begin
        return signed(cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlUnsigned));
    end;
    function u2u_cast (inp : unsigned; old_bin_pt, new_width,
                   new_bin_pt : INTEGER)
        return unsigned
    is
    begin
        return unsigned(cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlUnsigned));
    end;
    function u2v_cast (inp : unsigned; old_bin_pt, new_width,
                   new_bin_pt : INTEGER)
        return std_logic_vector
    is
    begin
        return cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlUnsigned);
    end;
    function s2v_cast (inp : signed; old_bin_pt, new_width,
                   new_bin_pt : INTEGER)
        return std_logic_vector
    is
    begin
        return cast(std_logic_vector(inp), old_bin_pt, new_width, new_bin_pt, xlSigned);
    end;
    function boolean_to_signed (inp : boolean; width : integer)
        return signed
    is
        variable result : signed(width - 1 downto 0);
    begin
        result := (others => '0');
        if inp then
          result(0) := '1';
        else
          result(0) := '0';
        end if;
        return result;
    end;
    function boolean_to_unsigned (inp : boolean; width : integer)
        return unsigned
    is
        variable result : unsigned(width - 1 downto 0);
    begin
        result := (others => '0');
        if inp then
          result(0) := '1';
        else
          result(0) := '0';
        end if;
        return result;
    end;
    function boolean_to_vector (inp : boolean)
        return std_logic_vector
    is
        variable result : std_logic_vector(1 - 1 downto 0);
    begin
        result := (others => '0');
        if inp then
          result(0) := '1';
        else
          result(0) := '0';
        end if;
        return result;
    end;
    function std_logic_to_vector (inp : std_logic)
        return std_logic_vector
    is
        variable result : std_logic_vector(1 - 1 downto 0);
    begin
        result(0) := inp;
        return result;
    end;
    function trunc (inp : std_logic_vector; old_width, old_bin_pt, old_arith,
                                new_width, new_bin_pt, new_arith : INTEGER)
        return std_logic_vector
    is
        constant right_of_dp : integer := (old_bin_pt - new_bin_pt);
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if right_of_dp >= 0 then
            if new_arith = xlUnsigned then
                result := zero_ext(vec(old_width-1 downto right_of_dp), new_width);
            else
                result := sign_ext(vec(old_width-1 downto right_of_dp), new_width);
            end if;
        else
            if new_arith = xlUnsigned then
                result := zero_ext(pad_LSB(vec, old_width +
                                           abs(right_of_dp)), new_width);
            else
                result := sign_ext(pad_LSB(vec, old_width +
                                           abs(right_of_dp)), new_width);
            end if;
        end if;
        return result;
    end;
    function round_towards_inf (inp : std_logic_vector; old_width, old_bin_pt,
                                old_arith, new_width, new_bin_pt, new_arith
                                : INTEGER)
        return std_logic_vector
    is
        constant right_of_dp : integer := (old_bin_pt - new_bin_pt);
        constant expected_new_width : integer :=  old_width - right_of_dp  + 1;
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable one_or_zero : std_logic_vector(new_width-1 downto 0);
        variable truncated_val : std_logic_vector(new_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if right_of_dp >= 0 then
            if new_arith = xlUnsigned then
                truncated_val := zero_ext(vec(old_width-1 downto right_of_dp),
                                          new_width);
            else
                truncated_val := sign_ext(vec(old_width-1 downto right_of_dp),
                                          new_width);
            end if;
        else
            if new_arith = xlUnsigned then
                truncated_val := zero_ext(pad_LSB(vec, old_width +
                                                  abs(right_of_dp)), new_width);
            else
                truncated_val := sign_ext(pad_LSB(vec, old_width +
                                                  abs(right_of_dp)), new_width);
            end if;
        end if;
        one_or_zero := (others => '0');
        if (new_arith = xlSigned) then
            if (vec(old_width-1) = '0') then
                one_or_zero(0) := '1';
            end if;
            if (right_of_dp >= 2) and (right_of_dp <= old_width) then
                if (all_zeros(vec(right_of_dp-2 downto 0)) = false) then
                    one_or_zero(0) := '1';
                end if;
            end if;
            if (right_of_dp >= 1) and (right_of_dp <= old_width) then
                if vec(right_of_dp-1) = '0' then
                    one_or_zero(0) := '0';
                end if;
            else
                one_or_zero(0) := '0';
            end if;
        else
            if (right_of_dp >= 1) and (right_of_dp <= old_width) then
                one_or_zero(0) :=  vec(right_of_dp-1);
            end if;
        end if;
        if new_arith = xlSigned then
            result := signed_to_std_logic_vector(std_logic_vector_to_signed(truncated_val) +
                                                 std_logic_vector_to_signed(one_or_zero));
        else
            result := unsigned_to_std_logic_vector(std_logic_vector_to_unsigned(truncated_val) +
                                                  std_logic_vector_to_unsigned(one_or_zero));
        end if;
        return result;
    end;
    function round_towards_even (inp : std_logic_vector; old_width, old_bin_pt,
                                old_arith, new_width, new_bin_pt, new_arith
                                : INTEGER)
        return std_logic_vector
    is
        constant right_of_dp : integer := (old_bin_pt - new_bin_pt);
        constant expected_new_width : integer :=  old_width - right_of_dp  + 1;
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable one_or_zero : std_logic_vector(new_width-1 downto 0);
        variable truncated_val : std_logic_vector(new_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if right_of_dp >= 0 then
            if new_arith = xlUnsigned then
                truncated_val := zero_ext(vec(old_width-1 downto right_of_dp),
                                          new_width);
            else
                truncated_val := sign_ext(vec(old_width-1 downto right_of_dp),
                                          new_width);
            end if;
        else
            if new_arith = xlUnsigned then
                truncated_val := zero_ext(pad_LSB(vec, old_width +
                                                  abs(right_of_dp)), new_width);
            else
                truncated_val := sign_ext(pad_LSB(vec, old_width +
                                                  abs(right_of_dp)), new_width);
            end if;
        end if;
        one_or_zero := (others => '0');
        if (right_of_dp >= 1) and (right_of_dp <= old_width) then
            if (is_point_five(vec(right_of_dp-1 downto 0)) = false) then
                one_or_zero(0) :=  vec(right_of_dp-1);
            else
                one_or_zero(0) :=  vec(right_of_dp);
            end if;
        end if;
        if new_arith = xlSigned then
            result := signed_to_std_logic_vector(std_logic_vector_to_signed(truncated_val) +
                                                 std_logic_vector_to_signed(one_or_zero));
        else
            result := unsigned_to_std_logic_vector(std_logic_vector_to_unsigned(truncated_val) +
                                                  std_logic_vector_to_unsigned(one_or_zero));
        end if;
        return result;
    end;
    function saturation_arith(inp:  std_logic_vector;  old_width, old_bin_pt,
                              old_arith, new_width, new_bin_pt, new_arith
                              : INTEGER)
        return std_logic_vector
    is
        constant left_of_dp : integer := (old_width - old_bin_pt) -
                                         (new_width - new_bin_pt);
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
        variable overflow : boolean;
    begin
        vec := inp;
        overflow := true;
        result := (others => '0');
        if (new_width >= old_width) then
            overflow := false;
        end if;
        if ((old_arith = xlSigned and new_arith = xlSigned) and (old_width > new_width)) then
            if all_same(vec(old_width-1 downto new_width-1)) then
                overflow := false;
            end if;
        end if;
        if (old_arith = xlSigned and new_arith = xlUnsigned) then
            if (old_width > new_width) then
                if all_zeros(vec(old_width-1 downto new_width)) then
                    overflow := false;
                end if;
            else
                if (old_width = new_width) then
                    if (vec(new_width-1) = '0') then
                        overflow := false;
                    end if;
                end if;
            end if;
        end if;
        if (old_arith = xlUnsigned and new_arith = xlUnsigned) then
            if (old_width > new_width) then
                if all_zeros(vec(old_width-1 downto new_width)) then
                    overflow := false;
                end if;
            else
                if (old_width = new_width) then
                    overflow := false;
                end if;
            end if;
        end if;
        if ((old_arith = xlUnsigned and new_arith = xlSigned) and (old_width > new_width)) then
            if all_same(vec(old_width-1 downto new_width-1)) then
                overflow := false;
            end if;
        end if;
        if overflow then
            if new_arith = xlSigned then
                if vec(old_width-1) = '0' then
                    result := max_signed(new_width);
                else
                    result := min_signed(new_width);
                end if;
            else
                if ((old_arith = xlSigned) and vec(old_width-1) = '1') then
                    result := (others => '0');
                else
                    result := (others => '1');
                end if;
            end if;
        else
            if (old_arith = xlSigned) and (new_arith = xlUnsigned) then
                if (vec(old_width-1) = '1') then
                    vec := (others => '0');
                end if;
            end if;
            if new_width <= old_width then
                result := vec(new_width-1 downto 0);
            else
                if new_arith = xlUnsigned then
                    result := zero_ext(vec, new_width);
                else
                    result := sign_ext(vec, new_width);
                end if;
            end if;
        end if;
        return result;
    end;
   function wrap_arith(inp:  std_logic_vector;  old_width, old_bin_pt,
                       old_arith, new_width, new_bin_pt, new_arith : INTEGER)
        return std_logic_vector
    is
        variable result : std_logic_vector(new_width-1 downto 0);
        variable result_arith : integer;
    begin
        if (old_arith = xlSigned) and (new_arith = xlUnsigned) then
            result_arith := xlSigned;
        end if;
        result := cast(inp, old_bin_pt, new_width, new_bin_pt, result_arith);
        return result;
    end;
    function fractional_bits(a_bin_pt, b_bin_pt: INTEGER) return INTEGER is
    begin
        return max(a_bin_pt, b_bin_pt);
    end;
    function integer_bits(a_width, a_bin_pt, b_width, b_bin_pt: INTEGER)
        return INTEGER is
    begin
        return  max(a_width - a_bin_pt, b_width - b_bin_pt);
    end;
    function pad_LSB(inp : std_logic_vector; new_width: integer)
        return STD_LOGIC_VECTOR
    is
        constant orig_width : integer := inp'length;
        variable vec : std_logic_vector(orig_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
        variable pos : integer;
        constant pad_pos : integer := new_width - orig_width - 1;
    begin
        vec := inp;
        pos := new_width-1;
        if (new_width >= orig_width) then
            for i in orig_width-1 downto 0 loop
                result(pos) := vec(i);
                pos := pos - 1;
            end loop;
            if pad_pos >= 0 then
                for i in pad_pos downto 0 loop
                    result(i) := '0';
                end loop;
            end if;
        end if;
        return result;
    end;
    function sign_ext(inp : std_logic_vector; new_width : INTEGER)
        return std_logic_vector
    is
        constant old_width : integer := inp'length;
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if new_width >= old_width then
            result(old_width-1 downto 0) := vec;
            if new_width-1 >= old_width then
                for i in new_width-1 downto old_width loop
                    result(i) := vec(old_width-1);
                end loop;
            end if;
        else
            result(new_width-1 downto 0) := vec(new_width-1 downto 0);
        end if;
        return result;
    end;
    function zero_ext(inp : std_logic_vector; new_width : INTEGER)
        return std_logic_vector
    is
        constant old_width : integer := inp'length;
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if new_width >= old_width then
            result(old_width-1 downto 0) := vec;
            if new_width-1 >= old_width then
                for i in new_width-1 downto old_width loop
                    result(i) := '0';
                end loop;
            end if;
        else
            result(new_width-1 downto 0) := vec(new_width-1 downto 0);
        end if;
        return result;
    end;
    function zero_ext(inp : std_logic; new_width : INTEGER)
        return std_logic_vector
    is
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        result(0) := inp;
        for i in new_width-1 downto 1 loop
            result(i) := '0';
        end loop;
        return result;
    end;
    function extend_MSB(inp : std_logic_vector; new_width, arith : INTEGER)
        return std_logic_vector
    is
        constant orig_width : integer := inp'length;
        variable vec : std_logic_vector(orig_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if arith = xlUnsigned then
            result := zero_ext(vec, new_width);
        else
            result := sign_ext(vec, new_width);
        end if;
        return result;
    end;
    function pad_LSB(inp : std_logic_vector; new_width, arith: integer)
        return STD_LOGIC_VECTOR
    is
        constant orig_width : integer := inp'length;
        variable vec : std_logic_vector(orig_width-1 downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
        variable pos : integer;
    begin
        vec := inp;
        pos := new_width-1;
        if (arith = xlUnsigned) then
            result(pos) := '0';
            pos := pos - 1;
        else
            result(pos) := vec(orig_width-1);
            pos := pos - 1;
        end if;
        if (new_width >= orig_width) then
            for i in orig_width-1 downto 0 loop
                result(pos) := vec(i);
                pos := pos - 1;
            end loop;
            if pos >= 0 then
                for i in pos downto 0 loop
                    result(i) := '0';
                end loop;
            end if;
        end if;
        return result;
    end;
    function align_input(inp : std_logic_vector; old_width, delta, new_arith,
                         new_width: INTEGER)
        return std_logic_vector
    is
        variable vec : std_logic_vector(old_width-1 downto 0);
        variable padded_inp : std_logic_vector((old_width + delta)-1  downto 0);
        variable result : std_logic_vector(new_width-1 downto 0);
    begin
        vec := inp;
        if delta > 0 then
            padded_inp := pad_LSB(vec, old_width+delta);
            result := extend_MSB(padded_inp, new_width, new_arith);
        else
            result := extend_MSB(vec, new_width, new_arith);
        end if;
        return result;
    end;
    function max(L, R: INTEGER) return INTEGER is
    begin
        if L > R then
            return L;
        else
            return R;
        end if;
    end;
    function min(L, R: INTEGER) return INTEGER is
    begin
        if L < R then
            return L;
        else
            return R;
        end if;
    end;
    function "="(left,right: STRING) return boolean is
    begin
        if (left'length /= right'length) then
            return false;
        else
            test : for i in 1 to left'length loop
                if left(i) /= right(i) then
                    return false;
                end if;
            end loop test;
            return true;
        end if;
    end;
    -- synopsys translate_off
    function is_binary_string_invalid (inp : string)
        return boolean
    is
        variable vec : string(1 to inp'length);
        variable result : boolean;
    begin
        vec := inp;
        result := false;
        for i in 1 to vec'length loop
            if ( vec(i) = 'X' ) then
                result := true;
            end if;
        end loop;
        return result;
    end;
    function is_binary_string_undefined (inp : string)
        return boolean
    is
        variable vec : string(1 to inp'length);
        variable result : boolean;
    begin
        vec := inp;
        result := false;
        for i in 1 to vec'length loop
            if ( vec(i) = 'U' ) then
                result := true;
            end if;
        end loop;
        return result;
    end;
    function is_XorU(inp : std_logic_vector)
        return boolean
    is
        constant width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
        variable result : boolean;
    begin
        vec := inp;
        result := false;
        for i in 0 to width-1 loop
            if (vec(i) = 'U') or (vec(i) = 'X') then
                result := true;
            end if;
        end loop;
        return result;
    end;
    function to_real(inp : std_logic_vector; bin_pt : integer; arith : integer)
        return real
    is
        variable  vec : std_logic_vector(inp'length-1 downto 0);
        variable result, shift_val, undefined_real : real;
        variable neg_num : boolean;
    begin
        vec := inp;
        result := 0.0;
        neg_num := false;
        if vec(inp'length-1) = '1' then
            neg_num := true;
        end if;
        for i in 0 to inp'length-1 loop
            if  vec(i) = 'U' or vec(i) = 'X' then
                return undefined_real;
            end if;
            if arith = xlSigned then
                if neg_num then
                    if vec(i) = '0' then
                        result := result + 2.0**i;
                    end if;
                else
                    if vec(i) = '1' then
                        result := result + 2.0**i;
                    end if;
                end if;
            else
                if vec(i) = '1' then
                    result := result + 2.0**i;
                end if;
            end if;
        end loop;
        if arith = xlSigned then
            if neg_num then
                result := result + 1.0;
                result := result * (-1.0);
            end if;
        end if;
        shift_val := 2.0**(-1*bin_pt);
        result := result * shift_val;
        return result;
    end;
    function std_logic_to_real(inp : std_logic; bin_pt : integer; arith : integer)
        return real
    is
        variable result : real := 0.0;
    begin
        if inp = '1' then
            result := 1.0;
        end if;
        if arith = xlSigned then
            assert false
                report "It doesn't make sense to convert a 1 bit number to a signed real.";
        end if;
        return result;
    end;
    -- synopsys translate_on
    function integer_to_std_logic_vector (inp : integer;  width, arith : integer)
        return std_logic_vector
    is
        variable result : std_logic_vector(width-1 downto 0);
        variable unsigned_val : unsigned(width-1 downto 0);
        variable signed_val : signed(width-1 downto 0);
    begin
        if (arith = xlSigned) then
            signed_val := to_signed(inp, width);
            result := signed_to_std_logic_vector(signed_val);
        else
            unsigned_val := to_unsigned(inp, width);
            result := unsigned_to_std_logic_vector(unsigned_val);
        end if;
        return result;
    end;
    function std_logic_vector_to_integer (inp : std_logic_vector;  arith : integer)
        return integer
    is
        constant width : integer := inp'length;
        variable unsigned_val : unsigned(width-1 downto 0);
        variable signed_val : signed(width-1 downto 0);
        variable result : integer;
    begin
        if (arith = xlSigned) then
            signed_val := std_logic_vector_to_signed(inp);
            result := to_integer(signed_val);
        else
            unsigned_val := std_logic_vector_to_unsigned(inp);
            result := to_integer(unsigned_val);
        end if;
        return result;
    end;
    function std_logic_to_integer(constant inp : std_logic := '0')
        return integer
    is
    begin
        if inp = '1' then
            return 1;
        else
            return 0;
        end if;
    end;
    function makeZeroBinStr (width : integer) return STRING is
        variable result : string(1 to width+3);
    begin
        result(1) := '0';
        result(2) := 'b';
        for i in 3 to width+2 loop
            result(i) := '0';
        end loop;
        result(width+3) := '.';
        return result;
    end;
    -- synopsys translate_off
    function real_string_to_std_logic_vector (inp : string;  width, bin_pt, arith : integer)
        return std_logic_vector
    is
        variable result : std_logic_vector(width-1 downto 0);
    begin
        result := (others => '0');
        return result;
    end;
    function real_to_std_logic_vector (inp : real;  width, bin_pt, arith : integer)
        return std_logic_vector
    is
        variable real_val : real;
        variable int_val : integer;
        variable result : std_logic_vector(width-1 downto 0) := (others => '0');
        variable unsigned_val : unsigned(width-1 downto 0) := (others => '0');
        variable signed_val : signed(width-1 downto 0) := (others => '0');
    begin
        real_val := inp;
        int_val := integer(real_val * 2.0**(bin_pt));
        if (arith = xlSigned) then
            signed_val := to_signed(int_val, width);
            result := signed_to_std_logic_vector(signed_val);
        else
            unsigned_val := to_unsigned(int_val, width);
            result := unsigned_to_std_logic_vector(unsigned_val);
        end if;
        return result;
    end;
    -- synopsys translate_on
    function valid_bin_string (inp : string)
        return boolean
    is
        variable vec : string(1 to inp'length);
    begin
        vec := inp;
        if (vec(1) = '0' and vec(2) = 'b') then
            return true;
        else
            return false;
        end if;
    end;
    function hex_string_to_std_logic_vector(inp: string; width : integer)
        return std_logic_vector is
        constant strlen       : integer := inp'LENGTH;
        variable result       : std_logic_vector(width-1 downto 0);
        variable bitval       : std_logic_vector((strlen*4)-1 downto 0);
        variable posn         : integer;
        variable ch           : character;
        variable vec          : string(1 to strlen);
    begin
        vec := inp;
        result := (others => '0');
        posn := (strlen*4)-1;
        for i in 1 to strlen loop
            ch := vec(i);
            case ch is
                when '0' => bitval(posn downto posn-3) := "0000";
                when '1' => bitval(posn downto posn-3) := "0001";
                when '2' => bitval(posn downto posn-3) := "0010";
                when '3' => bitval(posn downto posn-3) := "0011";
                when '4' => bitval(posn downto posn-3) := "0100";
                when '5' => bitval(posn downto posn-3) := "0101";
                when '6' => bitval(posn downto posn-3) := "0110";
                when '7' => bitval(posn downto posn-3) := "0111";
                when '8' => bitval(posn downto posn-3) := "1000";
                when '9' => bitval(posn downto posn-3) := "1001";
                when 'A' | 'a' => bitval(posn downto posn-3) := "1010";
                when 'B' | 'b' => bitval(posn downto posn-3) := "1011";
                when 'C' | 'c' => bitval(posn downto posn-3) := "1100";
                when 'D' | 'd' => bitval(posn downto posn-3) := "1101";
                when 'E' | 'e' => bitval(posn downto posn-3) := "1110";
                when 'F' | 'f' => bitval(posn downto posn-3) := "1111";
                when others => bitval(posn downto posn-3) := "XXXX";
                               -- synopsys translate_off
                               ASSERT false
                                   REPORT "Invalid hex value" SEVERITY ERROR;
                               -- synopsys translate_on
            end case;
            posn := posn - 4;
        end loop;
        if (width <= strlen*4) then
            result :=  bitval(width-1 downto 0);
        else
            result((strlen*4)-1 downto 0) := bitval;
        end if;
        return result;
    end;
    function bin_string_to_std_logic_vector (inp : string)
        return std_logic_vector
    is
        variable pos : integer;
        variable vec : string(1 to inp'length);
        variable result : std_logic_vector(inp'length-1 downto 0);
    begin
        vec := inp;
        pos := inp'length-1;
        result := (others => '0');
        for i in 1 to vec'length loop
            -- synopsys translate_off
            if (pos < 0) and (vec(i) = '0' or vec(i) = '1' or vec(i) = 'X' or vec(i) = 'U')  then
                assert false
                    report "Input string is larger than output std_logic_vector. Truncating output.";
                return result;
            end if;
            -- synopsys translate_on
            if vec(i) = '0' then
                result(pos) := '0';
                pos := pos - 1;
            end if;
            if vec(i) = '1' then
                result(pos) := '1';
                pos := pos - 1;
            end if;
            -- synopsys translate_off
            if (vec(i) = 'X' or vec(i) = 'U') then
                result(pos) := 'U';
                pos := pos - 1;
            end if;
            -- synopsys translate_on
        end loop;
        return result;
    end;
    function bin_string_element_to_std_logic_vector (inp : string;  width, index : integer)
        return std_logic_vector
    is
        constant str_width : integer := width + 4;
        constant inp_len : integer := inp'length;
        constant num_elements : integer := (inp_len + 1)/str_width;
        constant reverse_index : integer := (num_elements-1) - index;
        variable left_pos : integer;
        variable right_pos : integer;
        variable vec : string(1 to inp'length);
        variable result : std_logic_vector(width-1 downto 0);
    begin
        vec := inp;
        result := (others => '0');
        if (reverse_index = 0) and (reverse_index < num_elements) and (inp_len-3 >= width) then
            left_pos := 1;
            right_pos := width + 3;
            result := bin_string_to_std_logic_vector(vec(left_pos to right_pos));
        end if;
        if (reverse_index > 0) and (reverse_index < num_elements) and (inp_len-3 >= width) then
            left_pos := (reverse_index * str_width) + 1;
            right_pos := left_pos + width + 2;
            result := bin_string_to_std_logic_vector(vec(left_pos to right_pos));
        end if;
        return result;
    end;
   -- synopsys translate_off
    function std_logic_vector_to_bin_string(inp : std_logic_vector)
        return string
    is
        variable vec : std_logic_vector(1 to inp'length);
        variable result : string(vec'range);
    begin
        vec := inp;
        for i in vec'range loop
            result(i) := to_char(vec(i));
        end loop;
        return result;
    end;
    function std_logic_to_bin_string(inp : std_logic)
        return string
    is
        variable result : string(1 to 3);
    begin
        result(1) := '0';
        result(2) := 'b';
        result(3) := to_char(inp);
        return result;
    end;
    function std_logic_vector_to_bin_string_w_point(inp : std_logic_vector; bin_pt : integer)
        return string
    is
        variable width : integer := inp'length;
        variable vec : std_logic_vector(width-1 downto 0);
        variable str_pos : integer;
        variable result : string(1 to width+3);
    begin
        vec := inp;
        str_pos := 1;
        result(str_pos) := '0';
        str_pos := 2;
        result(str_pos) := 'b';
        str_pos := 3;
        for i in width-1 downto 0  loop
            if (((width+3) - bin_pt) = str_pos) then
                result(str_pos) := '.';
                str_pos := str_pos + 1;
            end if;
            result(str_pos) := to_char(vec(i));
            str_pos := str_pos + 1;
        end loop;
        if (bin_pt = 0) then
            result(str_pos) := '.';
        end if;
        return result;
    end;
    function real_to_bin_string(inp : real;  width, bin_pt, arith : integer)
        return string
    is
        variable result : string(1 to width);
        variable vec : std_logic_vector(width-1 downto 0);
    begin
        vec := real_to_std_logic_vector(inp, width, bin_pt, arith);
        result := std_logic_vector_to_bin_string(vec);
        return result;
    end;
    function real_to_string (inp : real) return string
    is
        variable result : string(1 to display_precision) := (others => ' ');
    begin
        result(real'image(inp)'range) := real'image(inp);
        return result;
    end;
    -- synopsys translate_on
end conv_pkg;

-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity srl17e is
    generic (width : integer:=16;
             latency : integer :=8);
    port (clk   : in std_logic;
          ce    : in std_logic;
          d     : in std_logic_vector(width-1 downto 0);
          q     : out std_logic_vector(width-1 downto 0));
end srl17e;
architecture structural of srl17e is
    component SRL16E
        port (D   : in STD_ULOGIC;
              CE  : in STD_ULOGIC;
              CLK : in STD_ULOGIC;
              A0  : in STD_ULOGIC;
              A1  : in STD_ULOGIC;
              A2  : in STD_ULOGIC;
              A3  : in STD_ULOGIC;
              Q   : out STD_ULOGIC);
    end component;
    attribute syn_black_box of SRL16E : component is true;
    attribute fpga_dont_touch of SRL16E : component is "true";
    component FDE
        port(
            Q  :        out   STD_ULOGIC;
            D  :        in    STD_ULOGIC;
            C  :        in    STD_ULOGIC;
            CE :        in    STD_ULOGIC);
    end component;
    attribute syn_black_box of FDE : component is true;
    attribute fpga_dont_touch of FDE : component is "true";
    constant a : std_logic_vector(4 downto 0) :=
        integer_to_std_logic_vector(latency-2,5,xlSigned);
    signal d_delayed : std_logic_vector(width-1 downto 0);
    signal srl16_out : std_logic_vector(width-1 downto 0);
begin
    d_delayed <= d after 200 ps;
    reg_array : for i in 0 to width-1 generate
        srl16_used: if latency > 1 generate
            u1 : srl16e port map(clk => clk,
                                 d => d_delayed(i),
                                 q => srl16_out(i),
                                 ce => ce,
                                 a0 => a(0),
                                 a1 => a(1),
                                 a2 => a(2),
                                 a3 => a(3));
        end generate;
        srl16_not_used: if latency <= 1 generate
            srl16_out(i) <= d_delayed(i);
        end generate;
        fde_used: if latency /= 0  generate
            u2 : fde port map(c => clk,
                              d => srl16_out(i),
                              q => q(i),
                              ce => ce);
        end generate;
        fde_not_used: if latency = 0  generate
            q(i) <= srl16_out(i);
        end generate;
    end generate;
 end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity synth_reg is
    generic (width           : integer := 8;
             latency         : integer := 1);
    port (i       : in std_logic_vector(width-1 downto 0);
          ce      : in std_logic;
          clr     : in std_logic;
          clk     : in std_logic;
          o       : out std_logic_vector(width-1 downto 0));
end synth_reg;
architecture structural of synth_reg is
    component srl17e
        generic (width : integer:=16;
                 latency : integer :=8);
        port (clk : in std_logic;
              ce  : in std_logic;
              d   : in std_logic_vector(width-1 downto 0);
              q   : out std_logic_vector(width-1 downto 0));
    end component;
    function calc_num_srl17es (latency : integer)
        return integer
    is
        variable remaining_latency : integer;
        variable result : integer;
    begin
        result := latency / 17;
        remaining_latency := latency - (result * 17);
        if (remaining_latency /= 0) then
            result := result + 1;
        end if;
        return result;
    end;
    constant complete_num_srl17es : integer := latency / 17;
    constant num_srl17es : integer := calc_num_srl17es(latency);
    constant remaining_latency : integer := latency - (complete_num_srl17es * 17);
    type register_array is array (num_srl17es downto 0) of
        std_logic_vector(width-1 downto 0);
    signal z : register_array;
begin
    z(0) <= i;
    complete_ones : if complete_num_srl17es > 0 generate
        srl17e_array: for i in 0 to complete_num_srl17es-1 generate
            delay_comp : srl17e
                generic map (width => width,
                             latency => 17)
                port map (clk => clk,
                          ce  => ce,
                          d       => z(i),
                          q       => z(i+1));
        end generate;
    end generate;
    partial_one : if remaining_latency > 0 generate
        last_srl17e : srl17e
            generic map (width => width,
                         latency => remaining_latency)
            port map (clk => clk,
                      ce  => ce,
                      d   => z(num_srl17es-1),
                      q   => z(num_srl17es));
    end generate;
    o <= z(num_srl17es);
end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity synth_reg_reg is
    generic (width           : integer := 8;
             latency         : integer := 1);
    port (i       : in std_logic_vector(width-1 downto 0);
          ce      : in std_logic;
          clr     : in std_logic;
          clk     : in std_logic;
          o       : out std_logic_vector(width-1 downto 0));
end synth_reg_reg;
architecture behav of synth_reg_reg is
  type reg_array_type is array (latency-1 downto 0) of std_logic_vector(width -1 downto 0);
  signal reg_bank : reg_array_type := (others => (others => '0'));
  signal reg_bank_in : reg_array_type := (others => (others => '0'));
  attribute syn_allow_retiming : boolean;
  attribute syn_srlstyle : string;
  attribute syn_allow_retiming of reg_bank : signal is true;
  attribute syn_allow_retiming of reg_bank_in : signal is true;
  attribute syn_srlstyle of reg_bank : signal is "registers";
  attribute syn_srlstyle of reg_bank_in : signal is "registers";
begin
  latency_eq_0: if latency = 0 generate
    o <= i;
  end generate latency_eq_0;
  latency_gt_0: if latency >= 1 generate
    o <= reg_bank(latency-1);
    reg_bank_in(0) <= i;
    loop_gen: for idx in latency-2 downto 0 generate
      reg_bank_in(idx+1) <= reg_bank(idx);
    end generate loop_gen;
    sync_loop: for sync_idx in latency-1 downto 0 generate
      sync_proc: process (clk)
      begin
        if clk'event and clk = '1' then
          if ce = '1'  then
            reg_bank(sync_idx) <= reg_bank_in(sync_idx);
          end if;
        end if;
      end process sync_proc;
    end generate sync_loop;
  end generate latency_gt_0;
end behav;

-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity single_reg_w_init is
  generic (
    width: integer := 8;
    init_index: integer := 0;
    init_value: bit_vector := b"0000"
  );
  port (
    i: in std_logic_vector(width - 1 downto 0);
    ce: in std_logic;
    clr: in std_logic;
    clk: in std_logic;
    o: out std_logic_vector(width - 1 downto 0)
  );
end single_reg_w_init;
architecture structural of single_reg_w_init is
  function build_init_const(width: integer;
                            init_index: integer;
                            init_value: bit_vector)
    return std_logic_vector
  is
    variable result: std_logic_vector(width - 1 downto 0);
  begin
    if init_index = 0 then
      result := (others => '0');
    elsif init_index = 1 then
      result := (others => '0');
      result(0) := '1';
    else
      result := to_stdlogicvector(init_value);
    end if;
    return result;
  end;
  component fdre
    port (
      q: out std_ulogic;
      d: in  std_ulogic;
      c: in  std_ulogic;
      ce: in  std_ulogic;
      r: in  std_ulogic
    );
  end component;
  attribute syn_black_box of fdre: component is true;
  attribute fpga_dont_touch of fdre: component is "true";
  component fdse
    port (
      q: out std_ulogic;
      d: in  std_ulogic;
      c: in  std_ulogic;
      ce: in  std_ulogic;
      s: in  std_ulogic
    );
  end component;
  attribute syn_black_box of fdse: component is true;
  attribute fpga_dont_touch of fdse: component is "true";
  constant init_const: std_logic_vector(width - 1 downto 0)
    := build_init_const(width, init_index, init_value);
begin
  fd_prim_array: for index in 0 to width - 1 generate
    bit_is_0: if (init_const(index) = '0') generate
      fdre_comp: fdre
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          r => clr
        );
    end generate;
    bit_is_1: if (init_const(index) = '1') generate
      fdse_comp: fdse
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          s => clr
        );
    end generate;
  end generate;
end architecture structural;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity synth_reg_w_init is
  generic (
    width: integer := 8;
    init_index: integer := 0;
    init_value: bit_vector := b"0000";
    latency: integer := 1
  );
  port (
    i: in std_logic_vector(width - 1 downto 0);
    ce: in std_logic;
    clr: in std_logic;
    clk: in std_logic;
    o: out std_logic_vector(width - 1 downto 0)
  );
end synth_reg_w_init;
architecture structural of synth_reg_w_init is
  component single_reg_w_init
    generic (
      width: integer := 8;
      init_index: integer := 0;
      init_value: bit_vector := b"0000"
    );
    port (
      i: in std_logic_vector(width - 1 downto 0);
      ce: in std_logic;
      clr: in std_logic;
      clk: in std_logic;
      o: out std_logic_vector(width - 1 downto 0)
    );
  end component;
  signal dly_i: std_logic_vector((latency + 1) * width - 1 downto 0);
  signal dly_clr: std_logic;
begin
  latency_eq_0: if (latency = 0) generate
    o <= i;
  end generate;
  latency_gt_0: if (latency >= 1) generate
    dly_i((latency + 1) * width - 1 downto latency * width) <= i
      after 200 ps;
    dly_clr <= clr after 200 ps;
    fd_array: for index in latency downto 1 generate
       reg_comp: single_reg_w_init
          generic map (
            width => width,
            init_index => init_index,
            init_value => init_value
          )
          port map (
            clk => clk,
            i => dly_i((index + 1) * width - 1 downto index * width),
            o => dly_i(index * width - 1 downto (index - 1) * width),
            ce => ce,
            clr => dly_clr
          );
    end generate;
    o <= dly_i(width - 1 downto 0);
  end generate;
end structural;

-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
entity xlclockenablegenerator is
  generic (
    period: integer := 2;
    log_2_period: integer := 0;
    pipeline_regs: integer := 5
  );
  port (
    clk: in std_logic;
    clr: in std_logic;
    ce: out std_logic
  );
end xlclockenablegenerator;
architecture behavior of xlclockenablegenerator is
  component synth_reg_w_init
    generic (
      width: integer;
      init_index: integer;
      init_value: bit_vector;
      latency: integer
    );
    port (
      i: in std_logic_vector(width - 1 downto 0);
      ce: in std_logic;
      clr: in std_logic;
      clk: in std_logic;
      o: out std_logic_vector(width - 1 downto 0)
    );
  end component;
  function size_of_uint(inp: integer; power_of_2: boolean)
    return integer
  is
    constant inp_vec: std_logic_vector(31 downto 0) :=
      integer_to_std_logic_vector(inp,32, xlUnsigned);
    variable result: integer;
  begin
    result := 32;
    for i in 0 to 31 loop
      if inp_vec(i) = '1' then
        result := i;
      end if;
    end loop;
    if power_of_2 then
      return result;
    else
      return result+1;
    end if;
  end;
  function is_power_of_2(inp: std_logic_vector)
    return boolean
  is
    constant width: integer := inp'length;
    variable vec: std_logic_vector(width - 1 downto 0);
    variable single_bit_set: boolean;
    variable more_than_one_bit_set: boolean;
    variable result: boolean;
  begin
    vec := inp;
    single_bit_set := false;
    more_than_one_bit_set := false;
    -- synopsys translate_off
    if (is_XorU(vec)) then
      return false;
    end if;
     -- synopsys translate_on
    if width > 0 then
      for i in 0 to width - 1 loop
        if vec(i) = '1' then
          if single_bit_set then
            more_than_one_bit_set := true;
          end if;
          single_bit_set := true;
        end if;
      end loop;
    end if;
    if (single_bit_set and not(more_than_one_bit_set)) then
      result := true;
    else
      result := false;
    end if;
    return result;
  end;
  function ce_reg_init_val(index, period : integer)
    return integer
  is
     variable result: integer;
   begin
      result := 0;
      if ((index mod period) = 0) then
          result := 1;
      end if;
      return result;
  end;
  function remaining_pipe_regs(num_pipeline_regs, period : integer)
    return integer
  is
     variable factor, result: integer;
  begin
      factor := (num_pipeline_regs / period);
      result := num_pipeline_regs - (period * factor) + 1;
      return result;
  end;

  function sg_min(L, R: INTEGER) return INTEGER is
  begin
      if L < R then
            return L;
      else
            return R;
      end if;
  end;
  constant max_pipeline_regs : integer := 8;
  constant pipe_regs : integer := 5;
  constant num_pipeline_regs : integer := sg_min(pipeline_regs, max_pipeline_regs);
  constant rem_pipeline_regs : integer := remaining_pipe_regs(num_pipeline_regs,period);
  constant period_floor: integer := max(2, period);
  constant power_of_2_counter: boolean :=
    is_power_of_2(integer_to_std_logic_vector(period_floor,32, xlUnsigned));
  constant cnt_width: integer :=
    size_of_uint(period_floor, power_of_2_counter);
  constant clk_for_ce_pulse_minus1: std_logic_vector(cnt_width - 1 downto 0) :=
    integer_to_std_logic_vector((period_floor - 2),cnt_width, xlUnsigned);
  constant clk_for_ce_pulse_minus2: std_logic_vector(cnt_width - 1 downto 0) :=
    integer_to_std_logic_vector(max(0,period - 3),cnt_width, xlUnsigned);
  constant clk_for_ce_pulse_minus_regs: std_logic_vector(cnt_width - 1 downto 0) :=
    integer_to_std_logic_vector(max(0,period - rem_pipeline_regs),cnt_width, xlUnsigned);
  signal clk_num: unsigned(cnt_width - 1 downto 0) := (others => '0');
  signal ce_vec : std_logic_vector(num_pipeline_regs downto 0);
  signal internal_ce: std_logic_vector(0 downto 0);
  signal cnt_clr, cnt_clr_dly: std_logic_vector (0 downto 0);
begin
  cntr_gen: process(clk)
  begin
    if clk'event and clk = '1'  then
        if ((cnt_clr_dly(0) = '1') or (clr = '1')) then
          clk_num <= (others => '0');
        else
          clk_num <= clk_num + 1;
        end if;
    end if;
  end process;
  clr_gen: process(clk_num, clr)
  begin
    if power_of_2_counter then
      cnt_clr(0) <= clr;
    else
      if (unsigned_to_std_logic_vector(clk_num) = clk_for_ce_pulse_minus1
          or clr = '1') then
        cnt_clr(0) <= '1';
      else
        cnt_clr(0) <= '0';
      end if;
    end if;
  end process;
  clr_reg: synth_reg_w_init
    generic map (
      width => 1,
      init_index => 0,
      init_value => b"0000",
      latency => 1
    )
    port map (
      i => cnt_clr,
      ce => '1',
      clr => clr,
      clk => clk,
      o => cnt_clr_dly
    );
  pipelined_ce : if period > 1 generate
      ce_gen: process(clk_num)
      begin
          if unsigned_to_std_logic_vector(clk_num) = clk_for_ce_pulse_minus_regs then
              ce_vec(num_pipeline_regs) <= '1';
          else
              ce_vec(num_pipeline_regs) <= '0';
          end if;
      end process;
      ce_pipeline: for index in num_pipeline_regs downto 1 generate
          ce_reg : synth_reg_w_init
              generic map (
                  width => 1,
                  init_index => ce_reg_init_val(index, period),
                  init_value => b"0000",
                  latency => 1
                  )
              port map (
                  i => ce_vec(index downto index),
                  ce => '1',
                  clr => clr,
                  clk => clk,
                  o => ce_vec(index-1 downto index-1)
                  );
      end generate;
      internal_ce <= ce_vec(0 downto 0);
  end generate;
  generate_clock_enable: if period > 1 generate
    ce <= internal_ce(0);
  end generate;
  generate_clock_enable_constant: if period = 1 generate
    ce <= '1';
  end generate;
end architecture behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f is 
  port(
    ai:in std_logic_vector(18 downto 0);
    ar:in std_logic_vector(18 downto 0);
    bi:in std_logic_vector(15 downto 0);
    br:in std_logic_vector(15 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    pi:out std_logic_vector(16 downto 0);
    pr:out std_logic_vector(16 downto 0)
  );
end xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f;


architecture behavior of xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f  is
  component cmpy_v3_1_3bed636cf80cb482
    port(
      ai:in std_logic_vector(18 downto 0);
      ar:in std_logic_vector(18 downto 0);
      bi:in std_logic_vector(15 downto 0);
      br:in std_logic_vector(15 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      pi:out std_logic_vector(16 downto 0);
      pr:out std_logic_vector(16 downto 0)
    );
end component;
begin
  cmpy_v3_1_3bed636cf80cb482_instance : cmpy_v3_1_3bed636cf80cb482
    port map(
      ai=>ai,
      ar=>ar,
      bi=>bi,
      br=>br,
      ce=>ce,
      clk=>clk,
      pi=>pi,
      pr=>pr
    );
end  behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity convert_func_call is
    generic (
        din_width    : integer := 16;
        din_bin_pt   : integer := 4;
        din_arith    : integer := xlUnsigned;
        dout_width   : integer := 8;
        dout_bin_pt  : integer := 2;
        dout_arith   : integer := xlUnsigned;
        quantization : integer := xlTruncate;
        overflow     : integer := xlWrap);
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call;
architecture behavior of convert_func_call is
begin
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity xlconvert is
    generic (
        din_width    : integer := 16;
        din_bin_pt   : integer := 4;
        din_arith    : integer := xlUnsigned;
        dout_width   : integer := 8;
        dout_bin_pt  : integer := 2;
        dout_arith   : integer := xlUnsigned;
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;
        latency      : integer := 0;
        quantization : integer := xlTruncate;
        overflow     : integer := xlWrap);
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));
end xlconvert;
architecture behavior of xlconvert is
    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;
    component convert_func_call
        generic (
            din_width    : integer := 16;
            din_bin_pt   : integer := 4;
            din_arith    : integer := xlUnsigned;
            dout_width   : integer := 8;
            dout_bin_pt  : integer := 2;
            dout_arith   : integer := xlUnsigned;
            quantization : integer := xlTruncate;
            overflow     : integer := xlWrap);
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;
    -- synopsys translate_off
    -- synopsys translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;
begin
    -- synopsys translate_off
    -- synopsys translate_on
    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate;
    std_conversion_generate : if (bool_conversion = 0)
    generate
      convert : convert_func_call
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate;
    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;
    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;
end  behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    cosine:out std_logic_vector(18 downto 0);
    data:in std_logic_vector(27 downto 0);
    sine:out std_logic_vector(18 downto 0);
    we:in std_logic
  );
end xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e;


architecture behavior of xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e  is
  component dds_cmplr_v4_0_3754692d6c95399c
    port(
      ce:in std_logic;
      clk:in std_logic;
      cosine:out std_logic_vector(18 downto 0);
      data:in std_logic_vector(27 downto 0);
      sine:out std_logic_vector(18 downto 0);
      we:in std_logic
    );
end component;
begin
  dds_cmplr_v4_0_3754692d6c95399c_instance : dds_cmplr_v4_0_3754692d6c95399c
    port map(
      ce=>ce,
      clk=>clk,
      cosine=>cosine,
      data=>data,
      sine=>sine,
      we=>we
    );
end  behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity xldelay is
   generic(width        : integer := -1;
           latency      : integer := -1;
           reg_retiming : integer := 0);
   port(d       : in std_logic_vector (width-1 downto 0);
        ce      : in std_logic;
        clk     : in std_logic;
        en      : in std_logic;
        q       : out std_logic_vector (width-1 downto 0));
end xldelay;
architecture behavior of xldelay is
   component synth_reg
      generic (width       : integer;
               latency     : integer);
      port (i       : in std_logic_vector(width-1 downto 0);
            ce      : in std_logic;
            clr     : in std_logic;
            clk     : in std_logic;
            o       : out std_logic_vector(width-1 downto 0));
   end component;
   component synth_reg_reg
      generic (width       : integer;
               latency     : integer);
      port (i       : in std_logic_vector(width-1 downto 0);
            ce      : in std_logic;
            clr     : in std_logic;
            clk     : in std_logic;
            o       : out std_logic_vector(width-1 downto 0));
   end component;
   signal internal_ce  : std_logic;
begin
   internal_ce  <= ce and en;
   srl_delay: if (reg_retiming = 0) or (latency < 1) generate
     synth_reg_srl_inst : synth_reg
       generic map (
         width   => width,
         latency => latency)
       port map (
         i   => d,
         ce  => internal_ce,
         clr => '0',
         clk => clk,
         o   => q);
   end generate srl_delay;
   reg_delay: if (reg_retiming = 1) and (latency >= 1) generate
     synth_reg_reg_inst : synth_reg_reg
       generic map (
         width   => width,
         latency => latency)
       port map (
         i   => d,
         ce  => internal_ce,
         clr => '0',
         clk => clk,
         o   => q);
   end generate reg_delay;
end architecture behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8 is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(16 downto 0);
    din_2:in std_logic_vector(16 downto 0);
    dout_1:out std_logic_vector(18 downto 0);
    dout_2:out std_logic_vector(18 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8;


architecture behavior of xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8  is
  component fr_cmplr_v5_0_1877c081b7e2f892
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(16 downto 0);
      din_2:in std_logic_vector(16 downto 0);
      dout_1:out std_logic_vector(18 downto 0);
      dout_2:out std_logic_vector(18 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_1877c081b7e2f892_instance : fr_cmplr_v5_0_1877c081b7e2f892
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_8bbf91ad55b0353987e969c586c6098d is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(15 downto 0);
    din_2:in std_logic_vector(15 downto 0);
    dout_1:out std_logic_vector(17 downto 0);
    dout_2:out std_logic_vector(17 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_8bbf91ad55b0353987e969c586c6098d;


architecture behavior of xlfir_compiler_8bbf91ad55b0353987e969c586c6098d  is
  component fr_cmplr_v5_0_f5119409b2c69958
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(15 downto 0);
      din_2:in std_logic_vector(15 downto 0);
      dout_1:out std_logic_vector(17 downto 0);
      dout_2:out std_logic_vector(17 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_f5119409b2c69958_instance : fr_cmplr_v5_0_f5119409b2c69958
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    din_1:in std_logic_vector(17 downto 0);
    din_2:in std_logic_vector(17 downto 0);
    dout_1:out std_logic_vector(37 downto 0);
    dout_2:out std_logic_vector(37 downto 0);
    nd:in std_logic;
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce;


architecture behavior of xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce  is
  component fr_cmplr_v5_0_cd9071975244390f
    port(
      ce:in std_logic;
      clk:in std_logic;
      din_1:in std_logic_vector(17 downto 0);
      din_2:in std_logic_vector(17 downto 0);
      dout_1:out std_logic_vector(37 downto 0);
      dout_2:out std_logic_vector(37 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
begin
  fr_cmplr_v5_0_cd9071975244390f_instance : fr_cmplr_v5_0_cd9071975244390f
    port map(
      ce=>ce,
      clk=>clk,
      din_1=>din_1,
      din_2=>din_2,
      dout_1=>dout_1,
      dout_2=>dout_2,
      nd=>nd,
      rdy=>rdy,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity constant_6293007044 is
  port (
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end constant_6293007044;


architecture behavior of constant_6293007044 is
begin
  op <= "1";
end behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
entity xldsamp is
  generic (
    d_width: integer := 12;
    d_bin_pt: integer := 0;
    d_arith: integer := xlUnsigned;
    q_width: integer := 12;
    q_bin_pt: integer := 0;
    q_arith: integer := xlUnsigned;
    en_width: integer := 1;
    en_bin_pt: integer := 0;
    en_arith: integer := xlUnsigned;
    ds_ratio: integer := 2;
    phase: integer := 0;
    latency: integer := 1
  );
  port (
    d: in std_logic_vector(d_width - 1 downto 0);
    src_clk: in std_logic;
    src_ce: in std_logic;
    src_clr: in std_logic;
    dest_clk: in std_logic;
    dest_ce: in std_logic;
    dest_clr: in std_logic;
    en: in std_logic_vector(en_width - 1 downto 0);
    q: out std_logic_vector(q_width - 1 downto 0)
  );
end xldsamp;
architecture struct of xldsamp is
  component synth_reg
    generic (
      width: integer := 16;
      latency: integer := 5
    );
    port (
      i: in std_logic_vector(width - 1 downto 0);
      ce: in std_logic;
      clr: in std_logic;
      clk: in std_logic;
      o: out std_logic_vector(width - 1 downto 0)
    );
  end component;
  component fdse
    port (
      q: out   std_ulogic;
      d: in    std_ulogic;
      c: in    std_ulogic;
      s: in    std_ulogic;
      ce: in    std_ulogic
    );
  end component;
  attribute syn_black_box of fdse: component is true;
  attribute fpga_dont_touch of fdse: component is "true";
  signal adjusted_dest_ce: std_logic;
  signal adjusted_dest_ce_w_en: std_logic;
  signal dest_ce_w_en: std_logic;
  signal smpld_d: std_logic_vector(d_width-1 downto 0);
begin
  adjusted_ce_needed: if ((latency = 0) or (phase /= (ds_ratio - 1))) generate
    dest_ce_reg: fdse
      port map (
        q => adjusted_dest_ce,
        d => dest_ce,
        c => src_clk,
        s => src_clr,
        ce => src_ce
      );
  end generate;
  latency_eq_0: if (latency = 0) generate
    shutter_d_reg: synth_reg
      generic map (
        width => d_width,
        latency => 1
      )
      port map (
        i => d,
        ce => adjusted_dest_ce,
        clr => src_clr,
        clk => src_clk,
        o => smpld_d
      );
    shutter_mux: process (adjusted_dest_ce, d, smpld_d)
    begin
      if adjusted_dest_ce = '0' then
        q <= smpld_d;
      else
        q <= d;
      end if;
    end process;
  end generate;
  latency_gt_0: if (latency > 0) generate
    dbl_reg_test: if (phase /= (ds_ratio-1)) generate
        smpl_d_reg: synth_reg
          generic map (
            width => d_width,
            latency => 1
          )
          port map (
            i => d,
            ce => adjusted_dest_ce_w_en,
            clr => src_clr,
            clk => src_clk,
            o => smpld_d
          );
    end generate;
    sngl_reg_test: if (phase = (ds_ratio -1)) generate
      smpld_d <= d;
    end generate;
    latency_pipe: synth_reg
      generic map (
        width => d_width,
        latency => latency
      )
      port map (
        i => smpld_d,
        ce => dest_ce_w_en,
        clr => src_clr,
        clk => dest_clk,
        o => q
      );
  end generate;
  dest_ce_w_en <= dest_ce and en(0);
  adjusted_dest_ce_w_en <= adjusted_dest_ce and en(0);
end architecture struct;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity inverter_e5b38cca3b is
  port (
    ip : in std_logic_vector((1 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end inverter_e5b38cca3b;


architecture behavior of inverter_e5b38cca3b is
  signal ip_1_26: boolean;
  type array_type_op_mem_22_20 is array (0 to (1 - 1)) of boolean;
  signal op_mem_22_20: array_type_op_mem_22_20 := (
    0 => false);
  signal op_mem_22_20_front_din: boolean;
  signal op_mem_22_20_back: boolean;
  signal op_mem_22_20_push_front_pop_back_en: std_logic;
  signal internal_ip_12_1_bitnot: boolean;
begin
  ip_1_26 <= ((ip) = "1");
  op_mem_22_20_back <= op_mem_22_20(0);
  proc_op_mem_22_20: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_22_20_push_front_pop_back_en = '1')) then
        op_mem_22_20(0) <= op_mem_22_20_front_din;
      end if;
    end if;
  end process proc_op_mem_22_20;
  internal_ip_12_1_bitnot <= ((not boolean_to_vector(ip_1_26)) = "1");
  op_mem_22_20_push_front_pop_back_en <= '0';
  op <= boolean_to_vector(internal_ip_12_1_bitnot);
end behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity logical_799f62af22 is
  port (
    d0 : in std_logic_vector((1 - 1) downto 0);
    d1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end logical_799f62af22;


architecture behavior of logical_799f62af22 is
  signal d0_1_24: std_logic;
  signal d1_1_27: std_logic;
  type array_type_latency_pipe_5_26 is array (0 to (1 - 1)) of std_logic;
  signal latency_pipe_5_26: array_type_latency_pipe_5_26 := (
    0 => '0');
  signal latency_pipe_5_26_front_din: std_logic;
  signal latency_pipe_5_26_back: std_logic;
  signal latency_pipe_5_26_push_front_pop_back_en: std_logic;
  signal fully_2_1_bit: std_logic;
begin
  d0_1_24 <= d0(0);
  d1_1_27 <= d1(0);
  latency_pipe_5_26_back <= latency_pipe_5_26(0);
  proc_latency_pipe_5_26: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (latency_pipe_5_26_push_front_pop_back_en = '1')) then
        latency_pipe_5_26(0) <= latency_pipe_5_26_front_din;
      end if;
    end if;
  end process proc_latency_pipe_5_26;
  fully_2_1_bit <= d0_1_24 and d1_1_27;
  latency_pipe_5_26_front_din <= fully_2_1_bit;
  latency_pipe_5_26_push_front_pop_back_en <= '1';
  y <= std_logic_to_vector(latency_pipe_5_26_back);
end behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
entity xlregister is
   generic (d_width          : integer := 5;
            init_value       : bit_vector := b"00");
   port (d   : in std_logic_vector (d_width-1 downto 0);
         rst : in std_logic_vector(0 downto 0) := "0";
         en  : in std_logic_vector(0 downto 0) := "1";
         ce  : in std_logic;
         clk : in std_logic;
         q   : out std_logic_vector (d_width-1 downto 0));
end xlregister;
architecture behavior of xlregister is
   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component;
   -- synopsys translate_off
   signal real_d, real_q           : real;
   -- synopsys translate_on
   signal internal_clr             : std_logic;
   signal internal_ce              : std_logic;
begin
   internal_clr <= rst(0) and ce;
   internal_ce  <= en(0) and ce;
   synth_reg_inst : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_ce,
                clr => internal_clr,
                clk => clk,
                o   => q);
end architecture behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b is 
  port(
    ai:in std_logic_vector(18 downto 0);
    ar:in std_logic_vector(18 downto 0);
    bi:in std_logic_vector(17 downto 0);
    br:in std_logic_vector(17 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    pi:out std_logic_vector(35 downto 0);
    pr:out std_logic_vector(35 downto 0)
  );
end xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b;


architecture behavior of xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b  is
  component cmpy_v3_1_974daf98b885ddbd
    port(
      ai:in std_logic_vector(18 downto 0);
      ar:in std_logic_vector(18 downto 0);
      bi:in std_logic_vector(17 downto 0);
      br:in std_logic_vector(17 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      pi:out std_logic_vector(35 downto 0);
      pr:out std_logic_vector(35 downto 0)
    );
end component;
begin
  cmpy_v3_1_974daf98b885ddbd_instance : cmpy_v3_1_974daf98b885ddbd
    port map(
      ai=>ai,
      ar=>ar,
      bi=>bi,
      br=>br,
      ce=>ce,
      clk=>clk,
      pi=>pi,
      pr=>pr
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xldds_compiler_94274096bf4e0b94b9506335cad96a90 is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    cosine:out std_logic_vector(18 downto 0);
    data:in std_logic_vector(27 downto 0);
    sine:out std_logic_vector(18 downto 0);
    we:in std_logic
  );
end xldds_compiler_94274096bf4e0b94b9506335cad96a90;


architecture behavior of xldds_compiler_94274096bf4e0b94b9506335cad96a90  is
  component dds_cmplr_v4_0_d6a7998cfc77acb2
    port(
      ce:in std_logic;
      clk:in std_logic;
      cosine:out std_logic_vector(18 downto 0);
      data:in std_logic_vector(27 downto 0);
      sine:out std_logic_vector(18 downto 0);
      we:in std_logic
    );
end component;
begin
  dds_cmplr_v4_0_d6a7998cfc77acb2_instance : dds_cmplr_v4_0_d6a7998cfc77acb2
    port map(
      ce=>ce,
      clk=>clk,
      cosine=>cosine,
      data=>data,
      sine=>sine,
      we=>we
    );
end  behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
entity xlusamp is
    generic (
             d_width      : integer := 5;
             d_bin_pt     : integer := 2;
             d_arith      : integer := xlUnsigned;
             q_width      : integer := 5;
             q_bin_pt     : integer := 2;
             q_arith      : integer := xlUnsigned;
             en_width     : integer := 1;
             en_bin_pt    : integer := 0;
             en_arith     : integer := xlUnsigned;
             sampling_ratio     : integer := 2;
             latency      : integer := 1;
             copy_samples : integer := 0);
    port (
          d        : in std_logic_vector (d_width-1 downto 0);
          src_clk  : in std_logic;
          src_ce   : in std_logic;
          src_clr  : in std_logic;
          dest_clk : in std_logic;
          dest_ce  : in std_logic;
          dest_clr : in std_logic;
          en       : in std_logic_vector(en_width-1 downto 0);
          q        : out std_logic_vector (q_width-1 downto 0)
         );
end xlusamp;
architecture struct of xlusamp is
    component synth_reg
      generic (
        width: integer := 16;
        latency: integer := 5
      );
      port (
        i: in std_logic_vector(width - 1 downto 0);
        ce: in std_logic;
        clr: in std_logic;
        clk: in std_logic;
        o: out std_logic_vector(width - 1 downto 0)
      );
    end component;
    component FDSE
        port (q  : out   std_ulogic;
              d  : in    std_ulogic;
              c  : in    std_ulogic;
              s  : in    std_ulogic;
              ce : in    std_ulogic);
    end component;
    attribute syn_black_box of FDSE : component is true;
    attribute fpga_dont_touch of FDSE : component is "true";
    signal zero    : std_logic_vector (d_width-1 downto 0);
    signal mux_sel : std_logic;
    signal sampled_d  : std_logic_vector (d_width-1 downto 0);
    signal internal_ce : std_logic;
begin
   sel_gen : FDSE
                port map (q  => mux_sel,
                        d  => src_ce,
            c  => src_clk,
            s  => src_clr,
            ce => dest_ce);
  internal_ce <= src_ce and en(0);
  copy_samples_false : if (copy_samples = 0) generate
      zero <= (others => '0');
      gen_q_cp_smpls_0_and_lat_0: if (latency = 0) generate
        cp_smpls_0_and_lat_0: process (mux_sel, d, zero)
        begin
          if (mux_sel = '1') then
            q <= d;
          else
            q <= zero;
          end if;
        end process cp_smpls_0_and_lat_0;
      end generate;
      gen_q_cp_smpls_0_and_lat_gt_0: if (latency > 0) generate
        sampled_d_reg: synth_reg
          generic map (
            width => d_width,
            latency => latency
          )
          port map (
            i => d,
            ce => internal_ce,
            clr => src_clr,
            clk => src_clk,
            o => sampled_d
          );

        gen_q_check_mux_sel: process (mux_sel, sampled_d, zero)
        begin
          if (mux_sel = '1') then
            q <= sampled_d;
          else
            q <= zero;
          end if;
        end process gen_q_check_mux_sel;
      end generate;
   end generate;
   copy_samples_true : if (copy_samples = 1) generate
     gen_q_cp_smpls_1_and_lat_0: if (latency = 0) generate
       q <= d;
     end generate;
     gen_q_cp_smpls_1_and_lat_gt_0: if (latency > 0) generate
       q <= sampled_d;
       sampled_d_reg2: synth_reg
         generic map (
           width => d_width,
           latency => latency
         )
         port map (
           i => d,
           ce => internal_ce,
           clr => src_clr,
           clk => src_clk,
           o => sampled_d
         );
     end generate;
   end generate;
end architecture struct;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_6b538e2e420c55fbcb179300d415c30e is 
  port(
    ce:in std_logic;
    ce_12:in std_logic;
    ce_24:in std_logic;
    ce_logic_24:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_12:in std_logic;
    clk_24:in std_logic;
    clk_logic_24:in std_logic;
    din:in std_logic_vector(15 downto 0);
    dout:out std_logic_vector(17 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_6b538e2e420c55fbcb179300d415c30e;


architecture behavior of xlfir_compiler_6b538e2e420c55fbcb179300d415c30e  is
  component fr_cmplr_v5_0_670158bb462abb3d
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(15 downto 0);
      dout:out std_logic_vector(17 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal dout_ps_net: std_logic_vector(17 downto 0) := (others=>'0');
signal rdy_ps_net: std_logic := '0';
signal rdy_ps_net_captured: std_logic := '0';
signal rdy_ps_net_or_captured_net: std_logic := '0';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_24,
        clr => '0',
        clk => clk_24, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_12,
        clr => '0',
        clk => clk_12, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 18,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_12,
        clr => '0',
        clk => clk_12, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_12,
        clr => '0',
        clk => clk_12, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => '1',
        ce => rdy_ps_net,
        clr => '0',
        clk => clk_12, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_670158bb462abb3d_instance : fr_cmplr_v5_0_670158bb462abb3d
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_24,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758 is 
  port(
    ce:in std_logic;
    ce_12:in std_logic;
    ce_6:in std_logic;
    ce_logic_12:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_12:in std_logic;
    clk_6:in std_logic;
    clk_logic_12:in std_logic;
    din:in std_logic_vector(16 downto 0);
    dout:out std_logic_vector(31 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758;


architecture behavior of xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758  is
  component fr_cmplr_v5_0_51d88ed999503974
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(16 downto 0);
      dout:out std_logic_vector(31 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal dout_ps_net: std_logic_vector(31 downto 0) := (others=>'0');
signal rdy_ps_net: std_logic := '0';
signal rdy_ps_net_captured: std_logic := '0';
signal rdy_ps_net_or_captured_net: std_logic := '0';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_12,
        clr => '0',
        clk => clk_12, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_6,
        clr => '0',
        clk => clk_6, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 32,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_6,
        clr => '0',
        clk => clk_6, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_6,
        clr => '0',
        clk => clk_6, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => '1',
        ce => rdy_ps_net,
        clr => '0',
        clk => clk_6, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_51d88ed999503974_instance : fr_cmplr_v5_0_51d88ed999503974
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_12,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397 is 
  port(
    ce:in std_logic;
    ce_3:in std_logic;
    ce_6:in std_logic;
    ce_logic_6:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_3:in std_logic;
    clk_6:in std_logic;
    clk_logic_6:in std_logic;
    din:in std_logic_vector(16 downto 0);
    dout:out std_logic_vector(18 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397;


architecture behavior of xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397  is
  component fr_cmplr_v5_0_e37f692facadc69b
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(16 downto 0);
      dout:out std_logic_vector(18 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal dout_ps_net: std_logic_vector(18 downto 0) := (others=>'0');
signal rdy_ps_net: std_logic := '0';
signal rdy_ps_net_captured: std_logic := '0';
signal rdy_ps_net_or_captured_net: std_logic := '0';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_6,
        clr => '0',
        clk => clk_6, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_3,
        clr => '0',
        clk => clk_3, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 19,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_3,
        clr => '0',
        clk => clk_3, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_3,
        clr => '0',
        clk => clk_3, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => '1',
        ce => rdy_ps_net,
        clr => '0',
        clk => clk_3, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_e37f692facadc69b_instance : fr_cmplr_v5_0_e37f692facadc69b
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_6,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end  behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.conv_pkg.all;

entity xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26 is 
  port(
    ce:in std_logic;
    ce_24:in std_logic;
    ce_48:in std_logic;
    ce_logic_48:in std_logic;
    chan_in:out std_logic_vector(0 downto 0);
    chan_out:out std_logic_vector(0 downto 0);
    clk:in std_logic;
    clk_24:in std_logic;
    clk_48:in std_logic;
    clk_logic_48:in std_logic;
    din:in std_logic_vector(15 downto 0);
    dout:out std_logic_vector(15 downto 0);
    rdy:out std_logic;
    rfd:out std_logic;
    src_ce:in std_logic;
    src_clk:in std_logic
  );
end xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26;


architecture behavior of xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26  is
  component fr_cmplr_v5_0_059cf50081fc492c
    port(
      ce:in std_logic;
      chan_in:out std_logic_vector(0 downto 0);
      chan_out:out std_logic_vector(0 downto 0);
      clk:in std_logic;
      din:in std_logic_vector(15 downto 0);
      dout:out std_logic_vector(15 downto 0);
      nd:in std_logic;
      rdy:out std_logic;
      rfd:out std_logic
    );
end component;
signal chan_in_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal chan_out_ps_net: std_logic_vector(0 downto 0) := (others=>'0');
signal dout_ps_net: std_logic_vector(15 downto 0) := (others=>'0');
signal rdy_ps_net: std_logic := '0';
signal rdy_ps_net_captured: std_logic := '0';
signal rdy_ps_net_or_captured_net: std_logic := '0';
begin
  chan_in_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_in_ps_net,
        ce => ce_48,
        clr => '0',
        clk => clk_48, 
        o => chan_in
    );
  chan_out_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => chan_out_ps_net,
        ce => ce_24,
        clr => '0',
        clk => clk_24, 
        o => chan_out
    );
  dout_ps_net_synchronizer : entity work.synth_reg_w_init
    generic map(
        width => 16,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i => dout_ps_net,
        ce => ce_24,
        clr => '0',
        clk => clk_24, 
        o => dout
    );
  rdy_ps_net_or_captured_net <= rdy_ps_net or rdy_ps_net_captured;
rdy_ps_net_synchronizer_1 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => rdy_ps_net_or_captured_net,
        ce => ce_24,
        clr => '0',
        clk => clk_24, 
        o(0) => rdy
    );
rdy_ps_net_synchronizer_2 : entity work.synth_reg_w_init
    generic map(
        width => 1,
        init_index => 0,
        init_value => "0",
        latency => 1
    )
    port map (
        i(0) => '1',
        ce => rdy_ps_net,
        clr => '0',
        clk => clk_24, 
        o(0) => rdy_ps_net_captured
    );
  fr_cmplr_v5_0_059cf50081fc492c_instance : fr_cmplr_v5_0_059cf50081fc492c
    port map(
      ce=>ce,
      chan_in=>chan_in_ps_net,
      chan_out=>chan_out_ps_net,
      clk=>clk,
      din=>din,
      dout=>dout_ps_net,
      nd=>ce_logic_48,
      rdy=>rdy_ps_net,
      rfd=>rfd
    );
end  behavior;


-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.conv_pkg.all;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
entity xltdd_multich is
    generic (
        frame_length  : integer := 4;
        data_width    : integer := 4);
    port (
        d : in std_logic_vector (data_width-1 downto 0);
        vin : in std_logic_vector (0 downto 0) := (others => '1');
        src_clk: in std_logic;
        src_ce: in std_logic;
        src_clr: in std_logic;
        dest_clk: in std_logic;
        dest_ce: in std_logic;
        dest_clr: in std_logic;
        q0 : out std_logic_vector (data_width-1 downto 0);
        q1 : out std_logic_vector (data_width-1 downto 0);
        q2 : out std_logic_vector (data_width-1 downto 0);
        q3 : out std_logic_vector (data_width-1 downto 0);
        q4 : out std_logic_vector (data_width-1 downto 0);
        q5 : out std_logic_vector (data_width-1 downto 0);
        q6 : out std_logic_vector (data_width-1 downto 0);
        q7 : out std_logic_vector (data_width-1 downto 0);
        q8 : out std_logic_vector (data_width-1 downto 0);
        q9 : out std_logic_vector (data_width-1 downto 0);
        q10 : out std_logic_vector (data_width-1 downto 0);
        q11 : out std_logic_vector (data_width-1 downto 0);
        q12 : out std_logic_vector (data_width-1 downto 0);
        q13 : out std_logic_vector (data_width-1 downto 0);
        q14 : out std_logic_vector (data_width-1 downto 0);
        q15 : out std_logic_vector (data_width-1 downto 0);
        q16 : out std_logic_vector (data_width-1 downto 0);
        q17 : out std_logic_vector (data_width-1 downto 0);
        q18 : out std_logic_vector (data_width-1 downto 0);
        q19 : out std_logic_vector (data_width-1 downto 0);
        q20 : out std_logic_vector (data_width-1 downto 0);
        q21 : out std_logic_vector (data_width-1 downto 0);
        q22 : out std_logic_vector (data_width-1 downto 0);
        q23 : out std_logic_vector (data_width-1 downto 0);
        q24 : out std_logic_vector (data_width-1 downto 0);
        q25 : out std_logic_vector (data_width-1 downto 0);
        q26 : out std_logic_vector (data_width-1 downto 0);
        q27 : out std_logic_vector (data_width-1 downto 0);
        q28 : out std_logic_vector (data_width-1 downto 0);
        q29 : out std_logic_vector (data_width-1 downto 0);
        q30 : out std_logic_vector (data_width-1 downto 0);
        q31 : out std_logic_vector (data_width-1 downto 0);
        vout : out std_logic_vector (0 downto 0));
end xltdd_multich;
architecture behavior of xltdd_multich is
    component SRL16E
        -- synopsys translate_off
        generic (INIT : bit_vector := X"0000");
        -- synopsys translate_on
        port (D   : in std_ulogic;
              CE  : in std_ulogic;
              CLK : in std_ulogic;
              A0  : in std_ulogic;
              A1  : in std_ulogic;
              A2  : in std_ulogic;
              A3  : in std_ulogic;
              Q   : out std_ulogic);
    end component;
    attribute syn_black_box of SRL16E : component is true;
    attribute fpga_dont_touch of SRL16E : component is "true";

    component synth_reg
        generic (width   : integer;
                 latency : integer);
        port (i   : in std_logic_vector(width-1 downto 0);
              ce  : in std_logic;
              clr : in std_logic;
              clk : in std_logic;
              o   : out std_logic_vector(width-1 downto 0));
    end component;

    type temp_array is array (0 to 31) of std_logic_vector(data_width-1 downto 0);
    signal i, o, dout_temp, capture_in : temp_array;

    constant pg_addr : std_logic_vector(3 downto 0)
        := integer_to_std_logic_vector(frame_length-1, 4, xlUnsigned);
    constant cnt_zero : std_logic_vector(3 downto 0) := (others => '0');
    signal smpl_dout: std_logic_vector(data_width-1 downto 0);
    signal fifo_addr: std_logic_vector(3 downto 0) := (others => '0');
    signal dly_fifo_en,fifo_en, pg_out, src_en : std_logic;
    signal tmp_vout : std_logic_vector(0 downto 0) := (others => '0');
    signal cnt_by_one : std_logic_vector(0 downto 0);

begin
    src_en <= src_ce and vin(0);
    fifo_en <= src_en and pg_out;
    dly_fifo_en <= fifo_en after 200 ps;
    cnt_by_one(0) <= '1';
       fd_array: for index in frame_length - 1 downto 0 generate
          comp : synth_reg
             generic map (width      => data_width,
                          latency    => 1)
             port map (i   => i(index),
                       ce  => src_en,
                       clr => src_clr,
                       clk => src_clk,
                       o   => o(index));
          capture : synth_reg
             generic map (width      => data_width,
                           latency    => 1)
             port map (i   => capture_in(index),
                       ce  => dest_ce,
                       clr => dest_clr,
                       clk => dest_clk,
                       o   => dout_temp(frame_length-1-index));
          signal_0: if (index = 0) generate
                       i(index) <= d;
                       capture_in(index) <= d;
          end generate;
          signal_gt_0: if (index > 0) generate
                        i(index) <= o(index - 1);
                        capture_in(index) <= o(index - 1);
          end generate;
       end generate;
        q0 <= dout_temp(0);
        q1 <= dout_temp(1);
        q2 <= dout_temp(2);
        q3 <= dout_temp(3);
        q4 <= dout_temp(4);
        q5 <= dout_temp(5);
        q6 <= dout_temp(6);
        q7 <= dout_temp(7);
        q8 <= dout_temp(8);
        q9 <= dout_temp(9);
        q10 <= dout_temp(10);
        q11 <= dout_temp(11);
        q12 <= dout_temp(12);
        q13 <= dout_temp(13);
        q14 <= dout_temp(14);
        q15 <= dout_temp(15);
        q16 <= dout_temp(16);
        q17 <= dout_temp(17);
        q18 <= dout_temp(18);
        q19 <= dout_temp(19);
        q20 <= dout_temp(20);
        q21 <= dout_temp(21);
        q22 <= dout_temp(22);
        q23 <= dout_temp(23);
        q24 <= dout_temp(24);
        q25 <= dout_temp(25);
        q26 <= dout_temp(26);
        q27 <= dout_temp(27);
        q28 <= dout_temp(28);
        q29 <= dout_temp(29);
        q30 <= dout_temp(30);
        q31 <= dout_temp(31);

end  behavior;

-------------------------------------------------------------------
-- System Generator version 13.1 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2011 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.conv_pkg.all;
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
entity xltdm is
    generic (
        num_inputs    : integer := 2;
        data_width    : integer := 4;
        hasValid      : integer := 0);
    port (
        src_clk: in std_logic;
        src_ce: in std_logic;
        dest_clk: in std_logic;
        dest_ce: in std_logic;
    d0 : in std_logic_vector (data_width-1 downto 0):= (others => '0');
        d1 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d2 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d3 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d4 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d5 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d6 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d7 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d8 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d9 : in std_logic_vector (data_width-1 downto 0) := (others => '0');

    d10 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d11 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d12 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d13 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d14 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d15 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d16 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d17 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d18 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d19 : in std_logic_vector (data_width-1 downto 0) := (others => '0');

    d20 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d21 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d22 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d23 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d24 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d25 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d26 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d27 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d28 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d29 : in std_logic_vector (data_width-1 downto 0) := (others => '0');

    d30 : in std_logic_vector (data_width-1 downto 0) := (others => '0');
        d31 : in std_logic_vector (data_width-1 downto 0) := (others => '0');

    vin : in std_logic_vector (0 downto 0) := (others => '1');
        q : out std_logic_vector (data_width-1 downto 0);
        vout : out std_logic_vector (0 downto 0));
end xltdm;
architecture behavior of xltdm is
    type temp_array is array (0 to 31) of std_logic_vector(data_width-1 downto 0);
    signal din_temp : temp_array;

    signal src_en, dest_en : std_logic;
    signal indx_cntr : integer := 0;

begin
    dest_en <= dest_ce and vin(0);
    src_en <= src_ce and vin(0);
    vout <= vin;
    q <= din_temp(indx_cntr);
   index_counter : process(src_clk)
    begin
        if rising_edge(src_clk) then
                      if (src_en = '1') then
                indx_cntr <= 0;
            else
                if (dest_en = '1') then
                    indx_cntr <= indx_cntr + 1;
                end if;
                        end if;
        end if;
    end process;

    din_temp(0) <= d0;
    din_temp(1) <= d1;
    din_temp(2) <= d2;
    din_temp(3) <= d3;
    din_temp(4) <= d4;
    din_temp(5) <= d5;
    din_temp(6) <= d6;
    din_temp(7) <= d7;
    din_temp(8) <= d8;
    din_temp(9) <= d9;
    din_temp(10) <= d10;
    din_temp(11) <= d11;
    din_temp(12) <= d12;
    din_temp(13) <= d13;
    din_temp(14) <= d14;
    din_temp(15) <= d15;
    din_temp(16) <= d16;
    din_temp(17) <= d17;
    din_temp(18) <= d18;
    din_temp(19) <= d19;
    din_temp(20) <= d20;
    din_temp(21) <= d21;
    din_temp(22) <= d22;
    din_temp(23) <= d23;
    din_temp(24) <= d24;
    din_temp(25) <= d25;
    din_temp(26) <= d26;
    din_temp(27) <= d27;
    din_temp(28) <= d28;
    din_temp(29) <= d29;
    din_temp(30) <= d30;
    din_temp(31) <= d31;

end  behavior;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC/Complex Mixer"

entity complex_mixer_entity_e3ec7de3fc is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    freq_we: in std_logic; 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end complex_mixer_entity_e3ec7de3fc;

architecture structural of complex_mixer_entity_e3ec7de3fc is
  signal ce_1_sg_x0: std_logic;
  signal ce_6_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal clk_6_sg_x0: std_logic;
  signal complex_multiplier_3_1_pi_net: std_logic_vector(16 downto 0);
  signal complex_multiplier_3_1_pr_net: std_logic_vector(16 downto 0);
  signal convert1_dout_net: std_logic_vector(15 downto 0);
  signal convert1_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert_dout_net: std_logic_vector(15 downto 0);
  signal convert_dout_net_x1: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x0: std_logic;
  signal dds_compiler_4_0_cosine_net: std_logic_vector(18 downto 0);
  signal dds_compiler_4_0_sine_net: std_logic_vector(18 downto 0);
  signal delay1_q_net_x0: std_logic_vector(15 downto 0);
  signal delay2_q_net: std_logic;
  signal delay3_q_net_x0: std_logic;
  signal delay_q_net_x0: std_logic_vector(15 downto 0);
  signal freq_net_x0: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x0: std_logic;

begin
  ce_1_sg_x0 <= ce_1;
  ce_6_sg_x0 <= ce_6;
  clk_1_sg_x0 <= clk_1;
  clk_6_sg_x0 <= clk_6;
  convert_dout_net_x1 <= din_i;
  convert1_dout_net_x1 <= din_q;
  freq_net_x0 <= freq;
  freq_we_delay_q_net_x0 <= freq_we;
  data_valid_delay_q_net_x0 <= vin;
  dout_i <= delay_q_net_x0;
  dout_q <= delay1_q_net_x0;
  vout <= delay3_q_net_x0;

  complex_multiplier_3_1: entity work.xlcomplex_multiplier_8bbad598f7b6113e77ee226ca763393f
    port map (
      ai => dds_compiler_4_0_sine_net,
      ar => dds_compiler_4_0_cosine_net,
      bi => convert1_dout_net_x1,
      br => convert_dout_net_x1,
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      pi => complex_multiplier_3_1_pi_net,
      pr => complex_multiplier_3_1_pr_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 17,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      clr => '0',
      din => complex_multiplier_3_1_pr_net,
      en => "1",
      dout => convert_dout_net
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 17,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      clr => '0',
      din => complex_multiplier_3_1_pi_net,
      en => "1",
      dout => convert1_dout_net
    );

  dds_compiler_4_0: entity work.xldds_compiler_2c12a80d02115adc29f55df6e1d9fc1e
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      data => freq_net_x0,
      we => freq_we_delay_q_net_x0,
      cosine => dds_compiler_4_0_cosine_net,
      sine => dds_compiler_4_0_sine_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 16
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      d => convert_dout_net,
      en => '1',
      q => delay_q_net_x0
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 16
    )
    port map (
      ce => ce_6_sg_x0,
      clk => clk_6_sg_x0,
      d => convert1_dout_net,
      en => '1',
      q => delay1_q_net_x0
    );

  delay2: entity work.xldelay
    generic map (
      latency => 30,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      d(0) => data_valid_delay_q_net_x0,
      en => '1',
      q(0) => delay2_q_net
    );

  delay3: entity work.xldelay
    generic map (
      latency => 6,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      d(0) => delay2_q_net,
      en => '1',
      q(0) => delay3_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC/Decimation FIR Filters"

entity decimation_fir_filters_entity_08685b80e0 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end decimation_fir_filters_entity_08685b80e0;

architecture structural of decimation_fir_filters_entity_08685b80e0 is
  signal ce_1_sg_x1: std_logic;
  signal clk_1_sg_x1: std_logic;
  signal convert1_dout_net: std_logic_vector(16 downto 0);
  signal convert2_dout_net: std_logic_vector(16 downto 0);
  signal convert4_dout_net: std_logic_vector(17 downto 0);
  signal convert5_dout_net: std_logic_vector(17 downto 0);
  signal convert6_dout_net_x0: std_logic_vector(15 downto 0);
  signal convert7_dout_net_x0: std_logic_vector(15 downto 0);
  signal delay1_q_net: std_logic_vector(16 downto 0);
  signal delay1_q_net_x1: std_logic_vector(15 downto 0);
  signal delay3_q_net_x1: std_logic;
  signal delay_q_net: std_logic_vector(16 downto 0);
  signal delay_q_net_x1: std_logic_vector(15 downto 0);
  signal imf1_dout_1_net: std_logic_vector(18 downto 0);
  signal imf1_dout_2_net: std_logic_vector(18 downto 0);
  signal imf1_rdy_net: std_logic;
  signal imf2_dout_1_net: std_logic_vector(17 downto 0);
  signal imf2_dout_2_net: std_logic_vector(17 downto 0);
  signal imf2_rdy_net: std_logic;
  signal srrc_dout_1_net: std_logic_vector(37 downto 0);
  signal srrc_dout_2_net: std_logic_vector(37 downto 0);
  signal srrc_rdy_net_x0: std_logic;

begin
  ce_1_sg_x1 <= ce_1;
  clk_1_sg_x1 <= clk_1;
  delay_q_net_x1 <= din_i;
  delay1_q_net_x1 <= din_q;
  delay3_q_net_x1 <= vin;
  dout_i <= convert6_dout_net_x0;
  dout_q <= convert7_dout_net_x0;
  vout <= srrc_rdy_net_x0;

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf2_dout_1_net,
      en => "1",
      dout => convert1_dout_net
    );

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf2_dout_2_net,
      en => "1",
      dout => convert2_dout_net
    );

  convert4: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf1_dout_1_net,
      en => "1",
      dout => convert4_dout_net
    );

  convert5: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 15,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => imf1_dout_2_net,
      en => "1",
      dout => convert5_dout_net
    );

  convert6: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 32,
      din_width => 38,
      dout_arith => 2,
      dout_bin_pt => 12,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => srrc_dout_1_net,
      en => "1",
      dout => convert6_dout_net_x0
    );

  convert7: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 32,
      din_width => 38,
      dout_arith => 2,
      dout_bin_pt => 12,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      din => srrc_dout_2_net,
      en => "1",
      dout => convert7_dout_net_x0
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 17
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      d => convert1_dout_net,
      en => '1',
      q => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 17
    )
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      d => convert2_dout_net,
      en => '1',
      q => delay1_q_net
    );

  imf1: entity work.xlfir_compiler_2dd42942cc81de000d4cc82c9b7048f8
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => delay_q_net,
      din_2 => delay1_q_net,
      nd => imf2_rdy_net,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => imf1_dout_1_net,
      dout_2 => imf1_dout_2_net,
      rdy => imf1_rdy_net
    );

  imf2: entity work.xlfir_compiler_8bbf91ad55b0353987e969c586c6098d
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => delay_q_net_x1,
      din_2 => delay1_q_net_x1,
      nd => delay3_q_net_x1,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => imf2_dout_1_net,
      dout_2 => imf2_dout_2_net,
      rdy => imf2_rdy_net
    );

  srrc: entity work.xlfir_compiler_e2c7cac926eb709d4e75f57e195835ce
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      din_1 => convert4_dout_net,
      din_2 => convert5_dout_net,
      nd => imf1_rdy_net,
      src_ce => ce_1_sg_x1,
      src_clk => clk_1_sg_x1,
      dout_1 => srrc_dout_1_net,
      dout_2 => srrc_dout_2_net,
      rdy => srrc_rdy_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DDC"

entity ddc_entity_66861caa4b is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    freq_we: in std_logic; 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vout: out std_logic
  );
end ddc_entity_66861caa4b;

architecture structural of ddc_entity_66861caa4b is
  signal ce_1_sg_x2: std_logic;
  signal ce_6_sg_x1: std_logic;
  signal clk_1_sg_x2: std_logic;
  signal clk_6_sg_x1: std_logic;
  signal convert1_dout_net_x2: std_logic_vector(15 downto 0);
  signal convert6_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert7_dout_net_x1: std_logic_vector(15 downto 0);
  signal convert_dout_net_x2: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x1: std_logic;
  signal delay1_q_net_x1: std_logic_vector(15 downto 0);
  signal delay3_q_net_x1: std_logic;
  signal delay_q_net_x1: std_logic_vector(15 downto 0);
  signal freq_net_x1: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x1: std_logic;
  signal srrc_rdy_net_x1: std_logic;

begin
  ce_1_sg_x2 <= ce_1;
  ce_6_sg_x1 <= ce_6;
  clk_1_sg_x2 <= clk_1;
  clk_6_sg_x1 <= clk_6;
  convert_dout_net_x2 <= din_i;
  convert1_dout_net_x2 <= din_q;
  freq_net_x1 <= freq;
  freq_we_delay_q_net_x1 <= freq_we;
  data_valid_delay_q_net_x1 <= vin;
  dout_i <= convert6_dout_net_x1;
  dout_q <= convert7_dout_net_x1;
  vout <= srrc_rdy_net_x1;

  complex_mixer_e3ec7de3fc: entity work.complex_mixer_entity_e3ec7de3fc
    port map (
      ce_1 => ce_1_sg_x2,
      ce_6 => ce_6_sg_x1,
      clk_1 => clk_1_sg_x2,
      clk_6 => clk_6_sg_x1,
      din_i => convert_dout_net_x2,
      din_q => convert1_dout_net_x2,
      freq => freq_net_x1,
      freq_we => freq_we_delay_q_net_x1,
      vin => data_valid_delay_q_net_x1,
      dout_i => delay_q_net_x1,
      dout_q => delay1_q_net_x1,
      vout => delay3_q_net_x1
    );

  decimation_fir_filters_08685b80e0: entity work.decimation_fir_filters_entity_08685b80e0
    port map (
      ce_1 => ce_1_sg_x2,
      clk_1 => clk_1_sg_x2,
      din_i => delay_q_net_x1,
      din_q => delay1_q_net_x1,
      vin => delay3_q_net_x1,
      dout_i => convert6_dout_net_x1,
      dout_q => convert7_dout_net_x1,
      vout => srrc_rdy_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Complex Mixer/one_shot"

entity one_shot_entity_995adbad00 is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    in_x0: in std_logic; 
    one_shot: out std_logic
  );
end one_shot_entity_995adbad00;

architecture structural of one_shot_entity_995adbad00 is
  signal ce_1_sg_x3: std_logic;
  signal ce_6_sg_x2: std_logic;
  signal clk_1_sg_x3: std_logic;
  signal clk_6_sg_x2: std_logic;
  signal constant1_op_net: std_logic;
  signal delay1_q_net: std_logic;
  signal delay_q_net: std_logic;
  signal down_sample_q_net: std_logic;
  signal inverter_op_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal register_q_net: std_logic;
  signal up_sample_q_net_x0: std_logic;

begin
  ce_1_sg_x3 <= ce_1;
  ce_6_sg_x2 <= ce_6;
  clk_1_sg_x3 <= clk_1;
  clk_6_sg_x2 <= clk_6;
  up_sample_q_net_x0 <= in_x0;
  one_shot <= logical_y_net_x0;

  constant1: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 2,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      d(0) => up_sample_q_net_x0,
      en => '1',
      q(0) => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      d(0) => inverter_op_net,
      en => '1',
      q(0) => delay1_q_net
    );

  down_sample: entity work.xldsamp
    generic map (
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 1,
      ds_ratio => 6,
      latency => 14,
      phase => 5,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 1
    )
    port map (
      d(0) => register_q_net,
      dest_ce => ce_6_sg_x2,
      dest_clk => clk_6_sg_x2,
      dest_clr => '0',
      en => "1",
      src_ce => ce_1_sg_x3,
      src_clk => clk_1_sg_x3,
      src_clr => '0',
      q(0) => down_sample_q_net
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      clr => '0',
      ip(0) => down_sample_q_net,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_799f62af22
    port map (
      ce => ce_6_sg_x2,
      clk => clk_6_sg_x2,
      clr => '0',
      d0(0) => delay1_q_net,
      d1(0) => down_sample_q_net,
      y(0) => logical_y_net_x0
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      d(0) => constant1_op_net,
      en(0) => delay_q_net,
      rst => "0",
      q(0) => register_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Complex Mixer"

entity complex_mixer_entity_a71e69fac1 is
  port (
    ce_1: in std_logic; 
    ce_6: in std_logic; 
    clk_1: in std_logic; 
    clk_6: in std_logic; 
    din_i: in std_logic_vector(17 downto 0); 
    din_q: in std_logic_vector(17 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    freq_we: out std_logic; 
    vout: out std_logic
  );
end complex_mixer_entity_a71e69fac1;

architecture structural of complex_mixer_entity_a71e69fac1 is
  signal ce_1_sg_x4: std_logic;
  signal ce_6_sg_x3: std_logic;
  signal clk_1_sg_x4: std_logic;
  signal clk_6_sg_x3: std_logic;
  signal complex_multiplier_3_1_pi_net: std_logic_vector(35 downto 0);
  signal complex_multiplier_3_1_pr_net: std_logic_vector(35 downto 0);
  signal convert1_dout_net_x3: std_logic_vector(15 downto 0);
  signal convert_dout_net_x3: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x2: std_logic;
  signal dds_compiler_4_0_cosine_net: std_logic_vector(18 downto 0);
  signal dds_compiler_4_0_sine_net: std_logic_vector(18 downto 0);
  signal delay4_q_net_x0: std_logic_vector(17 downto 0);
  signal delay5_q_net_x0: std_logic_vector(17 downto 0);
  signal freq_we_delay_q_net_x2: std_logic;
  signal logical_y_net_x0: std_logic;
  signal register1_q_net_x0: std_logic_vector(27 downto 0);
  signal register4_q_net_x0: std_logic;
  signal up_sample_q_net_x0: std_logic;

begin
  ce_1_sg_x4 <= ce_1;
  ce_6_sg_x3 <= ce_6;
  clk_1_sg_x4 <= clk_1;
  clk_6_sg_x3 <= clk_6;
  delay5_q_net_x0 <= din_i;
  delay4_q_net_x0 <= din_q;
  register1_q_net_x0 <= freq;
  register4_q_net_x0 <= vin;
  dout_i <= convert_dout_net_x3;
  dout_q <= convert1_dout_net_x3;
  freq_we <= freq_we_delay_q_net_x2;
  vout <= data_valid_delay_q_net_x2;

  complex_multiplier_3_1: entity work.xlcomplex_multiplier_9e4c67c2b380023e933597c31412ff2b
    port map (
      ai => dds_compiler_4_0_sine_net,
      ar => dds_compiler_4_0_cosine_net,
      bi => delay4_q_net_x0,
      br => delay5_q_net_x0,
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      pi => complex_multiplier_3_1_pi_net,
      pr => complex_multiplier_3_1_pr_net
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 35,
      din_width => 36,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      clr => '0',
      din => complex_multiplier_3_1_pr_net,
      en => "1",
      dout => convert_dout_net_x3
    );

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 35,
      din_width => 36,
      dout_arith => 2,
      dout_bin_pt => 15,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlRound
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      clr => '0',
      din => complex_multiplier_3_1_pi_net,
      en => "1",
      dout => convert1_dout_net_x3
    );

  data_valid_delay: entity work.xldelay
    generic map (
      latency => 24,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      d(0) => up_sample_q_net_x0,
      en => '1',
      q(0) => data_valid_delay_q_net_x2
    );

  dds_compiler_4_0: entity work.xldds_compiler_94274096bf4e0b94b9506335cad96a90
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      data => register1_q_net_x0,
      we => logical_y_net_x0,
      cosine => dds_compiler_4_0_cosine_net,
      sine => dds_compiler_4_0_sine_net
    );

  freq_we_delay: entity work.xldelay
    generic map (
      latency => 4,
      reg_retiming => 0,
      width => 1
    )
    port map (
      ce => ce_6_sg_x3,
      clk => clk_6_sg_x3,
      d(0) => logical_y_net_x0,
      en => '1',
      q(0) => freq_we_delay_q_net_x2
    );

  one_shot_995adbad00: entity work.one_shot_entity_995adbad00
    port map (
      ce_1 => ce_1_sg_x4,
      ce_6 => ce_6_sg_x3,
      clk_1 => clk_1_sg_x4,
      clk_6 => clk_6_sg_x3,
      in_x0 => up_sample_q_net_x0,
      one_shot => logical_y_net_x0
    );

  up_sample: entity work.xlusamp
    generic map (
      copy_samples => 0,
      d_arith => xlUnsigned,
      d_bin_pt => 0,
      d_width => 1,
      latency => 0,
      q_arith => xlUnsigned,
      q_bin_pt => 0,
      q_width => 1
    )
    port map (
      d(0) => register4_q_net_x0,
      dest_ce => ce_1_sg_x4,
      dest_clk => clk_1_sg_x4,
      dest_clr => '0',
      en => "1",
      src_ce => ce_6_sg_x3,
      src_clk => clk_6_sg_x3,
      src_clr => '0',
      q(0) => up_sample_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Input Registers1"

entity input_registers1_entity_b4c9c3869f is
  port (
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    in1: in std_logic_vector(15 downto 0); 
    in2: in std_logic_vector(15 downto 0); 
    in3: in std_logic; 
    in4: in std_logic_vector(27 downto 0); 
    out1: out std_logic_vector(15 downto 0); 
    out2: out std_logic_vector(15 downto 0); 
    out3: out std_logic; 
    out4: out std_logic_vector(27 downto 0)
  );
end input_registers1_entity_b4c9c3869f;

architecture structural of input_registers1_entity_b4c9c3869f is
  signal ce_6_sg_x4: std_logic;
  signal ce_96_sg_x0: std_logic;
  signal clk_6_sg_x4: std_logic;
  signal clk_96_sg_x0: std_logic;
  signal din_i_net_x0: std_logic_vector(15 downto 0);
  signal din_q_net_x0: std_logic_vector(15 downto 0);
  signal freq_net_x2: std_logic_vector(27 downto 0);
  signal register1_q_net_x1: std_logic_vector(27 downto 0);
  signal register4_q_net_x1: std_logic;
  signal register5_q_net_x0: std_logic_vector(15 downto 0);
  signal register6_q_net_x0: std_logic_vector(15 downto 0);
  signal vin_net_x0: std_logic;

begin
  ce_6_sg_x4 <= ce_6;
  ce_96_sg_x0 <= ce_96;
  clk_6_sg_x4 <= clk_6;
  clk_96_sg_x0 <= clk_96;
  din_i_net_x0 <= in1;
  din_q_net_x0 <= in2;
  vin_net_x0 <= in3;
  freq_net_x2 <= in4;
  out1 <= register6_q_net_x0;
  out2 <= register5_q_net_x0;
  out3 <= register4_q_net_x1;
  out4 <= register1_q_net_x1;

  register1: entity work.xlregister
    generic map (
      d_width => 28,
      init_value => b"0000000000000000000000000000"
    )
    port map (
      ce => ce_6_sg_x4,
      clk => clk_6_sg_x4,
      d => freq_net_x2,
      en => "1",
      rst => "0",
      q => register1_q_net_x1
    );

  register4: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_6_sg_x4,
      clk => clk_6_sg_x4,
      d(0) => vin_net_x0,
      en => "1",
      rst => "0",
      q(0) => register4_q_net_x1
    );

  register5: entity work.xlregister
    generic map (
      d_width => 16,
      init_value => b"0000000000000000"
    )
    port map (
      ce => ce_96_sg_x0,
      clk => clk_96_sg_x0,
      d => din_q_net_x0,
      en => "1",
      rst => "0",
      q => register5_q_net_x0
    );

  register6: entity work.xlregister
    generic map (
      d_width => 16,
      init_value => b"0000000000000000"
    )
    port map (
      ce => ce_96_sg_x0,
      clk => clk_96_sg_x0,
      d => din_i_net_x0,
      en => "1",
      rst => "0",
      q => register6_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC/Interpolation Filters"

entity interpolation_filters_entity_5d99efe737 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    dout_i: out std_logic_vector(17 downto 0); 
    dout_q: out std_logic_vector(17 downto 0)
  );
end interpolation_filters_entity_5d99efe737;

architecture structural of interpolation_filters_entity_5d99efe737 is
  signal ce_12_sg_x0: std_logic;
  signal ce_1_sg_x5: std_logic;
  signal ce_24_sg_x0: std_logic;
  signal ce_3_sg_x0: std_logic;
  signal ce_48_sg_x0: std_logic;
  signal ce_6_sg_x5: std_logic;
  signal ce_96_sg_x1: std_logic;
  signal ce_logic_12_sg_x0: std_logic;
  signal ce_logic_24_sg_x0: std_logic;
  signal ce_logic_48_sg_x0: std_logic;
  signal ce_logic_6_sg_x0: std_logic;
  signal clk_12_sg_x0: std_logic;
  signal clk_1_sg_x5: std_logic;
  signal clk_24_sg_x0: std_logic;
  signal clk_3_sg_x0: std_logic;
  signal clk_48_sg_x0: std_logic;
  signal clk_6_sg_x5: std_logic;
  signal clk_96_sg_x1: std_logic;
  signal convert2_dout_net: std_logic_vector(16 downto 0);
  signal convert3_dout_net: std_logic_vector(16 downto 0);
  signal convert4_dout_net: std_logic_vector(17 downto 0);
  signal delay4_q_net_x1: std_logic_vector(17 downto 0);
  signal delay5_q_net_x1: std_logic_vector(17 downto 0);
  signal delay_q_net: std_logic_vector(17 downto 0);
  signal imf1_dout_net: std_logic_vector(17 downto 0);
  signal imf2_dout_net: std_logic_vector(31 downto 0);
  signal imf3_dout_net: std_logic_vector(18 downto 0);
  signal register5_q_net_x1: std_logic_vector(15 downto 0);
  signal register6_q_net_x1: std_logic_vector(15 downto 0);
  signal srrc_dout_net: std_logic_vector(15 downto 0);
  signal time_division_demultiplexer_q0_net: std_logic_vector(17 downto 0);
  signal time_division_demultiplexer_q1_net: std_logic_vector(17 downto 0);
  signal time_division_multiplexer_q_net: std_logic_vector(15 downto 0);

begin
  ce_1_sg_x5 <= ce_1;
  ce_12_sg_x0 <= ce_12;
  ce_24_sg_x0 <= ce_24;
  ce_3_sg_x0 <= ce_3;
  ce_48_sg_x0 <= ce_48;
  ce_6_sg_x5 <= ce_6;
  ce_96_sg_x1 <= ce_96;
  ce_logic_12_sg_x0 <= ce_logic_12;
  ce_logic_24_sg_x0 <= ce_logic_24;
  ce_logic_48_sg_x0 <= ce_logic_48;
  ce_logic_6_sg_x0 <= ce_logic_6;
  clk_1_sg_x5 <= clk_1;
  clk_12_sg_x0 <= clk_12;
  clk_24_sg_x0 <= clk_24;
  clk_3_sg_x0 <= clk_3;
  clk_48_sg_x0 <= clk_48;
  clk_6_sg_x5 <= clk_6;
  clk_96_sg_x1 <= clk_96;
  register6_q_net_x1 <= din_i;
  register5_q_net_x1 <= din_q;
  dout_i <= delay5_q_net_x1;
  dout_q <= delay4_q_net_x1;

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 30,
      din_width => 32,
      dout_arith => 2,
      dout_bin_pt => 16,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      clr => '0',
      din => imf2_dout_net,
      en => "1",
      dout => convert2_dout_net
    );

  convert3: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 16,
      din_width => 18,
      dout_arith => 2,
      dout_bin_pt => 16,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_12_sg_x0,
      clk => clk_12_sg_x0,
      clr => '0',
      din => imf1_dout_net,
      en => "1",
      dout => convert3_dout_net
    );

  convert4: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 2,
      din_bin_pt => 17,
      din_width => 19,
      dout_arith => 2,
      dout_bin_pt => 17,
      dout_width => 18,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_3_sg_x0,
      clk => clk_3_sg_x0,
      clr => '0',
      din => imf3_dout_net,
      en => "1",
      dout => convert4_dout_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 4,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => time_division_demultiplexer_q0_net,
      en => '1',
      q => delay_q_net
    );

  delay4: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => time_division_demultiplexer_q1_net,
      en => '1',
      q => delay4_q_net_x1
    );

  delay5: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 18
    )
    port map (
      ce => ce_6_sg_x5,
      clk => clk_6_sg_x5,
      d => delay_q_net,
      en => '1',
      q => delay5_q_net_x1
    );

  imf1: entity work.xlfir_compiler_6b538e2e420c55fbcb179300d415c30e
    port map (
      ce => ce_1_sg_x5,
      ce_12 => ce_12_sg_x0,
      ce_24 => ce_24_sg_x0,
      ce_logic_24 => ce_logic_24_sg_x0,
      clk => clk_1_sg_x5,
      clk_12 => clk_12_sg_x0,
      clk_24 => clk_24_sg_x0,
      clk_logic_24 => clk_24_sg_x0,
      din => srrc_dout_net,
      src_ce => ce_24_sg_x0,
      src_clk => clk_24_sg_x0,
      dout => imf1_dout_net
    );

  imf2: entity work.xlfir_compiler_f9a675d885fdf6e63e5f8cc84cc59758
    port map (
      ce => ce_1_sg_x5,
      ce_12 => ce_12_sg_x0,
      ce_6 => ce_6_sg_x5,
      ce_logic_12 => ce_logic_12_sg_x0,
      clk => clk_1_sg_x5,
      clk_12 => clk_12_sg_x0,
      clk_6 => clk_6_sg_x5,
      clk_logic_12 => clk_12_sg_x0,
      din => convert3_dout_net,
      src_ce => ce_12_sg_x0,
      src_clk => clk_12_sg_x0,
      dout => imf2_dout_net
    );

  imf3: entity work.xlfir_compiler_8c48d2195e59b239f95c4f1fbadcc397
    port map (
      ce => ce_1_sg_x5,
      ce_3 => ce_3_sg_x0,
      ce_6 => ce_6_sg_x5,
      ce_logic_6 => ce_logic_6_sg_x0,
      clk => clk_1_sg_x5,
      clk_3 => clk_3_sg_x0,
      clk_6 => clk_6_sg_x5,
      clk_logic_6 => clk_6_sg_x5,
      din => convert2_dout_net,
      src_ce => ce_6_sg_x5,
      src_clk => clk_6_sg_x5,
      dout => imf3_dout_net
    );

  srrc: entity work.xlfir_compiler_c55ac62f34fcb67f1e47b728d2285b26
    port map (
      ce => ce_1_sg_x5,
      ce_24 => ce_24_sg_x0,
      ce_48 => ce_48_sg_x0,
      ce_logic_48 => ce_logic_48_sg_x0,
      clk => clk_1_sg_x5,
      clk_24 => clk_24_sg_x0,
      clk_48 => clk_48_sg_x0,
      clk_logic_48 => clk_48_sg_x0,
      din => time_division_multiplexer_q_net,
      src_ce => ce_48_sg_x0,
      src_clk => clk_48_sg_x0,
      dout => srrc_dout_net
    );

  time_division_demultiplexer: entity work.xltdd_multich
    generic map (
      data_width => 18,
      frame_length => 2
    )
    port map (
      d => convert4_dout_net,
      dest_ce => ce_6_sg_x5,
      dest_clk => clk_6_sg_x5,
      dest_clr => '0',
      src_ce => ce_3_sg_x0,
      src_clk => clk_3_sg_x0,
      src_clr => '0',
      q0 => time_division_demultiplexer_q0_net,
      q1 => time_division_demultiplexer_q1_net
    );

  time_division_multiplexer: entity work.xltdm
    generic map (
      data_width => 16,
      hasValid => 0,
      num_inputs => 2
    )
    port map (
      d0 => register6_q_net_x1,
      d1 => register5_q_net_x1,
      dest_ce => ce_48_sg_x0,
      dest_clk => clk_48_sg_x0,
      src_ce => ce_96_sg_x1,
      src_clk => clk_96_sg_x1,
      q => time_division_multiplexer_q_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6/DUC"

entity duc_entity_8eb6622f87 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    freq_we: out std_logic; 
    vout: out std_logic
  );
end duc_entity_8eb6622f87;

architecture structural of duc_entity_8eb6622f87 is
  signal ce_12_sg_x1: std_logic;
  signal ce_1_sg_x6: std_logic;
  signal ce_24_sg_x1: std_logic;
  signal ce_3_sg_x1: std_logic;
  signal ce_48_sg_x1: std_logic;
  signal ce_6_sg_x6: std_logic;
  signal ce_96_sg_x2: std_logic;
  signal ce_logic_12_sg_x1: std_logic;
  signal ce_logic_24_sg_x1: std_logic;
  signal ce_logic_48_sg_x1: std_logic;
  signal ce_logic_6_sg_x1: std_logic;
  signal clk_12_sg_x1: std_logic;
  signal clk_1_sg_x6: std_logic;
  signal clk_24_sg_x1: std_logic;
  signal clk_3_sg_x1: std_logic;
  signal clk_48_sg_x1: std_logic;
  signal clk_6_sg_x6: std_logic;
  signal clk_96_sg_x2: std_logic;
  signal convert1_dout_net_x4: std_logic_vector(15 downto 0);
  signal convert_dout_net_x4: std_logic_vector(15 downto 0);
  signal data_valid_delay_q_net_x3: std_logic;
  signal delay4_q_net_x1: std_logic_vector(17 downto 0);
  signal delay5_q_net_x1: std_logic_vector(17 downto 0);
  signal din_i_net_x1: std_logic_vector(15 downto 0);
  signal din_q_net_x1: std_logic_vector(15 downto 0);
  signal freq_net_x3: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x3: std_logic;
  signal register1_q_net_x1: std_logic_vector(27 downto 0);
  signal register4_q_net_x1: std_logic;
  signal register5_q_net_x1: std_logic_vector(15 downto 0);
  signal register6_q_net_x1: std_logic_vector(15 downto 0);
  signal vin_net_x1: std_logic;

begin
  ce_1_sg_x6 <= ce_1;
  ce_12_sg_x1 <= ce_12;
  ce_24_sg_x1 <= ce_24;
  ce_3_sg_x1 <= ce_3;
  ce_48_sg_x1 <= ce_48;
  ce_6_sg_x6 <= ce_6;
  ce_96_sg_x2 <= ce_96;
  ce_logic_12_sg_x1 <= ce_logic_12;
  ce_logic_24_sg_x1 <= ce_logic_24;
  ce_logic_48_sg_x1 <= ce_logic_48;
  ce_logic_6_sg_x1 <= ce_logic_6;
  clk_1_sg_x6 <= clk_1;
  clk_12_sg_x1 <= clk_12;
  clk_24_sg_x1 <= clk_24;
  clk_3_sg_x1 <= clk_3;
  clk_48_sg_x1 <= clk_48;
  clk_6_sg_x6 <= clk_6;
  clk_96_sg_x2 <= clk_96;
  din_i_net_x1 <= din_i;
  din_q_net_x1 <= din_q;
  freq_net_x3 <= freq;
  vin_net_x1 <= vin;
  dout_i <= convert_dout_net_x4;
  dout_q <= convert1_dout_net_x4;
  freq_we <= freq_we_delay_q_net_x3;
  vout <= data_valid_delay_q_net_x3;

  complex_mixer_a71e69fac1: entity work.complex_mixer_entity_a71e69fac1
    port map (
      ce_1 => ce_1_sg_x6,
      ce_6 => ce_6_sg_x6,
      clk_1 => clk_1_sg_x6,
      clk_6 => clk_6_sg_x6,
      din_i => delay5_q_net_x1,
      din_q => delay4_q_net_x1,
      freq => register1_q_net_x1,
      vin => register4_q_net_x1,
      dout_i => convert_dout_net_x4,
      dout_q => convert1_dout_net_x4,
      freq_we => freq_we_delay_q_net_x3,
      vout => data_valid_delay_q_net_x3
    );

  input_registers1_b4c9c3869f: entity work.input_registers1_entity_b4c9c3869f
    port map (
      ce_6 => ce_6_sg_x6,
      ce_96 => ce_96_sg_x2,
      clk_6 => clk_6_sg_x6,
      clk_96 => clk_96_sg_x2,
      in1 => din_i_net_x1,
      in2 => din_q_net_x1,
      in3 => vin_net_x1,
      in4 => freq_net_x3,
      out1 => register6_q_net_x1,
      out2 => register5_q_net_x1,
      out3 => register4_q_net_x1,
      out4 => register1_q_net_x1
    );

  interpolation_filters_5d99efe737: entity work.interpolation_filters_entity_5d99efe737
    port map (
      ce_1 => ce_1_sg_x6,
      ce_12 => ce_12_sg_x1,
      ce_24 => ce_24_sg_x1,
      ce_3 => ce_3_sg_x1,
      ce_48 => ce_48_sg_x1,
      ce_6 => ce_6_sg_x6,
      ce_96 => ce_96_sg_x2,
      ce_logic_12 => ce_logic_12_sg_x1,
      ce_logic_24 => ce_logic_24_sg_x1,
      ce_logic_48 => ce_logic_48_sg_x1,
      ce_logic_6 => ce_logic_6_sg_x1,
      clk_1 => clk_1_sg_x6,
      clk_12 => clk_12_sg_x1,
      clk_24 => clk_24_sg_x1,
      clk_3 => clk_3_sg_x1,
      clk_48 => clk_48_sg_x1,
      clk_6 => clk_6_sg_x6,
      clk_96 => clk_96_sg_x2,
      din_i => register6_q_net_x1,
      din_q => register5_q_net_x1,
      dout_i => delay5_q_net_x1,
      dout_q => delay4_q_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "duc_ddc_umts_virtex6"

entity duc_ddc_umts_virtex6 is
  port (
    ce_1: in std_logic; 
    ce_12: in std_logic; 
    ce_24: in std_logic; 
    ce_3: in std_logic; 
    ce_48: in std_logic; 
    ce_6: in std_logic; 
    ce_96: in std_logic; 
    ce_logic_12: in std_logic; 
    ce_logic_24: in std_logic; 
    ce_logic_48: in std_logic; 
    ce_logic_6: in std_logic; 
    clk_1: in std_logic; 
    clk_12: in std_logic; 
    clk_24: in std_logic; 
    clk_3: in std_logic; 
    clk_48: in std_logic; 
    clk_6: in std_logic; 
    clk_96: in std_logic; 
    din_i: in std_logic_vector(15 downto 0); 
    din_q: in std_logic_vector(15 downto 0); 
    freq: in std_logic_vector(27 downto 0); 
    vin: in std_logic; 
    dif_i: out std_logic_vector(15 downto 0); 
    dif_q: out std_logic_vector(15 downto 0); 
    dout_i: out std_logic_vector(15 downto 0); 
    dout_q: out std_logic_vector(15 downto 0); 
    vif: out std_logic; 
    vout: out std_logic
  );
end duc_ddc_umts_virtex6;

architecture structural of duc_ddc_umts_virtex6 is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "duc_ddc_umts_virtex6,sysgen_core,{clock_period=20.00000000,clocking=Clock_Enables,compilation=ML605_(Point-to-point_Ethernet),sample_periods=1.00000000000 3.00000000000 6.00000000000 12.00000000000 24.00000000000 48.00000000000 96.00000000000,testbench=0,total_blocks=199,xilinx_complex_multiplier_3_1_block=2,xilinx_constant_block_block=1,xilinx_copyright_notice_block=1,xilinx_dds_compiler_4_0_block=2,xilinx_delay_block=13,xilinx_down_sampler_block=1,xilinx_fir_compiler_5_0_block=7,xilinx_gateway_in_block=4,xilinx_gateway_out_block=6,xilinx_inverter_block=1,xilinx_logical_block_block=1,xilinx_register_block=5,xilinx_system_generator_block=1,xilinx_time_division_demultiplexer_block=1,xilinx_time_division_multiplexer_block=1,xilinx_type_converter_block=13,xilinx_up_sampler_block=1,}";

  signal ce_12_sg_x2: std_logic;
  signal ce_1_sg_x7: std_logic;
  signal ce_24_sg_x2: std_logic;
  signal ce_3_sg_x2: std_logic;
  signal ce_48_sg_x2: std_logic;
  signal ce_6_sg_x7: std_logic;
  signal ce_96_sg_x3: std_logic;
  signal ce_logic_12_sg_x2: std_logic;
  signal ce_logic_24_sg_x2: std_logic;
  signal ce_logic_48_sg_x2: std_logic;
  signal ce_logic_6_sg_x2: std_logic;
  signal clk_12_sg_x2: std_logic;
  signal clk_1_sg_x7: std_logic;
  signal clk_24_sg_x2: std_logic;
  signal clk_3_sg_x2: std_logic;
  signal clk_48_sg_x2: std_logic;
  signal clk_6_sg_x7: std_logic;
  signal clk_96_sg_x3: std_logic;
  signal dif_i_net: std_logic_vector(15 downto 0);
  signal dif_q_net: std_logic_vector(15 downto 0);
  signal din_i_net: std_logic_vector(15 downto 0);
  signal din_q_net: std_logic_vector(15 downto 0);
  signal dout_i_net: std_logic_vector(15 downto 0);
  signal dout_q_net: std_logic_vector(15 downto 0);
  signal freq_net: std_logic_vector(27 downto 0);
  signal freq_we_delay_q_net_x3: std_logic;
  signal vif_net: std_logic;
  signal vin_net: std_logic;
  signal vout_net: std_logic;

begin
  ce_1_sg_x7 <= ce_1;
  ce_12_sg_x2 <= ce_12;
  ce_24_sg_x2 <= ce_24;
  ce_3_sg_x2 <= ce_3;
  ce_48_sg_x2 <= ce_48;
  ce_6_sg_x7 <= ce_6;
  ce_96_sg_x3 <= ce_96;
  ce_logic_12_sg_x2 <= ce_logic_12;
  ce_logic_24_sg_x2 <= ce_logic_24;
  ce_logic_48_sg_x2 <= ce_logic_48;
  ce_logic_6_sg_x2 <= ce_logic_6;
  clk_1_sg_x7 <= clk_1;
  clk_12_sg_x2 <= clk_12;
  clk_24_sg_x2 <= clk_24;
  clk_3_sg_x2 <= clk_3;
  clk_48_sg_x2 <= clk_48;
  clk_6_sg_x7 <= clk_6;
  clk_96_sg_x3 <= clk_96;
  din_i_net <= din_i;
  din_q_net <= din_q;
  freq_net <= freq;
  vin_net <= vin;
  dif_i <= dif_i_net;
  dif_q <= dif_q_net;
  dout_i <= dout_i_net;
  dout_q <= dout_q_net;
  vif <= vif_net;
  vout <= vout_net;

  ddc_66861caa4b: entity work.ddc_entity_66861caa4b
    port map (
      ce_1 => ce_1_sg_x7,
      ce_6 => ce_6_sg_x7,
      clk_1 => clk_1_sg_x7,
      clk_6 => clk_6_sg_x7,
      din_i => dif_i_net,
      din_q => dif_q_net,
      freq => freq_net,
      freq_we => freq_we_delay_q_net_x3,
      vin => vif_net,
      dout_i => dout_i_net,
      dout_q => dout_q_net,
      vout => vout_net
    );

  duc_8eb6622f87: entity work.duc_entity_8eb6622f87
    port map (
      ce_1 => ce_1_sg_x7,
      ce_12 => ce_12_sg_x2,
      ce_24 => ce_24_sg_x2,
      ce_3 => ce_3_sg_x2,
      ce_48 => ce_48_sg_x2,
      ce_6 => ce_6_sg_x7,
      ce_96 => ce_96_sg_x3,
      ce_logic_12 => ce_logic_12_sg_x2,
      ce_logic_24 => ce_logic_24_sg_x2,
      ce_logic_48 => ce_logic_48_sg_x2,
      ce_logic_6 => ce_logic_6_sg_x2,
      clk_1 => clk_1_sg_x7,
      clk_12 => clk_12_sg_x2,
      clk_24 => clk_24_sg_x2,
      clk_3 => clk_3_sg_x2,
      clk_48 => clk_48_sg_x2,
      clk_6 => clk_6_sg_x7,
      clk_96 => clk_96_sg_x3,
      din_i => din_i_net,
      din_q => din_q_net,
      freq => freq_net,
      vin => vin_net,
      dout_i => dif_i_net,
      dout_q => dif_q_net,
      freq_we => freq_we_delay_q_net_x3,
      vout => vif_net
    );

end structural;
