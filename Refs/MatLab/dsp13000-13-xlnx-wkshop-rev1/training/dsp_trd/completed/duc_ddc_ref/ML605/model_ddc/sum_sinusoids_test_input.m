function yout = sum_sinusoids_test_input

Nfft = 1024;                  % FFT size for spectral plot
SymLength = 38400;

% DDC Input Generation
Fs = 61.44;
F0 = 7.1;
F1 = 12.2;
F2 = 17.3;

w0 = 2*pi*round(F0/Fs*2^24)/2^24;
w1 = 2*pi*round(F1/Fs*2^24)/2^24; 
w2 = 2*pi*round(F2/Fs*2^24)/2^24;

y0 = exp(j*w0*[1:SymLength]');
y1 = exp(j*w1*[1:SymLength]');
y2 = exp(j*w2*[1:SymLength]');

% Complex Mixing
yout = y0 + y1 + y2;
 
% Plot power spectral density of the DDC
figure;
[Pxx, f] = psd(yout, Nfft, 1, Nfft, Nfft/2);
Pxx = Pxx./max(Pxx);
plot((f-1/2)*Fs, 10*log10(fftshift(Pxx)),'b');
hold on; grid on; zoom on;
axis([-Fs/2, Fs/2, -140, 0]);
xlabel('Frequency (MHz)');
ylabel('Watts/Hz (dB)');
title('Power Spectral Density for Single Carrier UMTS');

% pick the real part as the input to DDC
yout = real(yout);