function [yout, y_ref, h_duc] = dynamic_range_test_input

Quantize = 1;                 % double (0) or quantized (1) coefficients and data 
Nfft = 1024;                  % FFT size for spectral plot
ShowPlots = 0;                % Show filter impluse response plots
coding_gain = 3.84e6/12.2e3;  % coding gain = chip rate/baseband data rate

% Generate input based on Test Model 1
u0 = wcdma_input_tm1; 

% Quantize input to 16 bits and keep the range between +/-0.5
u0_q = xquantize(u0, 16)/2^15;

% Carrier Frequency
Fc0 = 12;

% Sampling Frequency
Fs = 61.44;

% Normalize and quantize the carrier frequency to 24 bits
w0 = 2*pi*round(Fc0/Fs*2^24)/2^24;

% Filter Coefficients and Interpolation filtering
[h1_duc, h2_duc, h3_duc, h4_duc, h_duc] = umts_duc_filter_conf1(Quantize, Nfft, ShowPlots);
y1 = upfirdn(u0_q, h1_duc, 2, 1);   
y2 = upfirdn(y1, h2_duc, 2, 1);
y3 = upfirdn(y2, h3_duc, 2, 1);
y0 = upfirdn(y3, h4_duc, 2, 1);
y0 = y0/std(y0) * sqrt(coding_gain) * 10.^(-91/20);

% Adding noise
n = randn([length(y0), 1]);
n = n/std(n)* 10.^(-73/20);

% Complex Mixing
yout = y0 .* exp(j*w0*[1:length(y0)]') + n;

% Create reference signal
y_ref = real(y0 .* exp(j*w0*[1:length(y0)]')) .* exp(-j*w0*[1:length(y0)]');

% Plot power spectral density of the DDC
figure;
[Pxx, f] = psd(yout, Nfft, 1, Nfft, Nfft/2);
Pxx = Pxx./max(Pxx);
plot((f-1/2)*Fs, 10*log10(fftshift(Pxx)),'b');
hold on; grid on; zoom on;
axis([-Fs/2, Fs/2, -120, 0]);
xlabel('Frequency (MHz)');
ylabel('Watts/Hz (dB)');
title('Power Spectral Density for Single Carrier UMTS');

% pick the real part as the input to DDC
yout = real(yout);