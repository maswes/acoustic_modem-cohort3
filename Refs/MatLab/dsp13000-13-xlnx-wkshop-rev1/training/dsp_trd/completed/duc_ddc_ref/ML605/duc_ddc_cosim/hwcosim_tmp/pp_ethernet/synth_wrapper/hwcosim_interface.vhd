------------------------------------------------------------------------------
-- System Generator version 12.3 VHDL source file.
--
-- Copyright(C) 2010 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2010 Xilinx, Inc.  All rights
-- reserved.
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity hwcosim_shared_memory_lock_manager is
  port (
    clk : in  std_logic;
    hw_req : in  std_logic;
    hw_grant : out std_logic;
    sw_req : out std_logic;
    sw_grant : in  std_logic
  );
end hwcosim_shared_memory_lock_manager;

architecture rtl of hwcosim_shared_memory_lock_manager is

  signal hw_req_int : std_logic;
  signal hr_reg1 : std_logic;
  signal hr_reg1_en : std_logic;
  signal hr_reg2 : std_logic;
  signal sw_req_int : std_logic;
  signal sw_grant_int : std_logic;

begin
  sw_grant_int <= sw_grant;
  sw_req <= sw_req_int;
  hr_reg1_en <= not hr_reg2;

  -- Increase # pulses for hw_req signal
  process (clk, hw_req)
  begin
    if hw_req = '0' then
      hr_reg1 <= '0';
    elsif rising_edge(clk) then
      if hr_reg1_en = '1' then
        hr_reg1 <= '1';
      end if;
    end if;
  end process;

  process (clk)
  begin
    if rising_edge(clk) then
      hr_reg2 <= hr_reg1;
    end if;
  end process;

  hw_req_int <= hw_req and hr_reg1;

  -- Generate sw_req signal
  process (clk, hw_req_int)
  begin
    if hw_req_int = '0' then
      sw_req_int <= '0';
    elsif rising_edge(clk) then
      if sw_grant_int = '0' then
        sw_req_int <= '1';
      end if;
    end if;
  end process;

  -- Generate hw_grant signal
  process (clk, hw_req_int)
  begin
    if hw_req_int = '0' then
      hw_grant <= '0';
    elsif rising_edge(clk) then
      if sw_req_int = '1' and sw_grant_int = '1' then
        hw_grant <= '1';
      end if;
    end if;
  end process;
end rtl;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity hwcosim_shared_register is
  generic (
    WIDTH : integer := 32;
    INIT_VALUE : bit_vector := "00000000000000000000000000000000"
  );
  port (
    clk : in  std_logic;
    ce : in  std_logic;
    clr : in  std_logic;
    i : in  std_logic_vector(WIDTH - 1 downto 0);
    o: out std_logic_vector(WIDTH - 1 downto 0)
  );
end hwcosim_shared_register;

architecture structural of hwcosim_shared_register is
begin
  fd_prim_array: for index in 0 to WIDTH - 1 generate
    bit_is_0: if INIT_VALUE(index) = '0' generate
      fdre_comp: FDRE
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          r => clr
        );
    end generate;

    bit_is_1: if INIT_VALUE(index) = '1' generate
      fdse_comp: FDSE
        port map (
          c => clk,
          d => i(index),
          q => o(index),
          ce => ce,
          s => clr
        );
    end generate;
  end generate;
end structural;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hwcosim_memory_map is
  port (
    mm_o_din_i : out std_logic_vector(15 downto 0);
    mm_o_din_q : out std_logic_vector(15 downto 0);
    mm_o_freq : out std_logic_vector(27 downto 0);
    mm_o_vin : out std_logic;
    mm_i_dif_i : in  std_logic_vector(15 downto 0);
    mm_i_dif_q : in  std_logic_vector(15 downto 0);
    mm_i_dout_i : in  std_logic_vector(15 downto 0);
    mm_i_dout_q : in  std_logic_vector(15 downto 0);
    mm_i_vif : in  std_logic;
    mm_i_vout : in  std_logic;
    hwcosim_mm_clk : in  std_logic;
    hwcosim_mm_we : in  std_logic;
    hwcosim_mm_re : in  std_logic;
    hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
    hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
    hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
    hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
  );
end hwcosim_memory_map;

architecture behavioral of hwcosim_memory_map is

  function resize_vector(x : std_logic; size : integer) return std_logic_vector is
    variable result : std_logic_vector(size-1 downto 0) := (others => '0');
  begin
    result(0) := x;
    return result;
  end resize_vector;

  function resize_vector(x : std_logic_vector; size : integer) return std_logic_vector is
    variable temp : std_logic_vector(x'length-1 downto 0) := x;
    variable result : std_logic_vector(size-1 downto 0) := (others => '0');
  begin
    if temp'length < result'length then
      result(temp'length-1 downto 0) := temp;
    else
      result := temp(result'length-1 downto 0);
    end if;
    return result;
  end resize_vector;

  signal hwcosim_mm_data_out_bank0 : std_logic_vector(31 downto 0);

  signal int_o_din_i : std_logic_vector(15 downto 0);
  signal int_o_din_q : std_logic_vector(15 downto 0);
  signal int_o_freq : std_logic_vector(27 downto 0);
  signal int_o_vin : std_logic;
  signal int_i_dif_i : std_logic_vector(15 downto 0);
  signal int_i_dif_q : std_logic_vector(15 downto 0);
  signal int_i_dout_i : std_logic_vector(15 downto 0);
  signal int_i_dout_q : std_logic_vector(15 downto 0);
  signal int_i_vif : std_logic;
  signal int_i_vout : std_logic;

begin

  process (hwcosim_mm_clk)
  begin
    if rising_edge(hwcosim_mm_clk) then
      if (hwcosim_mm_we = '1') and (unsigned(hwcosim_mm_bank_sel) = 0) then
        case to_integer(unsigned(hwcosim_mm_addr)) is
          when 0 => int_o_din_i(15 downto 0) <= hwcosim_mm_data_in(15 downto 0);
          when 1 => int_o_din_q(15 downto 0) <= hwcosim_mm_data_in(15 downto 0);
          when 2 => int_o_freq(27 downto 0) <= hwcosim_mm_data_in(27 downto 0);
          when 3 => int_o_vin <= hwcosim_mm_data_in(0);
          when others => null;
        end case;
      else
        case to_integer(unsigned(hwcosim_mm_addr)) is
          when 0 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_dif_i(15 downto 0), hwcosim_mm_data_out_bank0'length);
          when 1 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_dif_q(15 downto 0), hwcosim_mm_data_out_bank0'length);
          when 2 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_dout_i(15 downto 0), hwcosim_mm_data_out_bank0'length);
          when 3 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_dout_q(15 downto 0), hwcosim_mm_data_out_bank0'length);
          when 4 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_vif, hwcosim_mm_data_out_bank0'length);
          when 5 => hwcosim_mm_data_out_bank0 <= resize_vector(int_i_vout, hwcosim_mm_data_out_bank0'length);
          when others => null;
        end case;
      end if;
    end if;
  end process;

  int_i_dif_i <= mm_i_dif_i;
  int_i_dif_q <= mm_i_dif_q;
  int_i_dout_i <= mm_i_dout_i;
  int_i_dout_q <= mm_i_dout_q;
  int_i_vif <= mm_i_vif;
  int_i_vout <= mm_i_vout;
  mm_o_din_i <= int_o_din_i;
  mm_o_din_q <= int_o_din_q;
  mm_o_freq <= int_o_freq;
  mm_o_vin <= int_o_vin;

  process (hwcosim_mm_bank_sel,
           hwcosim_mm_data_out_bank0)
  begin
    case to_integer(unsigned(hwcosim_mm_bank_sel)) is
      when 0 => hwcosim_mm_data_out <= hwcosim_mm_data_out_bank0;
      when others => hwcosim_mm_data_out <= std_logic_vector(to_unsigned(0, hwcosim_mm_data_out'length));
    end case;
  end process;

end behavioral;

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hwcosim_interface is
  port (
    hwcosim_sys_clk : in  std_logic;
    hwcosim_dut_fr_clk : in  std_logic;
    hwcosim_dut_ss_clk : in  std_logic;
    hwcosim_mm_we : in  std_logic;
    hwcosim_mm_re : in  std_logic;
    hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
    hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
    hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
    hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
  );
end hwcosim_interface;

architecture rtl of hwcosim_interface is
  component hwcosim_shared_memory_lock_manager is
    port (
      clk : in  std_logic;
      hw_req : in  std_logic;
      hw_grant : out std_logic;
      sw_req : out std_logic;
      sw_grant : in  std_logic
    );
  end component;

  component hwcosim_shared_register is
    generic (
      WIDTH : integer := 32;
      INIT_VALUE : bit_vector := "00000000000000000000000000000000"
    );
    port (
      clk : in  std_logic;
      ce : in  std_logic;
      clr : in  std_logic;
      i : in  std_logic_vector(WIDTH - 1 downto 0);
      o: out std_logic_vector(WIDTH - 1 downto 0)
    );
  end component;

  component hwcosim_memory_map is
    port (
      mm_o_din_i : out std_logic_vector(15 downto 0);
      mm_o_din_q : out std_logic_vector(15 downto 0);
      mm_o_freq : out std_logic_vector(27 downto 0);
      mm_o_vin : out std_logic;
      mm_i_dif_i : in  std_logic_vector(15 downto 0);
      mm_i_dif_q : in  std_logic_vector(15 downto 0);
      mm_i_dout_i : in  std_logic_vector(15 downto 0);
      mm_i_dout_q : in  std_logic_vector(15 downto 0);
      mm_i_vif : in  std_logic;
      mm_i_vout : in  std_logic;
      hwcosim_mm_clk : in  std_logic;
      hwcosim_mm_we : in  std_logic;
      hwcosim_mm_re : in  std_logic;
      hwcosim_mm_bank_sel : in  std_logic_vector(7 downto 0);
      hwcosim_mm_addr : in  std_logic_vector(23 downto 0);
      hwcosim_mm_data_in : in  std_logic_vector(31 downto 0);
      hwcosim_mm_data_out : out std_logic_vector(31 downto 0)
    );
  end component;

  component duc_ddc_umts_virtex6_cw is
    port (
      ce : in std_logic;
      clk : in std_logic;
      dif_i : out std_logic_vector(15 downto 0);
      dif_q : out std_logic_vector(15 downto 0);
      din_i : in std_logic_vector(15 downto 0);
      din_q : in std_logic_vector(15 downto 0);
      dout_i : out std_logic_vector(15 downto 0);
      dout_q : out std_logic_vector(15 downto 0);
      freq : in std_logic_vector(27 downto 0);
      vif : out std_logic;
      vin : in std_logic;
      vout : out std_logic
    );
  end component;

  attribute box_type : string;
  attribute box_type of duc_ddc_umts_virtex6_cw : component is "user_black_box";
  attribute syn_black_box : boolean;
  attribute syn_black_box of duc_ddc_umts_virtex6_cw : component is true;

  signal mm_o_din_i : std_logic_vector(15 downto 0);
  signal mm_o_din_q : std_logic_vector(15 downto 0);
  signal mm_o_freq : std_logic_vector(27 downto 0);
  signal mm_o_vin : std_logic;
  signal mm_i_dif_i : std_logic_vector(15 downto 0);
  signal mm_i_dif_q : std_logic_vector(15 downto 0);
  signal mm_i_dout_i : std_logic_vector(15 downto 0);
  signal mm_i_dout_q : std_logic_vector(15 downto 0);
  signal mm_i_vif : std_logic;
  signal mm_i_vout : std_logic;

  signal dut_o_ce : std_logic;
  signal dut_o_clk : std_logic;
  signal dut_o_din_i : std_logic_vector(15 downto 0);
  signal dut_o_din_q : std_logic_vector(15 downto 0);
  signal dut_o_freq : std_logic_vector(27 downto 0);
  signal dut_o_vin : std_logic;
  signal dut_i_dif_i : std_logic_vector(15 downto 0);
  signal dut_i_dif_q : std_logic_vector(15 downto 0);
  signal dut_i_dout_i : std_logic_vector(15 downto 0);
  signal dut_i_dout_q : std_logic_vector(15 downto 0);
  signal dut_i_vif : std_logic;
  signal dut_i_vout : std_logic;


begin

  hwcosim_memory_map_inst : hwcosim_memory_map
    port map (
      mm_o_din_i => mm_o_din_i,
      mm_o_din_q => mm_o_din_q,
      mm_o_freq => mm_o_freq,
      mm_o_vin => mm_o_vin,
      mm_i_dif_i => mm_i_dif_i,
      mm_i_dif_q => mm_i_dif_q,
      mm_i_dout_i => mm_i_dout_i,
      mm_i_dout_q => mm_i_dout_q,
      mm_i_vif => mm_i_vif,
      mm_i_vout => mm_i_vout,
      hwcosim_mm_clk => hwcosim_dut_fr_clk,
      hwcosim_mm_we => hwcosim_mm_we,
      hwcosim_mm_re => hwcosim_mm_re,
      hwcosim_mm_bank_sel => hwcosim_mm_bank_sel,
      hwcosim_mm_addr => hwcosim_mm_addr,
      hwcosim_mm_data_in => hwcosim_mm_data_in,
      hwcosim_mm_data_out => hwcosim_mm_data_out
    );

  hwcosim_dut_inst : duc_ddc_umts_virtex6_cw
    port map (
      ce => dut_o_ce,
      clk => hwcosim_dut_ss_clk,
      dif_i => dut_i_dif_i,
      dif_q => dut_i_dif_q,
      din_i => dut_o_din_i,
      din_q => dut_o_din_q,
      dout_i => dut_i_dout_i,
      dout_q => dut_i_dout_q,
      freq => dut_o_freq,
      vif => dut_i_vif,
      vin => dut_o_vin,
      vout => dut_i_vout
    );

  dut_o_ce <= '1';
  dut_o_din_i <= mm_o_din_i;
  dut_o_din_q <= mm_o_din_q;
  dut_o_freq <= mm_o_freq;
  dut_o_vin <= mm_o_vin;
  mm_i_dif_i <= dut_i_dif_i;
  mm_i_dif_q <= dut_i_dif_q;
  mm_i_dout_i <= dut_i_dout_i;
  mm_i_dout_q <= dut_i_dout_q;
  mm_i_vif <= dut_i_vif;
  mm_i_vout <= dut_i_vout;


end rtl;
