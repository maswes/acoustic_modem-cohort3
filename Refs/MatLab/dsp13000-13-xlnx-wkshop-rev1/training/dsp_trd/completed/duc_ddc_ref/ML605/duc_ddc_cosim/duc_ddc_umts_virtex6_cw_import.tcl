#
# Created by System Generator     Wed Mar 16 22:21:16 2011
#
# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator.
#

source SgIseProject.tcl

namespace eval ::xilinx::dsptool::iseproject::param {

    set Project {duc_ddc_umts_virtex6_cw}
    set Family {Virtex6}
    set Device {xc6vlx240t}
    set Package {ff1156}
    set Speed {-1}
    set HDLLanguage {vhdl}
    set SynthesisTool {XST}
    set Simulator {Modelsim-SE}
    set ReadCores {False}
    set MapEffortLevel {High}
    set ParEffortLevel {High}
    set Frequency {50}
    set ProjectFiles {
        {{duc_ddc_umts_virtex6_cw.vhd} -view All}
        {{duc_ddc_umts_virtex6.vhd} -view All}
        {{duc_ddc_umts_virtex6_cw.ucf}}
        {{fir_compiler_v5_0.mif}}
        {{fr_cmplr_v5_0_059cf50081fc492c.mif}}
        {{fr_cmplr_v5_0_059cf50081fc492cCOEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_059cf50081fc492cfilt_decode_rom.mif}}
        {{fr_cmplr_v5_0_1877c081b7e2f892.mif}}
        {{fr_cmplr_v5_0_1877c081b7e2f892COEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_1877c081b7e2f892COEFF_auto_HALFBAND_CENTRE0.mif}}
        {{fr_cmplr_v5_0_1877c081b7e2f892filt_decode_rom.mif}}
        {{fr_cmplr_v5_0_51d88ed999503974.mif}}
        {{fr_cmplr_v5_0_51d88ed999503974COEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_51d88ed999503974COEFF_auto_HALFBAND_CENTRE0.mif}}
        {{fr_cmplr_v5_0_51d88ed999503974filt_decode_rom.mif}}
        {{fr_cmplr_v5_0_670158bb462abb3d.mif}}
        {{fr_cmplr_v5_0_670158bb462abb3dCOEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_670158bb462abb3dCOEFF_auto_HALFBAND_CENTRE0.mif}}
        {{fr_cmplr_v5_0_670158bb462abb3dfilt_decode_rom.mif}}
        {{fr_cmplr_v5_0_cd9071975244390f.mif}}
        {{fr_cmplr_v5_0_cd9071975244390fCOEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_cd9071975244390ffilt_decode_rom.mif}}
        {{fr_cmplr_v5_0_e37f692facadc69b.mif}}
        {{fr_cmplr_v5_0_e37f692facadc69bCOEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_e37f692facadc69bCOEFF_auto_HALFBAND_CENTRE0.mif}}
        {{fr_cmplr_v5_0_e37f692facadc69bfilt_decode_rom.mif}}
        {{fr_cmplr_v5_0_f5119409b2c69958.mif}}
        {{fr_cmplr_v5_0_f5119409b2c69958COEFF_auto0_0.mif}}
        {{fr_cmplr_v5_0_f5119409b2c69958COEFF_auto_HALFBAND_CENTRE0.mif}}
        {{fr_cmplr_v5_0_f5119409b2c69958filt_decode_rom.mif}}
        {{c:\training\dsp_trd\labs\duc_ddc_ref\ML605\duc_ddc_umts_virtex6.mdl}}
    }
    set TopLevelModule {duc_ddc_umts_virtex6_cw}
    set SynthesisConstraintsFile {duc_ddc_umts_virtex6_cw.xcf}
    set ImplementationStopView {Structural}
    set ProjectGenerator {SysgenDSP}
}
::xilinx::dsptool::iseproject::create
