function Y = wcdma_input_tm1
%% 3GPP TS25.141 Test Model 1
%% DPCH Spreading Code, Timing offsets, and Level Settings
%% 64 DPCHs at 30 ksps (SF=128) distributed randomly across the code space, 
%% at random power levels, and random timing offsets are defined in this
%% test model so as to simulate a realistic traffice scenario which may have
%% high PAR (Peak to Average Ratio)

% for simplification, data from the pilot channels are generated using gaussin random variables
% P-CCPCH+SCH
NumSym = 38400;   
level_dB = -10;
level = 10.^(level_dB/20);    % Translate level value from dB scale                           
X_pccpch = level * complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data  

% Primary CPICH
level_dB = -10;
level = 10.^(level_dB/20);    % Translate level value from dB scale                         
X_cpich = level * complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data  

% PICH
level_dB = -18;
offset = 120;
level = 10.^(level_dB/20);    % Translate level value from dB scale                       
X_pich = level * complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data  
X_pich = [X_pich(offset+1:end), zeros([1, offset])];

% S-CCPCH
level_dB = -18;
level = 10.^(level_dB/20);   % Translate level value from dB scale                
X_sccpch = level * complex(sign(randn([1, NumSym])), sign(randn([1, NumSym])));   % QPSK Data  

% 64 DPCHs
code = [2,11,17,23,31,38,47,55,62,69,78,85,94,102,113,119,7,13,20,27,35,...
        41,51,58,64,74,82,88,97,108,117,125,4,9,12,14,19,22,26,28,34,36,40,44,...
        49,53,56,61,63,66,71,76,80,84,87,91,95,99,105,110,116,118,122,126];

offset = [86,134,52,45,143,112,59,23,1,88,30,18,30,61,128,143,83,25,103,97,...
          56,104,51,26,137,65,37,125,149,123,83,5,91,7,32,21,29,59,22,138,31,17,9,...
          69,49,20,57,121,127,114,100,76,141,82,64,149,87,98,46,37,87,149,85,69];

level_dB = [-16,-16,-16,-17,-18,-20,-16,-17,-16,-19,-22,-20,-16,-17,-19,-21,...
            -19,-21,-18,-20,-24,-24,-22,-21,-18,-20,-17,-18,-19,-23,-22,-21,...
            -17,-18,-20,-17,-19,-21,-19,-23,-22,-19,-24,-23,-22,-19,-22,-21,...
            -18,-19,-22,-21,-19,-21,-19,-21,-20,-25,-25,-25,-24,-22,-20,-15];
  
level = 10.^(level_dB/20);   % Translate level value from dB scale                
NumSym = 38400/128;          % Number of DPCH Symbols in each channel
                             % for 1 frame worth of data (38400 chips)          
SF = hadamard(128);          % Spreading Factor
Sym = complex(sign(randn([64, NumSym])), sign(randn([64, NumSym])));   % QPSK Data

Xi = zeros([64, 128*NumSym + max(offset)]);
Xq = zeros([64, 128*NumSym + max(offset)]);

for k = 1:64
    for m = 1: NumSym
        Xi(k, offset(k)+(1:128)+128*(m-1)) = real(Sym(k, m))*SF(code(k),:)*level(k);
        Xq(k, offset(k)+(1:128)+128*(m-1)) = imag(Sym(k, m))*SF(code(k),:)*level(k);
    end
end    

Xi = Xi(:, 1:128*NumSym);
Xq = Xq(:, 1:128*NumSym);

% Sum up all the DPCHs
Y = sum(Xi) + sqrt(-1)*sum(Xq);

% Sum up all the pilot channel with appropriate power level
Y = X_pccpch + X_cpich + X_pich + X_sccpch + Y;

Y = Y/std(Y);

 








