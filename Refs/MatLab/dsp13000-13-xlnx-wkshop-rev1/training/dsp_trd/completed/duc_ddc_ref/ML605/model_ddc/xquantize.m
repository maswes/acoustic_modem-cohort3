function y = quantize(x, nbits, rail)

%Useage: y = quantize(x, nbits, rail);
%
%y:		Quantized output sequence
%x:		Full precision input sequence
%rail:	Quantizer full-scale input value
%nbits:	Number of quantization bits
%
%Function that quantizes an input signal to the specified number of bits.
%This version assumes the signal is a signed two's complement number.

if (nargin < 3),
   rail = max(abs(x(:)));
end

maxpos = 2^(nbits-1) - 1;
xnorm = x(:)/rail;
xnorm(find(xnorm >  1)) =  1;
xnorm(find(xnorm < -1)) = -1;

y = round(xnorm * maxpos);
