{
  'Design' => {
    'ConstraintsFile' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/sysgen_hwcosim.ucf',
    'EntityName' => 'duc_ddc_umts_virtex6_cw',
    'HDLFileExtension' => 'vhd',
    'HDLFileList' => [
      {
        'Library' => 'work',
        'Path' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/duc_ddc_umts_virtex6.vhd',
        'Type' => 'vhdl'
      },
      {
        'Library' => 'work',
        'Path' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/duc_ddc_umts_virtex6_cw.vhd',
        'Type' => 'vhdl'
      }
    ],
    'HDLLanguage' => 'vhdl',
    'InstanceName' => 'sysgen_dut',
    'PeripheralList' => [

    ],
    'PortList' => [
      {
        'ConnectTo' => 1,
        'Direction' => 'in',
        'DisplayName' => 'ce',
        'IsMemoryMapped' => false,
        'Name' => 'ce',
        'SamplePeriod' => 1,
        'Type' => 'std_logic'
      },
      {
        'Clock' => {
          'Period' => 20.00000000000
        },
        'DataType' => 'logic',
        'Direction' => 'in',
        'DisplayName' => 'clk',
        'IsClock' => true,
        'Name' => 'clk',
        'SamplePeriod' => 1,
        'Type' => 'std_logic'
      },
      {
        'DataType' => 'Fix_16_15',
        'Direction' => 'out',
        'DisplayName' => 'DIF_I',
        'Name' => 'dif_i',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 6,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Fix_16_15',
        'Direction' => 'out',
        'DisplayName' => 'DIF_Q',
        'Name' => 'dif_q',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 6,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Fix_16_15',
        'Direction' => 'in',
        'DisplayName' => 'DIN_I',
        'Name' => 'din_i',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 96,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Fix_16_15',
        'Direction' => 'in',
        'DisplayName' => 'DIN_Q',
        'Name' => 'din_q',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 96,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Fix_16_12',
        'Direction' => 'out',
        'DisplayName' => 'DOUT_I',
        'Name' => 'dout_i',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 1,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Fix_16_12',
        'Direction' => 'out',
        'DisplayName' => 'DOUT_Q',
        'Name' => 'dout_q',
        'Range' => {
          'Left' => '15',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 1,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'UFix_28_28',
        'Direction' => 'in',
        'DisplayName' => 'FREQ',
        'Name' => 'freq',
        'Range' => {
          'Left' => '27',
          'Order' => 'downto',
          'Right' => '0'
        },
        'SamplePeriod' => 6,
        'Type' => 'std_logic_vector'
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'VIF',
        'Name' => 'vif',
        'SamplePeriod' => 1,
        'Type' => 'std_logic'
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'in',
        'DisplayName' => 'VIN',
        'Name' => 'vin',
        'SamplePeriod' => 6,
        'Type' => 'std_logic'
      },
      {
        'DataType' => 'Bool',
        'Direction' => 'out',
        'DisplayName' => 'VOUT',
        'Name' => 'vout',
        'SamplePeriod' => 1,
        'Type' => 'std_logic'
      }
    ],
    'ProjectFile' => 'c:/training/dsp_trd/labs/duc_ddc_ref/ML605/duc_ddc_cosim/xst_duc_ddc_umts_virtex6.prj',
    'SharedFIFOList' => [

    ],
    'SharedRAMList' => [

    ],
    'SharedRegisterList' => [

    ],
    'SynplifySynthesisConstraints' => [
      'define_attribute {clk} syn_maxfan {1000000}',
      'define_attribute {n:default_clock_driver.xlclockdriver_12.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_12.ce_vec*} max_fanout {"REDUCE"}',
      'define_attribute {n:default_clock_driver.xlclockdriver_24.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_24.ce_vec*} max_fanout {"REDUCE"}',
      'define_attribute {n:default_clock_driver.xlclockdriver_3.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_3.ce_vec*} max_fanout {"REDUCE"}',
      'define_attribute {n:default_clock_driver.xlclockdriver_48.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_48.ce_vec*} max_fanout {"REDUCE"}',
      'define_attribute {n:default_clock_driver.xlclockdriver_6.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_6.ce_vec*} max_fanout {"REDUCE"}',
      'define_attribute {n:default_clock_driver.xlclockdriver_96.ce_vec*} syn_keep {true}',
      'define_attribute {n:default_clock_driver.xlclockdriver_96.ce_vec*} max_fanout {"REDUCE"}',
      'define_scope_collection ce_12_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_12_sg_x2} ]}',
      'define_scope_collection ce_24_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_24_sg_x2} ]}',
      'define_scope_collection ce_3_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_3_sg_x2} ]}',
      'define_scope_collection ce_48_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_48_sg_x2} ]}',
      'define_scope_collection ce_6_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_6_sg_x7} ]}',
      'define_scope_collection ce_96_49d24bf1_group \\',
      '{find -seq * -in [ expand -hier -from {n:ce_96_sg_x3} ]}',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 24',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 48',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 96',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_12_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 24',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_24_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 24',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_3_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 24',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_48_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 48',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_6_49d24bf1_group} \\',
      '-to {$ce_96_49d24bf1_group} 6',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_12_49d24bf1_group} 12',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_24_49d24bf1_group} 24',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_3_49d24bf1_group} 3',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_48_49d24bf1_group} 48',
      'define_multicycle_path -from {$ce_96_49d24bf1_group} \\',
      '-to {$ce_6_49d24bf1_group} 6'
    ],
    'SynthesisTool' => 'xst',
    'SynthesisToolExecutable' => 'xst',
    'UsesSynplify' => false,
    'UsesXST' => true,
    'XSTSynthesisConstraints' => [
      'NET "clk" TNM_NET = "clk_49d24bf1";',
      'TIMESPEC "TS_clk_49d24bf1" = PERIOD "clk_49d24bf1" 20.0 ns HIGH 50 %;',
      'NET "ce_12_sg_x2*" TNM_NET = "ce_12_49d24bf1_group";',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_12_49d24bf1_group" 240.0 ns;',
      'NET "ce_24_sg_x2*" TNM_NET = "ce_24_49d24bf1_group";',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_24_49d24bf1_group" 480.0 ns;',
      'NET "ce_3_sg_x2*" TNM_NET = "ce_3_49d24bf1_group";',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'NET "ce_48_sg_x2*" TNM_NET = "ce_48_49d24bf1_group";',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_48_49d24bf1_group" 960.0 ns;',
      'NET "ce_6_sg_x7*" TNM_NET = "ce_6_49d24bf1_group";',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_6_49d24bf1_group" 120.0 ns;',
      'NET "ce_96_sg_x3*" TNM_NET = "ce_96_49d24bf1_group";',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_96_49d24bf1_group" 1.92 us;',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_24_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_48_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_6_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_12_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_12_49d24bf1_group" TO "ce_96_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_12_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_48_49d24bf1_group" 480.0 ns;',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_6_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_24_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_24_49d24bf1_group" TO "ce_96_49d24bf1_group" 480.0 ns;',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_12_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_24_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_48_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_6_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_3_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_3_49d24bf1_group" TO "ce_96_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_12_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_24_49d24bf1_group" 480.0 ns;',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_6_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_48_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_48_49d24bf1_group" TO "ce_96_49d24bf1_group" 960.0 ns;',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_12_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_24_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_48_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_6_49d24bf1_group_to_ce_96_49d24bf1_group" = FROM "ce_6_49d24bf1_group" TO "ce_96_49d24bf1_group" 120.0 ns;',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_12_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_12_49d24bf1_group" 240.0 ns;',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_24_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_24_49d24bf1_group" 480.0 ns;',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_3_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_3_49d24bf1_group" 60.0 ns;',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_48_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_48_49d24bf1_group" 960.0 ns;',
      'TIMESPEC "TS_ce_96_49d24bf1_group_to_ce_6_49d24bf1_group" = FROM "ce_96_49d24bf1_group" TO "ce_6_49d24bf1_group" 120.0 ns;'
    ]
  },
  'Flow' => 'sysgen',
  'Incremental' => '',
  'Platform' => {
    'Board' => 'ml605',
    'BoundaryScanPosition' => 2,
    'Clock' => [
      {
        'Differential' => true,
        'Period' => 5,
        'Pin' => {
          'Negative' => 'H9',
          'Positive' => 'J9'
        },
        'VariablePeriods' => [
          10,
          15,
          20,
          30
        ]
      }
    ],
    'CosimCore' => {
      'Constraints' => [
        'INST "ethernet_phy_rxd<?>"    TNM = "gmii_rx";',
        'INST "ethernet_phy_rx_dv"     TNM = "gmii_rx";',
        'INST "ethernet_phy_rx_er"     TNM = "gmii_rx";',
        'NET  "*emac_speed_is_10_100"  TIG;',
        'TIMEGRP "gmii_rx" OFFSET = IN 2 ns VALID 2.75 ns BEFORE "ethernet_phy_rx_clk" RISING;'
      ],
      'Interface' => {
        'Clock' => [
          {
            'Clock' => {
              'Period' => 8
            },
            'Direction' => 'in',
            'Name' => 'clk_125',
            'Width' => 1
          },
          {
            'Clock' => {
              'Period' => 5
            },
            'Direction' => 'in',
            'Name' => 'clk_200',
            'Width' => 1
          }
        ],
        'Ethernet' => [
          {
            'Constraints' => [
              'NET  "ethernet_phy_rst_n"     LOC = AH13;',
              'NET  "ethernet_phy_rst_n"     TIG;'
            ],
            'Direction' => 'out',
            'ExternalName' => 'ethernet_phy_rst_n',
            'Invert' => true,
            'Name' => 'ethernet_phy_rst',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_intr_n"    LOC = AH14;',
              'NET  "ethernet_phy_intr_n"    PULLUP;',
              'NET  "ethernet_phy_intr_n"    TIG;'
            ],
            'Direction' => 'in',
            'ExternalName' => 'ethernet_phy_intr_n',
            'Invert' => true,
            'Name' => 'ethernet_phy_intr',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_gtx_clk"   LOC = AH12;',
              'NET  "ethernet_phy_gtx_clk"   SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_gtx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_clk"    LOC = AD12;',
              'NET  "ethernet_phy_tx_clk"    CLOCK_DEDICATED_ROUTE = FALSE;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_tx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_txd(0)"    LOC = AM11;',
              'NET  "ethernet_phy_txd(0)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(1)"    LOC = AL11;',
              'NET  "ethernet_phy_txd(1)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(2)"    LOC = AG10;',
              'NET  "ethernet_phy_txd(2)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(3)"    LOC = AG11;',
              'NET  "ethernet_phy_txd(3)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(4)"    LOC = AL10;',
              'NET  "ethernet_phy_txd(4)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(5)"    LOC = AM10;',
              'NET  "ethernet_phy_txd(5)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(6)"    LOC = AE11;',
              'NET  "ethernet_phy_txd(6)"    SLEW = FAST;',
              'NET  "ethernet_phy_txd(7)"    LOC = AF11;',
              'NET  "ethernet_phy_txd(7)"    SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_txd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_en"     LOC = AJ10;',
              'NET  "ethernet_phy_tx_en"     SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_tx_en',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_tx_er"     LOC = AH10;',
              'NET  "ethernet_phy_tx_er"     SLEW = FAST;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_tx_er',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_clk"    LOC = AP11;'
            ],
            'Direction' => 'in',
            'IOType' => 'ibufg',
            'Name' => 'ethernet_phy_rx_clk',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rxd(0)"    LOC = AN13;',
              'NET  "ethernet_phy_rxd(1)"    LOC = AF14;',
              'NET  "ethernet_phy_rxd(2)"    LOC = AE14;',
              'NET  "ethernet_phy_rxd(3)"    LOC = AN12;',
              'NET  "ethernet_phy_rxd(4)"    LOC = AM12;',
              'NET  "ethernet_phy_rxd(5)"    LOC = AD11;',
              'NET  "ethernet_phy_rxd(6)"    LOC = AC12;',
              'NET  "ethernet_phy_rxd(7)"    LOC = AC13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rxd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_dv"     LOC = AM13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rx_dv',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_rx_er"     LOC = AG12;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_rx_er',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_col"       LOC = AK13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_col',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_crs"       LOC = AL13;'
            ],
            'Direction' => 'in',
            'Name' => 'ethernet_phy_crs',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_mdc"       LOC = AP14;'
            ],
            'Direction' => 'out',
            'Name' => 'ethernet_phy_mdc',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "ethernet_phy_mdio"      LOC = AN14;'
            ],
            'Direction' => 'inout',
            'Name' => 'ethernet_phy_mdio',
            'Width' => 1
          }
        ],
        'SystemACE' => [
          {
            'Constraints' => [
              'NET  "sysace_clk"             TNM_NET = "T_sysace_clk";',
              'TIMESPEC "TS_sysace_clk"      = PERIOD "T_sysace_clk" 30 ns;',
              'NET  "sysace_clk"             LOC = AE16;'
            ],
            'Direction' => 'in',
            'Name' => 'sysace_clk',
            'Type' => 'bufgp',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_mpa(0)"          LOC = AC15;',
              'NET  "sysace_mpa(1)"          LOC = AP15;',
              'NET  "sysace_mpa(2)"          LOC = AG17;',
              'NET  "sysace_mpa(3)"          LOC = AH17;',
              'NET  "sysace_mpa(4)"          LOC = AG15;',
              'NET  "sysace_mpa(5)"          LOC = AK14;',
              'NET  "sysace_mpa(6)"          LOC = AJ15;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_mpa',
            'Width' => 7
          },
          {
            'Constraints' => [
              'NET  "sysace_mpd(0)"          LOC = AM15;',
              'NET  "sysace_mpd(1)"          LOC = AJ17;',
              'NET  "sysace_mpd(2)"          LOC = AJ16;',
              'NET  "sysace_mpd(3)"          LOC = AP16;',
              'NET  "sysace_mpd(4)"          LOC = AG16;',
              'NET  "sysace_mpd(5)"          LOC = AH15;',
              'NET  "sysace_mpd(6)"          LOC = AF16;',
              'NET  "sysace_mpd(7)"          LOC = AN15;'
            ],
            'Direction' => 'inout',
            'Name' => 'sysace_mpd',
            'Width' => 8
          },
          {
            'Constraints' => [
              'NET  "sysace_cen"             LOC = AJ14;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_cen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_oen"             LOC = AL15;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_oen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_wen"             LOC = AL14;'
            ],
            'Direction' => 'out',
            'Name' => 'sysace_wen',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "sysace_mpirq"           LOC = L9;',
              'NET  "sysace_mpirq"           TIG;'
            ],
            'Direction' => 'in',
            'Name' => 'sysace_mpirq',
            'Width' => 1
          },
          {
            'Constraints' => [
              'NET  "reset"                  LOC = H10;',
              'NET  "reset"                  IOSTANDARD = LVCMOS15;',
              'NET  "reset"                  PULLDOWN;',
              'NET  "reset"                  TIG;'
            ],
            'Direction' => 'in',
            'IsSystemReset' => true,
            'Name' => 'reset',
            'Width' => 1
          }
        ]
      },
      'Variant' => 'ml605'
    },
    'Description' => 'ML605 (Point-to-point Ethernet)',
    'Interface' => 'ppethernet',
    'MaximumFrameSize' => 8184,
    'Part' => {
      'BaseFamily' => 'virtex6',
      'Device' => 'xc6vlx240t',
      'Family' => 'virtex6',
      'FamilyForSynplify' => 'virtex6',
      'Package' => 'ff1156',
      'Speed' => '-1'
    },
    'Type' => 'ppethernet',
    'Vendor' => 'Xilinx'
  },
  'Target' => {
    'ExcludedModules' => [

    ],
    'Modules' => [

    ]
  }
}
