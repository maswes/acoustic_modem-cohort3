#
# Created by System Generator     Wed Mar 16 13:56:10 2011
#
# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator.
#

source SgIseProject.tcl

namespace eval ::xilinx::dsptool::iseproject::param {

    set Project {modulate_cw}
    set Family {Virtex6}
    set Device {xc6vlx240t}
    set Package {ff1156}
    set Speed {-1}
    set HDLLanguage {vhdl}
    set SynthesisTool {XST}
    set Simulator {Modelsim-SE}
    set ReadCores {False}
    set MapEffortLevel {High}
    set ParEffortLevel {High}
    set Frequency {100}
    set ProjectFiles {
        {{modulate_cw.vhd} -view All}
        {{modulate.vhd} -view All}
        {{modulate_cw.ucf}}
        {{C:\training\dsp_trd\labs\sysgen_flow\ML605\modulate.mdl}}
    }
    set TopLevelModule {modulate_cw}
    set SynthesisConstraintsFile {modulate_cw.xcf}
    set ImplementationStopView {Structural}
    set ProjectGenerator {SysgenDSP}
}
::xilinx::dsptool::iseproject::create
