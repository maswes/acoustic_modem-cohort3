library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "modulate"

entity modulate is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    data_in: in std_logic_vector(15 downto 0); 
    sync_rst: in std_logic; 
    data_out: out std_logic_vector(15 downto 0); 
    dds_out: out std_logic_vector(5 downto 0); 
    modulated_out: out std_logic_vector(21 downto 0)
  );
end modulate;

architecture structural of modulate is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "modulate,sysgen_core,{clock_period=10.00000000,clocking=Clock_Enables,compilation=HDL_Netlist,sample_periods=1.00000000000,testbench=0,total_blocks=16,xilinx_dds_compiler_4_0_block=1,xilinx_dsp48_macro_2_1__block=1,xilinx_gateway_in_block=2,xilinx_gateway_out_block=3,xilinx_register_block=1,xilinx_system_generator_block=1,}";

  signal ce_1_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal data_in_net: std_logic_vector(15 downto 0);
  signal data_out_net: std_logic_vector(15 downto 0);
  signal dds_out_net: std_logic_vector(5 downto 0);
  signal modulated_out_net: std_logic_vector(21 downto 0);
  signal sync_rst_net: std_logic;

begin
  ce_1_sg_x0 <= ce_1;
  clk_1_sg_x0 <= clk_1;
  data_in_net <= data_in;
  sync_rst_net <= sync_rst;
  data_out <= data_out_net;
  dds_out <= dds_out_net;
  modulated_out <= modulated_out_net;

  dds_compiler_4_0: entity work.xldds_compiler_8bc7ed853990bc6b398dc0025bec0fca
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      rst => sync_rst_net,
      sine => dds_out_net
    );

  dsp48_macro_2_1: entity work.xldsp48_macro_d48266d37dd5d78f3c71ba958e768ad5
    port map (
      a => data_out_net,
      b => dds_out_net,
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      p => modulated_out_net
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 16,
      init_value => b"0000000000000000"
    )
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      d => data_in_net,
      en => "1",
      rst => "0",
      q => data_out_net
    );

end structural;
