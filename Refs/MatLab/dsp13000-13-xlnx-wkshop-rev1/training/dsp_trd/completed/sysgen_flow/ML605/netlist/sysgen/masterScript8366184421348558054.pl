
open(PIDFILE, '> pidfile.txt') || die 'Couldn\'t write process ID to file.';
print PIDFILE "$$\n";
close(PIDFILE);

eval {
  # Call script(s).
  my $instrs;
  my $results = [];
$ENV{'SYSGEN'} = 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen';
  use Sg;
  $instrs = {
    'HDLCodeGenStatus' => 0.0,
    'HDL_PATH' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605',
    'TEMP' => 'C:/TEMP',
    'TMP' => 'C:/TEMP',
    'Temp' => 'C:/TEMP',
    'Tmp' => 'C:/TEMP',
    'base_system_period_hardware' => 10.0,
    'base_system_period_simulink' => 4.0E-8,
    'block_icon_display' => 'Default',
    'block_type' => 'sysgen',
    'block_version' => '',
    'ce_clr' => 0.0,
    'clock_domain' => 'default',
    'clock_loc' => '',
    'clock_wrapper' => 'Clock Enables',
    'clock_wrapper_sgadvanced' => '',
    'compilation' => 'HDL Netlist',
    'compilation_lut' => {
      'keys' => [ 'HDL Netlist', ],
      'values' => [ 'target1', ],
    },
    'compilation_target' => 'HDL Netlist',
    'core_generation' => 1.0,
    'core_generation_sgadvanced' => '',
    'core_is_deployed' => 0.0,
    'coregen_core_generation_tmpdir' => 'C:/TEMP/sysgentmp-skoppol/cg_wk/c0e4810303d6a77cd',
    'coregen_part_family' => 'virtex6',
    'createTestbench' => 0,
    'create_interface_document' => 'off',
    'dbl_ovrd' => -1.0,
    'dbl_ovrd_sgadvanced' => '',
    'dcm_input_clock_period' => 10.0,
    'deprecated_control' => 'off',
    'deprecated_control_sgadvanced' => '',
    'design' => 'modulate',
    'design_full_path' => 'C:\\training\\dsp_trd\\labs\\sysgen_flow\\ML605\\modulate.mdl',
    'device' => 'xc6vlx240t-1ff1156',
    'device_speed' => '-1',
    'directory' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist',
    'dsp_cache_root_path' => 'C:/TEMP/sysgentmp-skoppol',
    'eval_field' => '0',
    'fileDeliveryDefaults' => [
      [
        '(?i)\\.vhd$',
        { 'fileName' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/perl_results.vhd', },
      ],
      [
        '(?i)\\.v$',
        { 'fileName' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/perl_results.v', },
      ],
    ],
    'fxdptinstalled' => 1.0,
    'generateUsing71FrontEnd' => 1,
    'generating_island_subsystem_handle' => 2075.00048828125,
    'generating_subsystem_handle' => 2075.00048828125,
    'generation_directory' => './netlist',
    'has_advanced_control' => '0',
    'hdlDir' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl',
    'hdlKind' => 'vhdl',
    'hdl_path' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605',
    'incr_netlist' => 'off',
    'incr_netlist_sgadvanced' => '',
    'infoedit' => ' System Generator',
    'isdeployed' => 0,
    'ise_version' => '13.1i',
    'master_sysgen_token_handle' => 2299.00048828125,
    'matlab' => 'C:/Program Files/MATLAB/R2010b',
    'matlab_fixedpoint' => 1.0,
    'mdlHandle' => 2075.00048828125,
    'mdlPath' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/modulate.mdl',
    'modelDiagnostics' => [
      {
        'count' => 16.0,
        'isMask' => 0.0,
        'type' => 'modulate Total blocks',
      },
      {
        'count' => 2.0,
        'isMask' => 0.0,
        'type' => 'DiscretePulseGenerator',
      },
      {
        'count' => 8.0,
        'isMask' => 0.0,
        'type' => 'S-Function',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'Scope',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'SignalGenerator',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'Step',
      },
      {
        'count' => 1.0,
        'isMask' => 0.0,
        'type' => 'SubSystem',
      },
      {
        'count' => 2.0,
        'isMask' => 0.0,
        'type' => 'Terminator',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx DDS Compiler 4.0 Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx DSP48 Macro 2.1  Block',
      },
      {
        'count' => 2.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Gateway In Block',
      },
      {
        'count' => 3.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Gateway Out Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx Register Block',
      },
      {
        'count' => 1.0,
        'isMask' => 1.0,
        'type' => 'Xilinx System Generator Block',
      },
    ],
    'model_globals_initialized' => 1.0,
    'model_path' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/modulate.mdl',
    'myxilinx' => 'C:/Xilinx/13.1/ISE_DS/ISE',
    'ngc_files' => [ 'xlpersistentdff.ngc', ],
    'num_sim_cycles' => '100',
    'package' => 'ff1156',
    'part' => 'xc6vlx240t',
    'partFamily' => 'virtex6',
    'port_data_types_enabled' => 0.0,
    'preserve_hierarchy' => 0.0,
    'run_coregen' => 'off',
    'run_coregen_sgadvanced' => '',
    'sample_time_colors_enabled' => 0.0,
    'sampletimecolors' => 0.0,
    'sg_blockgui_xml' => '',
    'sg_icon_stat' => '50,50,-1,-1,token,white,0,07734,right,,[ ],[ ]',
    'sg_list_contents' => '',
    'sg_mask_display' => 'fprintf(\'\',\'COMMENT: begin icon graphics\');
patch([0 50 50 0 0 ],[0 0 50 50 0 ],[1 1 1 ]);
patch([1.6375 16.81 27.31 37.81 48.31 27.31 12.1375 1.6375 ],[36.655 36.655 47.155 36.655 47.155 47.155 47.155 36.655 ],[0.933333 0.203922 0.141176 ]);
patch([12.1375 27.31 16.81 1.6375 12.1375 ],[26.155 26.155 36.655 36.655 26.155 ],[0.698039 0.0313725 0.219608 ]);
patch([1.6375 16.81 27.31 12.1375 1.6375 ],[15.655 15.655 26.155 26.155 15.655 ],[0.933333 0.203922 0.141176 ]);
patch([12.1375 48.31 37.81 27.31 16.81 1.6375 12.1375 ],[5.155 5.155 15.655 5.155 15.655 15.655 5.155 ],[0.698039 0.0313725 0.219608 ]);
fprintf(\'\',\'COMMENT: end icon graphics\');
fprintf(\'\',\'COMMENT: begin icon text\');
fprintf(\'\',\'COMMENT: end icon text\');',
    'sg_version' => '',
    'sggui_pos' => '-1,-1,-1,-1',
    'simulation_island_subsystem_handle' => 2075.00048828125,
    'simulink_accelerator_running' => 0.0,
    'simulink_debugger_running' => 0.0,
    'simulink_period' => 4.0E-8,
    'speed' => '-1',
    'synthesisTool' => 'XST',
    'synthesis_language' => 'vhdl',
    'synthesis_tool' => 'XST',
    'synthesis_tool_sgadvanced' => '',
    'sysclk_period' => 10.0,
    'sysgen' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'sysgenRoot' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'sysgenTokenSettings' => {
      'base_system_period_hardware' => 10.0,
      'base_system_period_simulink' => 4.0E-8,
      'block_icon_display' => 'Default',
      'block_type' => 'sysgen',
      'block_version' => '',
      'ce_clr' => 0.0,
      'clock_loc' => '',
      'clock_wrapper' => 'Clock Enables',
      'clock_wrapper_sgadvanced' => '',
      'compilation' => 'HDL Netlist',
      'compilation_lut' => {
        'keys' => [ 'HDL Netlist', ],
        'values' => [ 'target1', ],
      },
      'core_generation' => 1.0,
      'core_generation_sgadvanced' => '',
      'coregen_part_family' => 'virtex6',
      'create_interface_document' => 'off',
      'dbl_ovrd' => -1.0,
      'dbl_ovrd_sgadvanced' => '',
      'dcm_input_clock_period' => 10.0,
      'deprecated_control' => 'off',
      'deprecated_control_sgadvanced' => '',
      'directory' => './netlist',
      'eval_field' => '0',
      'has_advanced_control' => '0',
      'incr_netlist' => 'off',
      'incr_netlist_sgadvanced' => '',
      'infoedit' => ' System Generator',
      'master_sysgen_token_handle' => 2299.00048828125,
      'package' => 'ff1156',
      'part' => 'xc6vlx240t',
      'preserve_hierarchy' => 0.0,
      'run_coregen' => 'off',
      'run_coregen_sgadvanced' => '',
      'sg_blockgui_xml' => '',
      'sg_icon_stat' => '50,50,-1,-1,token,white,0,07734,right,,[ ],[ ]',
      'sg_list_contents' => '',
      'sg_mask_display' => 'fprintf(\'\',\'COMMENT: begin icon graphics\');
patch([0 50 50 0 0 ],[0 0 50 50 0 ],[1 1 1 ]);
patch([1.6375 16.81 27.31 37.81 48.31 27.31 12.1375 1.6375 ],[36.655 36.655 47.155 36.655 47.155 47.155 47.155 36.655 ],[0.933333 0.203922 0.141176 ]);
patch([12.1375 27.31 16.81 1.6375 12.1375 ],[26.155 26.155 36.655 36.655 26.155 ],[0.698039 0.0313725 0.219608 ]);
patch([1.6375 16.81 27.31 12.1375 1.6375 ],[15.655 15.655 26.155 26.155 15.655 ],[0.933333 0.203922 0.141176 ]);
patch([12.1375 48.31 37.81 27.31 16.81 1.6375 12.1375 ],[5.155 5.155 15.655 5.155 15.655 15.655 5.155 ],[0.698039 0.0313725 0.219608 ]);
fprintf(\'\',\'COMMENT: end icon graphics\');
fprintf(\'\',\'COMMENT: begin icon text\');
fprintf(\'\',\'COMMENT: end icon text\');',
      'sggui_pos' => '-1,-1,-1,-1',
      'simulation_island_subsystem_handle' => 2075.00048828125,
      'simulink_period' => 4.0E-8,
      'speed' => '-1',
      'synthesis_language' => 'vhdl',
      'synthesis_tool' => 'XST',
      'synthesis_tool_sgadvanced' => '',
      'sysclk_period' => 10.0,
      'testbench' => 0,
      'testbench_sgadvanced' => '',
      'trim_vbits' => 1.0,
      'trim_vbits_sgadvanced' => '',
      'xilinx_device' => 'xc6vlx240t-1ff1156',
      'xilinxfamily' => 'virtex6',
    },
    'sysgen_Root' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen',
    'systemClockPeriod' => 10.0,
    'tempdir' => 'C:/TEMP',
    'testbench' => 0,
    'testbench_sgadvanced' => '',
    'tmpDir' => 'C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen',
    'trim_vbits' => 1.0,
    'trim_vbits_sgadvanced' => '',
    'use_strict_names' => 1,
    'user_tips_enabled' => 0.0,
    'usertemp' => 'C:/TEMP/sysgentmp-skoppol',
    'using71Netlister' => 1,
    'verilog_files' => [
      'conv_pkg.v',
      'synth_reg.v',
      'synth_reg_w_init.v',
      'convert_type.v',
    ],
    'version' => '',
    'vhdl_files' => [
      'conv_pkg.vhd',
      'synth_reg.vhd',
      'synth_reg_w_init.vhd',
    ],
    'vsimtime' => '1375.000000 ns',
    'xilinx' => 'C:/Xilinx/13.1/ISE_DS/ISE',
    'xilinx_device' => 'xc6vlx240t-1ff1156',
    'xilinx_family' => 'virtex6',
    'xilinx_package' => 'ff1156',
    'xilinx_part' => 'xc6vlx240t',
    'xilinxdevice' => 'xc6vlx240t-1ff1156',
    'xilinxfamily' => 'virtex6',
    'xilinxpart' => 'xc6vlx240t',
  };
  push(@$results, &Sg::setAttributes($instrs));
  use SgDeliverFile;
  $instrs = {
    'collaborationName' => 'conv_pkg.vhd',
    'sourceFile' => 'hdl/conv_pkg.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'synth_reg.vhd',
    'sourceFile' => 'hdl/synth_reg.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'synth_reg_w_init.vhd',
    'sourceFile' => 'hdl/synth_reg_w_init.vhd',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'collaborationName' => 'xlpersistentdff.ngc',
    'sourceFile' => 'hdl/xlpersistentdff.ngc',
    'templateKeyValues' => {},
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  $instrs = {
    'entity_declaration_hash' => '3410b5f86733881502ee6c652040bccc',
    'sourceFile' => 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen/hdl/xlregister.vhd',
  };
  push(@$results, &SgDeliverFile::saveCollaborationInfo($instrs));
  use SgGenerateCores;
  $instrs = [
    'SELECT DSP48_Macro virtex6 Xilinx,_Inc. 2.1',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET a_binarywidth = 14',
    'CSET a_width = 16',
    'CSET areg_1 = false',
    'CSET areg_2 = false',
    'CSET areg_3 = true',
    'CSET areg_4 = true',
    'CSET b_binarywidth = 5',
    'CSET b_width = 6',
    'CSET breg_1 = false',
    'CSET breg_2 = false',
    'CSET breg_3 = true',
    'CSET breg_4 = true',
    'CSET c_binarywidth = 19',
    'CSET c_width = 48',
    'CSET cinreg_1 = false',
    'CSET cinreg_2 = false',
    'CSET cinreg_3 = false',
    'CSET cinreg_4 = false',
    'CSET cinreg_5 = false',
    'CSET concat_binarywidth = 19',
    'CSET concat_width = 48',
    'CSET concatreg_3 = false',
    'CSET concatreg_4 = false',
    'CSET concatreg_5 = false',
    'CSET creg_1 = false',
    'CSET creg_2 = false',
    'CSET creg_3 = false',
    'CSET creg_4 = false',
    'CSET creg_5 = false',
    'CSET d_binarywidth = 0',
    'CSET d_width = 18',
    'CSET dreg_1 = false',
    'CSET dreg_2 = false',
    'CSET dreg_3 = false',
    'CSET gui_behaviour = Coregen',
    'CSET has_a_ce = false',
    'CSET has_a_sclr = false',
    'CSET has_acout = false',
    'CSET has_b_ce = false',
    'CSET has_b_sclr = false',
    'CSET has_bcout = false',
    'CSET has_c_ce = false',
    'CSET has_c_sclr = false',
    'CSET has_carrycascout = false',
    'CSET has_carryout = false',
    'CSET has_ce = true',
    'CSET has_concat_ce = false',
    'CSET has_concat_sclr = false',
    'CSET has_d_ce = false',
    'CSET has_d_sclr = false',
    'CSET has_m_ce = false',
    'CSET has_m_sclr = false',
    'CSET has_p_ce = false',
    'CSET has_p_sclr = false',
    'CSET has_pcout = false',
    'CSET has_sclr = false',
    'CSET has_sel_ce = false',
    'CSET has_sel_sclr = false',
    'CSET instruction1 = #',
    'CSET instruction2 = #',
    'CSET instruction3 = #',
    'CSET instruction4 = #',
    'CSET instruction5 = #',
    'CSET instruction6 = #',
    'CSET instruction7 = #',
    'CSET instruction8 = #',
    'CSET instruction_list = A*B',
    'CSET mreg_5 = true',
    'CSET opreg_1 = false',
    'CSET opreg_2 = false',
    'CSET opreg_3 = false',
    'CSET opreg_4 = false',
    'CSET opreg_5 = false',
    'CSET output_properties = Full_Precision',
    'CSET p_binarywidth = 19',
    'CSET p_full_width = 22',
    'CSET p_width = 22',
    'CSET pcin_binarywidth = 19',
    'CSET pipeline_options = Automatic',
    'CSET preg_6 = true',
    'CSET show_filtered = false',
    'CSET tier_1 = false',
    'CSET tier_2 = false',
    'CSET tier_3 = false',
    'CSET tier_4 = false',
    'CSET tier_5 = false',
    'CSET tier_6 = false',
    'CSET use_dsp48 = true',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = xbp_dsp48_mcr_v2_1_4464337bc93fbbc9',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => 'a6f7062e211e62f3018591587a8110fb',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component xbp_dsp48_mcr_v2_1_4464337bc93fbbc9
    port(
      a:in std_logic_vector(15 downto 0);
      b:in std_logic_vector(5 downto 0);
      ce:in std_logic;
      clk:in std_logic;
      p:out std_logic_vector(21 downto 0)
    );
end component;
begin
  xbp_dsp48_mcr_v2_1_4464337bc93fbbc9_instance : xbp_dsp48_mcr_v2_1_4464337bc93fbbc9
    port map(
      a=>a,
      b=>b,
      ce=>ce,
      clk=>clk,
      p=>p
    );
end ',
      'crippled_entity' => 'is 
  port(
    a:in std_logic_vector(15 downto 0);
    b:in std_logic_vector(5 downto 0);
    ce:in std_logic;
    clk:in std_logic;
    p:out std_logic_vector(21 downto 0)
  );
end',
      'entity_name' => 'xldsp48_macro_d48266d37dd5d78f3c71ba958e768ad5',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  $instrs = [
    'SELECT DDS_Compiler virtex6 Xilinx,_Inc. 4.0',
    '# 13.1_O.40d',
    '# DEVICE virtex6',
    '# VHDL',
    'CSET amplitude_mode = Full_Range',
    'CSET channel_pin = false',
    'CSET channels = 1',
    'CSET clock_enable = true',
    'CSET dds_clock_rate = 100',
    'CSET dsp48_use = Maximal',
    'CSET explicit_period = false',
    'CSET frequency_resolution = 0.40000000000',
    'CSET gui_behaviour = Sysgen',
    'CSET has_phase_out = false',
    'CSET latency = 2',
    'CSET latency_configuration = Auto',
    'CSET memory_type = Block_ROM',
    'CSET negative_cosine = false',
    'CSET negative_sine = false',
    'CSET noise_shaping = None',
    'CSET optimization_goal = Auto',
    'CSET output_frequency1 = 5',
    'CSET output_frequency10 = 0',
    'CSET output_frequency11 = 0',
    'CSET output_frequency12 = 0',
    'CSET output_frequency13 = 0',
    'CSET output_frequency14 = 0',
    'CSET output_frequency15 = 0',
    'CSET output_frequency16 = 0',
    'CSET output_frequency2 = 0',
    'CSET output_frequency3 = 0',
    'CSET output_frequency4 = 0',
    'CSET output_frequency5 = 0',
    'CSET output_frequency6 = 0',
    'CSET output_frequency7 = 0',
    'CSET output_frequency8 = 0',
    'CSET output_frequency9 = 0',
    'CSET output_selection = Sine',
    'CSET output_width = 6',
    'CSET parameter_entry = System_Parameters',
    'CSET partspresent = Phase_Generator_and_SIN_COS_LUT',
    'CSET period = 1',
    'CSET phase_increment = Fixed',
    'CSET phase_offset = None',
    'CSET phase_offset_angles1 = 0',
    'CSET phase_offset_angles10 = 0',
    'CSET phase_offset_angles11 = 0',
    'CSET phase_offset_angles12 = 0',
    'CSET phase_offset_angles13 = 0',
    'CSET phase_offset_angles14 = 0',
    'CSET phase_offset_angles15 = 0',
    'CSET phase_offset_angles16 = 0',
    'CSET phase_offset_angles2 = 0',
    'CSET phase_offset_angles3 = 0',
    'CSET phase_offset_angles4 = 0',
    'CSET phase_offset_angles5 = 0',
    'CSET phase_offset_angles6 = 0',
    'CSET phase_offset_angles7 = 0',
    'CSET phase_offset_angles8 = 0',
    'CSET phase_offset_angles9 = 0',
    'CSET phase_width = 28',
    'CSET pinc1 = 110011001100110011001100',
    'CSET pinc10 = 0',
    'CSET pinc11 = 0',
    'CSET pinc12 = 0',
    'CSET pinc13 = 0',
    'CSET pinc14 = 0',
    'CSET pinc15 = 0',
    'CSET pinc16 = 0',
    'CSET pinc2 = 0',
    'CSET pinc3 = 0',
    'CSET pinc4 = 0',
    'CSET pinc5 = 0',
    'CSET pinc6 = 0',
    'CSET pinc7 = 0',
    'CSET pinc8 = 0',
    'CSET pinc9 = 0',
    'CSET poff1 = 0',
    'CSET poff10 = 0',
    'CSET poff11 = 0',
    'CSET poff12 = 0',
    'CSET poff13 = 0',
    'CSET poff14 = 0',
    'CSET poff15 = 0',
    'CSET poff16 = 0',
    'CSET poff2 = 0',
    'CSET poff3 = 0',
    'CSET poff4 = 0',
    'CSET poff5 = 0',
    'CSET poff6 = 0',
    'CSET poff7 = 0',
    'CSET poff8 = 0',
    'CSET poff9 = 0',
    'CSET por_mode = false',
    'CSET rdy = false',
    'CSET rfd = false',
    'CSET sclr_pin = true',
    'CSET spurious_free_dynamic_range = 36',
    'SET device = xc6vlx240t',
    'SET package = ff1156',
    'SET speedgrade = -1',
    'CSET component_name = dds_cmplr_v4_0_6a04b82566c6a378',
    'GENERATE',
  ];
  push(@$results, &SgGenerateCores::saveXcoSequence($instrs));
  $instrs = {
    'entity_declaration_hash' => 'f50b16edf13d68a57959f97bce3aa0b2',
    'sourceFile' => 'hdl/xlmcode.vhd',
    'templateKeyValues' => {
      'crippled_architecture' => ' is
  component dds_cmplr_v4_0_6a04b82566c6a378
    port(
      ce:in std_logic;
      clk:in std_logic;
      sclr:in std_logic;
      sine:out std_logic_vector(5 downto 0)
    );
end component;
signal sclr_net: std_logic := \'0\';
begin
  sclr_net <= rst and ce;
  dds_cmplr_v4_0_6a04b82566c6a378_instance : dds_cmplr_v4_0_6a04b82566c6a378
    port map(
      ce=>ce,
      clk=>clk,
      sclr=>sclr_net,
      sine=>sine
    );
end ',
      'crippled_entity' => 'is 
  port(
    ce:in std_logic;
    clk:in std_logic;
    rst:in std_logic;
    sine:out std_logic_vector(5 downto 0)
  );
end',
      'entity_name' => 'xldds_compiler_8bc7ed853990bc6b398dc0025bec0fca',
    },
  };
  push(@$results, &SgDeliverFile::deliverFile($instrs));
  local *wrapup = $Sg::{'wrapup'};
  push(@$results, &Sg::wrapup())   if (defined(&wrapup));
  local *wrapup = $SgDeliverFile::{'wrapup'};
  push(@$results, &SgDeliverFile::wrapup())   if (defined(&wrapup));
  local *wrapup = $SgGenerateCores::{'wrapup'};
  push(@$results, &SgGenerateCores::wrapup())   if (defined(&wrapup));
  use Carp qw(croak);
  $ENV{'SYSGEN'} = 'C:/Xilinx/13.1/ISE_DS/ISE/sysgen';
  open(RESULTS, '> C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550') || 
    croak 'couldn\'t open C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550';
  binmode(RESULTS);
  print RESULTS &Sg::toString($results) . "\n";
  close(RESULTS) || 
    croak 'trouble writing C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550';
};

if ($@) {
  open(RESULTS, '> C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550') || 
    croak 'couldn\'t open C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550';
  binmode(RESULTS);
  print RESULTS $@ . "\n";
  close(RESULTS) || 
    croak 'trouble writing C:/training/dsp_trd/labs/sysgen_flow/ML605/netlist/sysgen/script_results3661308914262841550';
  exit(1);
}

exit(0);
