# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This respository contains MATLAB Simulink, hardware and software design files of an underwater modem system.

Below are quick summary of each folder:
- MATLAB: contains a basic Simulink design of BPSK transmits and receiver. It also contains HDL codes, IP cores
that could run on ZedBoard.
- PCB_Acomm_Project: contains all printer circuit board files for the modem hardware
- Refs: Some good referrences related to implement QPSK on FPGA.
- Report: All slides progress reports and source of figures and sounds
- Vivado_Projects: Contains SDK C application files, hardware drivers, 2-FSK modem design with Vivado system
design, and other test & try files
- Acoustic Modem: The video was posted on youtube

### How do I get set up? ###
*MATLAB Simulink
   Version used: 2015a
   Main Simulink file: hoa2_0530.slx

	-Transmitter Block: Modulate BPSK signal 
	. Created 100 frames to transmit:
	. 130 bits per frame
	. A frame contains :
		- 32 bits Golay code preamble
		- Message �Hello World 00� to � Hello World 99�  :  98 bits per 1 message
		- Matlab file name:  Message.m

	- Receiver Block: demodulate BPSK signal
		- Synchronizer, demodulator, RRC, DDS 


* Hardware: the ZedBoard was chosen as a main DSP and processing engine so we can maximize utilizing
new Xilinx Zynq device, new Xilinx software tool, and Matlab. Since there are not available
hardware for underwater transmitter and receiver, our team build hard ware for it. The hardware design 
includes building PCB (Printer Circuit Board) schematic, layout, assembly, wiring, repair, and test.

All PCB design files are contained in "PCB_Acomm_Project" and desinged with Altium 2015 version

Main PCB project file: "PCB_Acomm_Project.PrjPcb"

* Software:
The software was build on Vivado 2015.1.

The main project was to build an 2-FSK modem using Vivado system block diagram design. The Vivado system
block design allows designer to design complete (PS and PL) Zynq system with blocks.  Most of blocks
are avaialble in Xilinx libraries. The Vivado lets designer create and import blocks from other design 
environment like MATLAB, Vivado HLS, VHDL, and Verilog. In the 2-FSK modem there are couple blocks are 
designs from Vivado HLS and VHDL. Each block in Vivado system is called IP core.

To open vivado project file:
- Click Start-> Programs -> Xilinx Design Tools -> Vivado 2015.1 -> Vivado 2015.1
- Click Open Project
- Browse to main project file in "Vivado_Projects -> fsk_modem_04" folder.
- Click "fsk_modem.xpr"

The Vivado can also generate bit file for Zynq device, run logic analizer to debug.

Finally the Vivado can export hardware design to SDK for application development. An application C code
was written allow user to control several parameters of the 2-FSK modem including changing center frequency,
adjust input/output gain, controlling TX/RX switch manually, and typing transmit message and display receive
message.

For more detail design, check out our wiki page of underwater modem.

### Who do I talk to? ###

* Repo owner or admin
Nghia Tran, txnghia@gmail.com, Cohort 3
Hoa Nguyen, nguyentuonghoa@gmail.com, Cohort 3




