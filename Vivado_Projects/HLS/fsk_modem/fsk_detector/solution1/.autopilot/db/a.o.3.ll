; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/fsk_modem/fsk_detector/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@str = internal constant [11 x i8] c"fsk_dector\00" ; [#uses=1 type=[11 x i8]*]

; [#uses=0]
define void @fsk_dector(i32 %cmp_val, i32 %one_val, i32 %zero_val, i1* %dat_out) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %cmp_val) nounwind, !map !0
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %one_val) nounwind, !map !6
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %zero_val) nounwind, !map !10
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %dat_out) nounwind, !map !14
  call void (...)* @_ssdm_op_SpecTopModule([11 x i8]* @str) nounwind
  %zero_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %zero_val) nounwind ; [#uses=3 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %zero_val_read}, i64 0, metadata !20), !dbg !33 ; [debug line = 3:51] [debug variable = zero_val]
  %one_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %one_val) nounwind ; [#uses=3 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %one_val_read}, i64 0, metadata !34), !dbg !35 ; [debug line = 3:38] [debug variable = one_val]
  %cmp_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %cmp_val) nounwind ; [#uses=2 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %cmp_val_read}, i64 0, metadata !36), !dbg !37 ; [debug line = 3:25] [debug variable = cmp_val]
  call void @llvm.dbg.value(metadata !{i32 %cmp_val}, i64 0, metadata !36), !dbg !37 ; [debug line = 3:25] [debug variable = cmp_val]
  call void @llvm.dbg.value(metadata !{i32 %one_val}, i64 0, metadata !34), !dbg !35 ; [debug line = 3:38] [debug variable = one_val]
  call void @llvm.dbg.value(metadata !{i32 %zero_val}, i64 0, metadata !20), !dbg !33 ; [debug line = 3:51] [debug variable = zero_val]
  call void @llvm.dbg.value(metadata !{i1* %dat_out}, i64 0, metadata !38), !dbg !39 ; [debug line = 3:68] [debug variable = dat_out]
  %neg = sub i32 0, %one_val_read                 ; [#uses=1 type=i32]
  %abscond = icmp sgt i32 %one_val_read, 0        ; [#uses=1 type=i1]
  %one_val_temp = select i1 %abscond, i32 %one_val_read, i32 %neg ; [#uses=1 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %one_val_temp}, i64 0, metadata !40), !dbg !42 ; [debug line = 7:17] [debug variable = one_val_temp]
  %neg2 = sub i32 0, %zero_val_read               ; [#uses=1 type=i32]
  %abscond3 = icmp sgt i32 %zero_val_read, 0      ; [#uses=1 type=i1]
  %zero_val_temp = select i1 %abscond3, i32 %zero_val_read, i32 %neg2 ; [#uses=1 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %zero_val_temp}, i64 0, metadata !43), !dbg !44 ; [debug line = 8:18] [debug variable = zero_val_temp]
  %tmp = icmp ugt i32 %one_val_temp, %cmp_val_read, !dbg !45 ; [#uses=1 type=i1] [debug line = 12:2]
  %tmp_1 = icmp ult i32 %zero_val_temp, %cmp_val_read, !dbg !45 ; [#uses=1 type=i1] [debug line = 12:2]
  %or_cond = and i1 %tmp, %tmp_1, !dbg !45        ; [#uses=1 type=i1] [debug line = 12:2]
  call void @_ssdm_op_Write.ap_auto.i1P(i1* %dat_out, i1 %or_cond) nounwind, !dbg !46 ; [debug line = 19:2]
  ret void, !dbg !47                              ; [debug line = 20:1]
}

; [#uses=9]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=4]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=3]
define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

; [#uses=1]
define weak void @_ssdm_op_Write.ap_auto.i1P(i1*, i1) {
entry:
  store i1 %1, i1* %0
  ret void
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{}

!0 = metadata !{metadata !1}
!1 = metadata !{i32 0, i32 31, metadata !2}
!2 = metadata !{metadata !3}
!3 = metadata !{metadata !"cmp_val", metadata !4, metadata !"uint32", i32 0, i32 31}
!4 = metadata !{metadata !5}
!5 = metadata !{i32 0, i32 0, i32 0}
!6 = metadata !{metadata !7}
!7 = metadata !{i32 0, i32 31, metadata !8}
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !"one_val", metadata !4, metadata !"int", i32 0, i32 31}
!10 = metadata !{metadata !11}
!11 = metadata !{i32 0, i32 31, metadata !12}
!12 = metadata !{metadata !13}
!13 = metadata !{metadata !"zero_val", metadata !4, metadata !"int", i32 0, i32 31}
!14 = metadata !{metadata !15}
!15 = metadata !{i32 0, i32 0, metadata !16}
!16 = metadata !{metadata !17}
!17 = metadata !{metadata !"dat_out", metadata !18, metadata !"uint1", i32 0, i32 0}
!18 = metadata !{metadata !19}
!19 = metadata !{i32 0, i32 0, i32 1}
!20 = metadata !{i32 786689, metadata !21, metadata !"zero_val", metadata !22, i32 50331651, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!21 = metadata !{i32 786478, i32 0, metadata !22, metadata !"fsk_dector", metadata !"fsk_dector", metadata !"_Z10fsk_dectorDq32_jiiPDq1_j", metadata !22, i32 3, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32, i32, i1*)* @fsk_dector, null, null, metadata !31, i32 4} ; [ DW_TAG_subprogram ]
!22 = metadata !{i32 786473, metadata !"../c/fsk_detector.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS/fsk_modem", null} ; [ DW_TAG_file_type ]
!23 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!24 = metadata !{null, metadata !25, metadata !27, metadata !27, metadata !28}
!25 = metadata !{i32 786454, null, metadata !"uint32", metadata !22, i32 34, i64 0, i64 0, i64 0, i32 0, metadata !26} ; [ DW_TAG_typedef ]
!26 = metadata !{i32 786468, null, metadata !"uint32", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!27 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!28 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !29} ; [ DW_TAG_pointer_type ]
!29 = metadata !{i32 786454, null, metadata !"uint1", metadata !22, i32 3, i64 0, i64 0, i64 0, i32 0, metadata !30} ; [ DW_TAG_typedef ]
!30 = metadata !{i32 786468, null, metadata !"uint1", null, i32 0, i64 1, i64 1, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!31 = metadata !{metadata !32}
!32 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!33 = metadata !{i32 3, i32 51, metadata !21, null}
!34 = metadata !{i32 786689, metadata !21, metadata !"one_val", metadata !22, i32 33554435, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!35 = metadata !{i32 3, i32 38, metadata !21, null}
!36 = metadata !{i32 786689, metadata !21, metadata !"cmp_val", metadata !22, i32 16777219, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!37 = metadata !{i32 3, i32 25, metadata !21, null}
!38 = metadata !{i32 786689, metadata !21, metadata !"dat_out", metadata !22, i32 67108867, metadata !28, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!39 = metadata !{i32 3, i32 68, metadata !21, null}
!40 = metadata !{i32 786688, metadata !41, metadata !"one_val_temp", metadata !22, i32 6, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!41 = metadata !{i32 786443, metadata !21, i32 4, i32 1, metadata !22, i32 0} ; [ DW_TAG_lexical_block ]
!42 = metadata !{i32 7, i32 17, metadata !41, null}
!43 = metadata !{i32 786688, metadata !41, metadata !"zero_val_temp", metadata !22, i32 6, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!44 = metadata !{i32 8, i32 18, metadata !41, null}
!45 = metadata !{i32 12, i32 2, metadata !41, null}
!46 = metadata !{i32 19, i32 2, metadata !41, null}
!47 = metadata !{i32 20, i32 1, metadata !41, null}
