; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/fsk_modem/fsk_detector/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@str = internal constant [11 x i8] c"fsk_dector\00"

define void @fsk_dector(i32 %cmp_val, i32 %one_val, i32 %zero_val, i1* %dat_out) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %cmp_val) nounwind, !map !0
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %one_val) nounwind, !map !6
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %zero_val) nounwind, !map !10
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %dat_out) nounwind, !map !14
  call void (...)* @_ssdm_op_SpecTopModule([11 x i8]* @str) nounwind
  %zero_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %zero_val) nounwind
  %one_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %one_val) nounwind
  %cmp_val_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %cmp_val) nounwind
  %neg = sub i32 0, %one_val_read
  %abscond = icmp sgt i32 %one_val_read, 0
  %one_val_temp = select i1 %abscond, i32 %one_val_read, i32 %neg
  %neg2 = sub i32 0, %zero_val_read
  %abscond3 = icmp sgt i32 %zero_val_read, 0
  %zero_val_temp = select i1 %abscond3, i32 %zero_val_read, i32 %neg2
  %tmp = icmp ugt i32 %one_val_temp, %cmp_val_read
  %tmp_1 = icmp ult i32 %zero_val_temp, %cmp_val_read
  %or_cond = and i1 %tmp, %tmp_1
  call void @_ssdm_op_Write.ap_auto.i1P(i1* %dat_out, i1 %or_cond) nounwind
  ret void
}

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

define weak void @_ssdm_op_Write.ap_auto.i1P(i1*, i1) {
entry:
  store i1 %1, i1* %0
  ret void
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{}

!0 = metadata !{metadata !1}
!1 = metadata !{i32 0, i32 31, metadata !2}
!2 = metadata !{metadata !3}
!3 = metadata !{metadata !"cmp_val", metadata !4, metadata !"uint32", i32 0, i32 31}
!4 = metadata !{metadata !5}
!5 = metadata !{i32 0, i32 0, i32 0}
!6 = metadata !{metadata !7}
!7 = metadata !{i32 0, i32 31, metadata !8}
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !"one_val", metadata !4, metadata !"int", i32 0, i32 31}
!10 = metadata !{metadata !11}
!11 = metadata !{i32 0, i32 31, metadata !12}
!12 = metadata !{metadata !13}
!13 = metadata !{metadata !"zero_val", metadata !4, metadata !"int", i32 0, i32 31}
!14 = metadata !{metadata !15}
!15 = metadata !{i32 0, i32 0, metadata !16}
!16 = metadata !{metadata !17}
!17 = metadata !{metadata !"dat_out", metadata !18, metadata !"uint1", i32 0, i32 0}
!18 = metadata !{metadata !19}
!19 = metadata !{i32 0, i32 0, i32 1}
