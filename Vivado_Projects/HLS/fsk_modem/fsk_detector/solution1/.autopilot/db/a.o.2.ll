; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/fsk_modem/fsk_detector/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@str = internal constant [11 x i8] c"fsk_dector\00" ; [#uses=1 type=[11 x i8]*]

; [#uses=0]
define void @fsk_dector(i32 %cmp_val, i32 %one_val, i32 %zero_val, i1* %dat_out) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %cmp_val) nounwind, !map !21
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %one_val) nounwind, !map !27
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %zero_val) nounwind, !map !31
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %dat_out) nounwind, !map !35
  call void (...)* @_ssdm_op_SpecTopModule([11 x i8]* @str) nounwind
  call void @llvm.dbg.value(metadata !{i32 %cmp_val}, i64 0, metadata !41), !dbg !42 ; [debug line = 3:25] [debug variable = cmp_val]
  call void @llvm.dbg.value(metadata !{i32 %one_val}, i64 0, metadata !43), !dbg !44 ; [debug line = 3:38] [debug variable = one_val]
  call void @llvm.dbg.value(metadata !{i32 %zero_val}, i64 0, metadata !45), !dbg !46 ; [debug line = 3:51] [debug variable = zero_val]
  call void @llvm.dbg.value(metadata !{i1* %dat_out}, i64 0, metadata !47), !dbg !48 ; [debug line = 3:68] [debug variable = dat_out]
  %neg = sub i32 0, %one_val                      ; [#uses=1 type=i32]
  %abscond = icmp sgt i32 %one_val, 0             ; [#uses=1 type=i1]
  %one_val_temp = select i1 %abscond, i32 %one_val, i32 %neg ; [#uses=1 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %one_val_temp}, i64 0, metadata !49), !dbg !51 ; [debug line = 7:17] [debug variable = one_val_temp]
  %neg2 = sub i32 0, %zero_val                    ; [#uses=1 type=i32]
  %abscond3 = icmp sgt i32 %zero_val, 0           ; [#uses=1 type=i1]
  %zero_val_temp = select i1 %abscond3, i32 %zero_val, i32 %neg2 ; [#uses=1 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %zero_val_temp}, i64 0, metadata !52), !dbg !53 ; [debug line = 8:18] [debug variable = zero_val_temp]
  %tmp = icmp ugt i32 %one_val_temp, %cmp_val, !dbg !54 ; [#uses=1 type=i1] [debug line = 12:2]
  %tmp.1 = icmp ult i32 %zero_val_temp, %cmp_val, !dbg !54 ; [#uses=1 type=i1] [debug line = 12:2]
  %or.cond = and i1 %tmp, %tmp.1, !dbg !54        ; [#uses=1 type=i1] [debug line = 12:2]
  store i1 %or.cond, i1* %dat_out, align 1, !dbg !55 ; [debug line = 19:2]
  ret void, !dbg !56                              ; [debug line = 20:1]
}

; [#uses=6]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=4]
declare void @_ssdm_op_SpecBitsMap(...)

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}
!llvm.map.gv = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"C:/Capstone2015/Vivado_Projects/HLS/fsk_modem/fsk_detector/solution1/.autopilot/db/fsk_detector.pragma.2.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS/fsk_modem", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !17} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"fsk_dector", metadata !"fsk_dector", metadata !"_Z10fsk_dectorDq32_jiiPDq1_j", metadata !6, i32 3, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32, i32, i1*)* @fsk_dector, null, null, metadata !15, i32 4} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"../c/fsk_detector.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS/fsk_modem", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{null, metadata !9, metadata !11, metadata !11, metadata !12}
!9 = metadata !{i32 786454, null, metadata !"uint32", metadata !6, i32 34, i64 0, i64 0, i64 0, i32 0, metadata !10} ; [ DW_TAG_typedef ]
!10 = metadata !{i32 786468, null, metadata !"uint32", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!11 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!12 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !13} ; [ DW_TAG_pointer_type ]
!13 = metadata !{i32 786454, null, metadata !"uint1", metadata !6, i32 3, i64 0, i64 0, i64 0, i32 0, metadata !14} ; [ DW_TAG_typedef ]
!14 = metadata !{i32 786468, null, metadata !"uint1", null, i32 0, i64 1, i64 1, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!15 = metadata !{metadata !16}
!16 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!17 = metadata !{metadata !18}
!18 = metadata !{metadata !19}
!19 = metadata !{i32 786484, i32 0, null, metadata !"_sys_nerr", metadata !"_sys_nerr", metadata !"", metadata !20, i32 157, metadata !11, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!20 = metadata !{i32 786473, metadata !"C:/Xilinx/Vivado_HLS/2015.1/win64/tools/clang/bin/../lib/clang/3.1/../../../x86_64-w64-mingw32/include\5Cstdlib.h", metadata !"c:/Capstone2015/Vivado_Projects/HLS/fsk_modem", null} ; [ DW_TAG_file_type ]
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 31, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"cmp_val", metadata !25, metadata !"uint32", i32 0, i32 31}
!25 = metadata !{metadata !26}
!26 = metadata !{i32 0, i32 0, i32 0}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"one_val", metadata !25, metadata !"int", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 31, metadata !33}
!33 = metadata !{metadata !34}
!34 = metadata !{metadata !"zero_val", metadata !25, metadata !"int", i32 0, i32 31}
!35 = metadata !{metadata !36}
!36 = metadata !{i32 0, i32 0, metadata !37}
!37 = metadata !{metadata !38}
!38 = metadata !{metadata !"dat_out", metadata !39, metadata !"uint1", i32 0, i32 0}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 0, i32 1}
!41 = metadata !{i32 786689, metadata !5, metadata !"cmp_val", metadata !6, i32 16777219, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!42 = metadata !{i32 3, i32 25, metadata !5, null}
!43 = metadata !{i32 786689, metadata !5, metadata !"one_val", metadata !6, i32 33554435, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!44 = metadata !{i32 3, i32 38, metadata !5, null}
!45 = metadata !{i32 786689, metadata !5, metadata !"zero_val", metadata !6, i32 50331651, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!46 = metadata !{i32 3, i32 51, metadata !5, null}
!47 = metadata !{i32 786689, metadata !5, metadata !"dat_out", metadata !6, i32 67108867, metadata !12, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!48 = metadata !{i32 3, i32 68, metadata !5, null}
!49 = metadata !{i32 786688, metadata !50, metadata !"one_val_temp", metadata !6, i32 6, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!50 = metadata !{i32 786443, metadata !5, i32 4, i32 1, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!51 = metadata !{i32 7, i32 17, metadata !50, null}
!52 = metadata !{i32 786688, metadata !50, metadata !"zero_val_temp", metadata !6, i32 6, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!53 = metadata !{i32 8, i32 18, metadata !50, null}
!54 = metadata !{i32 12, i32 2, metadata !50, null}
!55 = metadata !{i32 19, i32 2, metadata !50, null}
!56 = metadata !{i32 20, i32 1, metadata !50, null}
