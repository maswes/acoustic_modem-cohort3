; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/modem/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@p_str = private unnamed_addr constant [8 x i8] c"ap_fifo\00", align 1 ; [#uses=2 type=[8 x i8]*]
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=6 type=[1 x i8]*]
@str = internal constant [6 x i8] c"modem\00"     ; [#uses=1 type=[6 x i8]*]

; [#uses=0]
define void @modem(i32* %x, i32* %y) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %x) nounwind, !map !0
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !6
  call void (...)* @_ssdm_op_SpecTopModule([6 x i8]* @str) nounwind
  call void @llvm.dbg.value(metadata !{i32* %x}, i64 0, metadata !10), !dbg !20 ; [debug line = 6:26] [debug variable = x]
  call void @llvm.dbg.value(metadata !{i32* %y}, i64 0, metadata !21), !dbg !22 ; [debug line = 6:43] [debug variable = y]
  call void (...)* @_ssdm_op_SpecInterface(i32* %y, [8 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !23 ; [debug line = 8:1]
  call void (...)* @_ssdm_op_SpecInterface(i32* %x, [8 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !25 ; [debug line = 9:1]
  br label %1, !dbg !26                           ; [debug line = 25:6]

; <label>:1                                       ; preds = %2, %0
  %i = phi i4 [ 0, %0 ], [ %i_1, %2 ]             ; [#uses=2 type=i4]
  %exitcond5 = icmp eq i4 %i, -8, !dbg !26        ; [#uses=1 type=i1] [debug line = 25:6]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 8, i64 8, i64 8) nounwind ; [#uses=0 type=i32]
  %i_1 = add i4 %i, 1, !dbg !28                   ; [#uses=1 type=i4] [debug line = 25:20]
  br i1 %exitcond5, label %.preheader9.preheader_ifconv, label %2, !dbg !26 ; [debug line = 25:6]

.preheader9.preheader_ifconv:                     ; preds = %1
  ret void, !dbg !29                              ; [debug line = 67:1]

; <label>:2                                       ; preds = %1
  %x_load = call i32 @_ssdm_op_Read.ap_fifo.volatile.i32P(i32* %x) nounwind, !dbg !30 ; [#uses=0 type=i32] [debug line = 26:3]
  call void @llvm.dbg.value(metadata !{i4 %i_1}, i64 0, metadata !32), !dbg !28 ; [debug line = 25:20] [debug variable = i]
  br label %1, !dbg !28                           ; [debug line = 25:20]
}

; [#uses=2]
define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

; [#uses=3]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=2]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=1]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=1]
define weak i32 @_ssdm_op_Read.ap_fifo.volatile.i32P(i32*) {
entry:
  %empty = call i32 @_autotb_FifoRead_i32(i32* %0) ; [#uses=1 type=i32]
  ret i32 %empty
}

; [#uses=1]
declare i32 @_autotb_FifoRead_i32(i32*)

!hls.encrypted.func = !{}
!llvm.map.gv = !{}

!0 = metadata !{metadata !1}
!1 = metadata !{i32 0, i32 31, metadata !2}
!2 = metadata !{metadata !3}
!3 = metadata !{metadata !"x", metadata !4, metadata !"int", i32 0, i32 31}
!4 = metadata !{metadata !5}
!5 = metadata !{i32 0, i32 0, i32 1}
!6 = metadata !{metadata !7}
!7 = metadata !{i32 0, i32 31, metadata !8}
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !"y", metadata !4, metadata !"int", i32 0, i32 31}
!10 = metadata !{i32 786689, metadata !11, metadata !"x", metadata !12, i32 16777222, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!11 = metadata !{i32 786478, i32 0, metadata !12, metadata !"modem", metadata !"modem", metadata !"_Z5modemPViS0_", metadata !12, i32 6, metadata !13, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32*, i32*)* @modem, null, null, metadata !18, i32 7} ; [ DW_TAG_subprogram ]
!12 = metadata !{i32 786473, metadata !"c/modem.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS", null} ; [ DW_TAG_file_type ]
!13 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !14, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!14 = metadata !{null, metadata !15, metadata !15}
!15 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !16} ; [ DW_TAG_pointer_type ]
!16 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_volatile_type ]
!17 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!18 = metadata !{metadata !19}
!19 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!20 = metadata !{i32 6, i32 26, metadata !11, null}
!21 = metadata !{i32 786689, metadata !11, metadata !"y", metadata !12, i32 33554438, metadata !15, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!22 = metadata !{i32 6, i32 43, metadata !11, null}
!23 = metadata !{i32 8, i32 1, metadata !24, null}
!24 = metadata !{i32 786443, metadata !11, i32 7, i32 1, metadata !12, i32 0} ; [ DW_TAG_lexical_block ]
!25 = metadata !{i32 9, i32 1, metadata !24, null}
!26 = metadata !{i32 25, i32 6, metadata !27, null}
!27 = metadata !{i32 786443, metadata !24, i32 25, i32 2, metadata !12, i32 1} ; [ DW_TAG_lexical_block ]
!28 = metadata !{i32 25, i32 20, metadata !27, null}
!29 = metadata !{i32 67, i32 1, metadata !24, null}
!30 = metadata !{i32 26, i32 3, metadata !31, null}
!31 = metadata !{i32 786443, metadata !27, i32 25, i32 24, metadata !12, i32 2} ; [ DW_TAG_lexical_block ]
!32 = metadata !{i32 786688, metadata !24, metadata !"i", metadata !12, i32 22, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!33 = metadata !{i32 786454, null, metadata !"uint8", metadata !12, i32 10, i64 0, i64 0, i64 0, i32 0, metadata !34} ; [ DW_TAG_typedef ]
!34 = metadata !{i32 786468, null, metadata !"uint8", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
