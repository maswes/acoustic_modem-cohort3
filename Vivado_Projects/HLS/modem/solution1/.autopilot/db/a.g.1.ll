; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/modem/solution1/.autopilot/db/a.g.1.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@.str = private unnamed_addr constant [8 x i8] c"ap_fifo\00", align 1 ; [#uses=1 type=[8 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@preamp_seq = internal unnamed_addr constant [24 x i18] [i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1, i18 1, i18 -1], align 16 ; [#uses=1 type=[24 x i18]*]
@baker_seq = internal unnamed_addr constant [13 x i18] [i18 1, i18 1, i18 1, i18 1, i18 1, i18 -1, i18 -1, i18 1, i18 1, i18 -1, i18 1, i18 -1, i18 1], align 16 ; [#uses=1 type=[13 x i18]*]
@str = internal constant [6 x i8] c"modem\00"     ; [#uses=1 type=[6 x i8]*]

; [#uses=0]
define void @modem(i32* %x, i32* %y) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecTopModule([6 x i8]* @str) nounwind
  %payload_data = alloca [80 x i18], align 16     ; [#uses=12 type=[80 x i18]*]
  %payload_data_re = alloca [40 x i18], align 16  ; [#uses=2 type=[40 x i18]*]
  %payload_data_im = alloca [40 x i18], align 16  ; [#uses=2 type=[40 x i18]*]
  %data_in = alloca [8 x i8], align 1             ; [#uses=2 type=[8 x i8]*]
  call void @llvm.dbg.value(metadata !{i32* %x}, i64 0, metadata !30), !dbg !31 ; [debug line = 6:26] [debug variable = x]
  call void @llvm.dbg.value(metadata !{i32* %y}, i64 0, metadata !32), !dbg !33 ; [debug line = 6:43] [debug variable = y]
  call void (...)* @_ssdm_op_SpecInterface(i32* %y, i8* getelementptr inbounds ([8 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !34 ; [debug line = 8:1]
  call void (...)* @_ssdm_op_SpecInterface(i32* %x, i8* getelementptr inbounds ([8 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !36 ; [debug line = 9:1]
  call void @llvm.dbg.declare(metadata !{[80 x i18]* %payload_data}, metadata !37), !dbg !41 ; [debug line = 15:13] [debug variable = payload_data]
  call void @llvm.dbg.declare(metadata !{[40 x i18]* %payload_data_re}, metadata !42), !dbg !46 ; [debug line = 16:13] [debug variable = payload_data_re]
  call void @llvm.dbg.declare(metadata !{[40 x i18]* %payload_data_im}, metadata !47), !dbg !48 ; [debug line = 17:13] [debug variable = payload_data_im]
  call void @llvm.dbg.declare(metadata !{[8 x i8]* %data_in}, metadata !49), !dbg !55 ; [debug line = 21:8] [debug variable = data_in]
  br label %1, !dbg !56                           ; [debug line = 25:6]

; <label>:1                                       ; preds = %2, %0
  %i = phi i8 [ 0, %0 ], [ %i.6, %2 ]             ; [#uses=3 type=i8]
  %exitcond5 = icmp eq i8 %i, 8, !dbg !56         ; [#uses=1 type=i1] [debug line = 25:6]
  br i1 %exitcond5, label %.preheader9.preheader, label %2, !dbg !56 ; [debug line = 25:6]

.preheader9.preheader:                            ; preds = %1
  br label %.preheader9, !dbg !58                 ; [debug line = 30:6]

; <label>:2                                       ; preds = %1
  %x.load = load volatile i32* %x, align 4, !dbg !60 ; [#uses=1 type=i32] [debug line = 26:3]
  %tmp = trunc i32 %x.load to i8, !dbg !60        ; [#uses=1 type=i8] [debug line = 26:3]
  %tmp.1 = zext i8 %i to i64, !dbg !60            ; [#uses=1 type=i64] [debug line = 26:3]
  %data_in.addr = getelementptr inbounds [8 x i8]* %data_in, i64 0, i64 %tmp.1, !dbg !60 ; [#uses=1 type=i8*] [debug line = 26:3]
  store i8 %tmp, i8* %data_in.addr, align 1, !dbg !60 ; [debug line = 26:3]
  %i.6 = add i8 %i, 1, !dbg !62                   ; [#uses=1 type=i8] [debug line = 25:20]
  call void @llvm.dbg.value(metadata !{i8 %i.6}, i64 0, metadata !63), !dbg !62 ; [debug line = 25:20] [debug variable = i]
  br label %1, !dbg !62                           ; [debug line = 25:20]

.preheader9:                                      ; preds = %3, %.preheader9.preheader
  %i.1 = phi i8 [ %i.7, %3 ], [ 0, %.preheader9.preheader ] ; [#uses=4 type=i8]
  %exitcond4 = icmp eq i8 %i.1, 8, !dbg !58       ; [#uses=1 type=i1] [debug line = 30:6]
  br i1 %exitcond4, label %.preheader8.preheader, label %3, !dbg !58 ; [debug line = 30:6]

.preheader8.preheader:                            ; preds = %.preheader9
  br label %.preheader8, !dbg !64                 ; [debug line = 43:6]

; <label>:3                                       ; preds = %.preheader9
  %tmp.3 = zext i8 %i.1 to i32, !dbg !66          ; [#uses=1 type=i32] [debug line = 31:2]
  %tmp.4 = mul nsw i32 %tmp.3, 10, !dbg !66       ; [#uses=10 type=i32] [debug line = 31:2]
  %tmp.5 = zext i32 %tmp.4 to i64, !dbg !66       ; [#uses=1 type=i64] [debug line = 31:2]
  %payload_data.addr = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.5, !dbg !66 ; [#uses=1 type=i18*] [debug line = 31:2]
  store i18 0, i18* %payload_data.addr, align 8, !dbg !66 ; [debug line = 31:2]
  %tmp.6 = zext i8 %i.1 to i64, !dbg !68          ; [#uses=1 type=i64] [debug line = 32:2]
  %data_in.addr.1 = getelementptr inbounds [8 x i8]* %data_in, i64 0, i64 %tmp.6, !dbg !68 ; [#uses=1 type=i8*] [debug line = 32:2]
  %data_in.load = load i8* %data_in.addr.1, align 1, !dbg !68 ; [#uses=9 type=i8] [debug line = 32:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.7 = zext i8 %data_in.load to i18, !dbg !68 ; [#uses=8 type=i18] [debug line = 32:2]
  %tmp.8 = lshr i18 %tmp.7, 7, !dbg !68           ; [#uses=1 type=i18] [debug line = 32:2]
  %tmp.9 = or i32 %tmp.4, 1, !dbg !68             ; [#uses=1 type=i32] [debug line = 32:2]
  %tmp.10 = zext i32 %tmp.9 to i64, !dbg !68      ; [#uses=1 type=i64] [debug line = 32:2]
  %payload_data.addr.1 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.10, !dbg !68 ; [#uses=1 type=i18*] [debug line = 32:2]
  store i18 %tmp.8, i18* %payload_data.addr.1, align 4, !dbg !68 ; [debug line = 32:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.11 = lshr i18 %tmp.7, 6, !dbg !69          ; [#uses=1 type=i18] [debug line = 33:2]
  %tmp.12 = and i18 %tmp.11, 1, !dbg !69          ; [#uses=1 type=i18] [debug line = 33:2]
  %tmp.13 = add nsw i32 %tmp.4, 2, !dbg !69       ; [#uses=1 type=i32] [debug line = 33:2]
  %tmp.14 = zext i32 %tmp.13 to i64, !dbg !69     ; [#uses=1 type=i64] [debug line = 33:2]
  %payload_data.addr.2 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.14, !dbg !69 ; [#uses=1 type=i18*] [debug line = 33:2]
  store i18 %tmp.12, i18* %payload_data.addr.2, align 8, !dbg !69 ; [debug line = 33:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.15 = lshr i18 %tmp.7, 5, !dbg !70          ; [#uses=1 type=i18] [debug line = 34:2]
  %tmp.16 = and i18 %tmp.15, 1, !dbg !70          ; [#uses=1 type=i18] [debug line = 34:2]
  %tmp.17 = add nsw i32 %tmp.4, 3, !dbg !70       ; [#uses=1 type=i32] [debug line = 34:2]
  %tmp.18 = zext i32 %tmp.17 to i64, !dbg !70     ; [#uses=1 type=i64] [debug line = 34:2]
  %payload_data.addr.3 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.18, !dbg !70 ; [#uses=1 type=i18*] [debug line = 34:2]
  store i18 %tmp.16, i18* %payload_data.addr.3, align 4, !dbg !70 ; [debug line = 34:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.19 = lshr i18 %tmp.7, 4, !dbg !71          ; [#uses=1 type=i18] [debug line = 35:2]
  %tmp.20 = and i18 %tmp.19, 1, !dbg !71          ; [#uses=1 type=i18] [debug line = 35:2]
  %tmp.21 = add nsw i32 %tmp.4, 4, !dbg !71       ; [#uses=1 type=i32] [debug line = 35:2]
  %tmp.22 = zext i32 %tmp.21 to i64, !dbg !71     ; [#uses=1 type=i64] [debug line = 35:2]
  %payload_data.addr.4 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.22, !dbg !71 ; [#uses=1 type=i18*] [debug line = 35:2]
  store i18 %tmp.20, i18* %payload_data.addr.4, align 8, !dbg !71 ; [debug line = 35:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.23 = lshr i18 %tmp.7, 3, !dbg !72          ; [#uses=1 type=i18] [debug line = 36:2]
  %tmp.24 = and i18 %tmp.23, 1, !dbg !72          ; [#uses=1 type=i18] [debug line = 36:2]
  %tmp.25 = add nsw i32 %tmp.4, 5, !dbg !72       ; [#uses=1 type=i32] [debug line = 36:2]
  %tmp.26 = zext i32 %tmp.25 to i64, !dbg !72     ; [#uses=1 type=i64] [debug line = 36:2]
  %payload_data.addr.5 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.26, !dbg !72 ; [#uses=1 type=i18*] [debug line = 36:2]
  store i18 %tmp.24, i18* %payload_data.addr.5, align 4, !dbg !72 ; [debug line = 36:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.27 = lshr i18 %tmp.7, 2, !dbg !73          ; [#uses=1 type=i18] [debug line = 37:2]
  %tmp.28 = and i18 %tmp.27, 1, !dbg !73          ; [#uses=1 type=i18] [debug line = 37:2]
  %tmp.29 = add nsw i32 %tmp.4, 6, !dbg !73       ; [#uses=1 type=i32] [debug line = 37:2]
  %tmp.30 = zext i32 %tmp.29 to i64, !dbg !73     ; [#uses=1 type=i64] [debug line = 37:2]
  %payload_data.addr.6 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.30, !dbg !73 ; [#uses=1 type=i18*] [debug line = 37:2]
  store i18 %tmp.28, i18* %payload_data.addr.6, align 8, !dbg !73 ; [debug line = 37:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.31 = lshr i18 %tmp.7, 1, !dbg !74          ; [#uses=1 type=i18] [debug line = 38:2]
  %tmp.32 = and i18 %tmp.31, 1, !dbg !74          ; [#uses=1 type=i18] [debug line = 38:2]
  %tmp.33 = add nsw i32 %tmp.4, 7, !dbg !74       ; [#uses=1 type=i32] [debug line = 38:2]
  %tmp.34 = zext i32 %tmp.33 to i64, !dbg !74     ; [#uses=1 type=i64] [debug line = 38:2]
  %payload_data.addr.7 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.34, !dbg !74 ; [#uses=1 type=i18*] [debug line = 38:2]
  store i18 %tmp.32, i18* %payload_data.addr.7, align 4, !dbg !74 ; [debug line = 38:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i8 %data_in.load) nounwind
  %tmp.35 = and i18 %tmp.7, 1, !dbg !75           ; [#uses=1 type=i18] [debug line = 39:2]
  %tmp.36 = add nsw i32 %tmp.4, 8, !dbg !75       ; [#uses=1 type=i32] [debug line = 39:2]
  %tmp.37 = zext i32 %tmp.36 to i64, !dbg !75     ; [#uses=1 type=i64] [debug line = 39:2]
  %payload_data.addr.8 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.37, !dbg !75 ; [#uses=1 type=i18*] [debug line = 39:2]
  store i18 %tmp.35, i18* %payload_data.addr.8, align 8, !dbg !75 ; [debug line = 39:2]
  %tmp.38 = add nsw i32 %tmp.4, 9, !dbg !76       ; [#uses=1 type=i32] [debug line = 40:2]
  %tmp.39 = zext i32 %tmp.38 to i64, !dbg !76     ; [#uses=1 type=i64] [debug line = 40:2]
  %payload_data.addr.9 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.39, !dbg !76 ; [#uses=1 type=i18*] [debug line = 40:2]
  store i18 1, i18* %payload_data.addr.9, align 4, !dbg !76 ; [debug line = 40:2]
  %i.7 = add i8 %i.1, 1, !dbg !77                 ; [#uses=1 type=i8] [debug line = 30:20]
  call void @llvm.dbg.value(metadata !{i8 %i.7}, i64 0, metadata !63), !dbg !77 ; [debug line = 30:20] [debug variable = i]
  br label %.preheader9, !dbg !77                 ; [debug line = 30:20]

.preheader8:                                      ; preds = %4, %.preheader8.preheader
  %i.2 = phi i8 [ %i.8, %4 ], [ 0, %.preheader8.preheader ] ; [#uses=4 type=i8]
  %exitcond3 = icmp eq i8 %i.2, 40, !dbg !64      ; [#uses=1 type=i1] [debug line = 43:6]
  br i1 %exitcond3, label %.preheader7.preheader, label %4, !dbg !64 ; [debug line = 43:6]

.preheader7.preheader:                            ; preds = %.preheader8
  br label %.preheader7, !dbg !78                 ; [debug line = 50:6]

; <label>:4                                       ; preds = %.preheader8
  %tmp.41 = zext i8 %i.2 to i32, !dbg !80         ; [#uses=1 type=i32] [debug line = 44:3]
  %tmp.42 = shl nuw nsw i32 %tmp.41, 1, !dbg !80  ; [#uses=2 type=i32] [debug line = 44:3]
  %tmp.43 = zext i32 %tmp.42 to i64, !dbg !80     ; [#uses=1 type=i64] [debug line = 44:3]
  %payload_data.addr.10 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.43, !dbg !80 ; [#uses=1 type=i18*] [debug line = 44:3]
  %payload_data.load = load i18* %payload_data.addr.10, align 8, !dbg !80 ; [#uses=2 type=i18] [debug line = 44:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %payload_data.load) nounwind
  %tmp.44 = zext i8 %i.2 to i64, !dbg !80         ; [#uses=2 type=i64] [debug line = 44:3]
  %payload_data_re.addr = getelementptr inbounds [40 x i18]* %payload_data_re, i64 0, i64 %tmp.44, !dbg !80 ; [#uses=1 type=i18*] [debug line = 44:3]
  store i18 %payload_data.load, i18* %payload_data_re.addr, align 4, !dbg !80 ; [debug line = 44:3]
  %tmp.45 = or i32 %tmp.42, 1, !dbg !82           ; [#uses=1 type=i32] [debug line = 45:3]
  %tmp.46 = zext i32 %tmp.45 to i64, !dbg !82     ; [#uses=1 type=i64] [debug line = 45:3]
  %payload_data.addr.11 = getelementptr inbounds [80 x i18]* %payload_data, i64 0, i64 %tmp.46, !dbg !82 ; [#uses=1 type=i18*] [debug line = 45:3]
  %payload_data.load.1 = load i18* %payload_data.addr.11, align 4, !dbg !82 ; [#uses=2 type=i18] [debug line = 45:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %payload_data.load.1) nounwind
  %payload_data_im.addr = getelementptr inbounds [40 x i18]* %payload_data_im, i64 0, i64 %tmp.44, !dbg !82 ; [#uses=1 type=i18*] [debug line = 45:3]
  store i18 %payload_data.load.1, i18* %payload_data_im.addr, align 4, !dbg !82 ; [debug line = 45:3]
  %i.8 = add i8 %i.2, 1, !dbg !83                 ; [#uses=1 type=i8] [debug line = 43:21]
  call void @llvm.dbg.value(metadata !{i8 %i.8}, i64 0, metadata !63), !dbg !83 ; [debug line = 43:21] [debug variable = i]
  br label %.preheader8, !dbg !83                 ; [debug line = 43:21]

.preheader7:                                      ; preds = %5, %.preheader7.preheader
  %i.3 = phi i8 [ %i.9, %5 ], [ 0, %.preheader7.preheader ] ; [#uses=3 type=i8]
  %exitcond2 = icmp eq i8 %i.3, 24, !dbg !78      ; [#uses=1 type=i1] [debug line = 50:6]
  br i1 %exitcond2, label %.preheader6.preheader, label %5, !dbg !78 ; [debug line = 50:6]

.preheader6.preheader:                            ; preds = %.preheader7
  br label %.preheader6, !dbg !84                 ; [debug line = 55:6]

; <label>:5                                       ; preds = %.preheader7
  %tmp.48 = zext i8 %i.3 to i64, !dbg !86         ; [#uses=1 type=i64] [debug line = 51:3]
  %preamp_seq.addr = getelementptr inbounds [24 x i18]* @preamp_seq, i64 0, i64 %tmp.48, !dbg !86 ; [#uses=1 type=i18*] [debug line = 51:3]
  %preamp_seq.load = load i18* %preamp_seq.addr, align 4, !dbg !86 ; [#uses=2 type=i18] [debug line = 51:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %preamp_seq.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %preamp_seq.load) nounwind
  %i.9 = add i8 %i.3, 1, !dbg !88                 ; [#uses=1 type=i8] [debug line = 50:21]
  call void @llvm.dbg.value(metadata !{i8 %i.9}, i64 0, metadata !63), !dbg !88 ; [debug line = 50:21] [debug variable = i]
  br label %.preheader7, !dbg !88                 ; [debug line = 50:21]

.preheader6:                                      ; preds = %6, %.preheader6.preheader
  %i.4 = phi i8 [ %i.10, %6 ], [ 0, %.preheader6.preheader ] ; [#uses=3 type=i8]
  %exitcond1 = icmp eq i8 %i.4, 13, !dbg !84      ; [#uses=1 type=i1] [debug line = 55:6]
  br i1 %exitcond1, label %.preheader.preheader, label %6, !dbg !84 ; [debug line = 55:6]

.preheader.preheader:                             ; preds = %.preheader6
  br label %.preheader, !dbg !89                  ; [debug line = 60:6]

; <label>:6                                       ; preds = %.preheader6
  %tmp.50 = zext i8 %i.4 to i64, !dbg !91         ; [#uses=1 type=i64] [debug line = 56:3]
  %baker_seq.addr = getelementptr inbounds [13 x i18]* @baker_seq, i64 0, i64 %tmp.50, !dbg !91 ; [#uses=1 type=i18*] [debug line = 56:3]
  %baker_seq.load = load i18* %baker_seq.addr, align 4, !dbg !91 ; [#uses=2 type=i18] [debug line = 56:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %baker_seq.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %baker_seq.load) nounwind
  %i.10 = add i8 %i.4, 1, !dbg !93                ; [#uses=1 type=i8] [debug line = 55:21]
  call void @llvm.dbg.value(metadata !{i8 %i.10}, i64 0, metadata !63), !dbg !93 ; [debug line = 55:21] [debug variable = i]
  br label %.preheader6, !dbg !93                 ; [debug line = 55:21]

.preheader:                                       ; preds = %7, %.preheader.preheader
  %i.5 = phi i8 [ %i.11, %7 ], [ 0, %.preheader.preheader ] ; [#uses=3 type=i8]
  %exitcond = icmp eq i8 %i.5, 40, !dbg !89       ; [#uses=1 type=i1] [debug line = 60:6]
  br i1 %exitcond, label %8, label %7, !dbg !89   ; [debug line = 60:6]

; <label>:7                                       ; preds = %.preheader
  %tmp.52 = zext i8 %i.5 to i64, !dbg !94         ; [#uses=2 type=i64] [debug line = 61:3]
  %payload_data_re.addr.1 = getelementptr inbounds [40 x i18]* %payload_data_re, i64 0, i64 %tmp.52, !dbg !94 ; [#uses=1 type=i18*] [debug line = 61:3]
  %payload_data_re.load = load i18* %payload_data_re.addr.1, align 4, !dbg !94 ; [#uses=1 type=i18] [debug line = 61:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %payload_data_re.load) nounwind
  %payload_data_im.addr.1 = getelementptr inbounds [40 x i18]* %payload_data_im, i64 0, i64 %tmp.52, !dbg !96 ; [#uses=1 type=i18*] [debug line = 62:3]
  %payload_data_im.load = load i18* %payload_data_im.addr.1, align 4, !dbg !96 ; [#uses=1 type=i18] [debug line = 62:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(i18 %payload_data_im.load) nounwind
  %i.11 = add i8 %i.5, 1, !dbg !97                ; [#uses=1 type=i8] [debug line = 60:21]
  call void @llvm.dbg.value(metadata !{i8 %i.11}, i64 0, metadata !63), !dbg !97 ; [debug line = 60:21] [debug variable = i]
  br label %.preheader, !dbg !97                  ; [debug line = 60:21]

; <label>:8                                       ; preds = %.preheader
  ret void, !dbg !98                              ; [debug line = 67:1]
}

; [#uses=4]
declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

; [#uses=2]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=8]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=16]
declare void @_ssdm_SpecKeepArrayLoad(...)

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"C:/Capstone2015/Vivado_Projects/HLS/modem/solution1/.autopilot/db/modem.pragma.2.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !14} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"modem", metadata !"modem", metadata !"_Z5modemPViS0_", metadata !6, i32 6, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32*, i32*)* @modem, null, null, metadata !12, i32 7} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"c/modem.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{null, metadata !9, metadata !9}
!9 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !10} ; [ DW_TAG_pointer_type ]
!10 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_volatile_type ]
!11 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!12 = metadata !{metadata !13}
!13 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!14 = metadata !{metadata !15}
!15 = metadata !{metadata !16, metadata !24, metadata !28}
!16 = metadata !{i32 786484, i32 0, metadata !5, metadata !"preamp_seq", metadata !"preamp_seq", metadata !"", metadata !6, i32 13, metadata !17, i32 1, i32 1, [24 x i18]* @preamp_seq} ; [ DW_TAG_variable ]
!17 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 768, i64 32, i32 0, i32 0, metadata !18, metadata !22, i32 0, i32 0} ; [ DW_TAG_array_type ]
!18 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !19} ; [ DW_TAG_const_type ]
!19 = metadata !{i32 786454, null, metadata !"mix_data_t", metadata !6, i32 69, i64 0, i64 0, i64 0, i32 0, metadata !20} ; [ DW_TAG_typedef ]
!20 = metadata !{i32 786454, null, metadata !"int18", metadata !6, i32 20, i64 0, i64 0, i64 0, i32 0, metadata !21} ; [ DW_TAG_typedef ]
!21 = metadata !{i32 786468, null, metadata !"int18", null, i32 0, i64 18, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!22 = metadata !{metadata !23}
!23 = metadata !{i32 786465, i64 0, i64 23}       ; [ DW_TAG_subrange_type ]
!24 = metadata !{i32 786484, i32 0, metadata !5, metadata !"baker_seq", metadata !"baker_seq", metadata !"", metadata !6, i32 14, metadata !25, i32 1, i32 1, [13 x i18]* @baker_seq} ; [ DW_TAG_variable ]
!25 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 416, i64 32, i32 0, i32 0, metadata !18, metadata !26, i32 0, i32 0} ; [ DW_TAG_array_type ]
!26 = metadata !{metadata !27}
!27 = metadata !{i32 786465, i64 0, i64 12}       ; [ DW_TAG_subrange_type ]
!28 = metadata !{i32 786484, i32 0, null, metadata !"_sys_nerr", metadata !"_sys_nerr", metadata !"", metadata !29, i32 157, metadata !11, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!29 = metadata !{i32 786473, metadata !"C:/Xilinx/Vivado_HLS/2015.1/win64/tools/clang/bin/../lib/clang/3.1/../../../x86_64-w64-mingw32/include\5Cstdlib.h", metadata !"c:/Capstone2015/Vivado_Projects/HLS", null} ; [ DW_TAG_file_type ]
!30 = metadata !{i32 786689, metadata !5, metadata !"x", metadata !6, i32 16777222, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!31 = metadata !{i32 6, i32 26, metadata !5, null}
!32 = metadata !{i32 786689, metadata !5, metadata !"y", metadata !6, i32 33554438, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!33 = metadata !{i32 6, i32 43, metadata !5, null}
!34 = metadata !{i32 8, i32 1, metadata !35, null}
!35 = metadata !{i32 786443, metadata !5, i32 7, i32 1, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!36 = metadata !{i32 9, i32 1, metadata !35, null}
!37 = metadata !{i32 786688, metadata !35, metadata !"payload_data", metadata !6, i32 15, metadata !38, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!38 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 2560, i64 32, i32 0, i32 0, metadata !19, metadata !39, i32 0, i32 0} ; [ DW_TAG_array_type ]
!39 = metadata !{metadata !40}
!40 = metadata !{i32 786465, i64 0, i64 79}       ; [ DW_TAG_subrange_type ]
!41 = metadata !{i32 15, i32 13, metadata !35, null}
!42 = metadata !{i32 786688, metadata !35, metadata !"payload_data_re", metadata !6, i32 16, metadata !43, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!43 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1280, i64 32, i32 0, i32 0, metadata !19, metadata !44, i32 0, i32 0} ; [ DW_TAG_array_type ]
!44 = metadata !{metadata !45}
!45 = metadata !{i32 786465, i64 0, i64 39}       ; [ DW_TAG_subrange_type ]
!46 = metadata !{i32 16, i32 13, metadata !35, null}
!47 = metadata !{i32 786688, metadata !35, metadata !"payload_data_im", metadata !6, i32 17, metadata !43, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!48 = metadata !{i32 17, i32 13, metadata !35, null}
!49 = metadata !{i32 786688, metadata !35, metadata !"data_in", metadata !6, i32 21, metadata !50, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!50 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 64, i64 8, i32 0, i32 0, metadata !51, metadata !53, i32 0, i32 0} ; [ DW_TAG_array_type ]
!51 = metadata !{i32 786454, null, metadata !"uint8", metadata !6, i32 10, i64 0, i64 0, i64 0, i32 0, metadata !52} ; [ DW_TAG_typedef ]
!52 = metadata !{i32 786468, null, metadata !"uint8", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!53 = metadata !{metadata !54}
!54 = metadata !{i32 786465, i64 0, i64 7}        ; [ DW_TAG_subrange_type ]
!55 = metadata !{i32 21, i32 8, metadata !35, null}
!56 = metadata !{i32 25, i32 6, metadata !57, null}
!57 = metadata !{i32 786443, metadata !35, i32 25, i32 2, metadata !6, i32 1} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 30, i32 6, metadata !59, null}
!59 = metadata !{i32 786443, metadata !35, i32 30, i32 2, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!60 = metadata !{i32 26, i32 3, metadata !61, null}
!61 = metadata !{i32 786443, metadata !57, i32 25, i32 24, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!62 = metadata !{i32 25, i32 20, metadata !57, null}
!63 = metadata !{i32 786688, metadata !35, metadata !"i", metadata !6, i32 22, metadata !51, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!64 = metadata !{i32 43, i32 6, metadata !65, null}
!65 = metadata !{i32 786443, metadata !35, i32 43, i32 2, metadata !6, i32 5} ; [ DW_TAG_lexical_block ]
!66 = metadata !{i32 31, i32 2, metadata !67, null}
!67 = metadata !{i32 786443, metadata !59, i32 30, i32 24, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!68 = metadata !{i32 32, i32 2, metadata !67, null}
!69 = metadata !{i32 33, i32 2, metadata !67, null}
!70 = metadata !{i32 34, i32 2, metadata !67, null}
!71 = metadata !{i32 35, i32 2, metadata !67, null}
!72 = metadata !{i32 36, i32 2, metadata !67, null}
!73 = metadata !{i32 37, i32 2, metadata !67, null}
!74 = metadata !{i32 38, i32 2, metadata !67, null}
!75 = metadata !{i32 39, i32 2, metadata !67, null}
!76 = metadata !{i32 40, i32 2, metadata !67, null}
!77 = metadata !{i32 30, i32 20, metadata !59, null}
!78 = metadata !{i32 50, i32 6, metadata !79, null}
!79 = metadata !{i32 786443, metadata !35, i32 50, i32 2, metadata !6, i32 7} ; [ DW_TAG_lexical_block ]
!80 = metadata !{i32 44, i32 3, metadata !81, null}
!81 = metadata !{i32 786443, metadata !65, i32 43, i32 25, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!82 = metadata !{i32 45, i32 3, metadata !81, null}
!83 = metadata !{i32 43, i32 21, metadata !65, null}
!84 = metadata !{i32 55, i32 6, metadata !85, null}
!85 = metadata !{i32 786443, metadata !35, i32 55, i32 2, metadata !6, i32 9} ; [ DW_TAG_lexical_block ]
!86 = metadata !{i32 51, i32 3, metadata !87, null}
!87 = metadata !{i32 786443, metadata !79, i32 50, i32 25, metadata !6, i32 8} ; [ DW_TAG_lexical_block ]
!88 = metadata !{i32 50, i32 21, metadata !79, null}
!89 = metadata !{i32 60, i32 6, metadata !90, null}
!90 = metadata !{i32 786443, metadata !35, i32 60, i32 2, metadata !6, i32 11} ; [ DW_TAG_lexical_block ]
!91 = metadata !{i32 56, i32 3, metadata !92, null}
!92 = metadata !{i32 786443, metadata !85, i32 55, i32 25, metadata !6, i32 10} ; [ DW_TAG_lexical_block ]
!93 = metadata !{i32 55, i32 21, metadata !85, null}
!94 = metadata !{i32 61, i32 3, metadata !95, null}
!95 = metadata !{i32 786443, metadata !90, i32 60, i32 25, metadata !6, i32 12} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 62, i32 3, metadata !95, null}
!97 = metadata !{i32 60, i32 21, metadata !90, null}
!98 = metadata !{i32 67, i32 1, metadata !35, null}
