#include "hls_design_meta.h"
const Port_Property HLS_Design_Meta::port_props[]={
	Port_Property("ap_clk", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_rst", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_start", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_done", 1, hls_out, -1, "", "", 1),
	Port_Property("ap_idle", 1, hls_out, -1, "", "", 1),
	Port_Property("ap_ready", 1, hls_out, -1, "", "", 1),
	Port_Property("x_dout", 32, hls_in, 0, "ap_fifo", "fifo_data", 8),
	Port_Property("x_empty_n", 1, hls_in, 0, "ap_fifo", "fifo_status", 8),
	Port_Property("x_read", 1, hls_out, 0, "ap_fifo", "fifo_update", 8),
	Port_Property("y_dout", 32, hls_in, 1, "ap_fifo", "fifo_data", 0),
	Port_Property("y_empty_n", 1, hls_in, 1, "ap_fifo", "fifo_status", 0),
	Port_Property("y_read", 1, hls_out, 1, "ap_fifo", "fifo_update", 0),
};
const char* HLS_Design_Meta::dut_name = "modem";
