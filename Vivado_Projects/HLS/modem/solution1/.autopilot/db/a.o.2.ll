; ModuleID = 'C:/Capstone2015/Vivado_Projects/HLS/modem/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@.str = private unnamed_addr constant [8 x i8] c"ap_fifo\00", align 1 ; [#uses=2 type=[8 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=6 type=[1 x i8]*]
@str = internal constant [6 x i8] c"modem\00"     ; [#uses=1 type=[6 x i8]*]

; [#uses=0]
define void @modem(i32* %x, i32* %y) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %x) nounwind, !map !26
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !32
  call void (...)* @_ssdm_op_SpecTopModule([6 x i8]* @str) nounwind
  call void @llvm.dbg.value(metadata !{i32* %x}, i64 0, metadata !36), !dbg !37 ; [debug line = 6:26] [debug variable = x]
  call void @llvm.dbg.value(metadata !{i32* %y}, i64 0, metadata !38), !dbg !39 ; [debug line = 6:43] [debug variable = y]
  call void (...)* @_ssdm_op_SpecInterface(i32* %y, [8 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !40 ; [debug line = 8:1]
  call void (...)* @_ssdm_op_SpecInterface(i32* %x, [8 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !42 ; [debug line = 9:1]
  br label %1, !dbg !43                           ; [debug line = 25:6]

; <label>:1                                       ; preds = %3, %0
  %i = phi i4 [ 0, %0 ], [ %i.1, %3 ]             ; [#uses=2 type=i4]
  %exitcond5 = icmp eq i4 %i, -8, !dbg !43        ; [#uses=1 type=i1] [debug line = 25:6]
  %2 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 8, i64 8, i64 8) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond5, label %.preheader9.preheader_ifconv, label %3, !dbg !43 ; [debug line = 25:6]

.preheader9.preheader_ifconv:                     ; preds = %1
  ret void, !dbg !45                              ; [debug line = 67:1]

; <label>:3                                       ; preds = %1
  %x.load = load volatile i32* %x, align 4, !dbg !46 ; [#uses=0 type=i32] [debug line = 26:3]
  %i.1 = add i4 %i, 1, !dbg !48                   ; [#uses=1 type=i4] [debug line = 25:20]
  call void @llvm.dbg.value(metadata !{i4 %i.1}, i64 0, metadata !49), !dbg !48 ; [debug line = 25:20] [debug variable = i]
  br label %1, !dbg !48                           ; [debug line = 25:20]
}

; [#uses=2]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=3]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=2]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=1]
declare i32 @_ssdm_op_SpecLoopTripCount(...)

!hls.encrypted.func = !{}
!llvm.map.gv = !{}
!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"C:/Capstone2015/Vivado_Projects/HLS/modem/solution1/.autopilot/db/modem.pragma.2.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, null, null, null, metadata !1} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{metadata !3, metadata !20, metadata !24}
!3 = metadata !{i32 786484, i32 0, metadata !4, metadata !"baker_seq", metadata !"baker_seq", metadata !"", metadata !5, i32 14, metadata !13, i32 1, i32 1, null} ; [ DW_TAG_variable ]
!4 = metadata !{i32 786478, i32 0, metadata !5, metadata !"modem", metadata !"modem", metadata !"_Z5modemPViS0_", metadata !5, i32 6, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32*, i32*)* @modem, null, null, metadata !11, i32 7} ; [ DW_TAG_subprogram ]
!5 = metadata !{i32 786473, metadata !"c/modem.cpp", metadata !"c:/Capstone2015/Vivado_Projects/HLS", null} ; [ DW_TAG_file_type ]
!6 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!7 = metadata !{null, metadata !8, metadata !8}
!8 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !9} ; [ DW_TAG_pointer_type ]
!9 = metadata !{i32 786485, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !10} ; [ DW_TAG_volatile_type ]
!10 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!11 = metadata !{metadata !12}
!12 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!13 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 416, i64 32, i32 0, i32 0, metadata !14, metadata !18, i32 0, i32 0} ; [ DW_TAG_array_type ]
!14 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !15} ; [ DW_TAG_const_type ]
!15 = metadata !{i32 786454, null, metadata !"mix_data_t", metadata !5, i32 69, i64 0, i64 0, i64 0, i32 0, metadata !16} ; [ DW_TAG_typedef ]
!16 = metadata !{i32 786454, null, metadata !"int18", metadata !5, i32 20, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_typedef ]
!17 = metadata !{i32 786468, null, metadata !"int18", null, i32 0, i64 18, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!18 = metadata !{metadata !19}
!19 = metadata !{i32 786465, i64 0, i64 12}       ; [ DW_TAG_subrange_type ]
!20 = metadata !{i32 786484, i32 0, metadata !4, metadata !"preamp_seq", metadata !"preamp_seq", metadata !"", metadata !5, i32 13, metadata !21, i32 1, i32 1, null} ; [ DW_TAG_variable ]
!21 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 768, i64 32, i32 0, i32 0, metadata !14, metadata !22, i32 0, i32 0} ; [ DW_TAG_array_type ]
!22 = metadata !{metadata !23}
!23 = metadata !{i32 786465, i64 0, i64 23}       ; [ DW_TAG_subrange_type ]
!24 = metadata !{i32 786484, i32 0, null, metadata !"_sys_nerr", metadata !"_sys_nerr", metadata !"", metadata !25, i32 157, metadata !10, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!25 = metadata !{i32 786473, metadata !"C:/Xilinx/Vivado_HLS/2015.1/win64/tools/clang/bin/../lib/clang/3.1/../../../x86_64-w64-mingw32/include\5Cstdlib.h", metadata !"c:/Capstone2015/Vivado_Projects/HLS", null} ; [ DW_TAG_file_type ]
!26 = metadata !{metadata !27}
!27 = metadata !{i32 0, i32 31, metadata !28}
!28 = metadata !{metadata !29}
!29 = metadata !{metadata !"x", metadata !30, metadata !"int", i32 0, i32 31}
!30 = metadata !{metadata !31}
!31 = metadata !{i32 0, i32 0, i32 1}
!32 = metadata !{metadata !33}
!33 = metadata !{i32 0, i32 31, metadata !34}
!34 = metadata !{metadata !35}
!35 = metadata !{metadata !"y", metadata !30, metadata !"int", i32 0, i32 31}
!36 = metadata !{i32 786689, metadata !4, metadata !"x", metadata !5, i32 16777222, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!37 = metadata !{i32 6, i32 26, metadata !4, null}
!38 = metadata !{i32 786689, metadata !4, metadata !"y", metadata !5, i32 33554438, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!39 = metadata !{i32 6, i32 43, metadata !4, null}
!40 = metadata !{i32 8, i32 1, metadata !41, null}
!41 = metadata !{i32 786443, metadata !4, i32 7, i32 1, metadata !5, i32 0} ; [ DW_TAG_lexical_block ]
!42 = metadata !{i32 9, i32 1, metadata !41, null}
!43 = metadata !{i32 25, i32 6, metadata !44, null}
!44 = metadata !{i32 786443, metadata !41, i32 25, i32 2, metadata !5, i32 1} ; [ DW_TAG_lexical_block ]
!45 = metadata !{i32 67, i32 1, metadata !41, null}
!46 = metadata !{i32 26, i32 3, metadata !47, null}
!47 = metadata !{i32 786443, metadata !44, i32 25, i32 24, metadata !5, i32 2} ; [ DW_TAG_lexical_block ]
!48 = metadata !{i32 25, i32 20, metadata !44, null}
!49 = metadata !{i32 786688, metadata !41, metadata !"i", metadata !5, i32 22, metadata !50, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!50 = metadata !{i32 786454, null, metadata !"uint8", metadata !5, i32 10, i64 0, i64 0, i64 0, i32 0, metadata !51} ; [ DW_TAG_typedef ]
!51 = metadata !{i32 786468, null, metadata !"uint8", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
