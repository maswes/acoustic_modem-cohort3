#include "fsk.h"

void freq_mux (
    uint1	*ser_dat_in,
    uint16	*freq_val_in,
    uint16	*freq_val_out,
    uint1	*rst_ctrl_out
  )
{
  uint16 freq_center_in;
  uint1 serial_in;
 // static int loop_cnt1;
 // bool	send_zero_done = 0;
 // bool	send_one_done = 0;

  freq_center_in = *freq_val_in;
  serial_in	= *ser_dat_in;

  *freq_val_out = freq_center_in + serial_in*2 + 2;
  *rst_ctrl_out = 0;
  /*
  while(1){
	  if(ser_dat_in == 1){
		  loop_cnt1 = 0;
		  send_one_done = 0;
		  while(send_one_done==0){
#pragma HLS PROTOCOL
			  if(loop_cnt1 < 2){
				  *rst_ctrl_out = 0;
				  *freq_val_out = 0;
			  }
			  else if(loop_cnt1 >=2 && loop_cnt1 <= 4){
				  *freq_val_out = 0;
				  *rst_ctrl_out = 1;
			  }
			  else if(loop_cnt1 > 4 && loop_cnt1 < 100000){
				  *freq_val_out = freq_center_in + 4;
				  *rst_ctrl_out = 0;
			  }
			  else if (loop_cnt1 >= 100000){
				  *freq_val_out = 0;
				  *rst_ctrl_out = 0;
				  loop_cnt1 = 100000;
			  }
			  if(ser_dat_in == 0){
				  loop_cnt1 = 0;
				  send_one_done = 1;
			  }
			  loop_cnt1++;
		  }
	  }

	  if(ser_dat_in == 0){
		  loop_cnt1 = 0;
		  send_zero_done = 0;
		  while(send_zero_done == 0){
#pragma HLS UNROLL
			  if(loop_cnt1 < 2){
				  *rst_ctrl_out = 0;
				  *freq_val_out = 0;
			  }
			  else if(loop_cnt1 >=2 && loop_cnt1 <= 4){
				  *freq_val_out = 0;
				  *rst_ctrl_out = 1;
			  }
			  else if(loop_cnt1 > 4 && loop_cnt1 < 100000){
				  *freq_val_out = freq_center_in + 2;
				  *rst_ctrl_out = 0;
			  }
			  else if (loop_cnt1 >= 100000){
				  *freq_val_out = 0;
				  *rst_ctrl_out = 0;
				  loop_cnt1 = 100000;
			  }
			  if(ser_dat_in == 1){
				  loop_cnt1 = 0;
				  send_zero_done = 1;
			  }
			  loop_cnt1++;
		  }
	  }

  }
  */
}
