#include "duc.h"

void interface(volatile int *x, volatile int *y)
{
#pragma HLS INTERFACE ap_fifo port=y
#pragma HLS INTERFACE ap_fifo port=x
	int in_value;
	
	in_value = *(x++);
	*(y++) = in_value;
}

