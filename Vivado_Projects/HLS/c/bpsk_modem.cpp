#include "acomm.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//void bpsk_modem(volatile int *x, volatile int *y, uint12 *tx_sample, uint12 *rx_sample)
void bpsk_modem(uint12 *tx_sample, uint12 *rx_sample)
//void bpsk_modem(volatile int x[8], volatile int y[8], uint12 *tx_sample)
{
//#pragma HLS INTERFACE axis port=y
//#pragma HLS INTERFACE axis port=x

	const sample_t sine_table[128] = {
	#include "sine_table.h"
	};

	bit_t tx_frame[10];	//  24+13+80 = 117;
	uint8	i,j;
	uint8	Din;
	uint10	payload_data[10];

//	int loop_data;
//	for(i=0;i<8;i++){
//		y[i] = x[i];
//	}

	//int loop_data;
	//for(i=0;i<8;i++){
	//	*y = *x;
	//}
	//*y = (uint12)*rx_sample;

while(1){
	Din = 0x05;//uint8)x[2];
	//*y = (int)Din;

	payload_data[0] = (bit_t) 0;			// Adding start bit
	payload_data[1] = (bit_t) ((Din >> 7) & 0x01);
	payload_data[2] = (bit_t) ((Din >> 6) & 0x01);
	payload_data[3] = (bit_t) ((Din >> 5) & 0x01);
	payload_data[4] = (bit_t) ((Din >> 4) & 0x01);
	payload_data[5] = (bit_t) ((Din >> 3) & 0x01);
	payload_data[6] = (bit_t) ((Din >> 2) & 0x01);
	payload_data[7] = (bit_t) ((Din >> 1) & 0x01);
	payload_data[8] = (bit_t) ((Din >> 0) & 0x01);
	payload_data[9] = (bit_t) 1;		// Adding stop bit

	for(i = 0; i < 10; i++){
		tx_frame[i] = payload_data[i];
	}

		// Start transmitting
		bool tx_frame_done = 0;
		bool tx_symbol_done = 0;
		bool tx_sample_done = 0;
		int	clk_counter_1 = 0;
		int clk_counter_2 = 0;
		int symbol_idx;
		int bit_idx = 0;
		int sample_idx;
		bit_t tx_bit;
		uint12 sample_value;
	//	for(symbol_idx=0; symbol_idx<117; symbol_idx++){
	//		tx_bit = tx_frame[symbol_idx];

		while(tx_frame_done == 0){
			//if(clk_counter_1 == 6144){
				tx_bit = tx_frame[bit_idx];
				for(i=0;i<3;i++){
					for(j=0;j<64;j++){
						tx_sample_done = 0;
						while(tx_sample_done == 0){
							if(clk_counter_2 == 32){
								*tx_sample = sine_table[j + tx_bit*32];
								clk_counter_2 = 0;
								tx_sample_done = 1;
							}
							clk_counter_2++;
						}
					}
				}
				if(bit_idx >= 10)
					tx_frame_done = 1;

				bit_idx++;
				clk_counter_1 = 0;
			}
		//	clk_counter_1++;
		//}
	}

} //End function

/*
void bpsk_modem(volatile int *x, volatile int *y, uint12 *tx_sample)
{
#pragma HLS INTERFACE ap_fifo port=y
#pragma HLS INTERFACE ap_fifo port=x

	const sample_t sine_table[128] = {
	#include "sine_table.h"
	};

	const bit_t preamp_seq[24]={1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0};
	const bit_t baker_seq[13]={1,1,1,1,1,0,0,1,1,0,1,0,1};
	bit_t payload_data[80]; //	8 bytes data + (1 start bit + 1 stop bit) * 8
	bit_t tx_frame[117];	//  24+13+80 = 117;

	uint8	data_in[8];
	uint8	i,j;
	uint8	Din;


		// Read in 8 bytes from tx fifo
		for(i = 0; i < 8; i++){
			data_in[i] = (uint8)*x;
			*y = (int)data_in[i];
		}

		// Convert byte to bit
		for(i = 0; i < 8; i++){
		payload_data[i*10+0] = (bit_t) 0;			// Adding start bit
		payload_data[i*10+1] = (bit_t) ((data_in[i] >> 7) & 0x01);
		payload_data[i*10+2] = (bit_t) ((data_in[i] >> 6) & 0x01);
		payload_data[i*10+3] = (bit_t) ((data_in[i] >> 5) & 0x01);
		payload_data[i*10+4] = (bit_t) ((data_in[i] >> 4) & 0x01);
		payload_data[i*10+5] = (bit_t) ((data_in[i] >> 3) & 0x01);
		payload_data[i*10+6] = (bit_t) ((data_in[i] >> 2) & 0x01);
		payload_data[i*10+7] = (bit_t) ((data_in[i] >> 1) & 0x01);
		payload_data[i*10+8] = (data_in[i] >> 0) & 0x01;
		payload_data[i*10+9] = (bit_t) 1;		// Adding stop bit
		}

		// Assemble the transmitting frame

		for(i = 0; i < 24; i++){
			tx_frame[i] = preamp_seq[i];
		}

		for(i = 0; i < 13; i++){
			tx_frame[i+24] = baker_seq[i];
		}

		for(i = 0; i < 40; i++){
			tx_frame[i+24+13] = payload_data[i];

		}

		// Start transmitting
		bool tx_frame_done = 0;
		bool tx_symbol_done = 0;
		bool tx_sample_done = 0;
		int	clk_counter_1 = 0;
		int clk_counter_2 = 0;
		int symbol_idx;
		int bit_idx = 0;
		int sample_idx;
		bit_t tx_bit;
		uint12 sample_value;
	//	for(symbol_idx=0; symbol_idx<117; symbol_idx++){
	//		tx_bit = tx_frame[symbol_idx];

		while(tx_frame_done == 0){
			if(clk_counter_1 == 6144){
				tx_bit = tx_frame[bit_idx];
				for(i=0;i<3;i++){
					for(j=0;j<64;j++){
						tx_sample_done = 0;
						while(tx_sample_done == 0){
							if(clk_counter_2 == 32){
								*tx_sample = sine_table[j + tx_bit*32];
								clk_counter_2 = 0;
								tx_sample_done = 1;
							}
							clk_counter_2++;
						}
					}
				}
				if(bit_idx >= 117)
					tx_frame_done = 1;

				bit_idx++;
				clk_counter_1 = 0;
			}
			clk_counter_1++;
		}

} //End function
*/
