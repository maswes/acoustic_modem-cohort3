#include "fsk.h"
#include "math.h"
void compare_val(int val_a, int val_b, uint1 *cmp_out)
{
	if(abs(val_b) > val_a)
		*cmp_out = 1;
	else
		*cmp_out = 0;
}
