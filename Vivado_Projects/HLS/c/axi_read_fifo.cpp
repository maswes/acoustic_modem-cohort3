#include "acomm.h"

void axi_read_fifo(volatile int *x, volatile int *y)
{
#pragma HLS INTERFACE s_axilite port=y
#pragma HLS INTERFACE ap_fifo port=x
	int in_value;

	in_value = *x;
	*y = in_value;
}
