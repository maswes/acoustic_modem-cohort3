#ifndef _DDS_H_
#define _DDS_H_
#include "ap_cint.h"
#include <stdio.h>
#include <stdlib.h>

#define	N	16
#define	P	5
#define M	16
#define B	11
#define FREQ	6628
//#define FREQ	1657
//#define FREQ	0
/*
N	Accumulator
P	Phase
M	Output
B	N-P
Frequency
=========
Fomin	= (Fs/(2^N))
Fo	= (Fs/(2^N))*Dacc
Fomax	= (Fs/2^(N-P))

Phase Quantization
==================
SFDRest	= 6.02P - 3.92dB
P 	= (SFDR+3.92)/6.02

Amplitude Quantization
======================
SNRest	= -6.02M -1.76db
Best performance (-SFDR<SNR): P = M+1
*/
#define	int(x)		int_(x)
#define int_(x)		int ## x
#define uint(x)		uint_(x)
#define uint_(x)	uint ## x

typedef uint(N)		acc_t;
typedef uint(P)		phi_t;
typedef int(M)		dds_t;
typedef uint(B)		rnd_t;
typedef	int18		mix_data_t;


#endif
