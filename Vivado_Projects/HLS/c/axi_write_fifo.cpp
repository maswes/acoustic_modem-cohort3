#include "acomm.h"

void modem(volatile int *x, volatile int *y)
{
#pragma HLS INTERFACE ap_fifo port=y
#pragma HLS INTERFACE s_axilite port=x
	int in_value;

	in_value = *x;
	*y = in_value;
}
