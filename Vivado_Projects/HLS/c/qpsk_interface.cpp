#include "acomm.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//void qpsk_interface(volatile int *data_in, volatile int *data_out,
//		byte_t *data_out_i, byte_t *data_out_q,
//		byte_t *data_in_i, byte_t *data_in_q)
void qpsk_interface(byte_t *data_out_i, byte_t *data_out_q,
		byte_t *data_in_i, byte_t *data_in_q)
{

//#pragma HLS INTERFACE axis port=data_in
//#pragma HLS INTERFACE axis port=data_out
	byte_t d_in[8];			// Store 8 bytes of data sent from PS
	byte_t tx_payload_i[40];	//
	byte_t tx_payload_q[40];	//
	byte_t i,j;
	int	ii;
	int	d_in_temp;

//	for(i = 0; i < 8; i++){
//		d_in_temp = *data_in;
//		d_in[i]   = (byte_t)d_in_temp;
//		*data_out = d_in_temp;
//	}

//	for(ii=0;ii<1000;ii++){

	while(1){

	for(i=0;i<8;i++){
		d_in[i]=i;
	}
	for(i = 0; i < 4; i++){
		tx_payload_i[i*10+0] = 0;		// Adding start bit
		tx_payload_q[i*10+0] = 0;		// Adding start bit
		tx_payload_i[i*10+1] = (((d_in[2*i] >> 7) & 0x01) * 2) - 1;
		tx_payload_q[i*10+1] = (((d_in[2*i] >> 6) & 0x01) * 2) - 1;
		tx_payload_i[i*10+2] = (((d_in[2*i] >> 5) & 0x01) * 2) - 1;
		tx_payload_q[i*10+2] = (((d_in[2*i] >> 4) & 0x01) * 2) - 1;
		tx_payload_i[i*10+3] = (((d_in[2*i] >> 3) & 0x01) * 2) - 1;
		tx_payload_q[i*10+3] = (((d_in[2*i] >> 2) & 0x01) * 2) - 1;
		tx_payload_i[i*10+4] = (((d_in[2*i] >> 1) & 0x01) * 2) - 1;
		tx_payload_q[i*10+4] = (((d_in[2*i] >> 0) & 0x01) * 2) - 1;

		tx_payload_i[i*10+5] = (((d_in[2*i+1] >> 7) & 0x01) * 2) - 1;
		tx_payload_q[i*10+5] = (((d_in[2*i+1] >> 6) & 0x01) * 2) - 1;
		tx_payload_i[i*10+6] = (((d_in[2*i+1] >> 5) & 0x01) * 2) - 1;
		tx_payload_q[i*10+6] = (((d_in[2*i+1] >> 4) & 0x01) * 2) - 1;
		tx_payload_i[i*10+7] = (((d_in[2*i+1] >> 3) & 0x01) * 2) - 1;
		tx_payload_q[i*10+7] = (((d_in[2*i+1] >> 2) & 0x01) * 2) - 1;
		tx_payload_i[i*10+8] = (((d_in[2*i+1] >> 1) & 0x01) * 2) - 1;
		tx_payload_q[i*10+8] = (((d_in[2*i+1] >> 0) & 0x01) * 2) - 1;
		tx_payload_i[i*10+9] = 1;		// Adding stop bit
		tx_payload_q[i*10+9] = 1;		// Adding stop bit
	}


	int loop_cnt;
	bool tx_frame_done = 0;

	for(i = 0; i < 40; i++){
		*data_out_i = tx_payload_i[i];
		*data_out_q = tx_payload_q[i];

		loop_cnt = 0;
		tx_frame_done = 0;
		while(tx_frame_done == 0){
			if(loop_cnt == 10000){
			  loop_cnt = 0;
			  tx_frame_done = 1;
			}
		loop_cnt++;
		}
	}
	}
}




