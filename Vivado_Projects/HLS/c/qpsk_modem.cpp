#include "acomm.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void modem(volatile int *x, volatile int *y)
{
#pragma HLS INTERFACE ap_fifo port=y
#pragma HLS INTERFACE ap_fifo port=x

//	const mix_data_t preamp_seq[24]={1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0};
//	const mix_data_t baker_seq[13]={1,1,1,1,1,0,0,1,1,0,1,0,1};
	const mix_data_t preamp_seq[24]={1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1};
	const mix_data_t baker_seq[13]={1,1,1,1,1,-1,-1,1,1,-1,1,-1,1};
	mix_data_t payload_data[80]; //8 bytes + (1 start bit + 1 stop bit) * 8
	mix_data_t payload_data_re[40];
	mix_data_t payload_data_im[40];
	mix_data_t frame_re[77];	// 24+13+40 = 77;
   	mix_data_t frame_im[77];	// 24+13+40 = 77;

	uint8	data_in[8];
	uint8	i;

	// Read in 8 bytes from tx fifo
	for(i = 0; i < 8; i++){
		data_in[i] = (uint8)*x;
	}

	//
	for(i = 0; i < 8; i++){
	payload_data[i*10+0] = 0;			// Adding start bit
	payload_data[i*10+1]	= (data_in[i] >> 7) & 0x01;
	payload_data[i*10+2]	= (data_in[i] >> 6) & 0x01;
	payload_data[i*10+3]	= (data_in[i] >> 5) & 0x01;
	payload_data[i*10+4]	= (data_in[i] >> 4) & 0x01;
	payload_data[i*10+5]	= (data_in[i] >> 3) & 0x01;
	payload_data[i*10+6]	= (data_in[i] >> 2) & 0x01;
	payload_data[i*10+7]	= (data_in[i] >> 1) & 0x01;
	payload_data[i*10+8]	= (data_in[i] >> 0) & 0x01;
	payload_data[i*10+9] = 1;		// Adding stop bit
	}

	for(i = 0; i < 40; i++){
		payload_data_re[i]= payload_data[i*2]*2 - 1;
		payload_data_im[i]= payload_data[i*2+1]*2 - 1;
	}

	//Assemble the transmitting frame

	for(i = 0; i < 24; i++){
		frame_re[i] = preamp_seq[i];
		frame_im[i] = preamp_seq[i];
	}

	for(i = 0; i < 13; i++){
		frame_re[i+24] = baker_seq[i];
		frame_im[i+24] = baker_seq[i];
	}

	for(i = 0; i < 40; i++){
		frame_re[i+24+13] = payload_data_re[i];
		frame_im[i+24+13] = payload_data_im[i];

	}

	bool	tx_done = 0;
	int		time_counter = 0;
		while(tx_done == 0){

			time_counter=+1;
			if(time_counter == )

		}

}

