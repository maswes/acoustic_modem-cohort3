#include "fsk.h"

void fsk_dector (uint32	cmp_val, int one_val, int zero_val, uint1 *dat_out)
{
	
	uint32	one_val_temp, zero_val_temp;
	one_val_temp = abs(one_val);
	zero_val_temp = abs(zero_val);
	
	uint1 d;
	
	if((one_val_temp > cmp_val) && (zero_val_temp < cmp_val))
		d = 1;
	else if((one_val_temp < cmp_val) && (zero_val_temp > cmp_val))
		d = 0;
	else
		d = d+0;

	*dat_out = d;
}
