-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.1
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity freq_mux is
port (
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    ser_dat_in : IN STD_LOGIC_VECTOR (0 downto 0);
    freq_val_in : IN STD_LOGIC_VECTOR (15 downto 0);
    freq_val_out : OUT STD_LOGIC_VECTOR (15 downto 0);
    freq_val_out_ap_vld : OUT STD_LOGIC;
    rst_ctrl_out : OUT STD_LOGIC_VECTOR (0 downto 0);
    rst_ctrl_out_ap_vld : OUT STD_LOGIC );
end;


architecture behav of freq_mux is 
    attribute CORE_GENERATION_INFO : STRING;
    attribute CORE_GENERATION_INFO of behav : architecture is
    "freq_mux,hls_ip_2015_1,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=2.760000,HLS_SYN_LAT=0,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=0,HLS_SYN_FF=0,HLS_SYN_LUT=19}";
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv3_2 : STD_LOGIC_VECTOR (2 downto 0) := "010";

    signal tmp_1_fu_56_p3 : STD_LOGIC_VECTOR (1 downto 0);
    signal tmp_1_cast_fu_64_p1 : STD_LOGIC_VECTOR (2 downto 0);
    signal tmp1_fu_68_p2 : STD_LOGIC_VECTOR (2 downto 0);
    signal tmp1_cast_fu_74_p1 : STD_LOGIC_VECTOR (15 downto 0);


begin



    ap_done <= ap_start;
    ap_idle <= ap_const_logic_1;
    ap_ready <= ap_start;
    freq_val_out <= std_logic_vector(unsigned(freq_val_in) + unsigned(tmp1_cast_fu_74_p1));

    -- freq_val_out_ap_vld assign process. --
    freq_val_out_ap_vld_assign_proc : process(ap_start)
    begin
        if (not((ap_start = ap_const_logic_0))) then 
            freq_val_out_ap_vld <= ap_const_logic_1;
        else 
            freq_val_out_ap_vld <= ap_const_logic_0;
        end if; 
    end process;

    rst_ctrl_out <= ap_const_lv1_0;

    -- rst_ctrl_out_ap_vld assign process. --
    rst_ctrl_out_ap_vld_assign_proc : process(ap_start)
    begin
        if (not((ap_start = ap_const_logic_0))) then 
            rst_ctrl_out_ap_vld <= ap_const_logic_1;
        else 
            rst_ctrl_out_ap_vld <= ap_const_logic_0;
        end if; 
    end process;

    tmp1_cast_fu_74_p1 <= std_logic_vector(resize(unsigned(tmp1_fu_68_p2),16));
    tmp1_fu_68_p2 <= std_logic_vector(unsigned(tmp_1_cast_fu_64_p1) + unsigned(ap_const_lv3_2));
    tmp_1_cast_fu_64_p1 <= std_logic_vector(resize(unsigned(tmp_1_fu_56_p3),3));
    tmp_1_fu_56_p3 <= (ser_dat_in & ap_const_lv1_0);
end behav;
