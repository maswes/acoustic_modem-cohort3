--Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2015.1 (win64) Build 1215546 Mon Apr 27 19:22:08 MDT 2015
--Date        : Thu Jun 11 05:09:46 2015
--Host        : bluehouse running 64-bit major release  (build 9200)
--Command     : generate_target fsk_modem_system_wrapper.bd
--Design      : fsk_modem_system_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsk_modem_system_wrapper is
  port (
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    LEDs_out : out STD_LOGIC_VECTOR ( 7 downto 0 );
    PA_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Preamp_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    TxSwitch_out : out STD_LOGIC;
    rx_clk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_in : in STD_LOGIC_VECTOR ( 11 downto 0 );
    test : out STD_LOGIC_VECTOR ( 11 downto 0 );
    tx_clk : out STD_LOGIC;
    tx_out : out STD_LOGIC_VECTOR ( 11 downto 0 )
  );
end fsk_modem_system_wrapper;

architecture STRUCTURE of fsk_modem_system_wrapper is
  component fsk_modem_system is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    LEDs_out : out STD_LOGIC_VECTOR ( 7 downto 0 );
    TxSwitch_out : out STD_LOGIC;
    PA_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Preamp_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    tx_clk : out STD_LOGIC;
    rx_in : in STD_LOGIC_VECTOR ( 11 downto 0 );
    tx_out : out STD_LOGIC_VECTOR ( 11 downto 0 );
    rx_clk : out STD_LOGIC_VECTOR ( 0 to 0 );
    test : out STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  end component fsk_modem_system;
begin
fsk_modem_system_i: component fsk_modem_system
     port map (
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      LEDs_out(7 downto 0) => LEDs_out(7 downto 0),
      PA_out(1 downto 0) => PA_out(1 downto 0),
      Preamp_out(2 downto 0) => Preamp_out(2 downto 0),
      TxSwitch_out => TxSwitch_out,
      rx_clk(0) => rx_clk(0),
      rx_in(11 downto 0) => rx_in(11 downto 0),
      test(11 downto 0) => test(11 downto 0),
      tx_clk => tx_clk,
      tx_out(11 downto 0) => tx_out(11 downto 0)
    );
end STRUCTURE;
