/*****************************************************************************
 * fsk_modem_controller.c
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "fsk_modem_controller.h"
#include "xparameters.h"
#include "xstatus.h"
#include "xil_printf.h"
#include "xil_cache.h"
#include "sleep.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xuartlite.h"

// Define maximum LED value (2^8)-1 = 255
#define LED_LIMIT 255
// Define delay length
#define DELAY 10000000
// 	Define the base memory address of the led_controller IP core
#define MODEM_BASE XPAR_FSK_MODEM_CONTROLLER_0_S00_AXI_BASEADDR
#define UARTLITE_DEVICE_ID	XPAR_UARTLITE_0_DEVICE_ID
#define TEST_BUFFER_SIZE 1

#define LEDS_ADDR 			0
#define TXRX_SWITCH_ADDR 	4
#define PA_ADDR		 		8
#define PREAMP_ADDR			12
#define FREQ_ADDR			16


// Variables
char key_input;
const char msg_line[]   = "**********************************************************\n\r";
const char msg_title1[] = "********** Underwater FSK Modem			  ***********\n\r\n\r";
const char msg_title2[] = "Press key to select:\n\r";
const char msg_select1[] = "1: Select #1 - Set Transmit / Receive Switch\n\r";
const char msg_select2[] = "2: Select #2 - Set Power Amplifier Level\n\r";
const char msg_select3[] = "3: Select #3 - Set Pre-amplifier Gain\n\r";
const char msg_select4[] = "4: Select #4 - Set Center Frequency\n\r";
const char msg_select5[] = "5: Select #5 - Set LEDs\n\r";
const char msg_select5[] = "6: Select #6 - Enter message to send\n\r";
const char msg_about[]    = "a: About\n\r";
const char msg_about_content1[] = "About\n\r\n\r";
const char msg_about_content2[] = "* Project    : Underwater FSK Modem 						*\n\r";
const char msg_about_content3[] = "* Assignment : 1                                         *\n\r";
const char msg_about_content4[] = "* Team       : Nghia Tran and Hoa Nguyen                 *\n\r";
const char msg_about_content5[] = "* Date       : June 6, 2015                              *\n\r";

const char msg_error[]    = "Error\n\r";

char msg_buffer[10];
char msg_buffer_1[10];
char msg_temp[2] ="00";


XUartLite UartLite;		/* Instance of the UartLite Device */
u8 SendBuffer[TEST_BUFFER_SIZE];	/* Buffer for Transmitting Data */
u8 RecvBuffer[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */

//****************************************************************************
//Function protocol

void print_menu(void);
void clear_msg_buffer(char *msg_buff){
int UartLitePolledExample(u16);


/****************************************************************************
* Print main menu
****************************************************************************/
void print_menu(void)
{
	outbyte(0x0C);					// Clear screen
    xil_printf("%s", msg_title1);
    xil_printf("%s", msg_title2);
    xil_printf("%s", msg_select1);
    xil_printf("%s", msg_select2);
    xil_printf("%s", msg_select3);
    xil_printf("%s", msg_select4);
    xil_printf("%s", msg_select5);
    xil_printf("%s", msg_select6);
    xil_printf("%s", msg_about);
}

/****************************************************************************
* Print main menu
****************************************************************************/
void clear_msg_buffer(char *msg_buff){
	int i = 0;
	for (i = 0; i < 10; i++){
		msg_buff[i] = '\0';
	}
}



/****************************************************************************/
/**
* This function does a minimal test on the UartLite device and driver as a
* design example. The purpose of this function is to illustrate
* how to use the XUartLite component.
*
* This function sends data and expects to receive the data thru the UartLite
* such that a  physical loopback must be done with the transmit and receive
* signals of the UartLite.
*
* This function polls the UartLite and does not require the use of interrupts.
*
* @param	DeviceId is the Device ID of the UartLite and is the
*		XPAR_<uartlite_instance>_DEVICE_ID value from xparameters.h.
*
* @return	XST_SUCCESS if successful, XST_FAILURE if unsuccessful.
*
*
* @note
*
* This function calls the UartLite driver functions in a blocking mode such that
* if the transmit data does not loopback to the receive, this function may
* not return.
*
****************************************************************************/
int UartLiteInit(u16 DeviceId)
{
	int Status;
	unsigned int SentCount;
	unsigned int ReceivedCount = 0;
	int Index;

	/*
	 * Initialize the UartLite driver so that it is ready to use.
	 */
	Status = XUartLite_Initialize(&UartLite, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XUartLite_SelfTest(&UartLite);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Initialize the send buffer bytes with a pattern to send and the
	 * the receive buffer bytes to zero.
	 */
	for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
		SendBuffer[Index] = Index;
		RecvBuffer[Index] = 0;
	}

	/*
	 * Send the buffer through the UartLite waiting til the data can be sent
	 * (block), if the specified number of bytes was not sent successfully,
	 * then an error occurred.
	 */
	SentCount = XUartLite_Send(&UartLite, SendBuffer, TEST_BUFFER_SIZE);
	if (SentCount != TEST_BUFFER_SIZE) {
		return XST_FAILURE;
	}

	/*
	 * Receive the number of bytes which is transfered.
	 * Data may be received in fifo with some delay hence we continuously
	 * check the receive fifo for valid data and update the receive buffer
	 * accordingly.
	 */
	while (1) {
		ReceivedCount += XUartLite_Recv(&UartLite,
					   RecvBuffer + ReceivedCount,
					   TEST_BUFFER_SIZE - ReceivedCount);
		if (ReceivedCount == TEST_BUFFER_SIZE) {
			break;
		}
	}

	/*
	 * Check the receive buffer data against the send buffer and verify the
	 * data was correctly received.
	 */
	for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
		if (SendBuffer[Index] != RecvBuffer[Index]) {
			return XST_FAILURE;
		}
	}

	return XST_SUCCESS;
}


/****************************************************************************
* Main function
****************************************************************************/
int main()
{

	int freq_value = 0;
	int leds_value = 0;
	int done_flag = 0;
	int counter	= 0;

    int	pa_value = 0;
    int preamp_value = 0;

    init_platform();

    xil_printf("Hello World\n\r");

	/*
	 * Initialize the UartLite driver so that it is ready to use.
	 */
	Status = XUartLite_Initialize(&UartLite, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Initialize the send buffer bytes with a pattern to send and the
	 * the receive buffer bytes to zero.
	 */
	for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
		SendBuffer[Index] = Index;
		RecvBuffer[Index] = 0;
	}




	/* unsigned 32-bit variables for storing current LED value */
//	u32 led_val = 0;
//	int i=0;

//	xil_printf("led_controller IP test begin.\r\n");
//	xil_printf("--------------------------------------------\r\n\n");

	/* Loop forever */
/*	while(1){
			while(led_val<=LED_LIMIT){
				// Print value to terminal
				xil_printf("LED value: %d\r\n", led_val);
				// Write value to led_controller IP core using generated driver function
				FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, LEDS_ADDRS, led_val);
				// increment LED value
				led_val++;
				// run a simple delay to allow changes on LEDs to be visible
				for(i=0;i<DELAY;i++);
			}
			// Reset LED value to zero
			led_val = 0;
*/



	// Main while loop
	while(1)
	{
		//Print a prompt to the console. Show the CWD
		print_menu();
		xil_printf("\n\r> ");
		key_input = inbyte();

			// About
			if (key_input == 'a')
			{
				outbyte(0x0C);	// clear screen
				xil_printf("%s", msg_about_content1);
				xil_printf("%s", msg_line);
				xil_printf("%s", msg_about_content2);
				xil_printf("%s", msg_about_content3);
				xil_printf("%s", msg_about_content4);
				xil_printf("%s", msg_about_content5);
				xil_printf("%s", msg_line);

				xil_printf("\n\r\n\r");
				xil_printf("\n\rPress 'ESC' to back to main menu\n\r\n\r");

				done_flag = 0;
				while (done_flag == 0){
					key_input = inbyte();		// Wait to read key from computer terminal
					if (key_input == 0x1b)		// Escape key
						done_flag = 1;
				}

			}

			// Select 1: Set TX/RX Switch
			if(key_input == '1')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select1);
				xil_printf("%s", msg_line);
				xil_printf("\n\rType '1' for transmit, '0' for receive\n\r");
				xil_printf("\n\rPress 'ESC' to back to main menu\n\r\n\r");
				done_flag = 0;
				while (done_flag == 0){
					key_input = inbyte();		    // Wait to read key from computer terminal
					   if(key_input == '1')			// if 'Enter' is pressed
						{
						   FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, TXRX_SWITCH_ADDR, 1);
						   //outbyte(key_input);			// print new line
						}
						else if(key_input == '0')	{
						   FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, TXRX_SWITCH_ADDR, 0);
						   //outbyte(key_input);			// print new line
						}
						else{
							xil_printf("Type '1' or '0'\n\r");
						}
					outbyte(key_input);			// Send back the key to computer terminal
					if (key_input == 0x1b)		// Escape key
						done_flag = 1;
				}
			}

			// Set power amplifier level
			if(key_input == '2')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select2);
				xil_printf("%s", msg_line);
				xil_printf("\n\rType '0-3' and press ENTER\n\r");
				xil_printf("\n\rPress 'ESC' to back to main menu\n\r\n\r");
				done_flag = 0;
				while (done_flag == 0){
					key_input = inbyte();		    // Wait to read key from computer terminal
					   if(key_input == '0')		// if 'Enter' is pressed
						{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PA_ADDR, 0);
						   pa_value = 0;
						}
						else if(key_input == '1')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PA_ADDR, 1);
						   pa_value = 1;
						}
						else if(key_input == '2')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PA_ADDR, 2);
						   pa_value = 2;
						}
						else if(key_input == '3')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PA_ADDR, 3);
						   pa_value = 3;
						}

						else if (key_input == '\r'){    // if a 'ENTER' key is pressed

							xil_printf("Power amplifier level value: %d\n\r", pa_value);
							FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PA_ADDR, pa_value);

						}
						else{
							xil_printf("Type '0-3'\n\r");
						}
					outbyte(key_input);			// Send back the key to computer terminal
					if (key_input == 0x1b)		// Escape key
						done_flag = 1;
				}
			}

			// Select 3: Set preamp gain
			if(key_input == '3')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select3);
				xil_printf("%s", msg_line);
				xil_printf("\n\rType '0-7'\n\r");
				xil_printf("\n\rPress 'ESC' to back to main menu\n\r\n\r");
				done_flag = 0;
				while (done_flag == 0){
					key_input = inbyte();		// Wait to read key from computer terminal
					   if(key_input == '0')		// if 'Enter' is pressed
						{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 0);
						   preamp_value = 0;
						}
						else if(key_input == '1')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 1);
							preamp_value = 1;
						}
						else if(key_input == '2')	{
						  // FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 2);
							preamp_value = 2;
						}
						else if(key_input == '3')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 3);
							preamp_value = 3;
						}
						else if(key_input == '4')		// if 'Enter' is pressed
						{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 4);
							preamp_value = 4;
						}
						else if(key_input == '5')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 5);
							preamp_value = 5;
						}
						else if(key_input == '6')	{
						   //FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 6);
							preamp_value = 6;
						}
						else if(key_input == '7')	{
						  // FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, 7);
							preamp_value = 7;
						}

						else if (key_input == '\r'){    // if a 'ENTER' key is pressed
							xil_printf("Preamp gain: %d\n\r", preamp_value);
							FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, PREAMP_ADDR, preamp_value);
						}
						else{
							xil_printf("Type '0-7'\n\r");
						}
					outbyte(key_input);			// Send back the key to computer terminal
					if (key_input == 0x1b)		// Escape key
						done_flag = 1;
				}
			}

			// Select 4: Set center frequency
			if(key_input == '4')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select4);
				xil_printf("%s", msg_line);
				clear_msg_buffer(msg_buffer_1);


				xil_printf("\n\rType an integer number and press ENTER:\n\r");
				xil_printf("\n\r> ");
				counter = 0;
				done_flag = 0;

				while (done_flag == 0){
						key_input = inbyte();		// Wait to read key from computer terminal
						msg_buffer_1[counter] = key_input;
						counter++;
						outbyte(key_input);			// Send back the key to computer terminal

						if (key_input == '\r'){    // if a 'ENTER' key is pressed
							msg_buffer_1[counter] = '\0';  // NULL character to end message
							freq_value = atoi(msg_buffer_1);
							xil_printf("Frequency value: %d\n\r", freq_value);
							FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, FREQ_ADDR, freq_value);
							clear_msg_buffer(msg_buffer_1);
							counter = 0;

						}

						if (key_input == 0x1b){    // if a 'ENTER' key is pressed
//							msg_buffer_1[counter] = '\0';  // NULL character to end message
							done_flag = 1;

						}
					}

			}


			// Select 5: Set LEDs
			if(key_input == '5')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select5);
				xil_printf("%s", msg_line);
				clear_msg_buffer(msg_buffer_1);


				xil_printf("\n\rType an integer number and press ENTER:\n\r");
				xil_printf("\n\r> ");
				counter = 0;
				done_flag = 0;

				while (done_flag == 0){
						key_input = inbyte();		// Wait to read key from computer terminal
						msg_buffer_1[counter] = key_input;
						counter++;
						outbyte(key_input);			// Send back the key to computer terminal

						if (key_input == '\r'){    // if a 'ENTER' key is pressed
							msg_buffer_1[counter] = '\0';  // NULL character to end message
							leds_value = atoi(msg_buffer_1);
							xil_printf("LED value: %d\n\r", leds_value);
							FSK_MODEM_CONTROLLER_mWriteReg(MODEM_BASE, LEDS_ADDR, leds_value);
							clear_msg_buffer(msg_buffer_1);
							counter = 0;
						}

						if (key_input == 0x1b){    // if a 'ENTER' key is pressed
							//msg_buffer_1[counter] = '\0';  // NULL character to end message
							done_flag = 1;

						}
					}

			}

			// Select 5: Send message
			if(key_input == '6')
			{
				outbyte(0x0C);
				xil_printf("%s", msg_select5);
				xil_printf("%s", msg_line);
				clear_msg_buffer(msg_buffer_1);


				xil_printf("\n\rType an integer number and press ENTER:\n\r");
				xil_printf("\n\r> ");
				counter = 0;
				done_flag = 0;

				while (done_flag == 0){
						key_input = inbyte();		// Wait to read key from computer terminal
						msg_buffer_1[counter] = key_input;
						counter++;
						outbyte(key_input);			// Send back the key to computer terminal

						if (key_input == '\r'){    // if a 'ENTER' key is pressed
							SendBuffer[counter] = '\0';  // NULL character to end message
							xil_printf("Send message: %s\n\r", SendBuffer);
							/*
							 * Send the buffer through the UartLite waiting til the data can be sent
							 * (block), if the specified number of bytes was not sent successfully,
							 * then an error occurred.
							 */
							SentCount = XUartLite_Send(&UartLite, SendBuffer, TEST_BUFFER_SIZE);
							if (SentCount != TEST_BUFFER_SIZE) {
								return XST_FAILURE;
							}
							clear_msg_buffer(msg_buffer_1);
							counter = 0;
						}

						if (key_input == 0x1b){    // if a 'ENTER' key is pressed
							//msg_buffer_1[counter] = '\0';  // NULL character to end message
							done_flag = 1;

						}
					}

			}


		}

    cleanup_platform();
	return 1;
}

